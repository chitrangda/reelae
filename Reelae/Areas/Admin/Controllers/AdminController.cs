﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Reelae.Repositories;
using Reelae.Areas.Admin.Models;
using Reelae.Filters;
using Microsoft.AspNet.Identity;
using System.IO;
using Reelae.Controllers;
using Microsoft.AspNet.Identity.EntityFramework;
using Reelae.Models;
using ReelaeDataAccessLayer;
using ReelaeBusinessLayer.Utilities;
using Reelae.Utilities;
using System.Net;

namespace Reelae.Areas.Admin.Controllers
{

    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        // GET: Admin/Dashboard
        ISubjectRepository<Subject, int> _subjectRepo;
        IInstitutionRepository _insRepo;
        IUserRepository _userRepo;
        IThemeRepository _themeRepo;
        ISubjectStudentRepository _subjectstdRepo;
        ISubjectTeacherRepository _subjcetTechRepo;
        ILogRepository _dbLogrepo;
        IAssignmentRepository _assignRepo;
        AttendanceRepository _attenRepo;
        public AdminController(ISubjectRepository<Subject, int> subjectRepo, IInstitutionRepository __insRepo, IUserRepository __userRepo, IThemeRepository __themeRepo, ISubjectStudentRepository __subjectstdRepo, ISubjectTeacherRepository __subjcetTechRepo, ILogRepository __dbLogrepo, IAssignmentRepository __assignRepo,AttendanceRepository __attenRepo)
        {
            _subjectRepo = subjectRepo;
            _insRepo = __insRepo;
            _userRepo = __userRepo;
            _themeRepo = __themeRepo;
            _subjectstdRepo = __subjectstdRepo;
            _subjcetTechRepo = __subjcetTechRepo;
            _dbLogrepo = __dbLogrepo;
            _assignRepo = __assignRepo;
            _attenRepo = __attenRepo;
        }


        /// <summary>
        /// Displays an Admin Dashboard  page.
        /// </summary>
        /// <returns>An Admin Dashboard page.</returns>

        [checkSessionAlive]

        public ActionResult Dashboard(int? id)
        {
            AdminViewModel _adminDashboard = new AdminViewModel();
            int? _InstutionID = Session["Institution"] as int?;
            int? _UserID = Session["UserId"] as int?;

            try
            {
                var ArchiveSubjectList = _subjectRepo.getArchiveSubjectList(_InstutionID);
                if (ArchiveSubjectList != null)
                {
                    _adminDashboard.ArchiveSubjects = ArchiveSubjectList;
                }

                if (_subjectRepo.getLatestSubjectList(_InstutionID).Count() > 0)
                {
                    _adminDashboard.Subjects = _subjectRepo.getLatestSubjectList(_InstutionID);
                    if (id == null)
                    {
                        ViewBag.SubjectId = _subjectRepo.getLatestSubjectList(_InstutionID).First().SubjectID;
                        _adminDashboard._subjectMembers = _subjectRepo.GetSubjectMembers((int?)ViewBag.SubjectId);
                        ViewBag.SubjectName = _subjectRepo.getLatestSubjectList(_InstutionID).First().SubjectName;
                        ViewBag.TotalStudent = _subjectRepo.getLatestSubjectList(_InstutionID).First().TotalStudentCount;
                        ViewBag.TotalTeacher = _subjectRepo.getLatestSubjectList(_InstutionID).First().TotalTeacherCount;
                    }
                    else
                    {
                        ViewBag.SubjectId = _subjectRepo.getSubjectList(_InstutionID).Where(r => r.SubjectID == id).SingleOrDefault().SubjectID;
                        _adminDashboard._subjectMembers = _subjectRepo.GetSubjectMembers(id);
                        ViewBag.SubjectName = _subjectRepo.Get(id).SubjectName;
                        ViewBag.TotalStudent = _subjectRepo.getSubjectList(_InstutionID).Where(r => r.SubjectID == id).SingleOrDefault().TotalStudentCount;
                        ViewBag.TotalTeacher = _subjectRepo.getSubjectList(_InstutionID).Where(r => r.SubjectID == id).SingleOrDefault().TotalTeacherCount;
                    }

                }
                int[] studentteacherCount = _insRepo.GetTeacherStudentCount(_InstutionID, _UserID);
                _adminDashboard.StudentsCount = studentteacherCount[0];
                _adminDashboard.TeachersCount = studentteacherCount[1];
                _adminDashboard._userinfo = _userRepo.Get(User.Identity.GetUserId());
                Session["Theme"] = _insRepo.Get(Convert.ToInt32(Session["Institution"])).Theme.ThemeName;
                //changes
                ViewBag.InstitutionInitialCode = Session["InstutionCode"];
                ViewBag.PageHeading = "Main";
                return View(_adminDashboard);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = _UserID.ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message }; new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);

                return RedirectToAction("Login", "Home", new { area = "" });
            }


        }

        #region Subject 
        [checkSessionAlive]
        [HttpPost]

        /// <summary>
        /// Method for Adding subjects.
        /// </summary>
        /// <returns>Added Subjects Returns.</returns>
        public JsonResult AddSubject(AdminViewModel model)
        {
            try
            {
                int? InstitutionID = Convert.ToInt32(Session["Institution"].ToString());
                if (ModelState.IsValidField("SubjectName") && InstitutionID.HasValue)
                {
                    if (!_subjectRepo.IsSubjectExistsByInstitution(InstitutionID.Value, model._subject.SubjectName, model._subject.SubjectID))
                    {
                        #region SetDateTime
                        //set date format from string
                        string SDformValue = Request.Form.Get("hdn_SubjectStartDate");
                        string EDformValue = Request.Form.Get("hdn_SubjectEndDate");
                        if (!string.IsNullOrEmpty(SDformValue))
                        {
                            //MM/DD/YYYY
                            string[] dateParts = SDformValue.Split('/');
                            model._subject.StartDate = new DateTime(Int32.Parse(dateParts[2].Trim()), Int32.Parse(dateParts[0].Trim()), Int32.Parse(dateParts[1].Trim()));
                        }

                        if (!string.IsNullOrEmpty(EDformValue))
                        {
                            //MM/DD/YYYY
                            string[] dateParts = EDformValue.Split('/');
                            model._subject.EndDate = new DateTime(Int32.Parse(dateParts[2].Trim()), Int32.Parse(dateParts[0].Trim()), Int32.Parse(dateParts[1].Trim()));
                        }

                        // 
                        #endregion

                        _subjectRepo.Add(model._subject, InstitutionID.Value.ToString(), Session["UserName"].ToString());
                        if (model._subject.SubjectID == 0)
                        {
                            return this.Json(new
                            {
                                EnableSuccess = true,
                                SuccessTitle = MasterConstants.SUCCESSTITLE,
                                SuccessMsg = MasterConstants.SUBJECTADDEDSUCCESS
                            });
                        }
                        else
                        {

                            return this.Json(new
                            {
                                EnableSuccess = true,
                                SuccessTitle = MasterConstants.SUCCESSTITLE,
                                SuccessMsg = MasterConstants.SAVESUCCESSFULLY
                            });
                        }
                    }
                    else
                    {
                        return this.Json(new
                        {
                            EnableError = true,
                            ErrorTitle = MasterConstants.ERRORTITLE,
                            ErrorMsg = MasterConstants.DUPLICATESUBJECTNAME
                        });
                    }
                }
                else
                {
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = error
                    });

                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }

        }

        [checkSessionAlive]
        /// <summary>
        /// Method for Get Subject Members according to Subject Wise .
        /// </summary>
        /// <param name="id">a subject id, passed by reference,
        /// <returns> Returns Subject Members according to Subject id .</returns>
        public PartialViewResult GetSubjectMembers(int? id)
        {
            var _subjectInfo = _subjectRepo.getSubjectList(Session["Institution"] as int?).Where(r => r.SubjectID == id).SingleOrDefault();
            ViewBag.SubjectName = _subjectInfo.SubjectName;
            ViewBag.TotalMember = _subjectInfo.TotalMemberCount;
            ViewBag.SubjectId = _subjectInfo.SubjectID;
            return PartialView("_SubjectMembers", _subjectRepo.GetSubjectMembers(id).ToList());
        }

        [checkSessionAlive]
        public ActionResult GetSubjectView(int id)
        {
            AdminViewModel _adminDashboard = new AdminViewModel();
            _adminDashboard._subject = _subjectRepo.Get(id);
            return PartialView("_addSubject", _adminDashboard);
        }

        [HttpPost]
        [checkSessionAlive]
        public ActionResult deleteSubject(int? id)
        {
            try
            {
                _subjectRepo.Remove(id);

                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.SUBJECT_Remove_SUCESS
                });
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message }; new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);

                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                });
            }
        }
        #endregion

        /// <summary>
        /// Method for Adding users according to roles and subjectwise .
        /// </summary>
        /// <param name="role">role, passed by reference,
        ///  /// <param name="_subjectId">a subject id, passed by reference,
        /// <returns> Returns added Users.Display the view of Add user.</returns>
        [checkSessionAlive]
        public ActionResult GetAddUserView()
        {
            return PartialView("_addUser", new Reelae.Models.AddMemberNViewModal());
        }

        [checkSessionAlive]
        [HttpPost]
        public ActionResult getAssignMemberView(string userRole, int subjectId)
        {
            if (Session["Institution"] != null)
            {
                List<AssignMemberViewModel> _members = new List<AssignMemberViewModel>();
                var type = userRole == "Teacher" ? "t" : "s";
                var result = _userRepo.GetAll(Convert.ToInt32(Session["Institution"]), type.ToString(), subjectId);

                foreach (var _user in result)
                {
                    _members.Add(new AssignMemberViewModel() { isSelect = false, userId = _user.UserID, Name = _user.Name + " " + _user.Surname, Email = _user.Email, userPicUrl = _user.UserPicUrl, type = userRole });
                }

                return PartialView("_AssignMember", _members);
            }
            else
            {
                return RedirectToAction("Login", "Home", new { area = "" });
            }

        }

        public ActionResult getsearchMember(int? id, int? userid, string role)
        {
            try
            {
                List<AssignMemberViewModel> _members = new List<AssignMemberViewModel>();
                string type = role == "Teacher" ? "t" : "s";
                int? _instituionid = Session["Institution"] as int?;
                if (userid != null && userid != -1)
                {
                    var _user = _userRepo.Get(userid.Value);
                    _members.Add(new AssignMemberViewModel() { isSelect = false, userId = _user.UserID, Name = _user.Name + " " + _user.Surname, Email = _user.Email, userPicUrl = _user.UserPicUrl, type = role });

                }
                else if (userid == null)
                {
                    var model = _userRepo.GetAll(_instituionid.Value, type, id.Value);
                    foreach (var _user in model)
                    {
                        _members.Add(new AssignMemberViewModel() { isSelect = false, userId = _user.UserID, Name = _user.Name + " " + _user.Surname, Email = _user.Email, userPicUrl = _user.UserPicUrl, type = role });
                    }


                }
                return PartialView("_MembersList", _members);

            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message }; new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.DUPLICATESUBJECTNAME
                });
            }

        }


        [checkSessionAlive]
        public JsonResult SaveAssignMembers(List<AssignMemberViewModel> model, int subjectId, string userRole, string [] selectedItem)
        {
            try
            {
                if (selectedItem.Length > 0)
                {
                    var checkCount = selectedItem[0].ToString();
                    if (!string.IsNullOrEmpty(checkCount.ToString()))
                    {
                        if (checkCount.ToString().Contains(","))
                        {
                            var items = selectedItem[0].ToString().Split(',');
                            if (userRole == "Student")
                            {
                                foreach (var item in items)
                                {
                                    _subjectstdRepo.Add(new SubjectStudent() { SubjectID = subjectId, StudentID = int.Parse(item) }, Session["UserName"].ToString());
                                }
                            }
                            else
                            {
                                foreach (var item in items)
                                {
                                    _subjcetTechRepo.Add(new SubjectTeacher() { SubjectID = subjectId, TeacherID = int.Parse(item) }, Session["UserName"].ToString());
                                }
                            }
                        }
                        else
                        {
                            if (userRole == "Student")
                            {
                                _subjectstdRepo.Add(new SubjectStudent() { SubjectID = subjectId, StudentID = int.Parse(checkCount) }, Session["UserName"].ToString());
                            }
                            else
                            {
                                _subjcetTechRepo.Add(new SubjectTeacher() { SubjectID = subjectId, TeacherID = int.Parse(checkCount) }, Session["UserName"].ToString());
                            }
                        }
                    }

                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = "Assigned member successfully."
                    });
                }
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = "Assigned member successfully."
                });

                //if (ModelState.IsValid)
                //{
                //    if (userRole == "Student")
                //    {
                //        foreach (var _model in model)
                //        {
                //            if (_model.isSelect)
                //            {
                //                _subjectstdRepo.Add(new SubjectStudent() { SubjectID = subjectId, StudentID = _model.userId }, Session["UserName"].ToString());
                //            }
                //        }
                //    }
                //    else if (userRole == "Teacher")
                //    {
                //        foreach (var _model in model)
                //        {
                //            if (_model.isSelect)
                //            {
                //                _subjcetTechRepo.Add(new SubjectTeacher() { SubjectID = subjectId, TeacherID = _model.userId }, Session["UserName"].ToString());
                //            }
                //        }
                //    }
                //    return this.Json(new
                //    {
                //        EnableSuccess = true,
                //        SuccessTitle = MasterConstants.SUCCESSTITLE,
                //        SuccessMsg = "Assigned " + userRole + " successfully."
                //    });
                //}
                //else
                //{
                //    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                //    return this.Json(new
                //    {
                //        EnableError = true,
                //        ErrorTitle = MasterConstants.ERRORTITLE,
                //        ErrorMsg = error
                //    });

                //}
            }
            catch
            {
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.PLEASETRYAGAINLATER
                });
            }

        }

        /// <summary>
        /// Method for get the themes,view of Institution wise.
        /// </summary>
        /// 
        /// <returns> Display the page as Themes and view Institution wise.</returns>
        ///         
        [checkSessionAlive]
        public ActionResult GetDashboardStyleView()
        {
            DashboardStyleViewModel model = new DashboardStyleViewModel();
            model._institution = _insRepo.Get(Convert.ToInt32(Session["Institution"]));
            model._themes = _themeRepo.Get();
            return PartialView("_dashboardStyle", model);
        }

        [HttpPost]
        /// <summary>
        /// Method for set the themes,view of Institution wise.
        /// </summary>
        /// <param name="model">model paaing the Dashboard Style view Model,Instituion wise and themes as per institution , passed by reference,
        /// <returns> Display the page for set the Themes and view Institution wise.</returns>
        public JsonResult SetupDashboard(DashboardStyleViewModel model)
        {
            int? _UserID = Session["UserId"] as int?;
            int _InstutionID = Session["Institution"] != null ? Convert.ToInt32(Session["Institution"]) : 0;

            try
            {
                if (ModelState.IsValid)
                {
                    if (_insRepo.Get(_InstutionID).IntialName == null && _insRepo.Get(_InstutionID).IntialName == "")
                    {
                        if (!_insRepo.IsInstitutionInitialNameAlereadyExists(model._institution.IntialName, _InstutionID))
                        {
                            return this.Json(new
                            {
                                EnableError = true,
                                ErrorTitle = MasterConstants.ERRORTITLE,
                                ErrorMsg = MasterConstants.ALREADYEXISTS
                            });
                        }
                    }
                    _insRepo.update(model._institution, Session["UserName"].ToString());
                    Session["InstutionCode"] = model._institution.IntialName;
                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = MasterConstants.SUCCESSSETTINGSAVE
                    });
                }
                else
                {
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = error
                    });

                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = _UserID.ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    status = MasterConstants.FAILURETITLE.ToLower(),
                    msg = MasterConstants.SOMEPROBLEMOccurred,
                    Data = string.Empty
                }, JsonRequestBehavior.AllowGet);
            }

        }


        [HttpPost]
        /// <summary>
        /// Method for upload the Logo as per institution.
        /// </summary>

        /// <returns> Display the page for set the logo Institution wise.</returns>
        public ActionResult UploadLogo()
        {
            int? _UserID = Session["UserId"] as int?;
            // Checking no of files injected in Request object  
            string fname = "";
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        // Get the complete folder path and store the file inside it.  
                        Random rnd = new Random();
                        fname = "Logo_" + rnd.Next(1000) + "_" + Session["Institution"].ToString() + System.IO.Path.GetExtension(fname);
                        //fname = Path.Combine(Server.MapPath("~/InstituteLogo/"), fname);
                        file.SaveAs(Path.Combine(Server.MapPath("~/InstituteLogo/"), fname));
                    }
                    // Returns message that successfully uploaded  
                    return Json(fname);
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json(MasterConstants.NOFILESSELECTED);
            }
        }

        #region Members profile 
        public ActionResult MemberInfoView(int _id,string role)
        { 
            int? _insID = Session["Institution"] as int?;
            int? _instituionid = Session["Institution"] as int?;
            if(role.ToLower()=="t")
            {
                TeacherInfoAdminView model = new TeacherInfoAdminView();
                model.userInfo = _userRepo.Get(Convert.ToInt32(_id));
                model.Assignments = _assignRepo.GetTeacherAssignmentList(_id);
                model.Subjects = _subjectRepo.GetSubjectTeacher(_id);
                return PartialView("_PartialTeacherInfo", model);

            }
            else
            {
                StudentInfoAdminView model = new StudentInfoAdminView();
                model.userInfo = _userRepo.Get(Convert.ToInt32(_id));
                model.Subjects = _subjectRepo.GetSubjecStudent(_id).ToList();
                model.Assignments = _assignRepo.GetStudentAssignmentList(_id, null);
                model.AttendanceRate = _attenRepo.Get(_id).ToList();
                return PartialView("_PartialStudentInfo", model);

            }
        }

        #endregion

        #region [PartialStudanceAttandanceView]
        public ActionResult StudentAttandanceAdminView(string studentID)
        {
            StudentAttandanceAdminView model = new StudentAttandanceAdminView();
            int? _insID = Session["Institution"] as int?;
            model.userInfo = _userRepo.Get(Convert.ToInt32(studentID));
            return PartialView("_PartialStudentAttandance", model);
        }
        #endregion

        #region [Member Deactivation]
        public ActionResult DeactivateMemberFromSubject(string uid, string utype, string subid)
        {
            if (string.IsNullOrEmpty(uid) || string.IsNullOrEmpty(utype) || string.IsNullOrEmpty(subid))
            {
                return this.Json(new
                {
                    status = MasterConstants.FAILURETITLE.ToLower(),
                    msg = MasterConstants.INVALIDARGUMENT,
                    Data = string.Empty
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (utype == "t")
                {
                    _subjcetTechRepo.RemoveTeacherFromSubject(Convert.ToInt32(subid), Convert.ToInt32(uid));
                }
                else if (utype == "s" || utype == "l")
                {
                    _subjectstdRepo.RemoveStudentFromSubject(Convert.ToInt32(subid), Convert.ToInt32(uid));
                }

                return this.Json(new
                {
                    status = MasterConstants.SUCCESSTITLE,
                    msg = string.Empty,
                    Data = string.Empty
                }, JsonRequestBehavior.AllowGet);

            }
        }


        #endregion

        public ActionResult MemberSort(int id, int? userid, string role, string _column, string _order)
        {
            List<AssignMemberViewModel> _members = new List<AssignMemberViewModel>();
            string type = role == "Teacher" ? "t" : "s";
            int? _instituionid = Session["Institution"] as int?;
            if (userid != null && userid != -1)
            {
                var _user = _userRepo.Get(userid.Value);
                _members.Add(new AssignMemberViewModel() { isSelect = false, userId = _user.UserID, Name = _user.Name + " " + _user.Surname, Email = _user.Email, userPicUrl = _user.UserPicUrl, type = role });

            }
            else if (userid == null)
            {
                var model = _userRepo.GetAll(_instituionid.Value, type, id);
                if (_order == "asc")
                {
                    if (_column == "name")
                    {
                        model = model.OrderBy(r => r.Name);
                    }
                    else if (_column == "email")
                    {
                        model = model.OrderBy(r => r.Email);

                    }
                }
                else
                {
                    if (_column == "name")
                    {
                        model = model.OrderByDescending(r => r.Name);
                    }
                    else if (_column == "email")
                    {
                        model = model.OrderByDescending(r => r.Email);

                    }
                    foreach (var _user in model)
                    {
                        _members.Add(new AssignMemberViewModel() { isSelect = false, userId = _user.UserID, Name = _user.Name + " " + _user.Surname, Email = _user.Email, userPicUrl = _user.UserPicUrl, type = role });
                    }
                }


            }
            return PartialView("_MembersList", _members);

        }

    }
}