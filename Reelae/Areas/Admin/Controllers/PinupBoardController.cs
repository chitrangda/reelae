﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Reelae.Repositories;
using Reelae.Areas.Admin.Models;
using Reelae.Filters;
using Microsoft.AspNet.Identity;
using System.IO;
using Reelae.Controllers;
using Microsoft.AspNet.Identity.EntityFramework;
using Reelae.Models;
using ReelaeDataAccessLayer;
using ReelaeBusinessLayer.Utilities;
using Reelae.Utilities;
using System.Net;

namespace Reelae.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [checkSessionAlive]
    public class PinupBoardController : Controller
    {

        IUserRepository _userRepo;
        ILogRepository _dbLogrepo;
        IPinUpBoardRepository _pinupRepo;
        
        public PinupBoardController(IUserRepository __userRepo, ILogRepository __dbLogrepo, IPinUpBoardRepository __pinupRepo)
        {

            _userRepo = __userRepo;
            _dbLogrepo = __dbLogrepo;
            _pinupRepo = __pinupRepo;

        }

        // GET: Admin/PinupBoard
        public ActionResult Index()
        {
            PinupBoardViewModel model = new PinupBoardViewModel();
            int? _InstutionID = Session["Institution"] as int?;
            int? _UserID = Session["UserId"] as int?;
            model._userinfo = _userRepo.Get(User.Identity.GetUserId());
            model._pinups = _pinupRepo.GetAll(_InstutionID);
            ViewBag.PageHeading = "PinupBoard";
            return View(model);
        }

        [HttpPost]
        public ActionResult deletePinup(int id)
        {
            try
            {
                _pinupRepo.Remove(id);
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.REMOVE_SUCCESS
                });
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult getAttachment(int id)
        {
            try
            {
                ViewBag.PinupboardId = id;
                IEnumerable<PinUpBoardFile> model = _pinupRepo.getFiles(id);
                return PartialView("_PinupFiles", model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult deleteFile(int id, string fileUrl)
        {
            try
            {
                var filePath = Server.MapPath(fileUrl);
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
                _pinupRepo.RemoveFile(id);
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.REMOVE_SUCCESS
                });
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }
        }
    }
}