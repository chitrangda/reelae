﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Reelae.Repositories;
using Reelae.Areas.Admin.Models;
using Reelae.Filters;
using Microsoft.AspNet.Identity;
using System.IO;
using Reelae.Controllers;
using Microsoft.AspNet.Identity.EntityFramework;
using Reelae.Models;
using ReelaeDataAccessLayer;
using ReelaeBusinessLayer.Utilities;
using Reelae.Utilities;
using System.Net;

namespace Reelae.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [checkSessionAlive]
    public class NoticeBoardController : Controller
    {
        IUserRepository _userRepo;
        ILogRepository _dbLogrepo;
        INoticeBoardRepository _noticeRepo;
        public NoticeBoardController(IUserRepository __userRepo, ILogRepository __dbLogrepo, INoticeBoardRepository __noticeRepo)
        {

            _userRepo = __userRepo;
            _dbLogrepo = __dbLogrepo;
            _noticeRepo = __noticeRepo;

        }
        // GET: Admin/NoticeBoard
        public ActionResult Index()
        {
            NoticeBoardViewModel model = new NoticeBoardViewModel();
            int? _InstutionID = Session["Institution"] as int?;
            int? _UserID = Session["UserId"] as int?;
            model._userinfo = _userRepo.Get(User.Identity.GetUserId());
            model.noticeBoard = _noticeRepo.GetAll(_InstutionID);
            ViewBag.PageHeading = "NoticeBoard";
            return View(model);
        }

                
        [HttpPost]
        public ActionResult getComments(int? id)
        {
            try
            {
                ViewBag.NoticeBoardID = id;
                var model = _noticeRepo.getAllComments(id);
                return PartialView("_noticeComments", model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public JsonResult RemoveNotice(int id)
        {
            try
            {
                _noticeRepo.Remove(id);
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.REMOVE_SUCCESS
                });


            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult RemoveComment(int id)
        {
            try
            {
                _noticeRepo.RemoveComment(id);
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.REMOVE_SUCCESS
                });
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult getFiles(int id)
        {
            try
            {
                return PartialView("_noticeFiles", _noticeRepo.getnoticeFiles(id));

            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult DeleteNoticeFile(int id, string fileUrl)
        {
            if (!string.IsNullOrEmpty(fileUrl))
            {
                try
                {
                    var filePath = Server.MapPath(fileUrl);
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }
                    if (id != 0)
                    {
                        _noticeRepo.RemoveFile(id);
                    }
                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = MasterConstants.REMOVE_SUCCESS
                    });
                }
                catch (Exception ex)
                {
                    ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserId"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                    //   new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.NOFILESSELECTED
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}