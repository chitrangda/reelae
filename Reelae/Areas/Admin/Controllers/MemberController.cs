﻿using Reelae.Areas.Admin.Models;
using Reelae.Controllers;
using Reelae.Filters;
using Reelae.Repositories;
using Reelae.Utilities;
using ReelaeBusinessLayer.Utilities;
using ReelaeDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using System.Net;

namespace Reelae.Areas.Admin.Controllers
{
    public class MemberController : Controller
    {
        IUserRepository _userRepo;
        ILogRepository _dbLogrepo;

        ISubjectStudentRepository _subjectStudentRepository;
        ISubjectTeacherRepository _subjectTeacherRepository;


        private ApplicationUserManager _userManager;
        private IAspnetUsersRepository _aspNetUserrepo;

        public MemberController(IUserRepository __userRepo, ILogRepository __dbLogrepo, IAspnetUsersRepository __aspNetUserrepo, ISubjectStudentRepository __subjectStudentRepository, ISubjectTeacherRepository __subjectTeacherRepository)
        {
            _userRepo = __userRepo;
            _dbLogrepo = __dbLogrepo;
            _aspNetUserrepo = __aspNetUserrepo;

            _subjectStudentRepository = __subjectStudentRepository;
            _subjectTeacherRepository = __subjectTeacherRepository;
        }
        public MemberController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        // GET: Admin/Member
        public ActionResult Index()
        {
            return View();
        }

        [checkSessionAlive]
        [HttpPost]
        public ActionResult getMembersGridView(int id)
        {
            try
            {
                int _InstutionID = Session["Institution"] != null ? Convert.ToInt32(Session["Institution"]) : 0;
                ViewBag.Id = id;
                return PartialView("_MembersGridView", _userRepo.GetAll(_InstutionID, id));
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message }; new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                });
                //return RedirectToAction("Login", "Home", new { area = "" });
            }
        }

        [checkSessionAlive]
        public ActionResult getEditMembersView(int id)
        {
            try
            {
                return PartialView("_editMember", _userRepo.getUserDetail(id));
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message }; new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                });
                //return RedirectToAction("Login", "Home", new { area = "" });
            }
        }

        [checkSessionAlive]
        [HttpPost]
        public async Task<ActionResult> SaveEditMember(re_getUserDetails_sp_Result model)
        {
            try
            {
                var user = await UserManager.FindByIdAsync(model.UserMemberShipID);
                bool _exitingAccountStatus = user.isEnabled;
                if (user != null)
                {                   
                    //code added by Anshul
                    if (!model.isEnabled && model.isEnabled!= user.isEnabled)
                    {
                        if (model.type.ToLower().Equals("student"))
                        {
                            if (_subjectStudentRepository.IsStudentMappedWithAnySubject(model.UserID))
                            {
                                return this.Json(new
                                {
                                    EnableError = true,
                                    ErrorTitle = MasterConstants.ERRORTITLE,
                                    ErrorMsg = "This user is mapped with a subject" + "#" + model.UserID + "#" + user.Id + "#" + model.type
                                });
                            }
                        }
                        else
                        {
                            if (_subjectTeacherRepository.IsTeacherMappedWithAnySubject(model.UserID))
                            {
                                return this.Json(new
                                {
                                    EnableError = true,
                                    ErrorTitle = MasterConstants.ERRORTITLE,
                                    ErrorMsg = "This user is mapped with a subject" + "#" + model.UserID + "#" + user.Id + "#" + model.type
                                });
                            }
                        }
                    }

                    //end code added by Anshul
                    user.isEnabled = model.isEnabled;
                    if (_aspNetUserrepo.update(user.Id, user.isEnabled))
                    {
                        if (model.isEnabled != _exitingAccountStatus)
                        {
                            var callbackUrl = Url.Action("Login", "Home", null, protocol: Request.Url.Scheme);
                            string status = String.Empty;
                            status = model.isEnabled == true ? "activated" : "deactivated";
                            string mailbody = Reelae.Utilities.EmailHtml.getAccountStatusHtml(model.Name, model.Email, callbackUrl, status);
                            Reelae.Utilities.Mail.SendMail(model.Email, "Reeale account is now " + status, mailbody);
                        }
                       _userRepo.EditUserNameAndEmail(model.UserID, model.Name, model.Email);
                        var result = UserManager.UpdateAsync(user);
                        return this.Json(new
                        {
                            EnableSuccess = true,
                            SuccessTitle = MasterConstants.SUCCESSTITLE,
                            SuccessMsg = MasterConstants.SAVESUCCESSFULLY + MasterConstants.SEPERATOR_SYMBOL + model.type
                        });
                    }
                    else
                    {
                        return this.Json(new
                        {
                            EnableError = true,
                            ErrorTitle = MasterConstants.ERRORTITLE,
                            ErrorMsg = "Users does not exists!"
                        });
                    }
                }
                else
                {
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = "Users does not exists!"
                    });

                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message }; new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                });
            }
        }






        [checkSessionAlive]
        public ActionResult SaveEditMemberActiveOrInactive(int id,string uId,string type)
        {
            var user =  UserManager.FindById(uId);
            if (user != null)
            {
                //if (type.ToLower() == "student")
                //{
                //    _subjectStudentRepository.removeStudentById(id);
                //}
                //else {
                //    _subjectTeacherRepository.removeTeacherById(id);
                //}
               
                if (_aspNetUserrepo.update(user.Id, false))
                {
                    user.isEnabled = false;

                    var result = UserManager.UpdateAsync(user);
                    string mailbody = EmailHtml.getAccountStatusHtml(user.Name, user.Email,"", "deactivated");
                    Mail.SendMail(user.Email, "Reeale account is now deactivated", mailbody);
                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = MasterConstants.SAVESUCCESSFULLY + MasterConstants.SEPERATOR_SYMBOL + type
                    });
                }
                else
                {
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = "Users does not exists!"
                    });
                }

            }
            else
            {
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = "Users does not exists!"
                });

            }
        }












        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }


}