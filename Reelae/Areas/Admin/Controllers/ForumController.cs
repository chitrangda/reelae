﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Reelae.Repositories;
using Reelae.Areas.Admin.Models;
using Reelae.Filters;
using Microsoft.AspNet.Identity;
using System.IO;
using Reelae.Controllers;
using Microsoft.AspNet.Identity.EntityFramework;
using Reelae.Models;
using ReelaeDataAccessLayer;
using ReelaeBusinessLayer.Utilities;
using Reelae.Utilities;
using System.Net;

namespace Reelae.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [checkSessionAlive]
    public class ForumController : Controller
    {
        IUserRepository _userRepo;
        ILogRepository _dbLogrepo;
        IForumRepository _forumRepo;
        // GET: Admin/Forum
        public ForumController(IUserRepository __userRepo, ILogRepository __dbLogrepo, IForumRepository __forumTopic)
        {

            _userRepo = __userRepo;
            _dbLogrepo = __dbLogrepo;
            _forumRepo = __forumTopic;

        }

        public ActionResult Index()
        {
            ForumVewModel model = new ForumVewModel();
            int? _InstutionID = Session["Institution"] as int?;
            int? _UserID = Session["UserId"] as int?;
            model._userinfo = _userRepo.Get(User.Identity.GetUserId());
            model._forumTopics = _forumRepo.GetAll(_InstutionID);
            ViewBag.PageHeading = "Forum";
            return View(model);
        }

        [HttpPost]
        public JsonResult RemoveForumTopic(int id)
        {
            try
            {
                _forumRepo.Remove(id);
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.REMOVE_SUCCESS
                });


            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult getAllReply(int id)
        {
            ViewBag.Topic = _forumRepo.Get(id).Topic;
            var model = _forumRepo.GetAllReply(id);
            return PartialView("_AllReply", model);
        }

        [HttpPost]
        public JsonResult removeReply(int id)
        {
            try
            {
                _forumRepo.RemoveReply(id);
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.REMOVE_SUCCESS
                });


            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}