﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Reelae.Areas.Teacher.Models;
using Reelae.Filters;
using Reelae.Models;
using Reelae.Repositories;
using Reelae.Utilities;
using ReelaeBusinessLayer.Utilities;
using ReelaeDataAccessLayer;

namespace Reelae.Areas.Admin.Controllers
{
    [LoggingFilter]
    [checkSessionAlive]
    [Authorize(Roles = "Admin")]
    public class MessageController : Controller
    {
        // GET: Teacher/Message
        ILogRepository _dbLogrepo;
        IUserRepository _userRepo;
        IInstitutionRepository _insRepo;
        IChatRepository _chatRepo;

        public MessageController(ILogRepository __dbLogrepo, IUserRepository __userRepo, IInstitutionRepository __insRepo, IChatRepository __chatRepo)
        {
            _dbLogrepo = __dbLogrepo;
            _userRepo = __userRepo;
            _insRepo = __insRepo;
            _chatRepo = __chatRepo;
        }

        public ActionResult Index()
        {
            int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            int _instituionid = Session["Institution"] != null ? Convert.ToInt32(Session["Institution"]) : 0;
            MessageViewModal messageViewModal = new MessageViewModal();
            messageViewModal._userinfo = _userRepo.Get(User.Identity.GetUserId());
            messageViewModal.ActiveStudents = _userRepo.GetInstituteActiveMembersByRoleId(_instituionid, (int?)RolesMaster.Student);
            messageViewModal.ActiveTeachers = _userRepo.GetInstituteActiveMembersByRoleId(_instituionid, (int?)RolesMaster.Teacher);
            messageViewModal.Institutions = _userRepo.GetInstituteActiveMembersByRoleId(_instituionid, (int?)RolesMaster.Admin);
            ViewBag.UesrsUnreadMessagesWithCount = _chatRepo.GetUserSingleChatUnreadMessagesCount(_userid).ToList();
            ViewBag.PageHeading = "Message";
            return View(messageViewModal);
        }
        public ActionResult GetIndividualChatBody(int? from)
        {
            int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            try
            {
                ViewBag.UnreadMessagesCount = _chatRepo.UserUnreadMessagesCount(from, _userid);
                var GetUserIndividualMessages = _chatRepo.GetIndividualChatMessages(_userid, from);
                return PartialView("_IndividualChatBody", GetUserIndividualMessages);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult UpdateUserUnreadMessagesToRead(int? from)
        {
            int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            try
            {
                _chatRepo.UpdateUserMessageReadStatus(from, _userid);
                return this.Json(new
                {
                    status = MasterConstants.SUCCESSTITLE,
                    msg = ""
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult UpdateIndividualMessageDeletedStatus(int? messageId, bool isSent, int? messageTo)
        {
            int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            try
            {
                if (isSent)
                    _chatRepo.UpdateUserMessageDeletedStatus(_userid, messageTo, messageId, isSent);
                else
                    _chatRepo.UpdateUserMessageDeletedStatus(messageTo, _userid, messageId, isSent);

                return Json(new
                {
                    status = MasterConstants.SUCCESSTITLE,
                    msg = MasterConstants.SAVESUCCESSFULLY
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult GetFilteredMembersList(string filterString, int? role)
        {
            int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            int _instituionid = Session["Institution"] != null ? Convert.ToInt32(Session["Institution"]) : 0;
            try
            {
                IEnumerable<re_GetChatMembers_sp_Result> members;
                if (filterString == "")
                    members = _userRepo.GetInstituteActiveMembersByRoleId(_instituionid, role);
                else
                    members = _userRepo.GetInstituteActiveMembersByRoleId(_instituionid, role).Where(n => n.Name.ToLower().StartsWith(filterString.ToLower()));
                ViewBag.UesrsUnreadMessagesWithCount = _chatRepo.GetUserSingleChatUnreadMessagesCount(_userid).ToList();
                //members = members.Where(n => n.Name.ToLower().StartsWith(filterString.ToLower()));
                return PartialView("_ChatMembersLists", members);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }

        }

        //Not used yet..
        public ActionResult UpdateIndividualMessageReadStatus(int? messageId, int? messageFrom)
        {
            int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            try
            {

                return Json(new
                {
                    status = MasterConstants.SUCCESSTITLE,
                    msg = MasterConstants.SAVESUCCESSFULLY
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult UpdateIndividualMessageSentStatus(int? messageId, int? messageFrom)
        {
            int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            try
            {

                return Json(new
                {
                    status = MasterConstants.SUCCESSTITLE,
                    msg = MasterConstants.SAVESUCCESSFULLY
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}