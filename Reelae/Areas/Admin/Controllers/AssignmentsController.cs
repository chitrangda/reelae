﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Reelae.Repositories;
using Reelae.Areas.Admin.Models;
using ReelaeDataAccessLayer;
using Reelae.Filters;
using System.Web.Script.Serialization;
using ReelaeBusinessLayer.Utilities;
using Reelae.Models;
using System.Transactions;
using Microsoft.AspNet.Identity;
using Reelae.Utilities;

namespace Reelae.Areas.Admin.Controllers
{
    [checkSessionAlive]
    [Authorize(Roles = "Admin")]
    public class AssignmentsController : Controller
    {
        IGroupRepository _grouRepo;
        ISubjectRepository<Subject, int> _subjectRepo;
        ILogRepository _dbLogrepo;
        IUserRepository _userRepo;
        IInstitutionRepository _insRepo;
        IAssignmentRepository _anssignRepo;
        public AssignmentsController(ISubjectRepository<Subject, int> subjectRepo, IGroupRepository __groupRepo, ILogRepository __dbLogrepo, IUserRepository __userRepo, IInstitutionRepository __insRepo, IAssignmentRepository __anssignRepo)
        {
            _subjectRepo = subjectRepo;
            _grouRepo = __groupRepo;
            _dbLogrepo = __dbLogrepo;
            _userRepo = __userRepo;
            _insRepo = __insRepo;
            _anssignRepo = __anssignRepo;
        }
        // GET: Admin/Assignments

        /// <summary>
        /// Method for load the index Page view.
        /// </summary>
        /// 
        /// <returns> Load the index page of Assignment.</returns>
        public ActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// Method for load the Assignment  Page view.
        /// </summary>
        /// 
        /// <returns> Display the Assignment  page view.</returns>
        public ActionResult GetStudentAssignmentView()
        {
            TeacherInfoView model = new TeacherInfoView();
            //model.Subjects = _subjectRepo.Get();
            //model.Groups = _grouRepo.Get();
            int? _insID = Session["Institution"] as int?;
            model.Subjects = _subjectRepo.GetByInstitution(_insID.Value);
            //model.Groups = _grouRepo.Get();
            if (model.Subjects.Count() > 0)
                model.Groups = _grouRepo.GetBySubjectID(Convert.ToInt32(model.Subjects.First().SubjectID));
            else
                model.Groups = _grouRepo.Get();
            return PartialView("_Assignments", model);
        }

        /// <summary>
        /// Method for get the student Assignment list.
        /// </summary>
        /// <param name="SubjectId">SubjectId, passed by reference,
        /// <param name="GroupId">GroupId, passed by reference,
        /// <returns> Display the Assignment List  page view.</returns>
        public ActionResult GetStudentAssignmentList(int SubjectID, int GroupID)
        {
            if (SubjectID < 1 || GroupID < 1)
            {
                return Json(new
                {
                    status = MasterConstants.FAILURETITLE.ToLower(),
                    message = MasterConstants.INVALIDARGUMENT,//"subject and group data is not correct",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                List<StudentAssignmentInfoViewModel> assignments = new List<StudentAssignmentInfoViewModel>();
                //calculate assignments according to group
                return PartialView("_AssignmentInfo", assignments);
            }
        }

        #region [StudentAttandance]
        /// <summary>
        /// Method for get the student Attendance Viewlist.
        /// </summary>
        /// <returns> Display the Student Attendance page view.</returns>
        public ActionResult GetStudentAttandanceView()
        {
            StudentAttandanceViewModal model = new StudentAttandanceViewModal();
            int? _insID = Session["Institution"] as int?;
            model.Subjects = _subjectRepo.GetByInstitution(_insID.Value);
            //model.Groups = _grouRepo.Get();
            if (model.Subjects.Count() > 0)
                model.Groups = _grouRepo.GetBySubjectID(Convert.ToInt32(model.Subjects.First().SubjectID));
            else
                model.Groups = _grouRepo.Get();
            return PartialView("_StudentAttandance", model);
        }

        /// <summary>
        /// Method for get the Group According to Subject.
        /// </summary>
        /// <param name="Id">Id: Id Of Subject, passed by reference,
        /// <returns> Return  the Group according to Subject id.</returns>
        public ActionResult GetGroupsBySubject(string id)
        {
            int? _UserID = Session["UserId"] as int?;
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return this.Json(new
                    {
                        status = MasterConstants.FAILURETITLE,
                        msg = MasterConstants.INVALIDSUBJECTID,
                        Data = string.Empty
                    }, JsonRequestBehavior.AllowGet);
                }
                var subjectGroups = GetGroupDropDownData(Convert.ToInt32(id));
                return this.Json(new
                {
                    status = MasterConstants.SUCCESSTITLE,
                    msg = string.Empty,
                    Data = new JavaScriptSerializer().Serialize(subjectGroups)
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //exception db logging
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = _UserID.ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);

                return this.Json(new
                {
                    status = MasterConstants.FAILURETITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred,
                    Data = string.Empty
                }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Method for Add the Student Attendance.
        /// </summary>
        /// <param name="SubjectId">Id: Id Of Subject, passed by reference,
        /// <param name="AttendanceDate">AttendanceDate: Attendance Datewise, passed by reference,
        /// <param name="GroupID">GroupID: GroupID Of Groups, passed by reference,
        /// <returns> Return  the Added Student Attendance list .</returns>
        public ActionResult AddStudentAttandance([Bind(Include = "SubjectID,AttandanceDate,GroupID")]StudentAttandanceViewModal model)
        {
            int? _UserID = Session["UserId"] as int?;
            try
            {
                if (Request.IsAjaxRequest() && ModelState.IsValid)
                {
                    using (TransactionScope trans = new TransactionScope())
                    {
                        //add assignment keywords
                        trans.Complete();
                        return Json(new
                        {
                            status = MasterConstants.SUCCESSTITLE,
                            message = string.Empty,
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        status = MasterConstants.FAILURETITLE,
                        message = error
                    });
                }
            }
            catch (Exception ex)
            {
                //exception db logging
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = _UserID.ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);

                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    message = ex.Message
                });
            }
        }


        /// <summary>
        /// Method for Bind  the Group Dropdown.
        /// </summary>
        /// <param name="SubjectId">Id: Id Of Subject, passed by reference,
        /// <returns> Return  the list of groups as per studentid.</returns>
        private IEnumerable<DropdownViewModal> GetGroupDropDownData(int? subjectID)
        {
            var groupDropdownData = new List<DropdownViewModal>();
            var subjectGroups = _grouRepo.GetBySubjectID(Convert.ToInt32(subjectID));
            foreach (var item in subjectGroups)
            {
                groupDropdownData.Add(new DropdownViewModal() { Id = item.GroupID, Value = item.GroupName });
            }
            return groupDropdownData;
        }
        #endregion

        public ActionResult StudentAssignments()
        {
            ViewSubmissionsAdminModel _stuAssignView = new ViewSubmissionsAdminModel();
            int? _InstutionID = Session["Institution"] as int?;
            _stuAssignView.userInfo = _userRepo.Get(User.Identity.GetUserId());
            Session["Theme"] = _insRepo.Get(Convert.ToInt32(Session["Institution"])).Theme.ThemeName;
            return View(_stuAssignView);
        }

        #region [StudentAssociationWithGroupsAndAssignments]
        public ActionResult GetStudentsGroups(string uid, string utype, string subid)
        {
            if (string.IsNullOrEmpty(uid) || string.IsNullOrEmpty(utype) || string.IsNullOrEmpty(subid))
            {
                return this.Json(new
                {
                    status = MasterConstants.FAILURETITLE,
                    msg = MasterConstants.INVALIDARGUMENT,
                    Data = string.Empty
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                List<string> Data;
                string status = MasterConstants.SUCCESSTITLE;
                if (utype == "t")
                {
                    Data = new List<string>();
                }
                else if (utype == "s" || utype == "l")
                {
                    var groups = _grouRepo.GetBySubjectStudents(Convert.ToInt32(subid), Convert.ToInt32(uid));
                    Data = groups.Select(n => n.GroupName).ToList();
                }
                else
                {
                    status = MasterConstants.FAILURETITLE;
                    Data = new List<string>();
                }
                return this.Json(new
                {
                    status = status,
                    msg = string.Empty,
                    Data = Data
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}