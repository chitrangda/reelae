﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReelaeDataAccessLayer;

namespace Reelae.Areas.Admin.Models
{
    public class StudentAttandanceViewModal
    {
        public IEnumerable<Subject> Subjects { get; set; }
        public IEnumerable<Group> Groups { get; set; }
        public int SubjectID { get; set; }
        public int GroupID { get; set; }
        public string AttandanceDate { get; set; }
    }
}