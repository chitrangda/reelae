﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReelaeDataAccessLayer;

namespace Reelae.Areas.Admin.Models
{
    public class TeacherInfoAdminView
    {
        public User userInfo { get; set; }
        public IEnumerable<Assignment> Assignments { get; set; }

        public IEnumerable<SubjectTeacher> Subjects { get; set; }
    }
    public class StudentInfoAdminView
    {
        public User userInfo { get; set; }

        public IEnumerable<re_getStudentAssignment_sp_Result> Assignments { get; set; }

        public IEnumerable<SubjectStudent> Subjects { get; set; }

        public IEnumerable<re_getStudentAttendance_Result> AttendanceRate { get; set; }
    }

   
}