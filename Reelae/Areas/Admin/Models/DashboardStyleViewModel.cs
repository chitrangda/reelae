﻿using ReelaeDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reelae.Areas.Admin.Models
{
    public class DashboardStyleViewModel
    {
        public Institution _institution { get; set; }

        public IEnumerable<Theme> _themes { get; set; }
    }
}