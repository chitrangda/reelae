﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReelaeDataAccessLayer;
using Reelae.Models;


namespace Reelae.Areas.Admin.Models
{
    public class AdminViewModel
    {
        public AdminViewModel() {
            _subject = new Subject();
        }

        public IEnumerable<re_select_count_member_studentwise_sp_Result> Subjects { get; set; }

        public Subject _subject { get; set; }


        public IEnumerable<re_select_studenttecher_subject_sp_Result> _subjectMembers { get; set; }

        public User _userinfo { get; set; }


        #region [GetTeacher and Student Count]
        public int TeachersCount { get; set; }
        public int StudentsCount { get; set; }
        #endregion

        public IEnumerable<re_select_count_member_studentwise_sp_Result> ArchiveSubjects { get; set; }

    }


}