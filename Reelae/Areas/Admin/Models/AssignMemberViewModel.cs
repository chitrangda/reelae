﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reelae.Areas.Admin.Models
{
    public class AssignMemberViewModel
    {
        public bool isSelect { get; set; }

        public int userId { get; set; }

        public string Name { get; set; }

        public string userPicUrl { get; set; }

        public string Email { get; set; }

        public string type { get; set; }

    }
}