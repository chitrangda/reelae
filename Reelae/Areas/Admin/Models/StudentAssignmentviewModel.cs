﻿using ReelaeDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reelae.Areas.Admin.Models
{
    public class TeacherInfoView
    {
        public IEnumerable<Subject> Subjects { get; set; }
        public IEnumerable<Group> Groups { get; set; }
    }

    public class StudentAssignmentInfoViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public AssignmentStatus Status { get; set; }
        public bool IsSubmitted { get; set; }
    }

    public enum AssignmentStatus
    {
        Open = 0,
        Close,
        Pending,
        Checked
    }
}