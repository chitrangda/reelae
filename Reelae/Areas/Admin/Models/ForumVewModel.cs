﻿using ReelaeDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reelae.Areas.Admin.Models
{
    public class ForumVewModel
    {
        public User _userinfo { get; set; }

        public IEnumerable<Forum> _forumTopics { get; set; }
    }
}