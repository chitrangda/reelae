﻿using ReelaeDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reelae.Areas.Admin.Models
{
    public class PinupBoardViewModel
    {
        public User _userinfo { get; set; }

        public IEnumerable<PinUpBoard> _pinups { get; set; }
    }
}