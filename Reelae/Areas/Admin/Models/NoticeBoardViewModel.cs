﻿using System;
using ReelaeDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Reelae.Areas.Admin.Models
{
    public class NoticeBoardViewModel
    {
        public User _userinfo { get; set; }

        public IEnumerable<NoticeBoard> noticeBoard { get; set; }
    }
}