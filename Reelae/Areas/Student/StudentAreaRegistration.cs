﻿using System.Web.Mvc;

namespace Reelae.Areas.Student
{
    public class StudentAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Student";
            }
        }
        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Student_default",
                "Student/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "Reelae.Areas.Student.Controllers" }
            );
            //context.MapRoute(
            //    "Student_default",
            //    "Student/{controller}/{action}/{id}",
            //    new { action = "StudentDashboard", id = UrlParameter.Optional }
            //);
        }
    }
}