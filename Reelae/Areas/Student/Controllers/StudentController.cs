﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Reelae.Areas.Student.Models;
using Microsoft.AspNet.Identity;
using Reelae.Filters;
using Reelae.Repositories;
using ReelaeBusinessLayer.Utilities;
using ReelaeDataAccessLayer;
using Reelae.Utilities;
using System.Text;

namespace Reelae.Areas.Student.Controllers
{
    [LoggingFilter]
    [checkSessionAlive]
    [Authorize(Roles = "Student")]
    public class StudentController : Controller
    {
        ILogRepository _dbLogrepo;
        IUserRepository _userRepo;
        IInstitutionRepository _insRepo;
        IAssignmentRepository _assignRepo;
        IGroupRepository _groupRepo;
        /// <summary>
        /// Method for Load the view of Student Dashboard.
        /// </summary>
        /// <returns> Display the Student Dashboard .</returns>
        /// 

        public StudentController(ILogRepository __dbLogrepo, IUserRepository __userRepo, IInstitutionRepository __insRepo, IAssignmentRepository __assignRepo, IGroupRepository __groupRepo)
        {
            _dbLogrepo = __dbLogrepo;
            _userRepo = __userRepo;
            _insRepo = __insRepo;
            _assignRepo = __assignRepo;
            _groupRepo = __groupRepo;

        }

        public ActionResult Dashboard()
        {
            int? _UserID = Session["UserId"] as int?;
            StudentDashboardViewModal model = new StudentDashboardViewModal();
            try
            {
                model = new StudentDashboardViewModal(_UserID.Value);
                int _instituteId = Convert.ToInt32(Session["Institution"]);
                int _userid = Convert.ToInt32(Session["UserId"]);
                Session["Theme"] = _insRepo.Get(_instituteId).Theme.ThemeName;
                model._userinfo = _userRepo.Get(User.Identity.GetUserId());
                model.Assignments = GetActiveAssignments(model.FilterStartDate.ToString("MM/dd/yyyy"), model.FilterEndDate.ToString("MM/dd/yyyy"));
                ViewBag.PageHeading = "Main";

            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = _UserID.ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
            }
            return View(model);
        }

        public ActionResult GetStudentsActiveAssignments(string FilterSD, string FilterED)
        {
            return PartialView("_ActiveAssignments", GetActiveAssignments(FilterSD, FilterED));
        }

        [ChildActionOnly]
        public ActionResult AssignmentMembersProfilePic(int assignmentid)
        {
            int? _UserID = Session["UserId"] as int?;
            if (assignmentid > 0)
            {
                IEnumerable<User> userprofiles = _assignRepo.GetAssignmentMembersProfile(_UserID, assignmentid);
                StringBuilder sb = new StringBuilder();
                foreach (var item in userprofiles)
                {
                    sb.Append("<img title='" + item.Name + "' src='" + ((!string.IsNullOrEmpty(item.UserPicUrl) ? item.UserPicUrl : Url.Content("~/content/images/user.png"))) + "' class='img-responsive img-circle sm_img'>");
                }
                return Content(sb.ToString());
            }
            else
            {
                return Content("");
            }
        }

   

        #region [PrivateMethods]
        private IEnumerable<Assignment> GetActiveAssignments(string FilterSD, string FilterED)
        {
            IEnumerable<Assignment> filteredData = new List<Assignment>();
            int? _UserID = Session["UserId"] as int?;//studentID
            DateTime? FSD = CommonUtility.GetDateTimeFromString(FilterSD, MasterConstants.DATEFORMATMDY);
            DateTime? FED = CommonUtility.GetDateTimeFromString(FilterED, MasterConstants.DATEFORMATMDY);
            filteredData = _assignRepo.GetStudentsFilteredActiveAssignments(_UserID, FSD, FED);
            return filteredData;
        }

        #endregion


        private IEnumerable<Areas.Teacher.Models.DropdownViewModal> GetGroupDropDownData()
        {
            var groupDropdownData = new List<Areas.Teacher.Models.DropdownViewModal>();
            int _userid = Convert.ToInt32(Session["UserId"]);
            var result = _groupRepo.GetStudentGroups(_userid);
            foreach (var item in result)
            {
                groupDropdownData.Add(new Areas.Teacher.Models.DropdownViewModal() { Id = item.GroupID, Value = item.GroupName });
            }
            return groupDropdownData;
        }


       
    }
}