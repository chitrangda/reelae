﻿using Reelae.Repositories;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Reelae.Areas.Student.Models;
using Reelae.Filters;
using ReelaeBusinessLayer.Utilities;
using Reelae.Utilities;
using System.IO;
using ReelaeDataAccessLayer;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;

namespace Reelae.Areas.Student.Controllers
{
    [LoggingFilter]
    [checkSessionAlive]
    [Authorize(Roles = "Student")]
    public class NoticeBoardController : Controller
    {
        IUserRepository _userRepo;
        ILogRepository _dbLogrepo;
        INoticeBoardRepository _noticeRepo;
        IGroupRepository _groupRepo;

        public NoticeBoardController(IUserRepository __userRepo, ILogRepository __dbLogrepo, INoticeBoardRepository __noticeRepo, IGroupRepository __groupRepo)
        {
            _userRepo = __userRepo;
            _dbLogrepo = __dbLogrepo;
            _noticeRepo = __noticeRepo;
            _groupRepo = __groupRepo;
        }

        public ActionResult Index()
        {
            NoticeBoardViewModel model = new NoticeBoardViewModel();
            int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            model._userinfo = _userRepo.Get(User.Identity.GetUserId());
            ViewBag.NoticeCount = _noticeRepo.Get(_userid as int?).Count();
            ViewBag.PageHeading = "Notice Board";
            return View(model);
        }

        [HttpPost]
        public ActionResult getNotices()
        {
            try
            {
                NoticeViewModel model = new NoticeViewModel();
                int? _studentId = Session["UserId"] as int?;
                model.notices = _noticeRepo.Get(_studentId);
                //model.noticeComments = _noticeRepo.GetComments(_studentId);
                model.noticeComments = _noticeRepo.GetNoticeComments(_noticeRepo.Get(_studentId).Select(r => r.NoticeBoardID).ToList());

                model.noticeFiles = _noticeRepo.getnoticeFiles(_noticeRepo.Get(_studentId).Select(r => r.NoticeBoardID as int?).ToList());

                return PartialView("_noticeList", model);

            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveComment(int id, string _comment)
        {
            try
            {
                int? _studentId = Session["UserId"] as int?;
                _noticeRepo.AddComment(id, _studentId, _comment, Session["UserName"].ToString());
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.SAVESUCCESSFULLY
                });
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult getMoreComments(int? id)
        {
            try
            {
                ViewBag.NoticeBoardID = id;
                var model = _noticeRepo.getAllComments(id);
                return PartialView("_noticeComments", model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public JsonResult SaveStartTime(string[] id)
        {
            int? _studentId = Session["UserId"] as int?;
            _noticeRepo.Save_NoticeStartTime(_studentId, id, Session["UserName"].ToString());
            return this.Json("True");

        }

        [HttpPost]
        public JsonResult SaveEndTime()
        {
            int? _studentId = Session["UserId"] as int?;
            _noticeRepo.Save_NoticeEndTime(_studentId);
            return this.Json("True");

        }

        [HttpPost]
        public JsonResult SaveDownloadStatus(int id)
        {
            int? _studentId = Session["UserId"] as int?;
            _noticeRepo.Save_NoticeEndTime(id);
            return this.Json("True");
        }

        [HttpPost]
        public ActionResult EditComment(int id)
        {
            try
            {
                return PartialView("_editComment", _noticeRepo.GetComment(id));
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }

        }
        [HttpPost]
        public ActionResult UpdateComment(StudentNoticeBoardAction model)
        {
            try
            {
                _noticeRepo.Update(model,Session["UserName"].ToString());
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.SAVESUCCESSFULLY
                });
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }

        }
        [HttpPost]
        public ActionResult RemoveComment(int id)
        {
            try
            {
                _noticeRepo.RemoveComment(id);
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.REMOVE_SUCCESS
                });
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }
        }
    }
}
