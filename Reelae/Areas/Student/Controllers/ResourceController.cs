﻿using Microsoft.AspNet.Identity;
using Reelae.Areas.Student.Models;
using Reelae.Filters;
using Reelae.Models;
using Reelae.Repositories;
using Reelae.Utilities;
using ReelaeBusinessLayer.Utilities;
using ReelaeDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Reelae.Areas.Student.Controllers
{
    [LoggingFilter]
    [checkSessionAlive]
    [Authorize(Roles = "Student")]
    public class ResourceController : Controller
    {
        ILogRepository _dbLogrepo;
        IUserRepository _userRepo;
        IInstitutionRepository _insRepo;
        IAssignmentRepository _assignRepo;
        IGroupRepository _groupRepo;
        IResourceRepository _resourceRepository;
        ISubjectStudentRepository _subjectStudentRepository;
        public ResourceController(ILogRepository __dbLogrepo, IUserRepository __userRepo, IInstitutionRepository __insRepo, IAssignmentRepository __assignRepo, IGroupRepository __groupRepo, IResourceRepository __resourceRepository, ISubjectStudentRepository __subjectStudentRepository)
        {
            _dbLogrepo = __dbLogrepo;
            _userRepo = __userRepo;
            _insRepo = __insRepo;
            _assignRepo = __assignRepo;
            _groupRepo = __groupRepo;
            _resourceRepository = __resourceRepository;
            _subjectStudentRepository = __subjectStudentRepository;
        }
        // GET: Student/Resource
        public ActionResult Index()
        {
            int _instituteId = Convert.ToInt32(Session["Institution"]);
            int _userid = Convert.ToInt32(Session["UserId"]);
            Session["Theme"] = _insRepo.Get(_instituteId).Theme.ThemeName;
            StudentResourceViewModal _memberResourceView = new StudentResourceViewModal();
            _memberResourceView.userInfo = _userRepo.Get(User.Identity.GetUserId());
            ViewBag.PageHeading = "Resource";
            ViewBag.Subjects = _subjectStudentRepository.GetStudentSubjectList(_userid, _instituteId);//subject view bag
            return View(_memberResourceView);
        }

        [checkSessionAlive]
        [HttpPost]
        public ActionResult GetStudentReourceFileDetails(int id)
        {
            try
            {
                return PartialView("_PartialResourceDetail", _resourceRepository.Get(id));
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message }; new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                });
            }
        }

        //function for filtring
        [checkSessionAlive]
        [HttpPost]
        public ActionResult GetResources(int? subjectId, string sortOrder, string docType, string otherFilter, int? tag,int? labelId)
        {
            try
            {
                return PartialView("_PartialResourceGridView", _resourceRepository.GetAllStudentResource(subjectId, docType, tag, otherFilter, sortOrder, labelId));
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                });
            }
        }

        [checkSessionAlive]
        public FileResult DownloadFile(int resourceId)
        {
            ResourceActivity _resourceActivity = new ResourceActivity()
            {
                ResourceID = resourceId,
                IsDownloaded = 1,
                DownloadTime = DateTime.Now,
            };
            _resourceRepository.SaveDownloadActivity(_resourceActivity);

            var result = _resourceRepository.Get(resourceId);
            var test1 = Server.MapPath("~");
            var filePath = System.IO.Path.Combine(test1, "Resource/" + result.URL);
            return File(filePath, MimeMapping.GetMimeMapping(filePath), result.Title);
        }

        [checkSessionAlive]
        [HttpPost]
        public ActionResult Download(int resourceId)
        {
            return this.Json(new
            {
                EnableSuccess = true,
                SuccessTitle = MasterConstants.SUCCESSTITLE,
                SuccessMsg = MasterConstants.SAVESUCCESSFULLY
            });
        }

        //view description
        [checkSessionAlive]
        [HttpPost]
        public ActionResult ViewResourceDescription(int? id)
        {
            try
            {
                if (id != null)
                {
                    return PartialView("_PartialViewResourceDescription", _resourceRepository.Get(id));
                }
                else
                {
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = error
                    });

                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.PLEASETRYAGAINLATER
                });
            }
        }

        [checkSessionAlive]
        [HttpPost]
        public ActionResult GetStudentResourceTags(int? id)
        {
            try
            {
                return PartialView("_PartialResourceTags", _resourceRepository.GetStudentResourceTags());
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                });
            }
        }
        //update view activity
        [checkSessionAlive]
        [HttpPost]
        public ActionResult UpdateViewActivity(int? id)
        {
            try
            {
                _resourceRepository.UpdateViewActivity(id);
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.SAVESUCCESSFULLY 
                });
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                });
            }
        }

        //Save visit time
        [checkSessionAlive]
        [HttpPost]
        public ActionResult SaveResorceVisitTime(DateTime? startDate, DateTime? endTime,int? subjectId)
        {
            try
            {
                int UserId = int.Parse(Session["UserId"].ToString());
                if (startDate != null && endTime != null)
                {
                    StudentActivity studentActivity = new StudentActivity()
                    {
                        StartDateTime = startDate,
                        EndDateTime = endTime,
                        StudentId = UserId,
                        Page="Resource",
                        SubjectId= subjectId
                    };
                    int addedActivityId = _resourceRepository.SaveVisitDetails(studentActivity);
                    if (addedActivityId > 0)
                    {
                        return this.Json(new
                        {
                            EnableSuccess = true,
                            SuccessTitle = MasterConstants.SUCCESSTITLE,
                            SuccessMsg = "Resource label added successfully.",
                            addedId = addedActivityId
                        });
                    }
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = error
                    });
                }
                else
                {
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = error
                    });
                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.PLEASETRYAGAINLATER
                });
            }
        }


    }
}