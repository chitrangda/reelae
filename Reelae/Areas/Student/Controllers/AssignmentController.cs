﻿using Microsoft.AspNet.Identity;
using Reelae.Areas.Student.Models;
using Reelae.Filters;
using Reelae.Repositories;
using Reelae.Utilities;
using ReelaeBusinessLayer.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Reelae.Areas.Student.Controllers
{
    [LoggingFilter]
    [checkSessionAlive]
    [Authorize(Roles = "Student")]
    public class AssignmentController : Controller
    {
        ILogRepository _dbLogrepo;
        IUserRepository _userRepo;
        IInstitutionRepository _insRepo;
        IAssignmentRepository _assignRepo;
        IGroupRepository _groupRepo;
        public AssignmentController(ILogRepository __dbLogrepo, IUserRepository __userRepo, IInstitutionRepository __insRepo, IAssignmentRepository __assignRepo, IGroupRepository __groupRepo)
        {
            _dbLogrepo = __dbLogrepo;
            _userRepo = __userRepo;
            _insRepo = __insRepo;
            _assignRepo = __assignRepo;
            _groupRepo = __groupRepo;

        }
        // GET: Student/Assignment
        public ActionResult Index()
        {
            int? _UserID = Session["UserId"] as int?;
            int _instituteId = Convert.ToInt32(Session["Institution"]);
            StudentAssignmentViewModal svm = new StudentAssignmentViewModal();
            Session["Theme"] = _insRepo.Get(_instituteId).Theme.ThemeName;
            svm._userinfo = _userRepo.Get(User.Identity.GetUserId());
            ViewBag.PageHeading = "Assignments";
            return View(svm);
        }

        [checkSessionAlive]
        public ActionResult StudentAssignmentGrid(int? id)
        {
            //return PartialView("_AssignmentGrid", _assignRepo.GetStudentAssignmentList(Convert.ToInt32(Session["UserId"]), id).ToList());
            return PartialView("_AssignmentGrid");
        }

        [checkSessionAlive]
        [HttpPost]
        public JsonResult BindStudentAssignmentDetails(int draw, int start, int length, int id)
        {
            start = start == 0 ? start : start / 10;
            try
            {
                string searchKey = Request["search[value]"];
                string order = Request["order[0][column]"];
                string orderby = Request["order[0][dir]"];
                searchKey = searchKey.Trim().ToLower();

                var assignmentList = _assignRepo.GetStudentAssignmentList(Convert.ToInt32(Session["UserId"]), id, start).ToList();

                assignmentList = assignmentList.Where(x => x.AssignmentName.Trim().ToLower().Contains(searchKey) || x.CreatedDate.ToString().Trim().ToLower().Contains(searchKey) || x.DueDate.ToString().Trim().ToLower().Contains(searchKey) || x.Leader.Trim().ToLower().Contains(searchKey) || x.GroupName.Trim().ToLower().Contains(searchKey)).ToList();

                var count = assignmentList.Count > 0 ? assignmentList[0].totalrecords.Value : 0;
                switch (order)
                {
                    case "0":
                        assignmentList = orderby.Equals("asc") ? assignmentList.OrderBy(x => x.AssignmentName).ToList() : assignmentList.OrderByDescending(x => x.AssignmentName).ToList();
                        break;
                    case "1":
                        assignmentList = orderby.Equals("asc") ? assignmentList.OrderBy(x => x.CreatedDate).ToList() : assignmentList.OrderByDescending(x => x.CreatedDate).ToList();
                        break;
                    case "2":
                        assignmentList = orderby.Equals("asc") ? assignmentList.OrderBy(x => x.DueDate).ToList() : assignmentList.OrderByDescending(x => x.DueDate).ToList();
                        break;
                    case "3":
                        assignmentList = orderby.Equals("asc") ? assignmentList.OrderBy(x => x.Leader).ToList() : assignmentList.OrderByDescending(x => x.Leader).ToList();
                        break;
                    case "4":
                        assignmentList = orderby.Equals("asc") ? assignmentList.OrderBy(x => x.GroupName).ToList() : assignmentList.OrderByDescending(x => x.GroupName).ToList();
                        break;
                    default:
                        assignmentList = orderby.Equals("asc") ? assignmentList.OrderBy(x => x.AssignmentName).ToList() : assignmentList.OrderByDescending(x => x.AssignmentName).ToList();
                        break;
                }

                JsonTableData jsonData = new JsonTableData();
                jsonData.recordsTotal = count;
                jsonData.recordsFiltered = count;
                jsonData.draw = draw;
                List<List<string>> assignmentDetailList = new List<List<string>>();

                ////////columns end///////////////
                if (assignmentList.Count > 0)
                {
                    foreach (var item in assignmentList)
                    {
                        ////////columns////////////////////
                        string col1 = "" + item.AssignmentName + "<div class='clearfix'></div><small class='txt_lgry'>" + item.Description + "</small>";
                        string col2 = "" + Convert.ToDateTime(item.CreatedDate).ToString("d  MMM , yyyy") + "";

                        string col3 = "" + Convert.ToDateTime(item.DueDate).ToString("d  MMM , yyyy") + "";
                        col3 += "<div class='clearfix'></div>";
                        if (DateTime.Now.Date > item.DueDate.Value.Date)
                        {
                            col3 += "<small class='txt_red'>Overdue</small>";
                        }                    
                        string imageUrl = string.IsNullOrEmpty(item.USERPicUrl) ? "/content/images/leader.jpg" : item.USERPicUrl.Replace("~", "/..");
                        string col4 = "<a href='javascript:;' class='user-profile pull-left'> <img src='" + imageUrl.Replace("~", "/..") + "'  title=" + item.Leader + " alt=''> </a>";
                        col4 += "<div class='media-body'><a class='title' href='#'>" + item.Leader + " </a>";
                        col4 += "<p><small class='txt_lgry'>" + item.GroupName + "</small></p></div>";

                        string col5 = string.Empty;
                        if (item.GroupName.ToLower() == "individual")
                        {
                            //col5 += "<span>" + item.GroupName + "</span>";
                            col5 += col4;
                        }
                        else
                        {
                            col5 += "<div id='thumbcarousel' class='carousel slide' data-interval='false'><div class='carousel-inner'>";
                            col5 += "<div class='item active'><div class='thumb bg_green'>" + item.GroupIntialName + "</div></div>";
                            col5 += "<div class='group_name'>" + item.GroupName + "</div></div></div>";
                        }

                        string col6 = "<a href='#!' class='sm_btn btn_org mrg_t-4' style='cursor:default;'>"+ item.AssignmentStatus.AssignmentStatus()+ "</a>  ";
                        string col7 = string.Empty;
                        if (DateTime.Now.Date > item.DueDate.Value.Date)
                        {
                            col7 = "<a href='#!' class='btn_blue mml-0 min_w-120 text-center' style='cursor: default;'>Workspace</a>";
                        }
                        else
                        {
                             col7 = "<a target='_blank' href='" + Url.Action("Index", "RealTimeAssignment", new { area = "Student", id = item.AssignmentID }) + "' class='btn_blue mml-0 min_w-120 text-center'>Workspace</a>";
                        }

                        string col8 = "<div class='mad-select pull-right' style='width: 110px;'>";
                        col8 += "<ul id='ddlAction' class='ddlAction'>";
                        col8 += "<li data-value='1 sel' class='selected'>Actions</li></ul>";

                        // string editAction = "onclick = archiveAssignment(" + item.AssignmentID + ",'" + item.AssignmentName.ToString().Replace(" ", "&nbsp;") + "'," + item.AssignmentStatus + ")";
                        // string archiveAction = "onclick=cancelAssignment(" + item.AssignmentID + ",'" + item.AssignmentName.ToString().Replace(" ", "&nbsp;") + "'," + item.AssignmentStatus + ")";

                        // col8 += "<li data-value='Edit' " + editAction + ">Archive</li>";
                        // col8 += "<li data-value='Archive' " + archiveAction + ">Cancel</li></ul>";

                        List<string> common = new List<string>();
                        common.Add(col1);
                        common.Add(col2);
                        common.Add(col3);
                        common.Add(col4);
                        common.Add(col5);
                        common.Add(col6);
                        common.Add(col7);
                        common.Add(col8);
                        assignmentDetailList.Add(common);
                    }
                }

                jsonData.data = assignmentDetailList;
                return new JsonResult()
                {
                    Data = jsonData,
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                return Json(null);
            }
        }


    }
}