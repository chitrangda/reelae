﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.AspNet.Identity;
using Reelae.Areas.Student.Models;
using Reelae.Filters;
using Reelae.Repositories;
using Reelae.Utilities;
using ReelaeBusinessLayer.Utilities;
using ReelaeDataAccessLayer;

namespace Reelae.Areas.Student.Controllers
{
    [LoggingFilter]
    [checkSessionAlive]
    [Authorize(Roles = "Student")]
    public class RealTimeAssignmentController : Controller
    {
        ILogRepository _dbLogrepo;
        IUserRepository _userRepo;
        IInstitutionRepository _insRepo;
        IAssignmentRepository _assignRepo;
        IAssignmentKeywordsRepository _assignKeywordRepo;
        IGroupRepository _groupRepo;

        public object MasterConstants { get; private set; }

        public RealTimeAssignmentController(ILogRepository __dbLogrepo, IUserRepository __userRepo, IInstitutionRepository __insRepo, IAssignmentRepository __assignRepo, IAssignmentKeywordsRepository __assignKeywordRepo, IGroupRepository __groupRepo)
        {
            _dbLogrepo = __dbLogrepo;
            _userRepo = __userRepo;
            _insRepo = __insRepo;
            _assignRepo = __assignRepo;
            _groupRepo = __groupRepo;
            _assignKeywordRepo = __assignKeywordRepo;
        }

        // GET: Student/RealTimeAssignment
        public ActionResult Index(int id)
         {
            try
            {
                int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
                int _instituionid = Session["Institution"] != null ? Convert.ToInt32(Session["Institution"]) : 0;
                RealTimeAssignmentViewModel RTAssVM = new RealTimeAssignmentViewModel();
                RTAssVM._userinfo = _userRepo.Get(User.Identity.GetUserId());
                RTAssVM.CurAssignment = _assignRepo.Get(id);

                //TeacherName
                //var reqUser = _userRepo.getUserByEmailId(RTAssVM.CurAssignment.CreatedBy);
                ViewBag.TeacherName = RTAssVM.CurAssignment.CreatedBy;
                RTAssVM.AssignmentLeader = _userRepo.Get(RTAssVM.CurAssignment.LeaderID.Value);
                RTAssVM.AssignmentKeywords = _assignKeywordRepo.GetAssignmentKeywordsByAssignmentID(RTAssVM.CurAssignment.AssignmentID).Select(y => y.KeyWord.KeywordName);

                var userKeywordsCountData = _assignRepo.GetStudentKeywordWiseData(id, _userid);
                RTAssVM.UserKeywordUsageData = _assignKeywordRepo.GetAssignmentKeywordsByAssignmentID(RTAssVM.CurAssignment.AssignmentID).Select(y => new AssignmentKeywordData { KeyWordId = y.KeywordID.Value, KeyWordName = y.KeyWord.KeywordName, Count = (userKeywordsCountData.Any(n => n.KeywordId == y.KeywordID && n.StudentId == _userid)) ? (userKeywordsCountData.Where(n => n.KeywordId == y.KeywordID && n.StudentId == _userid).First().KeywordCount.Value) : 0 });

                RTAssVM.AssignmentMembersData = _assignRepo.GetAssignmentWorkSpaceContent(id).ToList();
                RTAssVM.totalWordCount = RTAssVM.AssignmentMembersData.Sum(n => n.WordCount) ?? 0;
                ViewBag.TotalWordsGoal = RTAssVM.CurAssignment.WordsGoal;

                if (RTAssVM.CurAssignment.GroupID != null)
                {
                    IEnumerable<AssignmentGroupMembers> allMembers = _GetGroupMembers(RTAssVM.CurAssignment.GroupID);
                    RTAssVM.AssignmentMembers = allMembers.Where(n => n.MemberId == _userid);

                    RTAssVM.IsGroupAssignment = true;
                    ViewBag.groupId = RTAssVM.CurAssignment.GroupID;
                    ViewBag.groupMembersList = allMembers.Select(n => n.Email).ToArray();
                    ViewBag.usedKeywordsCount = allMembers.Select(n => (RTAssVM.AssignmentMembersData.Any(x => x.StudentID == n.MemberId && x.WordCount.HasValue)) ? RTAssVM.AssignmentMembersData.Where(x => x.StudentID == n.MemberId).First().WordCount.Value : 0).ToList<int>();

                    ViewBag.membersWordsCount = allMembers.Select(n => (RTAssVM.AssignmentMembersData.Any(x => x.StudentID == n.MemberId && x.WordCount.HasValue)) ? RTAssVM.AssignmentMembersData.Where(x => x.StudentID == n.MemberId).First().WordCount.Value : 0).ToList<int>();

                }
                else
                {
                    ViewBag.groupId = "";
                    RTAssVM.AssignmentMembers = new List<AssignmentGroupMembers>() {
                    new AssignmentGroupMembers() {
                        MemberId=RTAssVM.CurAssignment.LeaderID.Value,
                        MemberName=RTAssVM.AssignmentLeader.Name,
                        Email=RTAssVM.AssignmentLeader.Email,
                        MemberPicUrl=RTAssVM.AssignmentLeader.UserPicUrl,
                        UserShortName=CommonUtility.GetUserShortName(RTAssVM.AssignmentLeader.Name)
                } };
                    ViewBag.groupMembersList = RTAssVM.AssignmentMembers.Select(n => n.Email).ToArray();
                    ViewBag.usedKeywordsCount = RTAssVM.AssignmentMembers.Select(n => (RTAssVM.AssignmentMembersData.Any(x => x.StudentID == n.MemberId && x.WordCount.HasValue)) ? RTAssVM.AssignmentMembersData.Where(x => x.StudentID == n.MemberId).First().WordCount.Value : 0).ToList<int>();
                    ViewBag.membersWordsCount = RTAssVM.AssignmentMembers.Select(n => (RTAssVM.AssignmentMembersData.Any(x => x.StudentID == n.MemberId && x.WordCount.HasValue)) ? RTAssVM.AssignmentMembersData.Where(x => x.StudentID == n.MemberId).First().WordCount.Value : 0).ToList<int>();
                }
                RTAssVM.LastUpdatedOn = RTAssVM.AssignmentMembersData.Max(n => n.ModifiedDate);
                ViewBag.PerWordsGoal = (int)Math.Floor((double)ViewBag.TotalWordsGoal / ViewBag.groupMembersList.Length);
                if (RTAssVM.AssignmentMembersData.Count() > 0)
                {
                    var maxData = RTAssVM.AssignmentMembersData.OrderByDescending(x => x.ModifiedDate).First();
                    RTAssVM.LastUpdatedOn = maxData.ModifiedDate;
                    RTAssVM.LastUpdatedBy = maxData.Name;
                }
                else
                {
                    RTAssVM.LastUpdatedOn = DateTime.Now;
                    RTAssVM.LastUpdatedBy = "";
                }

                RTAssVM.AssignmentMembersData = RTAssVM.AssignmentMembersData.Where(n => n.StudentID == _userid).ToList();
                ViewBag.IsLeader = (RTAssVM.CurAssignment.LeaderID.Value == _userid) ? true : false;
                //RTAssVM.UserKeywordUsageData = _assignRepo.GetStudentKeywordWiseData(id, _userid);
                return View(RTAssVM);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return RedirectToAction("Index", "Error");
            }
        }

        public ActionResult GetAssignmentWorkSpaceContentOfStudent(string userEmailId, int? assignmentId)
        {
            int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            try
            {
                var reqUser = _userRepo.getUserByEmailId(userEmailId);
                var assignmentContentList = _assignRepo.GetAssignmentWorkSpaceContent(assignmentId).ToList();
                ViewBag.userCls = "user2";
                ViewBag.useShortName = CommonUtility.GetUserShortName(reqUser.Name);
                ViewBag.userPicUrl = reqUser.UserPicUrl;
                ViewBag.PerWordsGoal = 1;
                if (assignmentContentList.Any(n => n.StudentID == reqUser.UserID))
                {
                    return PartialView("_RTE_MemberEditingSection", assignmentContentList.Where(n => n.StudentID == reqUser.UserID).First());
                }
                else
                {
                    var curUserdefaultContent = new ReelaeDataAccessLayer.re_getAssignmentWorkspaceContent_sp_Result()
                    {
                        StudentID = reqUser.UserID,
                        Name = reqUser.Name,
                        CreatedBy = reqUser.Email,
                        Content = "",
                        CreatedDate = null,
                        ModifiedBy = null,
                        ModifiedDate = null
                    };
                    return PartialView("_RTE_MemberEditingSection", curUserdefaultContent);
                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = "",
                    ErrorMsg = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UpdateAssignmentWorkSpaceContent(string assignId, string studentId, string wordCount, string keywordWiseData, string contentHtml)
        {
            int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            try
            {
                int kC = 0;
                _assignRepo.UpdateAssignmentWorkSpaceContent(Convert.ToInt32(assignId), Convert.ToInt32(studentId), contentHtml, User.Identity.Name, (Int32.TryParse(wordCount, out kC)) ? kC : 0);
                var keywordData = new JavaScriptSerializer().Deserialize<List<AssignmentKeywordData>>(keywordWiseData);

                if (keywordData != null)
                    SaveStudentKeywordWiseData(Convert.ToInt32(assignId), Convert.ToInt32(studentId), keywordData);
                else
                    SaveStudentKeywordWiseData(Convert.ToInt32(assignId), Convert.ToInt32(studentId), new List<AssignmentKeywordData>());

                return this.Json(new
                {
                    status = Utilities.MasterConstants.SUCCESSTITLE,
                    msg = Utilities.MasterConstants.SaveSucessfully
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    status = Utilities.MasterConstants.ERRORTITLE,
                    msg = Utilities.MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #region PrivateMembers
        private IEnumerable<AssignmentMembersData> LoadAssignmentMemberData(IEnumerable<AssignmentGroupMembers> groupsData)
        {
            var groupMembersAssignmentData = new List<AssignmentMembersData>();
            int _index = 0;
            foreach (var item in groupsData)
            {
                _index++;
                groupMembersAssignmentData.Add(new AssignmentMembersData() { Member = item, Data = "" });
            }
            return groupMembersAssignmentData;
        }
        private IEnumerable<AssignmentGroupMembers> _GetGroupMembers(int? groupid)
        {

            var groupMembersView = new List<AssignmentGroupMembers>();
            //var teachersCreatedGroups = _teacherRepository.GetGroups(teacherID.Value);
            var groupMembers = _groupRepo.getGroupMembers(groupid);
            foreach (var item in groupMembers)
            {
                groupMembersView.Add(new AssignmentGroupMembers() { MemberId = item.UserID, MemberName = item.Name, MemberPicUrl = item.UserPicUrl, UserShortName = Utilities.CommonUtility.GetUserShortName(item.Name), Email = item.Email });
            }
            return groupMembersView;

        }
        private IEnumerable<int> GetMembersKeywordCounts(IEnumerable<AssignmentMembersData> assignmentMembers, IEnumerable<re_getAssignmentWorkspaceContent_sp_Result> assignmentContentData)
        {
            List<int> objCountList = new List<int>();
            foreach (var item in assignmentMembers)
            {
                //string Content= assignmentContentData.Where(n=>n.StudentID==item.Member.MemberId).
                if (assignmentContentData.Any(n => n.StudentID == item.Member.MemberId))
                {
                    string Content = assignmentContentData.Where(n => n.StudentID == item.Member.MemberId).First().Content;
                }
                else
                    objCountList.Add(0);
            }

            return objCountList;
        }

        private void SaveStudentKeywordWiseData(int assignmentId, int studentId, List<AssignmentKeywordData> UsedKeywordsList)
        {
            if (UsedKeywordsList != null)
            {
                using (TransactionScope sc = new TransactionScope())
                {
                    for (int i = 0; i < UsedKeywordsList.Count; i++)
                    {
                        _assignRepo.UpdateAssignmentWorkSpaceKeywordData(assignmentId, UsedKeywordsList[i].KeyWordId, studentId, UsedKeywordsList[i].Count);
                    }
                    sc.Complete();
                }

            }
        }

        #endregion
    }
}