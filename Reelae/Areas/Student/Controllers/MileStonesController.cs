﻿using Reelae.Repositories;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Reelae.Areas.Student.Models;
using Reelae.Filters;
using ReelaeBusinessLayer.Utilities;
using Reelae.Utilities;
using System.IO;
using ReelaeDataAccessLayer;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;

namespace Reelae.Areas.Student.Controllers
{

    [LoggingFilter]
    [checkSessionAlive]
    [Authorize(Roles = "Student")]
    public class MileStonesController : Controller
    {
        IUserRepository _userRepo;
        ILogRepository _dbLogrepo;
        IMilestoneRepository _milestoneRepo;

        public MileStonesController(IUserRepository __userRepo, ILogRepository __dbLogrepo, IMilestoneRepository __milestoneRepo)
        {
            _userRepo = __userRepo;
            _dbLogrepo = __dbLogrepo;
            _milestoneRepo = __milestoneRepo;

        }

        // GET: Student/MileStones
        public ActionResult Index()
        {
            try
            {
                MilestonesViewModel model = new MilestonesViewModel();
                int? _studentId = Session["UserId"] as int?;
                int? _InstutionID = Session["Institution"] as int?;
                model._userinfo = _userRepo.Get(User.Identity.GetUserId());
                model.assignments = _milestoneRepo.GetAssignments(_studentId);
                ViewBag.PageHeading = "Milestones";
                return View(model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserId"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    status = MasterConstants.FAILURETITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred,
                    Data = string.Empty
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult getStudents(DateTime startDate, DateTime endDate, int assignmentId)
        {
            try
            {
                List<MilestoneStudentsData> _students = new List<MilestoneStudentsData>();
                //var _data = _milestoneRepo.getMilestoneStudents(startDate.Date, endDate.Date, assignmentId).ToList();
                _milestoneRepo.getMilestoneStudents(startDate.Date, endDate.Date, assignmentId).ToList().ForEach(std => _students.Add(new MilestoneStudentsData() { id = std.StudentId.Value, name = std.StudentName, pic = Url.Content(std.UserPicUrl) }));
                //_students.ForEach(r => r.UserPicUrl = Url.Content(r.UserPicUrl));
                return Json(_students);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult getData(DateTime startDate, DateTime endDate, int assignmentId)
        {
            try
            {
                List<taskData> _data = new List<taskData>();
                _milestoneRepo.getMilestoneData(startDate.Date, endDate.Date, assignmentId).ToList().ForEach(d => _data.Add(new taskData()
                {
                    studentId = d.StudentId,
                    text = d.MileStoneName,
                    pic = d.User.UserPicUrl != null ? Url.Content(d.User.UserPicUrl) : Url.Content("~/Content/images/student.jpg"),
                    startDateYear = d.StartDate.Year,
                    startDateMonth = (d.StartDate.Month - 1),
                    startDateDay = d.StartDate.Day,
                    startDateHour = d.StartDate.Hour,
                    startDateMin = d.StartDate.Minute,
                    endDateYear = d.endDate.Year,
                    endDateMonth = (d.endDate.Month - 1),
                    endDateDay = d.endDate.Day,
                    endDateHour = d.endDate.Hour,
                    endDateMin = d.endDate.Minute,
                    milestoneId = d.MileStoneID,
                    startDateInfo = d.StartDate.ToString("dd MMM yyyy hh:mm tt"),
                    endDateInfo = d.endDate.ToString("dd MMM yyyy hh:mm tt"),
                    TaskDescription = d.TaskDescription,
                    MilesStoneStatus = d.MilesStoneStatus
                }));
                return Json(_data);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult getNewMilestoneView(int id)
        {

            try
            {
                newMilestoneViewModel model = new newMilestoneViewModel();
                ViewBag.AssignmentDueDate = _milestoneRepo.getAssignmentDate(id).Value;
                ViewBag.AssignmentName = _milestoneRepo.getAssignmentName(id);
                model.Memebers = _milestoneRepo.GetAssignmentMembers(id).ToList();
                model.milestone = new MileStone() { AssignmentID = id, StudentId = model.Memebers.First().MemberId };
                return PartialView("_newMilestone", model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserId"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    status = MasterConstants.FAILURETITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred,
                    Data = string.Empty
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult geteditMilestoneView(int id)
        {

            try
            {
                newMilestoneViewModel model = new newMilestoneViewModel();
                model.milestone = _milestoneRepo.GetMilestone(id);
                ViewBag.AssignmentDueDate = _milestoneRepo.getAssignmentDate(model.milestone.AssignmentID.Value).Value;
                ViewBag.AssignmentName = _milestoneRepo.getAssignmentName(model.milestone.AssignmentID.Value);
                model.Memebers = _milestoneRepo.GetAssignmentMembers(model.milestone.AssignmentID.Value).ToList();
                return PartialView("_newMilestone", model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserId"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    status = MasterConstants.FAILURETITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred,
                    Data = string.Empty
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult addNewMilestone(newMilestoneViewModel model)
        {
            try
            {
                if (model.milestone.MileStoneID == 0)
                {
                    if (ModelState.IsValid)
                    {
                        model.milestone.MilesStoneStatus = 1;
                        _milestoneRepo.Add(model.milestone, Session["UserName"].ToString());
                        return this.Json(new
                        {
                            EnableSuccess = true,
                            SuccessTitle = MasterConstants.SUCCESSTITLE,
                            SuccessMsg = MasterConstants.SAVESUCCESSFULLY
                        });
                    }
                    else
                    {
                        var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                        return this.Json(new
                        {
                            EnableError = true,
                            ErrorTitle = MasterConstants.ERRORTITLE,
                            ErrorMsg = error
                        });
                    }
                }
                else
                {
                    _milestoneRepo.Update(model.milestone, Session["UserName"].ToString());
                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = MasterConstants.SAVESUCCESSFULLY
                    });

                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }
        }

    

        [HttpPost]
        public ActionResult deleteMilestone(int id)
        {
            try
            {
                _milestoneRepo.Remove(id);
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.REMOVE_SUCCESS
                });
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }
        }


        [HttpPost]
        public ActionResult getEditMilestoneStatusView(int id)
        {
            return PartialView("_editMilestoneStatus", _milestoneRepo.GetMilestone(id));
        }

        [HttpPost]
        public ActionResult saveMilestoneStatus(MileStone model)
        {
            try
            {
                _milestoneRepo.UpdateStatus(model, Session["UserName"].ToString());
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.SAVESUCCESSFULLY
                });
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult getCellMilestones(DateTime date,int studentId)
        {
            var model = _milestoneRepo.getMilestones(date, studentId).ToList();
            return PartialView("_markDone",model);
        }
    }

    public class MilestoneStudentsData
    {
        public int id { get; set; }

        public string name { get; set; }

        public string pic { get; set; }

    }

    public class taskData
    {
        public Nullable<int> studentId { get; set; }

        public string text { get; set; }

        public int startDateYear { get; set; }

        public int startDateMonth { get; set; }

        public int startDateDay { get; set; }

        public int startDateHour { get; set; }

        public int startDateMin { get; set; }

        public int endDateYear { get; set; }

        public int endDateMonth { get; set; }

        public int endDateDay { get; set; }

        public int endDateHour { get; set; }

        public int endDateMin { get; set; }

        public string pic { get; set; }

        public int milestoneId { get; set; }

        public string startDateInfo { get; set; }

        public string endDateInfo { get; set; }

        public string TaskDescription { get; set; }

        public int MilesStoneStatus { get; set; }


    }
}