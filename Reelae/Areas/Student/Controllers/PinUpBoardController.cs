﻿using Reelae.Repositories;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Reelae.Areas.Student.Models;
using Reelae.Filters;
using ReelaeBusinessLayer.Utilities;
using Reelae.Utilities;
using System.IO;
using ReelaeDataAccessLayer;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;

namespace Reelae.Areas.Student.Controllers
{
    [LoggingFilter]
    [checkSessionAlive]
    [Authorize(Roles = "Student")]
    public class PinUpBoardController : Controller
    {
        IUserRepository _userRepo;
        ILogRepository _dbLogrepo;
        IGroupRepository _groupRepo;
        IPinUpBoardRepository _pinupboardRepo;

        public PinUpBoardController(IUserRepository __userRepo, ILogRepository __dbLogrepo, IGroupRepository __groupRepo, IPinUpBoardRepository __pinupboardRepo)
        {
            _userRepo = __userRepo;
            _dbLogrepo = __dbLogrepo;
            _groupRepo = __groupRepo;
            _pinupboardRepo = __pinupboardRepo;
        }

        // GET: Student/PinUpBoard
        public ActionResult Index()
        {
            PinUpBoardViewModel model = new PinUpBoardViewModel();
            int? _studentId = Session["UserId"] as int?;
            int? _InstutionID = Session["Institution"] as int?;

            model._userinfo = _userRepo.Get(User.Identity.GetUserId());
            //model._stickynotes = _pinupboardRepo.Get(_studentId);
            //model._publicNotes = _pinupboardRepo.GetPublic(_InstutionID, _studentId);
            ViewBag.PageHeading = "Pin-Up Board";
            return View(model);
        }

        [HttpPost]
        public ActionResult getPrivatePinups()
        {
            try
            {
                int? _studentId = Session["UserId"] as int?;
                var model = _pinupboardRepo.Get(_studentId);
                return PartialView("_PrivatePinUp", model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }
        }


        [HttpPost]
        public ActionResult getPublicPinups()
        {
            try
            {
                int? _studentId = Session["UserId"] as int?;
                int? _InstutionID = Session["Institution"] as int?;
                var model = _pinupboardRepo.GetPublicPinUps(_InstutionID,_studentId);
                return PartialView("_PublicPinUp", model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult getGroupPinups()
        {
            try
            {
                int? _InstutionID = Session["Institution"] as int?;
                int? _studentId = Session["UserId"] as int?;
                var model = _pinupboardRepo.GetGroupPinUps(_InstutionID,_studentId);
                return PartialView("_GroupPinUp", model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult getNewNoteView()
        {
            newPinUpBoardViewModel model = new newPinUpBoardViewModel();
            int? _studentId = Session["UserId"] as int?;
            model._pinupBoard = new PinUpBoard() { Visibilty = "Private" };
            model._groups = _pinupboardRepo.getGroups(_studentId);
            return PartialView("_newNote", model);

        }

        [HttpPost]
        public ActionResult addNewNote(newPinUpBoardViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (model._pinupBoard.Visibilty == "Public" || model._pinupBoard.Visibilty == "Private")
                    {
                        model._pinupBoard.GroupId = null;
                    }
                    int? _studentId = Session["UserId"] as int?;
                    model._pinupBoard.StudentId = _studentId;

                    int? _InstutionID = Session["Institution"] as int?;
                    model._pinupBoard.InstitutionId = _InstutionID;

                    if (model._pinupBoard.PinUpBoardID == 0)
                    {
                        _pinupboardRepo.Add(model._pinupBoard, Session["UserName"].ToString());
                    }
                    else
                    {
                        _pinupboardRepo.Update(model._pinupBoard, Session["UserName"].ToString());

                    }
                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = MasterConstants.SAVESUCCESSFULLY
                    });
                }
                else
                {
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = error
                    });
                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult editNote(int id)
        {
            newPinUpBoardViewModel model = new newPinUpBoardViewModel();
            int? _studentId = Session["UserId"] as int?;
            model._pinupBoard = _pinupboardRepo.Get(id);
            model._groups = _pinupboardRepo.getGroups(_studentId);
            return PartialView("_newNote", model);

        }

        [HttpPost]
        public ActionResult deleteNote(int id)
        {
            try
            {
                _pinupboardRepo.Remove(id);
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.REMOVE_SUCCESS
                });
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult getAttachment(int id)
        {
            try
            {
                ViewBag.PinupboardId = id;
                IEnumerable<PinUpBoardFile> model = _pinupboardRepo.getFiles(id);
                return PartialView("_noteFiles", model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult UploadNoteFile()
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    int id = Convert.ToInt32(Request.Form[0]);
                    ViewBag.PinupboardId = id;
                    HttpFileCollectionBase files = Request.Files;
                    //  Get all files from Request object  
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        SaveinDirectory(file, id);

                    }

                    // Returns message that successfully uploaded  
                    return this.Json(new
                    {
                        status = MasterConstants.SUCCESSTITLE,
                        msg = MasterConstants.SAVESUCCESSFULLY,
                        Data = string.Empty
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserId"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                    //   new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                    return this.Json(new
                    {
                        status = MasterConstants.FAILURETITLE,
                        msg = MasterConstants.SOMEPROBLEMOccurred,
                        Data = string.Empty
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(MasterConstants.NOFILESSELECTED);
            }
        }

        public PinUpBoardFile SaveinDirectory(HttpPostedFileBase _HttpPostedFile, int id)
        {
            try
            {
                string folderName = "~/PinUpBoardFiles/";
                string fileType = _HttpPostedFile.ContentType;
                string fileExt = System.IO.Path.GetExtension(_HttpPostedFile.FileName);
                string orginalFileName = _HttpPostedFile.FileName;
                string fileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(_HttpPostedFile.FileName);
                long fileSize = _HttpPostedFile.ContentLength;
                var uid = Guid.NewGuid();
                string renameFile = uid.ToString() + fileExt;
                if (!Directory.Exists(Server.MapPath(folderName)))
                {
                    Directory.CreateDirectory(Server.MapPath(folderName));
                }
                _HttpPostedFile.SaveAs(Path.Combine(Server.MapPath(folderName), renameFile));
                PinUpBoardFile _file = new PinUpBoardFile();
                _file.FileUrl = Path.Combine(folderName, renameFile);
                _file.FileName = orginalFileName;
                _file.FileSize = fileSize.ToString();
                _file.FileType = fileType;
                _file.CreatedDate = DateTime.Now;
                _file.CreatedBy = Session["UserName"].ToString();
                _file.PinUpBoardID = id;
                _pinupboardRepo.Add(_file);


                return _file;
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return null;
            }
        }

        [HttpPost]
        public ActionResult deleteFile(int id, string fileUrl)
        {
            try
            {
                var filePath = Server.MapPath(fileUrl);
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
                _pinupboardRepo.RemoveFile(id);
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.REMOVE_SUCCESS
                });
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult UploadEditNoteFile()
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    int id = Convert.ToInt32(Request.Form[0]);
                    ViewBag.PinupboardId = id;
                    HttpPostedFileBase _HttpPostedFile = Request.Files[0];
                    //  Get all files from Request object  

                    string folderName = "~/PinUpBoardFiles/";
                    string fileType = _HttpPostedFile.ContentType;
                    string fileExt = System.IO.Path.GetExtension(_HttpPostedFile.FileName);
                    string orginalFileName = _HttpPostedFile.FileName;
                    string fileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(_HttpPostedFile.FileName);
                    long fileSize = _HttpPostedFile.ContentLength;
                    var uid = Guid.NewGuid();
                    string renameFile = uid.ToString() + fileExt;
                    if (!Directory.Exists(Server.MapPath(folderName)))
                    {
                        Directory.CreateDirectory(Server.MapPath(folderName));
                    }
                    _HttpPostedFile.SaveAs(Path.Combine(Server.MapPath(folderName), renameFile));
                    PinUpBoardFile _file = _pinupboardRepo.GetFile(id);
                    _file.FileUrl = Path.Combine(folderName, renameFile);
                    _file.FileName = orginalFileName;
                    _file.FileSize = fileSize.ToString();
                    _file.FileType = fileType;
                    _pinupboardRepo.Update(_file, Session["UserName"].ToString());
                    // Returns message that successfully uploaded  
                    return this.Json(new
                    {
                        status = MasterConstants.SUCCESSTITLE,
                        msg = MasterConstants.SAVESUCCESSFULLY,
                        Data = string.Empty
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserId"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                    //   new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                    return this.Json(new
                    {
                        status = MasterConstants.FAILURETITLE,
                        msg = MasterConstants.SOMEPROBLEMOccurred,
                        Data = string.Empty
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(MasterConstants.NOFILESSELECTED);
            }
        }

    }

}