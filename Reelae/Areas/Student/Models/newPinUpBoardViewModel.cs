﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReelaeDataAccessLayer;

namespace Reelae.Areas.Student.Models
{
    public class newPinUpBoardViewModel
    {
        public PinUpBoard _pinupBoard { get; set; }

        public IEnumerable<re_GetStudentGroup_sp_Result> _groups { get; set; }
    }
}