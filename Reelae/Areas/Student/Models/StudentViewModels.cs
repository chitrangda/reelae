﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Reelae.Models;
using ReelaeDataAccessLayer;

namespace Reelae.Areas.Student.Models
{
    public class StudentViewModels
    {
    }
    public class StudentDashboardViewModal
    {
        #region Constructors
        public StudentDashboardViewModal()
        {
            DashboardItems = new List<DashboardCustomizedItem>();
            FilterStartDate = DateTime.Now.AddMonths(-1);
            FilterEndDate = DateTime.Now.AddMonths(1);
            DashboardItems.Add(new DashboardCustomizedItem() { IsEnabled = true, ItemName = "ActiveAssignment", ItemDescription = "For Displaying Active Assignment For Students" });
        }
        public StudentDashboardViewModal(int UserID)
        {
            DashboardItems = new List<DashboardCustomizedItem>();
            FilterStartDate = DateTime.Now.AddMonths(-1);
            FilterEndDate = DateTime.Now.AddMonths(1);
            DashboardItems.Add(new DashboardCustomizedItem() { IsEnabled = true, ItemName = "ActiveAssignment", ItemDescription = "For Displaying Active Assignment For Students", UserID = UserID });
        }
        #endregion

        #region Properties
        public IEnumerable<Assignment> Assignments { get; set; }
        public User _userinfo { get; set; }
        public List<DashboardCustomizedItem> DashboardItems { get; set; }
        public DateTime FilterStartDate { get; set; }
        public DateTime FilterEndDate { get; set; }

        #endregion

    }

    public class StudentAssignmentViewModal
    {
        #region Properties
        public User _userinfo { get; set; }
        #endregion
        public IEnumerable<re_getStudentAssignment_sp_Result> GetStudentAssignment { get; set; }
    }
}