﻿using ReelaeDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reelae.Areas.Student.Models
{
    public class PinUpBoardViewModel
    {

        public User _userinfo { get; set; }

        public IEnumerable<re_getPinUpBoard_sp_Result> _stickynotes { get; set; }

        public IEnumerable<re_getPublicPinUpBoard_sp_Result> _publicNotes { get; set; }


    }
}