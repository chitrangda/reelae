﻿using ReelaeDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reelae.Areas.Student.Models
{
    public class MilestonesViewModel
    {
        public User _userinfo { get; set; }

        public IEnumerable<re_getStudentAssignment_sp_Result> assignments { get; set; }
    }
}