﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using ReelaeDataAccessLayer;

namespace Reelae.Areas.Student.Models
{
    public class RealTimeAssignmentViewModel
    {
        public User _userinfo { get; set; }
        public Assignment CurAssignment { get; set; }
        public IEnumerable<AssignmentGroupMembers> AssignmentMembers { get; set; }
        public User AssignmentLeader { get; set; }
        public IEnumerable<string> AssignmentKeywords { get; set; }
        public bool IsGroupAssignment { get; set; }
        public IEnumerable<re_getAssignmentWorkspaceContent_sp_Result> AssignmentMembersData { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public string LastUpdatedBy { get; set; }
        public int totalWordCount { get; set; }
        public IEnumerable<AssignmentKeywordData> UserKeywordUsageData { get; set; }
        //public IEnumerable<> MyProperty { get; set; }
    }
    public class AssignmentGroupMembers
    {
        public int MemberId { get; set; }
        public string MemberName { get; set; }
        public string MemberPicUrl { get; set; }
        public string UserShortName { get; set; }
        public string Email { get; set; }
    }
    public class AssignmentMembersData
    {
        public AssignmentGroupMembers Member { get; set; }
        public string Data { get; set; }
        public int UsedKeywordsCount { get; set; }
        public string lastUpdatedTime { get; set; }

        public AssignmentMembersData()
        {
            Data = "";
            lastUpdatedTime = DateTime.Now.ToString("dd/MM/yy");
        }

    }

    public class AssignmentKeywordData
    {
        public int KeyWordId { get; set; }
        public string KeyWordName { get; set; }
        public int Count { get; set; }
    }

    #region [HubUtilities]
    public class AssignmentRTEData
    {
        [JsonProperty("from")]
        public int From { get; set; }
        [JsonProperty("fromEmailId")]
        public string FromEmailId { get; set; }
        [JsonProperty("content")]
        public string Content { get; set; }
        [JsonProperty("assignmentToken")]
        public int AssignmentToken { get; set; }
        [JsonProperty("selectionStart")]
        public int SelectionStart { get; set; }
        [JsonProperty("selectionEnd")]
        public int SelectionEnd { get; set; }
        [JsonProperty("actualContent")]
        public string ActualContent { get; set; }
        [JsonProperty("contentToken")]
        public string ContentToken { get; set; }
        [JsonProperty("updateTime")]
        public string UpdateTime { get; set; }
        [JsonProperty("currentGroupId")]
        public int CurrentGroupId { get; set; }
    }
    #endregion
}