﻿using Reelae.Areas.Teacher.Models;
using Reelae.Filters;
using Reelae.Repositories;
using Reelae.Utilities;
using ReelaeBusinessLayer.Utilities;
using ReelaeDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Reelae.Areas.Teacher.Controllers
{
    [LoggingFilter]
    [checkSessionAlive]
    [Authorize(Roles = "Teacher")]
    public class KeywordMasterController : Controller
    {
        // GET: Teacher/KeywordMaster
        IKeywordRepository _keywordRepository;
        ILogRepository _dbLogrepo;
        public KeywordMasterController(IKeywordRepository __keywordRepository, ILogRepository __dbLogrepo)
        {
            _keywordRepository = __keywordRepository;
            _dbLogrepo = __dbLogrepo;
        }
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult getKeywordMasterView()
        {
            int _InstitutionID = Session["Institution"] != null ? Convert.ToInt32(Session["Institution"]) : 0;
            keywordMasterViewModal model = new keywordMasterViewModal();
            model.exitingKeywords = _keywordRepository.Get(_InstitutionID);
            return PartialView("_keywordMaster", model);
        }

        public JsonResult removeKeyword(string keyword)
        {
            int _InstitutionID = Convert.ToInt32(Session["Institution"]);
            string _User = Session["UserName"] != null ? Session["UserName"].ToString() : "";
            if (keyword.Trim() != "")
            {
                try
                {
                    _keywordRepository.Remove(keyword);
                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = MasterConstants.KEYWORD_Remove_SUCESS
                    });

                }
                catch (Exception ex)
                {
                    ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = _User, CreatedDate = DateTime.UtcNow, Description = ex.Message };
                    new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                    });

                }
            }
            else
            {
                var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = error
                });

            }
        }

        public JsonResult SaveKeywords(string addedKeywords, string removedKeywords)
        {
            int _InstitutionID = Convert.ToInt32(Session["Institution"]);
            string _User = Session["UserName"] != null ? Session["UserName"].ToString() : "";
            if (addedKeywords != "")
            {
                string[] _add = addedKeywords.Split('Ω');
                if (_add.Length > 0)
                {
                    foreach (string keyword in _add)
                    {
                        _keywordRepository.Add(keyword, _InstitutionID, _User);
                    }
                }
            }
            if (!string.IsNullOrEmpty(removedKeywords))
            {
                string[] _removed = removedKeywords.Split('Ω');

                if (_removed.Length > 0)
                {
                    foreach (string keyword in _removed)
                    {
                        _keywordRepository.Remove(keyword);
                    }
                }
            }
            if (string.IsNullOrEmpty(addedKeywords) && string.IsNullOrEmpty(removedKeywords))
            {
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = "",
                    SuccessMsg = ""
                });
            }
            else
            {
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.SAVESUCCESSFULLY
                });
            }
        }
    }
}