﻿using Reelae.Repositories;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Reelae.Areas.Teacher.Models;
using Reelae.Filters;
using ReelaeBusinessLayer.Utilities;
using Reelae.Utilities;
using System.IO;
using ReelaeDataAccessLayer;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;

namespace Reelae.Areas.Teacher.Controllers
{
    [LoggingFilter]
    [checkSessionAlive]
    [Authorize(Roles = "Teacher")]
    public class NoticeBoardController : Controller
    {
        IUserRepository _userRepo;
        ILogRepository _dbLogrepo;
        INoticeBoardRepository _noticeRepo;
        IGroupRepository _groupRepo;

        public NoticeBoardController(IUserRepository __userRepo, ILogRepository __dbLogrepo, INoticeBoardRepository __noticeRepo, IGroupRepository __groupRepo)
        {
            _userRepo = __userRepo;
            _dbLogrepo = __dbLogrepo;
            _noticeRepo = __noticeRepo;
            _groupRepo = __groupRepo;
        }

        public ActionResult Index()
        {
            NoticeBoardViewModel model = new NoticeBoardViewModel();
            int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            int? _teacherId = Session["UserId"] as int?;
            model._groups = _groupRepo.GetTeacherGroups(_teacherId).OrderBy(r => r.GroupName);
            model._userinfo = _userRepo.Get(User.Identity.GetUserId());
            ViewBag.NoticeCount = _noticeRepo.Get(null, _teacherId).Count();
            //model._notices= _noticeRepo.Get(_teacherId);
            ViewBag.PageHeading = "Notice Board";
            return View(model);
        }

        [HttpPost]
        public ActionResult getNotices(string groupIds)
        {
            try
            {
                if (groupIds == "")
                {
                    groupIds = null;
                }
                NoticeViewModel model = new NoticeViewModel();
                int? _teacherId = Session["UserId"] as int?;
                model.notices = _noticeRepo.Get(groupIds, _teacherId);
                model.noticeComments = _noticeRepo.GetComments(groupIds, _teacherId);
                model.noticeFiles = _noticeRepo.getnoticeFiles(_noticeRepo.Get(groupIds, _teacherId).Select(r => r.NoticeBoardID as int?).ToList());
                return PartialView("_noticeList", model);

            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [checkSessionAlive]
        [HttpPost]
        public ActionResult UploadNoticeFile()
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    newNoticeViewModel model = new newNoticeViewModel();
                    model._noticeBoardFiles = new List<NoticeBoardFile>();
                    //  Get all files from Request object  
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        NoticeBoardFile _noticeFile = SaveinDirectory(file);
                        model._noticeBoardFiles.Add(_noticeFile);
                    }

                    // Returns message that successfully uploaded  
                    return PartialView("_newNoticeFiles", model);
                }
                catch (Exception ex)
                {
                    ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserId"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                    //   new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                    return this.Json(new
                    {
                        status = MasterConstants.FAILURETITLE,
                        msg = MasterConstants.SOMEPROBLEMOccurred,
                        Data = string.Empty
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(MasterConstants.NOFILESSELECTED);
            }
        }

        public NoticeBoardFile SaveinDirectory(HttpPostedFileBase _HttpPostedFile)
        {
            try
            {
                string folderName = "~/NoticeBoardFiles/";
                string fileType = _HttpPostedFile.ContentType;
                string fileExt = System.IO.Path.GetExtension(_HttpPostedFile.FileName);
                string orginalFileName = _HttpPostedFile.FileName;
                string fileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(_HttpPostedFile.FileName);
                long fileSize = _HttpPostedFile.ContentLength;
                var uid = Guid.NewGuid();
                string renameFile = uid.ToString() + fileExt;
                if (!Directory.Exists(Server.MapPath(folderName)))
                {
                    Directory.CreateDirectory(Server.MapPath(folderName));
                }
                _HttpPostedFile.SaveAs(Path.Combine(Server.MapPath(folderName), renameFile));
                NoticeBoardFile _file = new NoticeBoardFile();
                _file.FileUrl = Path.Combine(folderName, renameFile);
                _file.FileName = orginalFileName;
                _file.FileSize = fileSize.ToString();
                _file.FileType = fileType;

                return _file;
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return null;
            }
        }

        [checkSessionAlive]
        [HttpPost]
        public ActionResult DeleteNoticeFile(int id, string fileUrl)
        {
            if (!string.IsNullOrEmpty(fileUrl))
            {
                try
                {
                    var filePath = Server.MapPath(fileUrl);
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }
                    if (id != 0)
                    {
                        _noticeRepo.RemoveFile(id);
                    }
                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = MasterConstants.REMOVE_SUCCESS
                    });
                }
                catch (Exception ex)
                {
                    ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserId"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                    //   new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.NOFILESSELECTED
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult getNewNoticeView(string id,string groupName)
        {
            try
            {
                ViewBag.GroupId = id;
                newNoticeViewModel model = new newNoticeViewModel();
                model._noticeBoard = new NoticeBoard();
                model._noticeBoardFiles = new List<NoticeBoardFile>();
                model._noticeBoard.GroupID = id;
                if (groupName != "")
                {
                    ViewBag.GroupName = groupName;
                }
                return PartialView("_newNotice", model);

            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult addNewNotice(newNoticeViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (model._noticeBoard.NoticeBoardID == 0)
                    {
                        int? _teacherId = Session["UserId"] as int?;
                        model._noticeBoard.TeacherID = _teacherId;
                        _noticeRepo.Add(model, Session["UserName"].ToString());
                    }
                    else
                    {
                        _noticeRepo.Update(model, Session["UserName"].ToString());
                    }
                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = MasterConstants.SAVESUCCESSFULLY
                    });
                }
                else
                {
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = error
                    });
                }

            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult getMoreComments(int? id)
        {
            try
            {
                ViewBag.NoticeBoardID = id;
                var model = _noticeRepo.getAllCommentsTeachers(id);
                return PartialView("_noticeComments", model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult getEditNoticeView(int id)
        {
            try
            {
                newNoticeViewModel model = _noticeRepo.GetNotice(id);
                return PartialView("_newNotice", model);

            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult RemoveNotice(int id)
        {
            try
            {
                _noticeRepo.Remove(id);
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.REMOVE_SUCCESS
                });


            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult getNoticeBoardFiles(int? id)
        {
            newNoticeViewModel model = new newNoticeViewModel();
            if (id != null)
            {
                model._noticeBoardFiles = _noticeRepo.getnoticeFiles(id.Value).ToList();
            }
            return PartialView("_newNoticeFiles", model);
        }

    }
}
