﻿using Microsoft.AspNet.Identity;
using Reelae.Areas.Teacher.Models;
using Reelae.Filters;
using Reelae.Repositories;
using Reelae.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Reelae.Areas.Teacher.Controllers
{
    public class SubmissionsController : Controller
    {
        IInstitutionRepository _insRepo;
        IUserRepository _userRepo;
        IResourceRepository _resourceRepository;
        SubjectRepository _SubjectRepo;
        AssignmentRepository _AssignmentRepo;
        GroupRepository _GroupRepo;
        public SubmissionsController(IInstitutionRepository __insRepo, IUserRepository __userRepo, IResourceRepository __resourceRepository, SubjectRepository ___SubjectRepo, AssignmentRepository __AssignmentRepo, GroupRepository __GroupRepository)
        {
            _insRepo = __insRepo;
            _userRepo = __userRepo;
            _resourceRepository = __resourceRepository;
            _SubjectRepo = ___SubjectRepo;
            _AssignmentRepo = __AssignmentRepo;
            _GroupRepo = __GroupRepository;
        }
        // GET: Teacher/Submissions

        /// <summary>
        /// Method for Load the view of Assignment Submitted By Student.
        /// </summary>
        /// <returns> Display the view of Assignment submitted By Student .</returns>


        [checkSessionAlive]
        public ActionResult TeacherAssignmentView()
        {
            int _instituteId = Convert.ToInt32(Session["Institution"]);
            int _userid = Convert.ToInt32(Session["UserId"]);
            Session["Theme"] = _insRepo.Get(_instituteId).Theme.ThemeName;
            ViewSubmissionViewModel _viewSubmissionModel = new ViewSubmissionViewModel();
            _viewSubmissionModel._userinfo = _userRepo.Get(User.Identity.GetUserId());
            ViewBag.PageHeading = "View Submissions";
            ViewBag.Groups = GetGroupDropDownData();
            return View(_viewSubmissionModel);
        }

        private IEnumerable<Areas.Teacher.Models.DropdownViewModal> GetGroupDropDownData()
        {
            var groupDropdownData = new List<Areas.Teacher.Models.DropdownViewModal>();
            int? _InstitutionID = Session["Institution"] as int?;
            int _userid = Convert.ToInt32(Session["UserId"]);
            var result = _GroupRepo.getGroupWithMemberCountByTeacher(_userid);
            foreach (var item in result)
            {
                groupDropdownData.Add(new Areas.Teacher.Models.DropdownViewModal() { Id = item.GroupID, Value = item.GroupName });
            }
            return groupDropdownData;
        }


        [checkSessionAlive]
        public ActionResult TeacherAssignmentViewGrid(int? id)
        {
           // return PartialView("PartialViewSubmissionAssignment", _AssignmentRepo.GetTeacherViewSubmissionassignment(Convert.ToInt32(Session["UserId"]), id).ToList());
            return PartialView("PartialViewSubmissionAssignment");
        }

        [checkSessionAlive]
        public ActionResult getsearchAssignment(int? groupId, string assignmentName)
        {
            try
            {
                var result = _AssignmentRepo.GetTeacherViewSubmissionassignment(Convert.ToInt32(Session["UserId"]), groupId).ToList();
                var searhResult = result.Where(x => x.AssignmentName.Contains(assignmentName));
                return PartialView("PartialViewSubmissionAssignment", searhResult);
            }
            catch (Exception ex)
            {
                //ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message }; new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.DUPLICATESUBJECTNAME
                });
            }
        }

        //Archive Assignment
        public ActionResult ArchiveAssignment(int? id)
        {
            try
            {
                if (id != null)
                {
                    _AssignmentRepo.ArchiveAssignment(id);
                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = "Assignment Archive successfully."
                    });
                }
                else
                {
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = error
                    });

                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.PLEASETRYAGAINLATER
                });
            }
        }
        //Cancle Assignment
        public ActionResult CancelAssignment(int? id)
        {
            try
            {
                if (id != null)
                {
                    _AssignmentRepo.CancelAssignment(id);
                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = "Assignment Cancle successfully."
                    });
                }
                else
                {
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = error
                    });

                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.PLEASETRYAGAINLATER
                });
            }
        }


        [checkSessionAlive]
        [HttpPost]
        public JsonResult BindSubmitAssignmentDetails(int draw, int start, int length, int? id)
        {
            start = start == 0 ? start : start / 10;
            id = id == 0 ? null : id;
            try
            {
                string searchKey = Request["search[value]"];
                string order = Request["order[0][column]"];
                string orderby = Request["order[0][dir]"];
                searchKey = searchKey.Trim().ToLower();

                var assignmentList = _AssignmentRepo.GetTeacherViewSubmissionassignment(Convert.ToInt32(Session["UserId"]), id, start).ToList();
                assignmentList = assignmentList.Where(x => x.AssignmentName.Trim().ToLower().Contains(searchKey) || x.SubmissionDate.ToString().Trim().ToLower().Contains(searchKey) || x.DueDate.ToString().Trim().ToLower().Contains(searchKey) || x.Name.Trim().ToLower().Contains(searchKey) || x.GroupName.Trim().ToLower().Contains(searchKey)).ToList();

                var count = assignmentList.Count > 0 ? assignmentList[0].totalrecords.Value : 0;
                if (id == -1)
                {
                    count = assignmentList.Count;
                }
                switch (order)
                {
                    case "0":
                        assignmentList = orderby.Equals("asc") ? assignmentList.OrderBy(x => x.AssignmentName).ToList() : assignmentList.OrderByDescending(x => x.AssignmentName).ToList();
                        break;
                    case "1":
                        assignmentList = orderby.Equals("asc") ? assignmentList.OrderBy(x => x.SubmissionDate).ToList() : assignmentList.OrderByDescending(x => x.SubmissionDate).ToList();
                        break;
                    case "2":
                        assignmentList = orderby.Equals("asc") ? assignmentList.OrderBy(x => x.DueDate).ToList() : assignmentList.OrderByDescending(x => x.DueDate).ToList();
                        break;
                    case "3":
                        assignmentList = orderby.Equals("asc") ? assignmentList.OrderBy(x => x.Name).ToList() : assignmentList.OrderByDescending(x => x.Name).ToList();
                        break;
                    case "4":
                        assignmentList = orderby.Equals("asc") ? assignmentList.OrderBy(x => x.GroupName).ToList() : assignmentList.OrderByDescending(x => x.GroupName).ToList();
                        break;
                    default:
                        assignmentList = orderby.Equals("asc") ? assignmentList.OrderBy(x => x.AssignmentName).ToList() : assignmentList.OrderByDescending(x => x.AssignmentName).ToList();
                        break;
                }

                JsonTableData jsonData = new JsonTableData();
                jsonData.recordsTotal = count;
                jsonData.recordsFiltered = count;
                jsonData.draw = draw;
                List<List<string>> assignmentDetailList = new List<List<string>>();

                ////////columns end///////////////
                if (assignmentList.Count > 0)
                {
                    int _outerIndex = 0;
                    string[] _attendance = null;
                    foreach (var item in assignmentList)
                    {
                        string curCarouselID = "thumbcarousel_" + (++_outerIndex);
                        if (item.UserPicUrl != null)
                        {
                            _attendance = item.UserPicUrl.Split(',');
                        }
                        string[] _studentName = item.Name.Split(',');

                        ////////columns////////////////////
                        string col1 = "" + item.AssignmentName + "<div class='clearfix'></div><small class='txt_lgry'>" + item.Description + "</small>";
                        string col2 = "" + Convert.ToDateTime(item.SubmissionDate).ToString("d  MMM , yyyy") + "";

                        string col3 = "" + Convert.ToDateTime(item.DueDate).ToString("d  MMM , yyyy") + "";
                        col3 += "<div class='clearfix'></div>";
                        if (Convert.ToDateTime(@item.SubmissionDate) > Convert.ToDateTime(item.DueDate))
                        {
                            col3 += "<small class='txt_red'>Overdue</small>";
                        }

                        string col4 = string.Empty;
                        col4 += "<ul class='list-unstyled top_profiles scroll-view'>";
                        col4 += "<li class='media event'>";
                        col4 += "<a href='javascript:;' class='user-profile pull-left'> <img src='/content/images/leader.jpg' alt=''></a>";
                        col4 += "<div class='media-body'>";
                        col4 += "<a class='title' href='#'>" + item.Name + "</a>";
                        col4 += "<p><small class='txt_lgry'>" + item.GroupName + "</small></p>";
                        col4 += "</div>";
                        col4 += "</li>";
                        col4 += "</ul>";

                        string col5 = string.Empty;
                        col5 += "<div id='" + curCarouselID + "' class='carousel slide' data-interval='false'>";
                        col5 += "<div class='carousel-inner'>";
                        col5 += "<div class='item active'>";
                        for (int i = 0; i < _studentName.Length; i++)
                        {
                            col5 += "<a href='javascript:;' class='user-profile pull-left groupitem' style='cursor:default'>";
                            if (_attendance != null && _attendance.Length > 0 && _attendance[i] != "")
                            {
                                col5 += "<img src='" + _attendance[i] + "' alt='' title='" + _studentName[i] + "'>";
                            }
                            else
                            {
                                col5 += "<img src='/Content/images/student.jpg' alt='' title='" + _studentName[i] + "'>";
                            }
                            col5 += "</a>";
                        }
                        col5 += "</div>";
                        col5 += "</div>";
                        col5 += "<a class='left carousel-control' href='#" + curCarouselID + "' role='button' data-slide='prev'> <span class='material-icons'>keyboard_arrow_left</span> </a> ";
                        col5 += "<a class='right carousel-control' href='#" + curCarouselID + "' role='button' data-slide='next'> <span class='material-icons'>keyboard_arrow_right</span> </a>";
                        col5 += "</div>";

                        string col6 = "<a href='#!' class='sm_btn btn_org mrg_t-4'>" + item.AssignmentStatus.AssignmentStatus() + "</a>";
                        col6 += "<i class='fa fa-bar-chart pull-right txt_lgry pull-right icon_tbl' aria-hidden='true'></i>";

                        string col7 = "<a href='#!' class='btn_blue mml-0 min_w-80 text-center'>Review</a>";
                        string col8 = "<div class='mad-select pull-right' style='width: 110px;'>";
                        col8 += "<ul id='ddlAction' class='ddlAction'>";
                        col8 += "<li data-value='1 sel' class='selected'>Actions</li>";

                        string editAction = "onclick = archiveAssignment(" + item.AssignmentID + ",'" + item.AssignmentName.ToString().Replace(" ", "&nbsp;") + "'," + item.AssignmentStatus + ")";
                        string archiveAction = "onclick=cancelAssignment(" + item.AssignmentID + ",'" + item.AssignmentName.ToString().Replace(" ", "&nbsp;") + "'," + item.AssignmentStatus + ")";

                        col8 += "<li data-value='Edit' " + editAction + ">Archive</li>";
                        col8 += "</ul>";

                        List<string> common = new List<string>();
                        common.Add(col1);
                        common.Add(col2);
                        common.Add(col3);
                        common.Add(col4);
                        common.Add(col5);
                        common.Add(col6);
                        common.Add(col7);
                        common.Add(col8);
                        assignmentDetailList.Add(common);
                    }
                }

                jsonData.data = assignmentDetailList;
                return new JsonResult()
                {
                    Data = jsonData,
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                return Json(null);
            }
        }
    }
}
