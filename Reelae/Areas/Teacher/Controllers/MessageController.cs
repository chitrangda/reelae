﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Reelae.Areas.Teacher.Models;
using Reelae.Filters;
using Reelae.Models;
using Reelae.Repositories;
using Reelae.Utilities;
using ReelaeBusinessLayer.Utilities;
using ReelaeDataAccessLayer;

namespace Reelae.Areas.Teacher.Controllers
{
    [LoggingFilter]
    [checkSessionAlive]
    [Authorize(Roles = "Teacher")]
    public class MessageController : Controller
    {
        // GET: Teacher/Message
        ILogRepository _dbLogrepo;
        IUserRepository _userRepo;
        IInstitutionRepository _insRepo;
        IChatRepository _chatRepo;

        public MessageController(ILogRepository __dbLogrepo, IUserRepository __userRepo, IInstitutionRepository __insRepo, IChatRepository __chatRepo)
        {
            _dbLogrepo = __dbLogrepo;
            _userRepo = __userRepo;
            _insRepo = __insRepo;
            _chatRepo = __chatRepo;
        }

        public ActionResult Index()
        {
            int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            int _instituionid = Session["Institution"] != null ? Convert.ToInt32(Session["Institution"]) : 0;
            MessageViewModal messageViewModal = new MessageViewModal();
            messageViewModal._userinfo = _userRepo.Get(User.Identity.GetUserId());
            messageViewModal.ActiveStudents = _userRepo.GetInstituteActiveMembersByRoleId(_instituionid, (int?)RolesMaster.Student);
            messageViewModal.ActiveTeachers = _userRepo.GetInstituteActiveMembersByRoleId(_instituionid, (int?)RolesMaster.Teacher);
            messageViewModal.Institutions = _userRepo.GetInstituteActiveMembersByRoleId(_instituionid, (int?)RolesMaster.Admin);
            ViewBag.UesrsUnreadMessagesWithCount = _chatRepo.GetUserSingleChatUnreadMessagesCount(_userid).ToList();
            ViewBag.PageHeading = "Message";
            return View(messageViewModal);
        }
        public ActionResult GetIndividualChatBody(int? from)
        {
            int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            try
            {
                ViewBag.UnreadMessagesCount = _chatRepo.UserUnreadMessagesCount(from, _userid);
                var GetUserIndividualMessages = _chatRepo.GetIndividualChatMessages(_userid, from);
                return PartialView("_IndividualChatBody", GetUserIndividualMessages);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult UpdateUserUnreadMessagesToRead(int? from)
        {
            int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            try
            {
                _chatRepo.UpdateUserMessageReadStatus(from, _userid);
                return this.Json(new
                {
                    status = MasterConstants.SUCCESSTITLE,
                    msg = ""
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult UpdateIndividualMessageDeletedStatus(int? messageId, bool isSent, int? messageTo)
        {
            int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            try
            {
                if (isSent)
                    _chatRepo.UpdateUserMessageDeletedStatus(_userid, messageTo, messageId, isSent);
                else
                    _chatRepo.UpdateUserMessageDeletedStatus(messageTo, _userid, messageId, isSent);

                return Json(new
                {
                    status = MasterConstants.SUCCESSTITLE,
                    msg = MasterConstants.SAVESUCCESSFULLY
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult GetFilteredMembersList(string filterString, int? role)
        {
            int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            int _instituionid = Session["Institution"] != null ? Convert.ToInt32(Session["Institution"]) : 0;
            try
            {
                IEnumerable<re_GetChatMembers_sp_Result> members;
                if (filterString == "")
                    members = _userRepo.GetInstituteActiveMembersByRoleId(_instituionid, role);
                else
                    members = _userRepo.GetInstituteActiveMembersByRoleId(_instituionid, role).Where(n => n.Name.ToLower().StartsWith(filterString.ToLower()));
                ViewBag.UesrsUnreadMessagesWithCount = _chatRepo.GetUserSingleChatUnreadMessagesCount(_userid).ToList();
                //members = members.Where(n => n.Name.ToLower().StartsWith(filterString.ToLower()));
                return PartialView("_ChatMembersLists", members);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }

        }

        //Not used yet..
        public ActionResult UpdateIndividualMessageReadStatus(int? messageId, int? messageFrom)
        {
            int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            try
            {

                return Json(new
                {
                    status = MasterConstants.SUCCESSTITLE,
                    msg = MasterConstants.SAVESUCCESSFULLY
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult UpdateIndividualMessageSentStatus(int? messageId, int? messageFrom)
        {
            int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            try
            {

                return Json(new
                {
                    status = MasterConstants.SUCCESSTITLE,
                    msg = MasterConstants.SAVESUCCESSFULLY
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [ValidateInput(false)]
        public ActionResult ExportChat(string userId, string userName)
        {
            int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            string Html = GetChatExportData(_userid, Convert.ToInt32(userId), userName);
            var htmlContent = String.Format("<body>Hello world: {0}</body>",
        DateTime.Now);
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdfBytes = htmlToPdf.GeneratePdf(Html);
            return File(pdfBytes, "application/pdf", "ChatHistroy_" + DateTime.Now.ToShortDateString() + ".pdf");
            //using (MemoryStream stream = new System.IO.MemoryStream())
            //{
            //    StringReader sr = new StringReader(Html);
            //    Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
            //    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
            //    pdfDoc.Open();
            //    XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
            //    pdfDoc.Close();
            //    return File(stream.ToArray(), "application/pdf", "Grid.pdf");
            //}
        }

        private string GetFileType(string type)
        {
            string result = "";
            int lastIndexOfDot = type.LastIndexOf('.');
            string extension = type.Substring(lastIndexOfDot + 1, type.Length - lastIndexOfDot - 1).ToLower();

            if (extension == "pdf" || extension == "doc" || extension == "docx" || extension == "txt" || extension == "rtf" || extension == "zip" || extension == "rar")
            {
                result = "doc";
            }
            else if (extension == "png" || extension == "jpg" || extension == "jpeg" || extension == "gif" || extension == "tif")
            {
                result = "image";
            }
            else if (extension == "mp3" || extension == "obb" || extension == "wave")
            {
                result = "audio";
            }
            else if (extension == "mp4" || extension == "avi" || extension == "wmv" || extension == "flv")
            {
                result = "video";
            }
            else
            {
                result = "text";
            }

            return result;
        }

        private string GetFileExtension(string fileName)
        {
            int lastIndexOfDot = fileName.LastIndexOf('.');
            string extension = fileName.Substring(lastIndexOfDot + 1, fileName.Length - lastIndexOfDot - 1).ToLower();
            return extension;
        }

        private string GetChatExportData(int from, int to, string userName)
        {
            userName = (string.IsNullOrEmpty(userName)) ? "" : userName;
            StringBuilder sr = new StringBuilder();
            var UserID = Session["UserId"] as int?;
            var curType = "text";
            bool isSent = false;
            bool isUnreadLabelCreated = false;
            var GetUserIndividualMessages = _chatRepo.GetIndividualChatMessages(from, to);
            foreach (var item in GetUserIndividualMessages)
            {
                string addonClass = "";
                string downloadUrl = Url.Action("DownloadFile", "Chat", new { area = "" });
                string timeString = "";
                curType = (item.IsAttachment) ? GetFileType(item.MessageText).Trim() : "text";
                bool isUnreadMessages = item.IsRead == 0;
                if (item.FromUserID == UserID)
                {
                    addonClass = " bubble_chat-alt white";
                    isSent = true;
                    timeString = "Delivered";
                }
                else
                {
                    isSent = false;
                    timeString = "Sent " + item.MessageDate.Value.ToShortTimeString();
                }

                if (curType == "text")
                {
                    sr.AppendFormat("<div  class='{0}'><span>{1}</span><span class='time'>{2}</span></div>", "bubble_chat" + addonClass, HttpUtility.HtmlDecode(item.MessageText), timeString);
                }
                else if (curType == "image")
                {
                    downloadUrl += "?" + string.Format("fP={0}&fN={1}", item.AttachmentURL, item.MessageText);
                    sr.AppendFormat("<div  class='{0}'><a class='fileView' target='_blank' href='{1}'><img src='{2}' alt='...' class='img - responsive' /><span class='time'>{3}</span></a></div>", "bubble_chat" + addonClass, Url.Content(downloadUrl), Request.Url.Scheme + "://" + Request.Url.Authority + Url.Content(item.AttachmentURL), timeString);
                }
                else if (curType == "video")
                {
                    downloadUrl += "?" + string.Format("fP={0}&fN={1}", item.AttachmentURL, item.MessageText);
                    sr.AppendFormat("<div  class='{0}'><a class='fileView' target='_blank' href='{1}'><span>{2}</span><span class='time'>{3}</span></a></div>", "bubble_chat" + addonClass, Url.Content(downloadUrl), item.MessageText, timeString);
                }
                else if (curType == "audio")
                {
                    downloadUrl += "?" + string.Format("fP={0}&fN={1}", item.AttachmentURL, item.MessageText);
                    sr.AppendFormat("<div  class='{0}'><a class='fileView' target='_blank' href='{1}'><span>{2}</span><span class='time'>{3}</span></a></div>", "bubble_chat" + addonClass, "#", item.MessageText, timeString);
                }
                else if (curType == "doc")
                {

                    downloadUrl += "?" + string.Format("fP={0}&fN={1}", item.AttachmentURL, item.MessageText);
                    sr.AppendFormat("<div  class='{0}'><a class='fileView' target='_blank' href='{1}'><span>{2}</span><span class='time'>{3}</span></a></div>", "bubble_chat" + addonClass, "#", item.MessageText, timeString);

                }

            }
            return string.Format("<!DOCTYPE html><head><meta http-equiv='content-type' content='text/html; charset=utf-8' /><title>Chat History</title><link href='{0}' rel='stylesheet'><link href='{1}' rel='stylesheet'><link href='{2}' rel='stylesheet'></head><body><div style='text-align:center;'><b>" + userName + "'s Chat History</b></div><br /><div>{3}</div></body></html>", Request.Url.Scheme + "://" + Request.Url.Authority + Url.Content("~/Content/css/style-dash.css"), Request.Url.Scheme + "://" + Request.Url.Authority + Url.Content("~/Content/css/media.css"), Request.Url.Scheme + "://" + Request.Url.Authority + Url.Content("~/Content/css/bootstrap.css"), sr.ToString());
        }

        private string GetChatExportDataOld(int from, int to)
        {
            StringBuilder sr = new StringBuilder();
            var UserID = Session["UserId"] as int?;
            var curType = "text";
            bool isSent = false;
            bool isUnreadLabelCreated = false;
            var GetUserIndividualMessages = _chatRepo.GetIndividualChatMessages(from, to);

            foreach (var item in GetUserIndividualMessages)
            {
                string addonClass = "";
                string downloadUrl = Url.Action("DownloadFile", "Chat", new { area = "" });
                string timeString = "";
                curType = (item.IsAttachment) ? GetFileType(item.MessageText).Trim() : "text";
                bool isUnreadMessages = item.IsRead == 0;

                if (item.FromUserID == UserID)
                {
                    addonClass = " bubble_chat-alt white";
                    isSent = true;
                    timeString = "Delivered";
                }
                else
                {
                    isSent = false;
                    timeString = "Sent " + item.MessageDate.Value.ToShortTimeString();
                }

                if (curType == "text")
                {
                    sr.AppendFormat("<div>{0}<br />{1}</div>", HttpUtility.HtmlDecode(item.MessageText), timeString);
                }
                else if (curType == "image")
                {
                    downloadUrl += "?" + string.Format("fP={0}&fN={1}", item.AttachmentURL, item.MessageText);
                    sr.AppendFormat("<div>{0}</div>", Url.Content(item.AttachmentURL));
                }
                else if (curType == "video")
                {
                    downloadUrl += "?" + string.Format("fP={0}&fN={1}", item.AttachmentURL, item.MessageText);
                    sr.AppendFormat("<div>{0}</div>", item.MessageText);
                }
                else if (curType == "text")
                {
                    downloadUrl += "?" + string.Format("fP={0}&fN={1}", item.AttachmentURL, item.MessageText);
                    sr.AppendFormat("<div>{0}</div>", item.MessageText);
                }
                else if (curType == "doc")
                {

                    downloadUrl += "?" + string.Format("fP={0}&fN={1}", item.AttachmentURL, item.MessageText);
                    sr.AppendFormat("<div>{0}</div>", item.MessageText);
                }

            }
            return string.Format("<!DOCTYPE html><head><title>Chat History</title></head><body><div>{0}</div></body></html>", sr.ToString());
        }
    }
}