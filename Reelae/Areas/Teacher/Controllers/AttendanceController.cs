﻿using Reelae.Areas.Teacher.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Reelae.Utilities;
using ReelaeDataAccessLayer;
using Reelae.Repositories;
using Reelae.Filters;
using Microsoft.AspNet.Identity;
using ReelaeBusinessLayer.Utilities;

namespace Reelae.Areas.Teacher.Controllers
{
    [LoggingFilter]
    [checkSessionAlive]
    [Authorize(Roles = "Teacher")]
    public class AttendanceController : Controller
    {
        IUserRepository _userRepo;
        ISubjectTeacherRepository _subjectTeachRepo;
        ISubjectStudentRepository _subjectStudentRepo;
        IAttendanceRepository _attenRepo;
        ILogRepository _dbLogrepo;

        public AttendanceController(IUserRepository __userRepo, ISubjectTeacherRepository __subjectTeachRepo, ISubjectStudentRepository __subjectStudentRepo, ILogRepository __dbLogrepo, IAttendanceRepository __attenRepo)
        {
            _userRepo = __userRepo;
            _subjectTeachRepo = __subjectTeachRepo;
            _dbLogrepo = __dbLogrepo;
            _subjectStudentRepo = __subjectStudentRepo;
            _attenRepo = __attenRepo;
        }
        public ActionResult Index()
        {
            int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            int _instituionid = Session["Institution"] != null ? Convert.ToInt32(Session["Institution"]) : 0;
            AttedanceViewModel model = new AttedanceViewModel();
            model._userinfo = _userRepo.Get(User.Identity.GetUserId());
            model._Subjects = _subjectTeachRepo.GetLatestSubject(_userid, _instituionid);
            ViewBag.PageHeading = "Attendance";
            return View(model);
        }

        [HttpPost]
        public ActionResult getStudentsView(int? id,DateTime? _date)
        {
            int? _teacherId = Session["UserId"] as int?;
            if(_date==null)
            {
                _date = DateTime.Now.Date;
            }
            var model = _attenRepo.get(id, _teacherId, _date);
            try
            {
               
                return PartialView("_subjectStudentsAttendance", model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult MarkAttendance(List<re_getAttendance_sp_Result> _students, string _date, string formAction,int? _subjectId)
        {
            try
            {
                int? _teacherId = Session["UserId"] as int?;
                DateTime? _attDate = Convert.ToDateTime(_date);
                if (formAction == "d")
                {
                    _attenRepo.remove(_students);
                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = MasterConstants.REMOVE_SUCCESS
                    });

                }
                else
                {
                    _attenRepo.save(_students,_attDate, _subjectId, _teacherId, Session["UserName"].ToString());
                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = MasterConstants.SAVESUCCESSFULLY
                    });
                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}