﻿using Reelae.Areas.Teacher.Models;
using Reelae.Repositories;
using ReelaeDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Reelae.Filters;
using Reelae.Utilities;
using ReelaeBusinessLayer.Utilities;
using System.Net;

namespace Reelae.Areas.Teacher.Controllers
{
    [checkSessionAlive]
    [Authorize(Roles = "Teacher")]
    public class GroupMembersController : Controller
    {

        ISubjectTeacherRepository _subjectTeacher;
        IUserRepository _userRepo;
        ISubjectRepository<Subject, int> _subjectRepo;
        ILogRepository _dbLogrepo;
        IGroupRepository _groupRepo;
        ISubjectStudentRepository _subjectstdRepo;
        public GroupMembersController(ISubjectTeacherRepository __subjectTeacher, IUserRepository __userRepo, ISubjectRepository<Subject, int> __subjectRepo, ILogRepository __dbLogrepo, IGroupRepository __groupRepo, ISubjectStudentRepository __subjectstdRepo)
        {
            _subjectTeacher = __subjectTeacher;
            _userRepo = __userRepo;
            _subjectRepo = __subjectRepo;
            _dbLogrepo = __dbLogrepo;
            _groupRepo = __groupRepo;
            _subjectRepo = __subjectRepo;
            _subjectstdRepo = __subjectstdRepo;

        }
        // GET: Teacher/GroupMembers
        public ActionResult Index(int? id, string cur_item)
        {
            GroupsMemeberViewModel model = new GroupsMemeberViewModel();
            int _UserID = Convert.ToInt32(Session["UserId"]);
            int _instituionid = Convert.ToInt32(Session["Institution"]);
            model.Subjects = _subjectTeacher.Get(_UserID, _instituionid);

            model.LatestSubjects = _subjectTeacher.GetLatestSubject(_UserID, _instituionid);//get latest subject
            model.ArchiveSubjects = _subjectTeacher.GetArchiveSubject(_UserID, _instituionid);//get archive subject

            model._userinfo = _userRepo.Get(User.Identity.GetUserId());
            model.Groups = _groupRepo.getGroupWithMemberCountByTeacher(_UserID);
            ViewBag.PageHeading = "Group Members";
            return View(model);
        }


        #region Subject functionailty

        public ActionResult getSubjectStudents(int? id)

        {
            try
            {
                SubjectMemberViewModel model = new SubjectMemberViewModel();
                int? _instituionid = Session["Institution"] as int?;
                int? teacherID = Session["UserId"] as int?;
                model._subjectStudents = _subjectRepo.GetSubjectMembers(id).Where(r => r.usertype == "S").ToList();
                model._subjectGroups = _groupRepo.GetSubjectGroups(id, teacherID);
                ViewBag.SubjectId = id;
                return PartialView("_SubjectStudents", model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message }; new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);

                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.DUPLICATESUBJECTNAME
                });
            }
        }

        public ActionResult getsearchStudent(int? id, string Email)
        {
            try
            {
                SubjectMemberViewModel model = new SubjectMemberViewModel();
                int? _instituionid = Session["Institution"] as int?;
                int? teacherID = Session["UserId"] as int?;
                ViewBag.SubjectId = id;


                if (Email != "")
                {
                    if (Email.StartsWith("G_"))
                    {
                        int _groupId = Convert.ToInt32(Email.Substring(2));
                        model._subjectStudents = new List<re_select_studenttecher_subject_sp_Result>();
                        model._subjectGroups = _groupRepo.GetSubjectGroups(id, teacherID).Where(g => g.GroupID == _groupId);
                    }
                    else
                    {
                        model._subjectGroups = new List<Group>();
                        model._subjectStudents = _subjectRepo.GetSubjectSearchedMembers(id, Email).ToList();
                    }
                    return PartialView("_StudentList", model);
                }
                else
                {
                    //var _subjectstudentsList = _subjectRepo.GetSubjectMembers(id).Where(r => r.usertype == "S").ToList();
                    model._subjectStudents = _subjectRepo.GetSubjectMembers(id).Where(r => r.usertype == "S").ToList();
                    model._subjectGroups = _groupRepo.GetSubjectGroups(id, teacherID);
                    return PartialView("_StudentList", model);

                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message }; new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.DUPLICATESUBJECTNAME
                });
            }
        }

        public ActionResult getStudentProfile(int id)
        {
            try
            {
                int? _insID = Session["Institution"] as int?;
                var model = _userRepo.Get(id);
                return PartialView("_StudentProfile", model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult getAddSubjectView(int? id)
        {
            try
            {
                Subject model;
                if (id == null || id == 0)
                {
                    model = new Subject();
                }
                else
                {
                    model = _subjectRepo.Get(id);
                }
                return PartialView("_addSubject", model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AddSubject(Subject model)
        {
            try
            {
                int? InstitutionID = Convert.ToInt32(Session["Institution"].ToString());
                if (ModelState.IsValidField("SubjectName") && InstitutionID.HasValue)
                {
                    if (!_subjectRepo.IsSubjectExistsByInstitution(InstitutionID.Value, model.SubjectName, model.SubjectID))
                    {
                        #region SetDateTime
                        ////set date format from string
                        string SDformValue = Request.Form.Get("hdn_SubjectStartDate");
                        string EDformValue = Request.Form.Get("hdn_SubjectEndDate");
                        if (!string.IsNullOrEmpty(SDformValue))
                        {
                            //MM/DD/YYYY
                            string[] dateParts = SDformValue.Split('/');
                            model.StartDate = new DateTime(Int32.Parse(dateParts[2].Trim()), Int32.Parse(dateParts[0].Trim()), Int32.Parse(dateParts[1].Trim()));
                        }

                        if (!string.IsNullOrEmpty(EDformValue))
                        {
                            //MM/DD/YYYY
                            string[] dateParts = EDformValue.Split('/');
                            model.EndDate = new DateTime(Int32.Parse(dateParts[2].Trim()), Int32.Parse(dateParts[0].Trim()), Int32.Parse(dateParts[1].Trim()));
                        }


                        #endregion

                        int newSubjectId = _subjectRepo.Add(model, InstitutionID.Value.ToString(), Session["UserName"].ToString());
                        SubjectTeacher _subjectTeacherNew = new SubjectTeacher() { SubjectID = newSubjectId, TeacherID = Convert.ToInt32(Session["UserId"]) };
                        _subjectTeacher.Add(_subjectTeacherNew, Session["UserName"].ToString());
                        if (model.SubjectID == 0)
                        {
                            return this.Json(new
                            {
                                EnableSuccess = true,
                                SuccessTitle = MasterConstants.SUCCESSTITLE,
                                SuccessMsg = MasterConstants.SUBJECTADDEDSUCCESS
                            });
                        }
                        else
                        {
                            return this.Json(new
                            {
                                EnableSuccess = true,
                                SuccessTitle = MasterConstants.SUCCESSTITLE,
                                SuccessMsg = MasterConstants.SAVESUCCESSFULLY
                            });
                        }
                    }
                    else
                    {
                        return this.Json(new
                        {
                            EnableError = true,
                            ErrorTitle = MasterConstants.ERRORTITLE,
                            ErrorMsg = MasterConstants.DUPLICATESUBJECTNAME
                        });
                    }
                }
                else
                {
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = error
                    });

                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);

            }

        }

        public ActionResult deleteSubject(int? id)
        {
            try
            {
                _subjectRepo.Remove(id);

                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.SUBJECT_Remove_SUCESS
                });
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message }; new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);

                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                });
            }
        }
        public ActionResult getAddStudentView(int subjectId)
        {
            try
            {
                int _institution = Session["Institution"] != null ? Convert.ToInt32(Session["Institution"]) : 0;
                List<AssignStudentViewModel> _members = new List<AssignStudentViewModel>();
                var result = _userRepo.GetAll(_institution, "s", subjectId);
                foreach (var _user in result)
                {
                    _members.Add(new AssignStudentViewModel() { isSelect = false, userId = _user.UserID, Name = _user.Name + " " + _user.Surname, Email = _user.Email, userPicUrl = _user.UserPicUrl, type = "s" });
                }

                return PartialView("_AddStudent", _members);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }


        }

        public ActionResult getsearchAddStudent(int id, string Email)
        {
            try
            {
                int _institution = Session["Institution"] != null ? Convert.ToInt32(Session["Institution"]) : 0;
                List<AssignStudentViewModel> _members = new List<AssignStudentViewModel>();
                var result = _userRepo.GetAll(_institution, "s", id).ToList();
                if (Email != "")
                {
                    result = result.Where(r => r.Email == Email).ToList();
                }
                foreach (var _user in result)
                {
                    _members.Add(new AssignStudentViewModel() { isSelect = false, userId = _user.UserID, Name = _user.Name + " " + _user.Surname, Email = _user.Email, userPicUrl = _user.UserPicUrl, type = "s" });
                }

                return PartialView("_addStudentList", _members);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AddStudents(List<AssignStudentViewModel> model, int subjectId, string [] selectedItem)
        {         
            try
            {
                if (selectedItem.Length > 0)
                {
                    var checkCount = selectedItem[0].ToString();
                    if (!string.IsNullOrEmpty(checkCount.ToString()))
                    {
                        if (checkCount.ToString().Contains(","))
                        {
                            var items = selectedItem[0].ToString().Split(',');
                            foreach (var item in items)
                            {
                                _subjectstdRepo.Add(new SubjectStudent() { SubjectID = subjectId, StudentID = int.Parse(item) }, Session["UserName"].ToString());
                            }
                        }
                        else
                        {
                            _subjectstdRepo.Add(new SubjectStudent() { SubjectID = subjectId, StudentID = int.Parse(checkCount) }, Session["UserName"].ToString());
                        }
                    }
                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = "Assigned member successfully."
                    });
                }
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = "Assigned member successfully."
                });
                //if (ModelState.IsValid)
                //{
                //    if (model != null)
                //    {
                //        foreach (var _model in model)
                //        {
                //            if (_model.isSelect)
                //            {
                //                _subjectstdRepo.Add(new SubjectStudent() { SubjectID = subjectId, StudentID = _model.userId }, Session["UserName"].ToString());
                //            }
                //        }
                //    }
                //    else
                //    {
                //        if (selectedItem.Length > 0)
                //        {
                //            foreach (var item in selectedItem)
                //            {
                //                _subjectstdRepo.Add(new SubjectStudent() { SubjectID = subjectId, StudentID = int.Parse(item) }, Session["UserName"].ToString());
                //            }
                //        }
                //    }

                //    return this.Json(new
                //    {
                //        EnableSuccess = true,
                //        SuccessTitle = MasterConstants.SUCCESSTITLE,
                //        SuccessMsg = "Assigned member successfully."
                //    });
                //}
                //else
                //{
                //    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                //    return this.Json(new
                //    {
                //        EnableError = true,
                //        ErrorTitle = MasterConstants.ERRORTITLE,
                //        ErrorMsg = error
                //    });

                //}
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.PLEASETRYAGAINLATER
                });
            }

        }

        public ActionResult getAddGroupView(int id)
        {
            int teacherID = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            try
            {
                var groups = _groupRepo.getTeacherGroup(teacherID);
                ViewBag.SubjectId = id;
                return PartialView("_addGroup", groups);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = teacherID.ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult AddGroup_Subject(int subjectId, string groupIds)
        {
            int? teacherID = Session["UserId"] as int?;
            try
            {

                _groupRepo.UpdateGroupSubject(groupIds, subjectId);
                return this.Json(new
                {
                    status = MasterConstants.SUCCESSTITLE,
                    msg = MasterConstants.SAVESUCCESSFULLY
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = teacherID.Value.ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult getNewGroupView(int id)
        {
            try
            {
                ViewBag.SubjectId = id;
                Group model = new Group();
                return PartialView("_newGroup_subject", model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult CreateGroupSubject(Group group, int SubjectId)
        {
            int? teacherID = Session["UserId"] as int?;
            try
            {
                if (ModelState.IsValid)
                {
                    if (_groupRepo.IsGroupNameAlreadyExistsByTeacher(teacherID.Value, group.GroupName, group.GroupID))
                    {
                        return this.Json(new
                        {
                            EnableError = true,
                            ErrorTitle = MasterConstants.ERRORTITLE,
                            ErrorMsg = MasterConstants.ALREADYEXISTS
                        });
                    }
                    else
                    {
                        //changes_07092017
                        group.GroupIntialName = Utilities.CommonUtility.GetGroupInitialName(group.GroupName);
                        int groupID = _groupRepo.AddGroupByTeacher(group, teacherID.Value);

                        return this.Json(new
                        {
                            EnableSuccess = true,
                            SuccessTitle = MasterConstants.SUCCESSTITLE,
                            SuccessMsg = MasterConstants.SUCCESSTITLE
                        });

                    }
                }
                else
                {
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        status = MasterConstants.ERRORTITLE,
                        msg = error
                    });
                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion


        #region Group Functionailty

        public ActionResult CreateEditGroupView(int id, string view, string subjectid)
        {
            int? teacherID = Session["UserId"] as int?;
            int _instituionid = Convert.ToInt32(Session["Institution"]);
            try
            {
                Group curGroup = new Group();
                if (id > 0)
                {
                    curGroup = _userRepo.GetGroupByTeacher(teacherID.Value, id);
                    if (curGroup == null)
                    {
                        Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        return this.Json(new
                        {
                            status = MasterConstants.FAILURETITLE,
                            msg = "Group " + MasterConstants.NOTFOUND,
                        });
                    }
                }
                ViewBag.SubjectId = subjectid;
                ViewBag.Subjects = _subjectTeacher.GetLatestSubject(teacherID.Value, _instituionid);
                ViewBag.From = view;
                return PartialView("_createEditGroup", curGroup);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = teacherID.Value.ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult CreateGroup([Bind(Include = "GroupID,GroupName,GroupIntialName,Description,SubjectID")]Group groupModal)
        {
            int? teacherID = Session["UserId"] as int?;
            try
            {
                if (ModelState.IsValid)
                {

                    if (_groupRepo.IsGroupNameAlreadyExistsByTeacher(teacherID.Value, groupModal.GroupName, groupModal.GroupID))
                    {
                        return this.Json(new
                        {
                            status = MasterConstants.FAILURETITLE,
                            msg = MasterConstants.ALREADYEXISTS
                        });
                    }
                    else
                    {
                        //changes_07092017
                        groupModal.GroupIntialName = Utilities.CommonUtility.GetGroupInitialName(groupModal.GroupName);
                        int groupID = _groupRepo.AddGroupByTeacher(groupModal, teacherID.Value);
                        return this.Json(new
                        {
                            status = MasterConstants.SUCCESSTITLE,
                            msg = MasterConstants.SAVESUCCESSFULLY,
                            Data = new { id = groupID, name = groupModal.GroupName, iName = groupModal.GroupIntialName }
                        });
                    }
                }
                else
                {
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        status = MasterConstants.ERRORTITLE,
                        msg = error
                    });
                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = teacherID.Value.ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult getGroupStudents(int? id)
        {
            int? teacherID = Session["UserId"] as int?;
            try
            {
                var _groupStudents = _groupRepo.getGroupMembers(id);
                var curGroup = _groupRepo.Get(id.Value);
                ViewBag.GroupID = curGroup.GroupID;
                ViewBag.SubjectID = curGroup.SubjectID;
                return PartialView("_GroupStudents", _groupStudents);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = teacherID.Value.ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddToGroupView(string studentId, string subjectId)
        {
            int? teacherID = Session["UserId"] as int?;
            try
            {

                IEnumerable<Group> associatedGroups = _groupRepo.GetStudentGroupsByTeacher(Convert.ToInt32(studentId), teacherID.Value);
                IEnumerable<Group> Groups = _groupRepo.GetSubjectGroups(Convert.ToInt32(subjectId), teacherID);
                Groups = Groups.Where(n => !associatedGroups.Any(y => y.GroupID == n.GroupID));
                ViewBag.AssignedGroups = associatedGroups;
                ViewBag.StudentID = studentId;
                return PartialView("_AddToGroupView", Groups);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = teacherID.Value.ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult AddToGroupMembers(string stuid, string groupIds)
        {
            int? teacherID = Session["UserId"] as int?;
            try
            {
                ViewBag.AssignedGroups = _groupRepo.GetStudentGroupsByTeacher(Convert.ToInt32(stuid), teacherID.Value);
                IEnumerable<string> curGroupIds = new List<string>();
                if (!string.IsNullOrEmpty(groupIds))
                {
                    curGroupIds = groupIds.Trim().Split(',');
                }
                _groupRepo.StudentAddToGroup(Convert.ToInt32(stuid), teacherID, curGroupIds);
                return this.Json(new
                {
                    status = MasterConstants.SUCCESSTITLE,
                    msg = MasterConstants.SAVESUCCESSFULLY
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = teacherID.Value.ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult MemberRemoveFromGroup(string stuId, string groupId)
        {
            int? teacherID = Session["UserId"] as int?;
            try
            {
                ViewBag.AssignedGroups = _groupRepo.GetStudentGroupsByTeacher(Convert.ToInt32(stuId), teacherID.Value);
                _groupRepo.StudentRemoveFromGroup(Convert.ToInt32(stuId), teacherID, Convert.ToInt32(groupId));
                return this.Json(new
                {
                    status = MasterConstants.SUCCESSTITLE,
                    msg = MasterConstants.SAVESUCCESSFULLY
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = teacherID.Value.ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult AddMembersToGroup(string studentIds, string groupId)
        {

            int? teacherID = Session["UserId"] as int?;
            try
            {
                IEnumerable<string> curStudentIds = new List<string>();
                if (!string.IsNullOrEmpty(studentIds))
                {
                    curStudentIds = studentIds.Trim().Split(',');
                }
                _groupRepo.AddStudentsToGroup(Convert.ToInt32(groupId), teacherID, curStudentIds);
                return this.Json(new
                {
                    status = MasterConstants.SUCCESSTITLE,
                    msg = MasterConstants.SAVESUCCESSFULLY
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = teacherID.Value.ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult getStudentsAddGroupView(int? groupid, int? subjectid)
        {
            int? _instituionid = Session["Institution"] as int?;
            //var _subjectstudentsList = _subjectRepo.GetSubjectMembers(subjectid).Where(r => r.usertype == "S").ToList();
            //var getExistingStudentIds = _groupRepo.getGroupMembers(groupid).ToList();
            //var actualMembersToAdd = _subjectstudentsList.Where(n => !getExistingStudentIds.Any(y => y.UserID == n.UserID)).ToList();
            var model = _groupRepo.getStudentsAddtoGroup(groupid, _instituionid);
            ViewBag.GroupID = groupid.Value;
            ViewBag.SubjectId = subjectid;
            return PartialView("_AddMembersToGroupView", model);
        }
        public ActionResult getsearchAddGroupStudent(int? id, string Email)
        {
            try
            {
                int? _instituionid = Session["Institution"] as int?;
                ViewBag.GroupID = id;
                var model = _groupRepo.getStudentsAddtoGroup(id, _instituionid);
                if (Email != "")
                {
                    model = model.Where(r => r.Email == Email).ToList();
                }
                return PartialView("_addGroupStudentList", model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message }; new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.DUPLICATESUBJECTNAME
                });
            }
        }


        public ActionResult getsearchGroupStudent(int? id, string Email)
        {
            try
            {
                int? _instituionid = Session["Institution"] as int?;
                ViewBag.GroupID = id;
                if (Email != "")
                {
                    var _groupStudents = _groupRepo.GetGroupSearchedMember(id, Email);
                    return PartialView("_groupStudentList", _groupStudents);
                }
                else
                {
                    var _groupStudents = _groupRepo.getGroupMembers(id);
                    return PartialView("_groupStudentList", _groupStudents);

                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message }; new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.DUPLICATESUBJECTNAME
                });
            }
        }
        #endregion

        #region [Remove Members & Group]

        public ActionResult RemoveStudentFromSubject(int studentId, int subjectId)
        {
            try
            {
                _subjectstdRepo.RemoveStudentFromSubject(subjectId, studentId);
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.SUCCESSTITLE
                });
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RemoveGroupFromSubject(int groupId, int subjectId)
        {
            try
            {
                _groupRepo.RemoveGroupFromSubject(groupId, subjectId);
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.SUCCESSTITLE
                });
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult deleteGroup(int? id)
        {
            try
            {
                _groupRepo.Remove(id);

                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.GROUP_Remove_SUCESS
                });
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message }; new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);

                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                });
            }
        }
        #endregion
    }

}

