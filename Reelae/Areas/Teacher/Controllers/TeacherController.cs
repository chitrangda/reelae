﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Reelae.Areas.Teacher.Models;
using Reelae.Filters;
using Reelae.Repositories;
using ReelaeBusinessLayer.Utilities;
using ReelaeDataAccessLayer;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Reelae.Utilities;

namespace Reelae.Areas.Teacher.Controllers
{
    [LoggingFilter]
    [checkSessionAlive]
    [Authorize(Roles = "Teacher")]
    public class TeacherController : Controller
    {
        ILogRepository _dbLogrepo;
        IUserRepository _userRepo;
        IInstitutionRepository _insRepo;
        IGroupRepository _groupRepo;
        TeacherDashboardRepository _dashboardRepo;
        ISubjectTeacherRepository _subjectTeacher;
        IAttendanceRepository _attenRepo;
        IAssignmentRepository _assignRepo;

        public TeacherController(ILogRepository __dbLogrepo, IUserRepository __userRepo, IInstitutionRepository __insRepo, IGroupRepository __groupRepo, TeacherDashboardRepository __dashboardRepo, ISubjectTeacherRepository __subjectTeacher, IAttendanceRepository __attenRepo, IAssignmentRepository __assignRepo)
        {
            _dbLogrepo = __dbLogrepo;
            _userRepo = __userRepo;
            _insRepo = __insRepo;
            _groupRepo = __groupRepo;
            _dashboardRepo = __dashboardRepo;
            _subjectTeacher = __subjectTeacher;
            _attenRepo = __attenRepo;
            _assignRepo = __assignRepo;
        }
        /// <summary>
        /// Method for Load the  Teacher Dashboard .
        /// </summary>
        /// <returns> Load the Teacher DashBoard view.</returns>
        public ActionResult Dashboard()
        {
            TeacherDashboardViewModel model = new TeacherDashboardViewModel();
            int? _UserID = Session["UserId"] as int?;
            try
            {
                int _instituteId = Session["Institution"] == null ? 0 : Convert.ToInt32(Session["Institution"]);
                int _userid = Session["UserId"] == null ? 0 : Convert.ToInt32(Session["UserId"]);
                Session["Theme"] = _insRepo.Get(_instituteId).Theme.ThemeName;
                model._userinfo = _userRepo.Get(User.Identity.GetUserId());

                model.GroupList = _groupRepo.getGroupWithMemberCountByTeacher(_userid);//_groupRepo.GetTeacherGroups(_userid);

                model.SubjectList = _subjectTeacher.Get(_userid, _instituteId).Where(r => r.EndDate.Value.Date >= DateTime.Now.Date).ToList();
                var _counts = _dashboardRepo.GetDashbordCount(_instituteId, _userid, null, null).FirstOrDefault();
                model.AssignmentSubmissionsCount = _counts.Submissioncount != null ? Convert.ToInt32(_counts.Submissioncount) : 0;
                model.AssignmentCount = _counts.AssignmentCount != null ? Convert.ToInt32(_counts.AssignmentCount) : 0;
                model.MasterKeywordCount = _counts.KeywordCount != null ? Convert.ToInt32(_counts.KeywordCount) : 0;
                model.StudentsCount = _counts.Studentcount != null ? Convert.ToInt32(_counts.Studentcount) : 0;

                ViewBag.PageHeading = "Main";
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
            }
            return View(model);
        }
        public ActionResult getAttedenceRate(int id, DateTime? _startDate, DateTime? _endDate)
        {
            try
            {
                int? _teacherId = Session["UserId"] as int?;
                var model = _attenRepo.getAll(id, _teacherId, _startDate, _endDate);
                return PartialView("_AttendanceRate", model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetData()
        {
            List<TeacherDashboardViewModel> dataList = new List<TeacherDashboardViewModel>();
            try
            {
                return Json(dataList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);

            }
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getDashboardCounts(DateTime? _startDate, DateTime? _endDate)
        {
            TeacherDashboardViewModel model = new TeacherDashboardViewModel();
            try
            {
                int _instituteId = Session["Institution"] == null ? 0 : Convert.ToInt32(Session["Institution"]);
                int _userid = Session["UserId"] == null ? 0 : Convert.ToInt32(Session["UserId"]);
                var _counts = _dashboardRepo.GetDashbordCount(_instituteId, _userid, _startDate, _endDate).FirstOrDefault();
                model.AssignmentSubmissionsCount = _counts.Submissioncount != null ? Convert.ToInt32(_counts.Submissioncount) : 0;
                model.AssignmentCount = _counts.AssignmentCount != null ? Convert.ToInt32(_counts.AssignmentCount) : 0;
                model.MasterKeywordCount = _counts.KeywordCount != null ? Convert.ToInt32(_counts.KeywordCount) : 0;
                model.StudentsCount = _counts.Studentcount != null ? Convert.ToInt32(_counts.Studentcount) : 0;
                return PartialView("_dashboardCounts", model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetAssignmentListDDL(int? groupId)
        {
            try
            {
                int? _teacherId = Session["UserId"] as int?;
                var model = _assignRepo.GetTeacherAssignmentList(_teacherId, groupId);
                return PartialView("_AssignmentListPartial", model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAnalyticsAttendanceGraph(int? groupId)
        {
            try
            {
                int? _teacherId = Session["UserId"] as int?;
                var model = _assignRepo.GetTeacherAssignmentList(_teacherId, groupId);
                return PartialView("_AttendanceAnalyticsPartial", model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }
        //Analytics Attendance
        public JsonResult GetAnalyticsAttendanceData(int subjectId, DateTime? _startDate, DateTime? _endDate,string type)
        {
            int? _teacherId = Session["UserId"] as int?;
            var model = _attenRepo.getAll(subjectId, _teacherId, _startDate, _endDate);
            try
            {              
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
