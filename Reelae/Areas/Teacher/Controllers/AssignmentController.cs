﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Reelae.Areas.Teacher.Models;
using Reelae.Filters;
using Reelae.Repositories;
using Reelae.Utilities;
using ReelaeBusinessLayer.Utilities;
using ReelaeDataAccessLayer;
using Microsoft.AspNet.Identity;

namespace Reelae.Areas.Teacher.Controllers
{
    [LoggingFilter]
    [checkSessionAlive]
    [Authorize(Roles = "Teacher")]
    public class AssignmentController : Controller
    {
        // GET: Teacher/Assignment

        ILogRepository _dbLogrepo;
        IUserRepository _userRepo;
        IInstitutionRepository _insRepo;
        IGroupRepository _groupRepo;
        IAssignmentRepository _assignRepo;
        IAssignmentKeywordsRepository _assignKeywordRepo;
        IKeywordRepository _keywordRepo;



        public AssignmentController(ILogRepository __dbLogrepo, IUserRepository __userRepo, IInstitutionRepository __insRepo, IGroupRepository __groupRepo, IAssignmentRepository __assignRepo, IAssignmentKeywordsRepository __assignKeywordRepo, IKeywordRepository __keywordRepo)
        {
            _dbLogrepo = __dbLogrepo;
            _userRepo = __userRepo;
            _insRepo = __insRepo;
            _groupRepo = __groupRepo;
            _assignRepo = __assignRepo;
            _assignKeywordRepo = __assignKeywordRepo;
            _keywordRepo = __keywordRepo;

        }


        public ActionResult Index()
        {
            return View();
        }

        #region AssignmentsSection

        /// <summary>
        /// Method for Get the View Of Creation of Assignment View.
        /// </summary>
        /// <returns> Returns view For create assignment view.</returns>
        public ActionResult GetCreateAssignmentView(int? id)
        {
            int? _UserID = Session["UserId"] as int?;
            try
            {
                AssignmentCreationViewModel objResult = new AssignmentCreationViewModel();
                if (Request.IsAjaxRequest())
                {
                    int? _InstitutionID = Session["Institution"] as int?;
                    ViewBag.Groups = GetGroupDropDownData(_UserID);
                    ViewBag.Keywords = GetKeyWordDropDownData(_InstitutionID);
                    ViewBag.AsignmentTypes = GetAssignmentTypesDropDownData();

                    //edit case 
                    if (id != null && id.HasValue)
                    {
                        var result = _assignRepo.Get(id.Value);
                        string existingKeyWords = string.Empty;
                        var resultExistingKeyWords = _assignKeywordRepo.GetAssignmentKeywordsByAssignmentID(id.Value).Select(x => new { name = x.KeyWord.KeywordName, value = x.KeywordID });
                        ViewBag.existingKeywordsData = resultExistingKeyWords;
                        existingKeyWords = string.Join(",", resultExistingKeyWords);
                        objResult = new AssignmentCreationViewModel()
                        {
                            AssignmentID = result.AssignmentID,
                            AssignmentType = 1,
                            AssignmentName = result.AssignmentName,
                            AssignmentDescription = result.Description,
                            StartDate = result.AssignmentDate.ToString(),
                            EndDate = result.DueDate.ToString(),
                            AssigneeID = (result.GroupID.HasValue) ? result.GroupID.Value : 0,
                            Keywords = existingKeyWords,
                            newKeyword = "",
                            LeaderID = result.LeaderID.Value,
                            delKeywords = "",
                            ExpectedWordCountGoal = result.WordsGoal ?? 0
                        };
                    }
                }
                return PartialView("_createAssignment", objResult);
            }
            catch (Exception ex)
            {
                //exception db logging
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = _UserID.ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// Method for Create the Assignment.
        /// </summary>
        /// <param name="model">Passing model for Assignment Creation view, passed by reference,
        /// <returns> Returns Latest Assignment Submitted By Student.</returns>
        public ActionResult CreateAssignment(AssignmentCreationViewModel model)
        {
            int? _UserID = Session["UserId"] as int?;
            int? _InstitutionID = Session["Institution"] as int?;
            string user = Session["UserName"].ToString();
            try
            {
                if (Request.IsAjaxRequest() && ModelState.IsValid)
                {
                    DateTime curDT = DateTime.Now;
                    using (TransactionScope trans = new TransactionScope())
                    {
                        int? subjectId;
                        int? groupId;

                        if (model.AssigneeID == 0)
                        {
                            subjectId = null;
                            groupId = null;
                        }
                        else
                        {
                            groupId = model.AssigneeID;
                            subjectId = _groupRepo.Get(model.AssigneeID).SubjectID;
                        }
                        //var assignment = new Assignment() { AssignmentName = model.AssignmentName, Description = model.AssignmentDescription, AssignmentDate = CommonUtility.GetDateTimeFromString(model.StartDate, MasterConstants.DATEFORMATMDY), DueDate = CommonUtility.GetDateTimeFromString(model.EndDate, MasterConstants.DATEFORMATMDY), GroupID = groupId, CreatedDate = curDT, CreatedBy = user, SubjectID = subjectId, LeaderID = model.LeaderID };
                        var assignment = new Assignment() { AssignmentName = model.AssignmentName, Description = model.AssignmentDescription, AssignmentDate = CommonUtility.GetDateTimeFromString(model.StartDate, MasterConstants.DATEFORMATMDY), DueDate = CommonUtility.GetDateTimeFromString(model.EndDate, MasterConstants.DATEFORMATMDY), GroupID = groupId, CreatedDate = curDT, CreatedBy = user, SubjectID = subjectId, LeaderID = model.LeaderID, TeacherId = _UserID, WordsGoal = model.ExpectedWordCountGoal };
                        //add assignment and edit assignment
                        if (model.AssignmentID > 0)
                        {
                            assignment.AssignmentID = model.AssignmentID;
                            assignment.ModifiedDate = DateTime.Now;
                            assignment.ModifiedBy = user;
                            _assignRepo.Update(ref assignment);
                        }
                        else
                            _assignRepo.Add(ref assignment);
                        //new keywords
                        if (!string.IsNullOrEmpty(model.newKeyword))
                        {
                            string[] _newKeywords = model.newKeyword.Split(',');
                            foreach (var item in _newKeywords)
                            {
                                int keywordId = _keywordRepo.Add(item, _InstitutionID.Value, Session["UserName"].ToString());
                                var curassignKeyword = new AssignmentKeyword();
                                curassignKeyword.AssignmentID = assignment.AssignmentID;
                                curassignKeyword.KeywordID = keywordId;
                                curassignKeyword.CreatedDate = curDT;
                                curassignKeyword.CreatedBy = user;
                                _assignKeywordRepo.Add(ref curassignKeyword);
                            }
                        }
                        //existing keywords
                        if (!string.IsNullOrEmpty(model.Keywords))
                        {
                            string[] _keywords = model.Keywords.Split(',');
                            foreach (var item in _keywords)
                            {
                                var curassignKeyword = new AssignmentKeyword();
                                curassignKeyword.AssignmentID = assignment.AssignmentID;
                                curassignKeyword.KeywordID = Convert.ToInt32(item);
                                curassignKeyword.CreatedDate = curDT;
                                curassignKeyword.CreatedBy = user;
                                _assignKeywordRepo.Add(ref curassignKeyword);
                            }
                        }
                        //deletedAssignments
                        if (!string.IsNullOrEmpty(model.delKeywords))
                        {
                            string[] _keywords = model.delKeywords.Split(',');
                            foreach (var item in _keywords)
                            {
                                _assignKeywordRepo.RemoveKeywordOfAssignment(assignment.AssignmentID, Convert.ToInt32(item));
                            }
                        }

                        //add assignment keywords
                        trans.Complete();
                        return Json(new
                        {
                            status = MasterConstants.SUCCESSTITLE,
                            msg = model.AssignmentName + " " + MasterConstants.SaveSucessfully
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        status = MasterConstants.FAILURETITLE,
                        msg = error
                    });
                }
            }
            catch (Exception ex)
            {
                //exception db logging
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = _UserID.ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult IsDuplicateAssignment(int AssignmentID, string AssignmentName)
        {
            int? _UserID = Session["UserId"] as int?;
            try
            {
                return this.Json(new
                {
                    status = MasterConstants.FAILURETITLE,
                    msg = MasterConstants.ALREADYEXISTS,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //exception db logging
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = _UserID.ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }

        }

        #endregion

        #region GroupSection
        #region GettingGroups
        /// <summary>
        /// Method for Get the Group.
        /// </summary>
        /// <returns> Returns the Group List.</returns>
        public ActionResult GetGroups()
        {
            int? _UserID = Session["UserId"] as int?;
            try
            {
                return this.Json(new
                {
                    status = "success",
                    Data = new JavaScriptSerializer().Serialize(GetGroupDropDownData(_UserID))
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //exception db logging
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = _UserID.ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Method for Get the Group Dropdown Records according to particular Teacherid.
        /// </summary>
        /// <param name="teacherID">teacherID:Passing the teacherID, passed by reference,
        /// <returns> Returns Records of group dropdown wise  according to teacherID.</returns>
        private IEnumerable<DropdownViewModal> GetGroupDropDownData(int? teacherID)
        {
            var groupDropdownData = new List<DropdownViewModal>();
            //var teachersCreatedGroups = _teacherRepository.GetGroups(teacherID.Value);
            var teachersCreatedGroups = _groupRepo.GetTeacherGroups(teacherID);
            foreach (var item in teachersCreatedGroups)
            {
                groupDropdownData.Add(new DropdownViewModal() { Id = item.GroupID, Value = item.GroupName });
            }
            return groupDropdownData;
        }
        #endregion
        #endregion


        #region View Assignment grid
        public ActionResult GetGroupMembers(string groupid)
        {
            int? _UserID = Session["UserId"] as int?;
            try
            {
                return PartialView("_AssignmentMembers", _GetGroupMembers(Convert.ToInt32(groupid)));
            }
            catch (Exception ex)
            {
                //exception db logging
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = _UserID.ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult GetTeacherStudents()
        {
            int? _UserID = Session["UserId"] as int?;
            try
            {
                return PartialView("_AssignmentMembers", _GetTeacherStudents(_UserID));
            }
            catch (Exception ex)
            {
                //exception db logging
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = _UserID.ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    status = MasterConstants.ERRORTITLE,
                    msg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }

        }

        private IEnumerable<AssignmentMemberViewModal> _GetGroupMembers(int? groupid)
        {

            var groupMembersView = new List<AssignmentMemberViewModal>();
            //var teachersCreatedGroups = _teacherRepository.GetGroups(teacherID.Value);
            var groupMembers = _groupRepo.getGroupMembers(groupid);
            foreach (var item in groupMembers)
            {
                groupMembersView.Add(new AssignmentMemberViewModal() { MemberId = item.UserID, MemberName = item.Name, MemberPicUrl = item.UserPicUrl });
            }
            return groupMembersView;

        }

        private IEnumerable<AssignmentMemberViewModal> _GetTeacherStudents(int? teacherid)
        {
            var teachersStudentView = new List<AssignmentMemberViewModal>();
            var teachersStudent = _userRepo.GetTeachersActiveMembers(teacherid);

            foreach (var item in teachersStudent)
            {
                teachersStudentView.Add(new AssignmentMemberViewModal() { MemberId = item.UserID, MemberName = item.Name, MemberPicUrl = item.UserPicUrl });
            }
            return teachersStudentView;

        }

        private IEnumerable<DropdownViewModal> GetKeyWordDropDownData(int? institutionID, string searchString = "", bool isInitialCall = true)
        {
            var keyWordDropdownData = new List<DropdownViewModal>();
            var keywords = _keywordRepo.GetInstitutionKeywords(institutionID);
            if (!isInitialCall)
                keywords = keywords.Where(n => n.KeywordName.ToLower() == searchString.ToLower()).ToList();
            foreach (var item in keywords)
            {
                keyWordDropdownData.Add(new DropdownViewModal() { Id = item.KeywordID, Value = item.KeywordName });
            }
            return keyWordDropdownData;
        }

        private IEnumerable<DropdownViewModal> GetAssignmentTypesDropDownData()
        {
            var assigntypeDropdownData = new List<DropdownViewModal>();
            assigntypeDropdownData.Add(new DropdownViewModal() { Id = 1, Value = "Document" });

            return assigntypeDropdownData;
        }
        #endregion

        #region [Assignment Functions]
        [checkSessionAlive]
        public ActionResult TeacherAssignmentView()
        {
            int _instituteId = Convert.ToInt32(Session["Institution"]);
            int _userid = Convert.ToInt32(Session["UserId"]);
            Session["Theme"] = _insRepo.Get(_instituteId).Theme.ThemeName;
            ViewSubmissionViewModel _viewSubmissionModel = new ViewSubmissionViewModel();
            _viewSubmissionModel._userinfo = _userRepo.Get(User.Identity.GetUserId());
            ViewBag.PageHeading = "Assignments";
            ViewBag.Groups = GetGroupDropDownData();
            return View(_viewSubmissionModel);
        }

        private IEnumerable<Areas.Teacher.Models.DropdownViewModal> GetGroupDropDownData()
        {
            var groupDropdownData = new List<Areas.Teacher.Models.DropdownViewModal>();
            int _userid = Convert.ToInt32(Session["UserId"]);
            var result = _groupRepo.getGroupWithMemberCountByTeacher(_userid);

            foreach (var item in result)
            {
                groupDropdownData.Add(new Areas.Teacher.Models.DropdownViewModal() { Id = item.GroupID, Value = item.GroupName });
            }
            return groupDropdownData;
        }
        [checkSessionAlive]
        public ActionResult TeacherAssignmentGrid(int? id)
        {
            //return PartialView("PartialAssignmentGrid", _assignRepo.GetAssignmentList(Convert.ToInt32(Session["UserId"]), id).ToList());
            return PartialView("PartialAssignmentGrid");
        }

        public ActionResult getsearchAssignment(int? groupId, string assignmentName)
        {
            try
            {
                var result = _assignRepo.GetAssignmentList(Convert.ToInt32(Session["UserId"]), groupId).ToList();
                var searhResult = result.Where(x => x.AssignmentName.Contains(assignmentName));
                return PartialView("PartialAssignmentGrid", searhResult);
            }
            catch (Exception ex)
            {
                //ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message }; new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.DUPLICATESUBJECTNAME
                });
            }
        }
        #endregion
        [checkSessionAlive]
        [HttpPost]
        public JsonResult BindCreateAssignmentDetails(int draw, int start, int length, int? id)
        {
            start = start == 0 ? start : start / 10;
            try
            {
                string searchKey = Request["search[value]"];
                string order = Request["order[0][column]"];
                string orderby = Request["order[0][dir]"];
                searchKey = searchKey.Trim().ToLower();

                var assignmentList = _assignRepo.GetAssignmentList(Convert.ToInt32(Session["UserId"]), id, start).ToList();
                assignmentList = assignmentList.Where(x => x.AssignmentName.Trim().ToLower().Contains(searchKey) || x.CreatedDate.ToString().Trim().ToLower().Contains(searchKey) || x.DueDate.ToString().Trim().ToLower().Contains(searchKey) || x.Leader.Trim().ToLower().Contains(searchKey) || x.GroupName.Trim().ToLower().Contains(searchKey)).ToList();
                var count = assignmentList.Count > 0 ? assignmentList[0].totalrecords.Value : 0;
                switch (order)
                {
                    case "0":
                        assignmentList = orderby.Equals("asc") ? assignmentList.OrderBy(x => x.AssignmentName).ToList() : assignmentList.OrderByDescending(x => x.AssignmentName).ToList();
                        break;
                    case "1":
                        assignmentList = orderby.Equals("asc") ? assignmentList.OrderBy(x => x.CreatedDate).ToList() : assignmentList.OrderByDescending(x => x.CreatedDate).ToList();
                        break;
                    case "2":
                        assignmentList = orderby.Equals("asc") ? assignmentList.OrderBy(x => x.DueDate).ToList() : assignmentList.OrderByDescending(x => x.DueDate).ToList();
                        break;
                    case "3":
                        assignmentList = orderby.Equals("asc") ? assignmentList.OrderBy(x => x.Leader).ToList() : assignmentList.OrderByDescending(x => x.Leader).ToList();
                        break;
                    case "4":
                        assignmentList = orderby.Equals("asc") ? assignmentList.OrderBy(x => x.GroupName).ToList() : assignmentList.OrderByDescending(x => x.GroupName).ToList();
                        break;
                    default:
                        assignmentList = orderby.Equals("asc") ? assignmentList.OrderBy(x => x.CreatedDate).ToList() : assignmentList.OrderByDescending(x => x.CreatedDate).ToList();
                        break;
                }

                JsonTableData jsonData = new JsonTableData();
                jsonData.recordsTotal = count;
                jsonData.recordsFiltered = count;
                jsonData.draw = draw;
                List<List<string>> assignmentDetailList = new List<List<string>>();

                ////////columns end///////////////
                if (assignmentList.Count > 0)
                {
                    foreach (var item in assignmentList)
                    {
                        ////////columns////////////////////
                        string col1 = "" + item.AssignmentName + "<div class='clearfix'></div><small class='txt_lgry'>" + item.Description + "</small>";
                        string col2 = "" + Convert.ToDateTime(item.CreatedDate).ToString("d  MMM , yyyy") + "";
                        string col3 = "" + Convert.ToDateTime(item.DueDate).ToString("d  MMM , yyyy") + "";
                        string imageUrl = string.IsNullOrEmpty(item.USERPicUrl) ? "/content/images/leader.jpg" : item.USERPicUrl.Replace("~", "/..");
                        string col4 = "<a href='javascript:;' class='user-profile pull-left'> <img src='" + imageUrl.Replace("~", "/..") + "'  title=" + item.Leader + " alt=''> </a>";
                        col4 += "<div class='media-body'><a class='title' href='#'>" + item.Leader + " </a>";
                        col4 += "<p><small class='txt_lgry'>" + item.GroupName + "</small></p></div>";
                        string col5 = string.Empty;
                        if (item.GroupName.ToLower() == "individual")
                        {
                            //col5 += "<span>" + item.GroupName + "</span>";
                            col5 += col4;
                        }
                        else
                        {
                            col5 += "<div id='thumbcarousel' class='carousel slide' data-interval='false'><div class='carousel-inner'>";
                            col5 += "<div class='item active'><div class='thumb bg_green'>" + item.GroupIntialName + "</div></div>";
                            col5 += "<div class='group_name'>" + item.GroupName + "</div></div></div>";
                        }

                        string col6 = "<a href='#!' class='sm_btn btn_org mrg_t-4' style='cursor:default;'>" + item.AssignmentStatus.AssignmentStatus() + "</a>  ";
                        string col7 = " <a href='#!' class='btn_blue mml-0 min_w-80 text-center' onclick='onEditAssignment(" + item.AssignmentID + ")'>Edit</a> ";
                        string col8 = "<div class='mad-select pull-right' style='width: 110px;'>";
                        col8 += "<ul id='ddlAction' class='ddlAction'>";
                        col8 += "<li data-value='1 sel' class='selected'>Actions</li>";

                        string editAction = "onclick = archiveAssignment(" + item.AssignmentID + ",'" + item.AssignmentName.ToString().Replace(" ", "&nbsp;") + "'," + item.AssignmentStatus + ")";
                        string archiveAction = "onclick=cancelAssignment(" + item.AssignmentID + ",'" + item.AssignmentName.ToString().Replace(" ", "&nbsp;") + "'," + item.AssignmentStatus + ")";

                        col8 += "<li data-value='Edit' " + editAction + ">Archive</li>";
                        col8 += "<li data-value='Archive' " + archiveAction + ">Cancel</li></ul>";
                        List<string> common = new List<string>();
                        common.Add(col1);
                        common.Add(col2);
                        common.Add(col3);
                        common.Add(col4);
                        common.Add(col5);
                        common.Add(col6);
                        common.Add(col7);
                        common.Add(col8);
                        assignmentDetailList.Add(common);
                    }
                }

                jsonData.data = assignmentDetailList;
                return new JsonResult()
                {
                    Data = jsonData,
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                return Json(null);
            }
        }

        public JsonResult GetKeyWordGraphData(int? assignmentId)
        {
            try
            {
                var result = _assignRepo.GetAssignmentCountUsesData(assignmentId);
                //List<AssignmentKeyWordCount> _listCount = new List<AssignmentKeyWordCount>();
                //foreach (var item in result)
                //{
                //    AssignmentKeyWordCount _assignmentKeyWordCount = new AssignmentKeyWordCount();
                //    _assignmentKeyWordCount.StudentName = item.User.Name;
                //    _assignmentKeyWordCount.KeyWordCount = item.WordCount;
                //    _assignmentKeyWordCount.StudentPicUrl = item.User.UserPicUrl == null ? "/../Content/images/student_small.jpg" : item.User.UserPicUrl.Replace("~", "/..");
                //    _listCount.Add(_assignmentKeyWordCount);
                //}
                return new JsonResult()
                {
                    Data = result,
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                return Json(null);
            }
        }
    }
}

