﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reelae.Areas.Teacher.Models
{
    public class MarkAttendanceViewModel
    {
        public int StudentId { get; set; }

        public string attedance { get; set; }

        public DateTime attedanceDate { get; set; }

        public string StudentName { get; set; }
    }
}