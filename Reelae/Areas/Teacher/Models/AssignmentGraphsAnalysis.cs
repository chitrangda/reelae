﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reelae.Areas.Teacher.Models
{
    public class AssignmentGraphsAnalysis
    {
        public string AssignmentName { get; set; }
    }

    public class AssignmentKeyWordCount
    {
        public string StudentName { get; set; }
        public Nullable<int> KeyWordCount { get; set; }
        public string StudentPicUrl { get; set; }
    }
}