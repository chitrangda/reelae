﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReelaeDataAccessLayer;

namespace Reelae.Areas.Teacher.Models
{
    public class newNoticeViewModel
    {
        public NoticeBoard _noticeBoard { get; set; }
        public List<NoticeBoardFile> _noticeBoardFiles { get; set; }
    }
}