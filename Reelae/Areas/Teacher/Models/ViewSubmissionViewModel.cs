﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReelaeDataAccessLayer;

namespace Reelae.Areas.Teacher.Models
{
    public class ViewSubmissionViewModel
    {
        public User _userinfo { get; set; }
        public Assignment assignmentInfo { get; set; }
        public IEnumerable<Assignment> assignmentList { get; set; }
        
        public IEnumerable<Assignment> getassignmentbyteacher { get; set; }

        public IEnumerable<re_TeacherAssignmentList_sp_Result> getgroupname { get; set; }

        public Group _groupinfo { get; set; }

        public IEnumerable<re_TeacherViewSubmission_sp_Result> getteacherassignment { get; set; }



    }
}