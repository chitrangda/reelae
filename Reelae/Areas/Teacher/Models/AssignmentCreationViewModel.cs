﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reelae.Areas.Teacher.Models
{
    public class AssignmentCreationViewModel
    {
        public int AssignmentID { get; set; }
        public int AssignmentType { get; set; }
        public string AssignmentName { get; set; }
        public string AssignmentDescription { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        //either groupID 
        public int AssigneeID { get; set; }
        public string Keywords { get; set; }
        public string newKeyword { get; set; }
        public string delKeywords { get; set; }
        public int LeaderID { get; set; }
        public int ExpectedWordCountGoal { get; set; }
    }

    public class AssignmentMemberViewModal
    {
        public int MemberId { get; set; }
        public string MemberName { get; set; }
        public string MemberPicUrl { get; set; }
    }
}