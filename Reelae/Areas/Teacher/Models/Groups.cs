﻿using ReelaeDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reelae.Areas.Teacher.Models
{
    public class GroupsMemeberViewModel
    {
        public IEnumerable<re_getTeacherSubject_sp_Result> Subjects { get; set; }
        public IEnumerable<re_getTeacherSubject_sp_Result> LatestSubjects { get; set; }
        public IEnumerable<re_getTeacherSubject_sp_Result> ArchiveSubjects { get; set; }
        public User _userinfo { get; set; }

        #region GroupsWork
        public IEnumerable<re_getGroupMemberCount_sp_Result> Groups { get; set; }
        
        #endregion

    }

    public class GroupMembers
    {
        public int MemberID { get; set; }
        public string MemberName { get; set; }
        public string Presense { get; set; }

    }
    
}