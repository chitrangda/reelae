﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReelaeDataAccessLayer;

namespace Reelae.Areas.Teacher.Models
{
    public class SubjectMemberViewModel
    {
        public IEnumerable<Group> _subjectGroups { get; set; }
        public IEnumerable<re_select_studenttecher_subject_sp_Result> _subjectStudents { get; set; }
    }
}