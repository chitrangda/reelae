﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReelaeDataAccessLayer;

namespace Reelae.Areas.Teacher.Models
{
    public class keywordMasterViewModal
    {
        public KeyWord newKeyword { get; set; }

        public IEnumerable<KeyWord> exitingKeywords { get; set; }
    }
}