﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReelaeDataAccessLayer;

namespace Reelae.Areas.Teacher.Models
{
    public class AttedanceViewModel
    {
        public User _userinfo { get; set; }

        public IEnumerable<re_getTeacherSubject_sp_Result> _Subjects { get; set; }

    }
}