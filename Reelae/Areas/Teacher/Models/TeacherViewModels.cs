﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReelaeDataAccessLayer;

namespace Reelae.Areas.Teacher.Models
{
    public class TeacherViewModels
    {
    }

    public class TeacherDashboardViewModel
    {
        public int AssignmentSubmissionsCount { get; set; }
        public int AssignmentCount { get; set; }
        public int StudentsCount { get; set; }
        public int MasterKeywordCount { get; set; }
        public string UserName { get; set; }
        public Assignment Assignment { get; set; }
        public User _userinfo { get; set; }
      //  public IEnumerable<Group> GroupList { get; set; }//
        public IEnumerable<re_getGroupMemberCount_sp_Result> GroupList { get; set; }
        public IEnumerable<re_getTeacherSubject_sp_Result> SubjectList { get; set; }

    }

    public class TeacherGroupMembersSection
    {

    }


    public class TeacherNoticeBoardSection
    {

    }

    public class TeacherViewSubmissionsSection
    {

    }
}