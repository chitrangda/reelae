﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reelae.Areas.Teacher.Models
{
    public class Assignments
    {
        public int AssignmentID { get; set; }
        public int AssignmentName { get; set; }
        public float StudentsProgressPercentage { get; set; }
        public int TimeTracked { get; set; }
        public float StudentWorkingPercent { get; set; }
        public int KeywordUsed { get; set; }
        public float AssignmentsProgress { get; set; }

    }
}