﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReelaeDataAccessLayer;

namespace Reelae.Areas.Teacher.Models
{
    public class NoticeViewModel
    {
        public IEnumerable<re_select_noticeboardinfo_sp_Result> notices { get; set; }

        public IEnumerable<re_getNoticeComment_sp_Result> noticeComments { get; set; }

        public IEnumerable<NoticeBoardFile> noticeFiles { get; set; }
    }
}