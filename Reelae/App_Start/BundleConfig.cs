﻿using System.Web;
using System.Web.Optimization;

namespace Reelae
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrapJS").Include(
                        "~/Content/js/jquery.min.js",
                      "~/Content/js/bootstrap.min.js",
                      "~/Content/js/bootstrap-progressbar.min.js"));

            bundles.Add(new StyleBundle("~/Content/bootstrapcss").Include(
                      "~/Content/css/bootstrap.min.css",
                      "~/Content/css/bootstrap-progressbar-3.3.4.min.css"));


            bundles.Add(new StyleBundle("~/Content/commoncss").Include(
                "~/Content/css/nprogress.css",
                   "~/Content/css/green.css",
                   "~/Content/css/daterangepicker.css",
                   "~/Content/css/custom.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/commonjQuery").Include(
                       "~/Content/js/nprogress.js",
                      "~/Content/js/icheck.min.js",
                       "~/Content/js/date.js",
                       "~/Content/js//moment.min.js",
                       "~/Content/js/daterangepicker.js",
                       "~/Content/js/custom.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/ajaxJS").Include(
                   "~/Scripts/jquery-1.10.2.js",
                 "~/Scripts/jquery.unobtrusive-ajax.js"
                 ));
        }
    }
}
