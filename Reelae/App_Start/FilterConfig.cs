﻿using System.Web;
using System.Web.Mvc;
using Reelae.Filters;

namespace Reelae
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
