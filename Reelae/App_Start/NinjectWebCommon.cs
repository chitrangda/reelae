﻿[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Reelae.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Reelae.NinjectWebCommon), "Stop")]
namespace Reelae
{
    using System;
    using System.Web;
    using log4net;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using ReelaeDataAccessLayer;
    using Repositories;

    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                //kernel.Bind<IEmployeeRepository>().To<EmployeeRepository>();
                #region [RepositoriesDependencies]
                kernel.Bind<IUserRepository>().To<UserRepository>();
                kernel.Bind<IInstitutionRepository>().To<InstitutionRepository>();
                kernel.Bind<ISubjectRepository<Subject, int>>().To<SubjectRepository>();
                kernel.Bind<ISubjectStudentRepository>().To<SubjectStudentRepository>();
                kernel.Bind<ISubjectTeacherRepository>().To<SubjectTeacherRepository>();
                kernel.Bind<IKeywordRepository>().To<KeywordRepository>();
                kernel.Bind<IAssignmentRepository>().To<AssignmentRepository>();
                kernel.Bind<IAssignmentKeywordsRepository>().To<AssignmentKeywordsRepository>();
                kernel.Bind<ILogRepository>().To<LogRepository>();
                kernel.Bind<IThemeRepository>().To<ThemeRepository>();
                kernel.Bind<IGroupRepository>().To<GroupRepository>();
                kernel.Bind<IAspnetUsersRepository>().To<AspnetUsersRepository>();
                kernel.Bind<ITeacherDashboardRepository>().To<TeacherDashboardRepository>();
                kernel.Bind<IResourceRepository>().To<ResourceRepository>();
                kernel.Bind<IAttendanceRepository>().To<AttendanceRepository>();
                kernel.Bind<INoticeBoardRepository>().To<NoticeBoardRepository>();
                kernel.Bind<IChatRepository>().To<ChatRepository>();
                kernel.Bind<IPinUpBoardRepository>().To<PinUpBoardRepository>();
                kernel.Bind<IForumRepository>().To<ForumRepository>();
                kernel.Bind<IMilestoneRepository>().To<MilestoneRepository>();
                #endregion
                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            try
            {
                kernel.Bind<ILog>().ToMethod(ctx =>
                    {
                        var name = ctx.Request.Target.Member.DeclaringType.FullName;
                        return LogManager.GetLogger(name);
                    });
            }
            catch (Exception e)
            {
            }
        }
    }
}