﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Reelae.Handlers
{
    /// <summary>
    /// Summary description for ResumableUploader
    /// </summary>
    public class ResumableUpload : IHttpHandler
    {
        HttpContext currentContext = null;
        public void ProcessRequest(HttpContext context)
        {
            currentContext = context;
            if (context.Request.HttpMethod.ToUpper() == "GET")
            {
                UploadFile();
            }
            else if (context.Request.HttpMethod.ToUpper() == "POST")
            {
                UploadFile(context.Request.InputStream);
            }
            else
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("Hello World");
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }


        public void UploadFile()
        {
            string _uploadFolder = "ResumableUpload";
            var queryString = currentContext.Request.QueryString;
            if (queryString.Count == 0) return;

            // Read parameters
            var uploadToken = queryString.Get("upload_Token");

            int resumableChunkNumber = int.Parse(queryString.Get("resumableChunkNumber"));
            var resumableIdentifier = queryString.Get("resumableIdentifier");
            //var resumableChunkSize = queryString.Get("resumableChunkSize");
            //var resumableTotalSize = queryString.Get("resumableTotalSize");
            var resumableFilename = queryString.Get("resumableFilename");
            uploadToken = (string.IsNullOrEmpty(uploadToken)) ? resumableIdentifier : "";
            var filePath = string.Format("{0}/{1}/{1}.part{2}", _uploadFolder,
                           uploadToken, resumableChunkNumber.ToString("0000"));
            var localFilePath = currentContext.Server.MapPath(filePath);

            // Response
            var fileExists = System.IO.File.Exists(localFilePath);
            if (fileExists)
            {
                currentContext.Response.Status = "200 OK";
                currentContext.Response.StatusCode = 200;
            }
            else
            {
                currentContext.Response.Status = "404 Not Found";
                currentContext.Response.StatusCode = 404;
            }
        }
        public void UploadFile(object data)
        {
            var queryString = currentContext.Request.Form;
            if (queryString.Count == 0) return;
            string _uploadFolder = "ChatUploads";
            // Read parameters
            var uploadToken = queryString.Get("upload_Token");
            int resumableChunkNumber = int.Parse(queryString.Get("resumableChunkNumber"));
            var resumableFilename = queryString.Get("resumableFilename");
            var resumableIdentifier = queryString.Get("resumableIdentifier");
            int resumableChunkSize = int.Parse(queryString.Get("resumableChunkSize"));
            long resumableTotalSize = long.Parse(queryString.Get("resumableTotalSize"));

            uploadToken = (string.IsNullOrEmpty(uploadToken)) ? resumableIdentifier : "";

            var filePath = string.Format("{0}/{1}/{1}.part{2}", _uploadFolder,
                                  uploadToken, resumableChunkNumber.ToString("0000"));
            var localFilePath = currentContext.Server.MapPath(filePath);
            var directory = System.IO.Path.GetDirectoryName(localFilePath);
            if (!System.IO.Directory.Exists(localFilePath)) System.IO.Directory.CreateDirectory(directory);

            if (currentContext.Request.Files.Count == 1)
            {
                // save chunk
                if (!System.IO.File.Exists(localFilePath))
                {
                    currentContext.Request.Files[0].SaveAs(localFilePath);
                }

                // Check if all chunks are ready and save file
                var files = System.IO.Directory.GetFiles(directory);
                if ((files.Length + 1) * (long)resumableChunkSize >= resumableTotalSize)
                {
                    filePath = string.Format("{0}/{1}{2}", _uploadFolder,
                      System.IO.Path.GetFileNameWithoutExtension(resumableFilename) + "_" + DateTime.Now.Ticks.ToString(), System.IO.Path.GetExtension(resumableFilename));
                    localFilePath = currentContext.Server.MapPath(filePath);
                    using (var fs = new FileStream(localFilePath, FileMode.CreateNew))
                    {
                        foreach (string file in files.OrderBy(x => x))
                        {
                            var buffer = System.IO.File.ReadAllBytes(file);
                            fs.Write(buffer, 0, buffer.Length);
                            System.IO.File.Delete(file);
                        }
                    }
                    System.IO.Directory.Delete(directory);
                    //after save details in db also and send the final identifier

                    currentContext.Response.Status = "200 OK";
                    currentContext.Response.StatusCode = 200;
                    currentContext.Response.Write("filePath=" + filePath + "");
                }
            }
            else
            {
                // log error
            }
        }

    }
}