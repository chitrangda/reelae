﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reelae.Handlers
{
    /// <summary>
    /// Summary description for Downloader
    /// </summary>
    public class Downloader : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            var queryString = context.Request.QueryString;
            if (queryString.Count == 0) return;
            var filePath = queryString.Get("fP");
            var fileName = queryString.Get("fN");
            if (!string.IsNullOrEmpty(filePath))
            {
                var localFilePath = context.Server.MapPath(filePath);
                var fileInfo = new System.IO.FileInfo(localFilePath);
                if (fileInfo.Exists)
                {

                    context.Response.ContentType = System.Net.Mime.MediaTypeNames.Application.Octet;
                    context.Response.AddHeader("Content-disposition", "attachment; filename=" + fileName);
                    //using (FileStream fs = File.OpenRead(localFilePath))
                    //{
                    //    byte[] buffer = new byte[64 * 1024];
                    //    int read;
                    //    while ((read = fs.Read(buffer, 0, buffer.Length)) > 0)
                    //    {
                    //        context.Response.OutputStream.Write(buffer, 0, read);
                    //        context.Response.OutputStream.Flush(); //seems to have no effect
                    //    }
                    //    context.Response.OutputStream.Close();
                    //}
                    //context.Response.Buffer = false;
                    context.Response.Status = "200 OK";
                    context.Response.StatusCode = 200;
                    context.Response.TransmitFile(localFilePath);
                    //context.Response.Close();
                }
                else
                {
                    context.Response.Status = "404 Not Found";
                    context.Response.StatusCode = 404;
                }
            }
            else
                return;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}