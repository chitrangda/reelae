﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Microsoft.AspNet.SignalR;
using Ninject;
using Reelae.Areas.Student.Models;
using Reelae.Areas.Teacher.Models;
using Reelae.Models;
using Reelae.Repositories;

namespace Reelae.Hubs
{
    [Authorize(Roles = "Student")]
    public class AssignmentHub : Hub
    {

        private UserChatMapping userMappingObject;
        private readonly static ConnectionMapping<string> _connections =
            new ConnectionMapping<string>();
        private readonly static ConnectionAssignmentGroupMapping<string> _userConnectedAssignments = new ConnectionAssignmentGroupMapping<string>();
        private readonly static ConcurrentDictionary<string, string> _connectionAssignmentMapping = new ConcurrentDictionary<string, string>();
        [Inject]
        private IAssignmentRepository _assignmentRepo;
        private JavaScriptSerializer jsSer;

        #region [Overrided Methods]
        public override Task OnConnected()
        {
            string name = Context.User.Identity.Name;
            _connections.Add(name, Context.ConnectionId);
            Clients.Caller.callOnOpenAssignmentWorkSpace();
            return base.OnConnected();
        }
        public override Task OnDisconnected()
        {
            string name = Context.User.Identity.Name;
            _connections.Remove(name, Context.ConnectionId);
            LeaveFromExistingGroups();
            return base.OnDisconnected();
        }
        public override Task OnReconnected()
        {
            string name = Context.User.Identity.Name;
            if (!_connections.GetConnections(name).Contains(Context.ConnectionId))
            {
                _connections.Add(name, Context.ConnectionId);
            }
            Clients.Caller.callOnOpenAssignmentWorkSpace();
            return base.OnReconnected();
        }
        protected override void Dispose(bool disposing)
        {
            _assignmentRepo = null;
            base.Dispose(disposing);
        }
        #endregion

        #region [SeverMethods]
        public void onOpenAssignmentWorkSpace(bool isGroup, int assignmentId, string GroupId)
        {
            string assignmentGroupName = assignmentId.ToString();
            Groups.Add(Context.ConnectionId, assignmentGroupName);
            Clients.OthersInGroup(assignmentGroupName).setPresenseStatus(Context.User.Identity.Name, TaskConstants.Online);
            AddToExistingGroups(Context.User.Identity.Name, assignmentGroupName, Context.ConnectionId);
            Clients.Caller.updateOthersConnectionStatus(GetActiveAssignmentMembers(assignmentGroupName), TaskConstants.Online);
        }
        public void onCloseAssignmentWorkSpace(bool isGroup, int assignmentId, string GroupId)
        {
            string assignmentGroupName = assignmentId.ToString();
            Groups.Remove(Context.ConnectionId, assignmentGroupName);
            Clients.OthersInGroup(assignmentGroupName).setPresenseStatus(Context.User.Identity.Name, TaskConstants.Offline);
        }

        public void sendAssignmentData(string serMessage)
        {
            try
            {
                if (jsSer == null)
                    jsSer = new JavaScriptSerializer();
                AssignmentRTEData parsedObj = jsSer.Deserialize<AssignmentRTEData>(serMessage);
                if (parsedObj != null)
                {
                    var GroupName = parsedObj.AssignmentToken.ToString();
                    Clients.OthersInGroup(GroupName).OnDataReceived(serMessage);
                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        #region [PrivateMethods]
        public void AddToExistingGroups(string user, string assignmentGroupName, string connectionId)
        {
            _connectionAssignmentMapping.TryAdd(connectionId, assignmentGroupName);
        }
        private void LeaveFromExistingGroups()
        {
            string assignVal;
            if (_connectionAssignmentMapping.TryGetValue(Context.ConnectionId, out assignVal)) {
                Clients.OthersInGroup(assignVal).setPresenseStatus(Context.User.Identity.Name, TaskConstants.Offline, "");
                Groups.Remove(Context.ConnectionId, assignVal);
                _connectionAssignmentMapping.TryRemove(Context.ConnectionId,out assignVal);
            }
        }
        private IEnumerable<string> GetActiveAssignmentMembers(string assignmentName) {
            var connectionIds = _connectionAssignmentMapping.Where(n => n.Value == assignmentName).Select(n => n.Key).ToList();
            return _connections.connectionNamesForConnectionId(connectionIds, Context.User.Identity.Name);
        }
        

        #endregion
    }
}