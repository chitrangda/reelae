﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using Microsoft.AspNet.SignalR;
using Ninject;
using Reelae.Areas.Teacher.Models;
using Reelae.Models;
using Reelae.Repositories;

namespace Reelae.Hubs
{
    [Authorize]
    public class ChattingHub : Hub
    {
        private UserChatMapping userMappingObject;
        private readonly static ConnectionMapping<string> _connections =
            new ConnectionMapping<string>();
        [Inject]
        private IChatRepository _chatrepo;
        private JavaScriptSerializer jsSer;

        public void SayHello(string message)
        {
            Clients.All.receiveHello("received " + message);
        }

        public void sendIndividualSystemMessage(ChatMessage message)
        {

        }

        #region [Overrided Methods]
        public override Task OnConnected()
        {
            string name = Context.User.Identity.Name;
            _connections.Add(name, Context.ConnectionId);
            var allConnections = _connections.GetOnlineConnectionsNames(name);
            Clients.Others.receiveConnectionStatusChangeMessage(this.Context.User.Identity.Name, TaskConstants.Online, "");
            var curConnection = _connections.GetConnections(name);
            Clients.Clients(curConnection.ToList()).updateOthersConnectionStatus(allConnections, TaskConstants.Online, "");
            return base.OnConnected();
        }
        public override Task OnDisconnected()
        {
            string name = Context.User.Identity.Name;
            _connections.Remove(name, Context.ConnectionId);
            string LastActiveTime = DateTime.UtcNow.ToShortTimeString();
            Clients.Others.receiveConnectionStatusChangeMessage(this.Context.User.Identity.Name, TaskConstants.Offline, LastActiveTime);
            return base.OnDisconnected();
        }
        public override Task OnReconnected()
        {

            string name = Context.User.Identity.Name;
            if (!_connections.GetConnections(name).Contains(Context.ConnectionId))
            {
                _connections.Add(name, Context.ConnectionId);
            }
            var curConnection = _connections.GetConnections(name);
            Clients.Others.receiveConnectionStatusChangeMessage(this.Context.User.Identity.Name, TaskConstants.Online, "");
            var allConnections = _connections.GetOnlineConnectionsNames(name);
            Clients.Clients(curConnection.ToList()).updateOthersConnectionStatus(allConnections, TaskConstants.Online, "");
            return base.OnReconnected();
        }
        protected override void Dispose(bool disposing)
        {
            _chatrepo = null;
            base.Dispose(disposing);
        }
        #endregion

        #region [SingleChatMethods]
        public void sendIndividualMessage(string serMessage)
        {
            try
            {
                if (jsSer == null)
                    jsSer = new JavaScriptSerializer();
                if (_chatrepo == null)
                    _chatrepo = new ChatRepository();
                ChatMessage message = jsSer.Deserialize<ChatMessage>(serMessage);

                //chat Obj
                ReelaeDataAccessLayer.Message msg = new ReelaeDataAccessLayer.Message()
                {
                    CreatedDate = DateTime.UtcNow,
                    CreatedBy = Context.User.Identity.Name,
                    MessageDate = DateTime.UtcNow,
                };

                if (message.type == "text")
                {
                    msg.MessageText = message.message;
                    msg.IsAttachment = false;
                }
                else
                {
                    msg.MessageText = message.message;
                    msg.IsAttachment = true;
                    msg.AttachmentURL = message.attachmentUrl;
                    msg.AttachementType = message.type;
                }

                ReelaeDataAccessLayer.ChatUser chatUser = new ReelaeDataAccessLayer.ChatUser()
                {
                    FromUserID = Convert.ToInt32(message.from),
                    ToUserID = Convert.ToInt32(message.sendTo),
                    GroupID = null,
                    CreatedDate = DateTime.UtcNow,
                    CreatedBy = Context.User.Identity.Name,
                    IsActive = true
                };
                int msgId = _chatrepo.Add(msg, chatUser);
                message.messageId = msgId.ToString();
                message.messageDate = msg.MessageDate.Value.ToShortTimeString();
                //for online message
                var allConnections = _connections.GetConnections(message.toEmailId);
                if (allConnections != null && allConnections.Count() > 0)
                {
                    msg.IsOnline = 1;
                    Clients.Clients(allConnections.ToList()).receiveIndividualMessage(jsSer.Serialize(message));
                }
                else
                {
                    msg.IsOnline = 0;
                }

                var allConnectionsOfCurrentUser = _connections.GetConnections(Context.User.Identity.Name);
                Clients.Clients(allConnectionsOfCurrentUser.ToList()).updateMessageID(msg.MessageID, message.messageToken);

            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        #region [NotificationMessages]
        public void sendConnectionStatusChange(string userId, string status)
        {
            Clients.Others.receiveConnectionStatusChangeMessage(userId, status);
        }
        private void ChangeConnectionStatus()
        {
            Clients.Others.UpdateConnectionStatus(Context.ConnectionId, Context.User.Identity.Name);
        }

        //private IEnumerable<int> GetConnectionIdsByUserID(int UserId) {


        //}

        #endregion

        #region [GroupMembers]
        public Task JoinRoom(string roomName)
        {
            return Groups.Add(Context.ConnectionId, roomName);
        }

        public Task LeaveRoom(string roomName)
        {
            return Groups.Remove(Context.ConnectionId, roomName);
        }
        #endregion
    }
}