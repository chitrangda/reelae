﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using log4net;
using log4net.Core;
using Ninject;

namespace Reelae.Filters
{
    public class LoggingFilterAttribute : ActionFilterAttribute
    {
        [Inject]
        private static ILogger logger;
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.Exception != null && logger == null) {
                logger = LoggerManager.GetLogger(Assembly.GetExecutingAssembly(),this.GetType());
            }

            if (filterContext.Exception != null)
            {
                var LogMessage = string.Format("Executed action {0}.{1}{2} ErrorMessage : {3} , Stack Trace : {4}", filterContext.ActionDescriptor.ControllerDescriptor.ControllerName, filterContext.ActionDescriptor.ActionName, Environment.NewLine, filterContext.Exception.Message.ToString(), filterContext.Exception.StackTrace);

                logger.Log(this.GetType(), Level.Error, LogMessage, null);
            }
        }
    }
}