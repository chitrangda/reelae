﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Reelae.Filters
{
    public class checkSessionAlive : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            HttpSessionStateBase objHttpSessionStateBase = filterContext.HttpContext.Session;
            if (filterContext.HttpContext.Session["Institution"] == null && filterContext.HttpContext.Session["UserId"] == null)
            {
                objHttpSessionStateBase.RemoveAll();
                objHttpSessionStateBase.Clear();
                objHttpSessionStateBase.Abandon();
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    var jsonRes = new JsonResult();
                    jsonRes.Data = new { LogOutKey = "SessionOut" };
                    jsonRes.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                    filterContext.Result = jsonRes;
                }
                else
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary {
                    { "controller", "Home" },
                    { "action", "Login" },
                    { "Area", string.Empty }
                     }
                    );
                }
            }
        }
    }
}