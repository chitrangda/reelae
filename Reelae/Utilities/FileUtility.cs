﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reelae.Utilities
{
    public class FileUtility
    {
        public string fileAbsoluteURL { get; set; }
        public string fileName { get; set; }
        public string fileType { get; set; }
        public string FileSize { get; set; }

        public static bool IsValidFileForUpload(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                return false;
            else
            {
                string[] filePart = fileName.Split('.');
                string extension = filePart[filePart.Length - 1];
                return MasterConstants.ValidFileExtentions.Any(n => n == extension.ToLower());
            }
        }

    }
}