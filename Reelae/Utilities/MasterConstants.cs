﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reelae.Utilities
{
    public static class MasterConstants
    {
        public const string SOMEPROBLEMOccurred = "Some Problem Occurred.Please Try Again Later!";
        public const string ERRORTITLE = "Error";
        public const string FAILURETITLE = "Failure";
        public const string SUCCESSTITLE = "Success";
        public const string PASSWORDVALIDATIONVIEWMESSAGE = "Password must be combination of Lower,Upper case and Special Characters";
        public const string INVALIDINSTITUTION = "Invalid Institution";
        public const string ALREADYEXISTS = "Already exists!";
        public const string PASSWORDRESETSUCCESS = "Your password has been reset sucessfully.";
        public const string EMAILREGISTRATIONALERT = "Email Address does not exists, please register first.";
        public const string ACCOUNTNOTACTIVATED = "Your account is deactivated, please contact to admin.";
        public const string RESETLINKSENT = "A password reset link has been sent on your registered email address.";
        public const string ACCOUNTACTIVATIONSUCCESS = "Your Reelae Account has been Activated";
        public const string EMAILSENTSUCCESS = "Email has been sent to user.";
        public const string SIGNUPEMAILSUBJECT = "Reelae Sign-Up Request";
        public const string EMAILALREADYEXISTS = "Email id already exists!";
        public const string INVALIDLOGINATTEMPT = "Invalid login attempt.";
        public const string CHOOSEINSTITUTIONTYPE = "Please choose your institution type.";
        public const string INVALIDARGUMENT = "Invalid Arguments.";
        public const string NOFILESSELECTED = "Invalid Arguments.";
        public const string SUCCESSSETTINGSAVE = "Invalid Arguments.";
        public const string PLEASETRYAGAINLATER = "Something goes wrong.Please try again later";
        public const string DUPLICATESUBJECTNAME = "Duplicate Subject Name.Please Choose Another Name.";
        public const string SAVESUCCESSFULLY = "Saved Successfully.";
        public const string SUBJECTADDEDSUCCESS = "Subject Added Successfully.";
        public const string SUBJECT_Remove_SUCESS = "Subject removed successfully.";
        public const string INVALIDSUBJECTID = "Subject ID is not valid.";
        public const string PROFILESAVESUCCESS = "Profile has been saved successfully.";
        public const string PASSWORDCHANGESUCCESS = "Password has been changed successfully.";
        public const string OLDPASSWORDINCORRECT = "Incorrect old password.";
        public const string KEYWORD_ADDED_SUCESS = "Keyword added successfully.";
        public const string KEYWORD_Remove_SUCESS = "Keyword removed successfully.";
        public const string SEPERATOR_SYMBOL = "Ω";
        public const string NOTFOUND = "Not Found";
        public const string SaveSucessfully = "Saved sucessfully.";
        public const string UserExists = "Users does not exists!";
        public const string GROUP_Remove_SUCESS = "Group removed successfully.";
        public const string REMOVE_SUCCESS = "Removed successfully.";

        //date Constants
        public const string DATEFORMATMDY = "MM/DD/YY";

        //file Constants
        public static string[] ValidFileExtentions = {"png", "jpg", "jpeg", "mp3", "mp4", "wmv", "avi", "gif", "tif", "zip", "xls", "xlsx", "doc", "docx", "pdf", "txt", "rtf", "rar", "flv", "acc"};
}
}
public enum RolesMaster : short
{
    SuperAdmin = 5,
    Admin = 0,
    Teacher = 1,
    Student = 2,
    Leader = 4
}





