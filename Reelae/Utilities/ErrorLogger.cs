﻿using System;
using System.IO;
using System.Net;
using System.Web;
using Ninject;
using Reelae.Repositories;
namespace ReelaeBusinessLayer.Utilities
{

    class ErrorLogger
    {
        public static string strLogFilePath = string.Empty;
        private static StreamWriter sw = null;
        [Inject]
        ILogRepository _logRepo { get; set; }
        public ErrorLogger(ILogRepository __logRepo)
        {
            _logRepo = __logRepo;
        }
        /// <summary>
        /// Write Source,method,date,time,computer,error 
        /// and stack trace information to the text file
        /// 
        /// <param name="strPathName"></param>
        /// <param name="objException"></param>
        /// /// <param name="FormName"></param>
        /// <param name="EventName"></param>
        /// <RETURNS>false if the problem persists</RETURNS>
        ///</summary>
        public static bool WriteErrorLog(Exception objException, string Class, string Method)
        {
            string strPathName = GetLogFilePath();
            bool bReturn = false;
            string strException = string.Empty;
            try
            {
                sw = new StreamWriter(strPathName, true);
                sw.WriteLine("Class        : " + Class);
                sw.WriteLine("Event        : " + Method);
                sw.WriteLine("Source        : " + objException.Source.ToString().Trim());
                sw.WriteLine("Method        : " + objException.TargetSite.Name.ToString());
                sw.WriteLine("Date        : " + DateTime.Now.ToLongTimeString());
                sw.WriteLine("Time        : " + DateTime.Now.ToShortDateString());
                sw.WriteLine("Error        : " + objException.Message.ToString().Trim());
                sw.WriteLine("Stack Trace    : " + objException.StackTrace.ToString().Trim());
                sw.WriteLine("Version    : " + "3.0");
                sw.WriteLine("^^------------------------------------------------------------------ -^^ ");

                sw.Flush();
                sw.Close();
                bReturn = true;
            }
            catch (Exception)
            {
                bReturn = false;
            }
            return bReturn;
        }
        private static string GetLogFilePath()
        {
            try
            {
                // get the base directory

                // search the file below the current directory
                string retFilePath = HttpContext.Current.Server.MapPath("~/Logs/ErrorLogFile" + DateTime.Now.Date.ToString("MMddyy") + ".txt");

                // if exists, return the path
                if (File.Exists(retFilePath) == true)
                    return retFilePath;
                //create a text file
                else
                {
                    CheckDirectory("Logs");
                    File.Create(retFilePath);
                    return retFilePath;
                }
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }
        private static bool CheckDirectory(string strLogPath)
        {
            try
            {


                if (false == Directory.Exists(strLogPath))
                    Directory.CreateDirectory(strLogPath);
                return true;
            }
            catch (Exception)
            {
                return false;

            }
        }

        #region [LogToDB] 
        public bool WriteDBLog(ReelaeDataAccessLayer.ErrorLog Data, Exception exception)
        {
            try
            {
                _logRepo.Add(Data);
                return true;
            }
            catch (Exception)
            {
                try
                {
                    WriteErrorLog(exception, Data.PageName, Data.MethodName);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
        #endregion
    }



}

