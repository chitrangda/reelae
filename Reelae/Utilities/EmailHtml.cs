﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Reelae.Utilities
{
    public class EmailHtml
    {
        public static string getAdminHtml(string _email, string path)
        {
            StringBuilder sbAdmin = new StringBuilder();


            sbAdmin.Append("<table cellpadding='0' cellspacing='0' border='0' width='600' style='margin:0px auto;'>"
                            + "<tr><td valign='top' style= 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color: #333333'>"
                            + "<table width='570' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>"
                            + "<tbody><tr>"
                            + "<td style='height:25px;'>&nbsp;</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td>"
                            //<!-- header start--->
                            + "<table width='520' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>");
            sbAdmin.Append("<tr>"
                            + "<td style='text-align:center'>"
                            + "<img src='" + GetApplicationPath() + "/Content/images/logo.jpg' alt='Reelae'/></td>"
                            + "</tr>"
                            //<!-- header end--->
                            + "<tbody><tr><td style='height:30px;'>&nbsp;</td></tr>"
                            //<!-- starts content --->
                            + "<tr>"
                            + "<td style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'><p>Hi <strong> Admin</strong>,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>" + _email + " has requested access.<br/><br/> To activate this account please click on the following link. </p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>" + path + "</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Thanks,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'> Customer Service Team Reelae </p>"
                            + "</tr>"
                            + "</tbody>"
                            + "</table>"
                            + "</td>"
                            + "</tr>"
                            //<!-- content end-->
                            + "</table>");
            return sbAdmin.ToString();

        }

        public static string getResetPasswordHtml(string _name, string path)
        {
            StringBuilder sbAdmin = new StringBuilder();


            sbAdmin.Append("<table cellpadding='0' cellspacing='0' border='0' width='600' style='margin:0px auto;'>"
                            + "<tr><td valign='top' style= 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color: #333333'>"
                            + "<table width='570' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>"
                            + "<tbody><tr>"
                            + "<td style='height:25px;'>&nbsp;</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td>"
                            //<!-- header start--->
                            + "<table width='520' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>");
            sbAdmin.Append("<tr>"
                            + "<td style='text-align:center'>"
                            + "<img src='" + GetApplicationPath() + "/Content/images/logo.jpg' alt='Reelae'/></td>"
                            + "</tr>"
                            //<!-- header end--->
                            + "<tbody><tr><td style='height:30px;'>&nbsp;</td></tr>"
                            //<!-- starts content --->
                            + "<tr>"
                            + "<td style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'><p>Hi <strong>" + _name + "</strong>,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'> Please reset your password by clicking <a href='"+path+"' >Reset Password</a></p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Thanks,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'> Customer Service Team Reelae </p>"
                            + "</tr>"
                            + "</tbody>"
                            + "</table>"
                            + "</td>"
                            + "</tr>"
                            //<!-- content end-->
                            + "</table>");
            return sbAdmin.ToString();

        }
        public static string getAccountActivatedHtml(string _name, string _email, string path)
        {
            StringBuilder sbAdmin = new StringBuilder();


            sbAdmin.Append("<table cellpadding='0' cellspacing='0' border='0' width='600' style='margin:0px auto;'>"
                            + "<tr><td valign='top' style= 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color: #333333'>"
                            + "<table width='570' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>"
                            + "<tbody><tr>"
                            + "<td style='height:25px;'>&nbsp;</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td>"
                            //<!-- header start--->
                            + "<table width='520' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>");
            sbAdmin.Append("<tr>"
                            + "<td style='text-align:center'>"
                            + "<img src='" + GetApplicationPath() + "/Content/images/logo.jpg' alt='Reelae'/></td>"
                            + "</tr>"
                            //<!-- header end--->
                            + "<tbody><tr><td style='height:30px;'>&nbsp;</td></tr>"
                            //<!-- starts content --->
                            + "<tr>"
                            + "<td style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'><p>Hi <strong>" + _name + "</strong>,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>" + _email + " account has been activated by admin, to login please click on the following link: </p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>" + path + "</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Thanks,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'> Customer Service Team Reelae </p>"
                            + "</tr>"
                            + "</tbody>"
                            + "</table>"
                            + "</td>"
                            + "</tr>"
                            //<!-- content end-->
                            + "</table>");
            return sbAdmin.ToString();

        }

        public static string getUserAddHtml(string _name, string _loginid, string _password, string role, string path)
        {
            StringBuilder sbAdmin = new StringBuilder();


            sbAdmin.Append("<table cellpadding='0' cellspacing='0' border='0' width='600' style='margin:0px auto;'>"
                            + "<tr><td valign='top' style= 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color: #333333'>"
                            + "<table width='570' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>"
                            + "<tbody><tr>"
                            + "<td style='height:25px;'>&nbsp;</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td>"
                            //<!-- header start--->
                            + "<table width='520' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>");
            sbAdmin.Append("<tr>"
                            + "<td style='text-align:center'>"
                            + "<img src='" + GetApplicationPath() + "/Content/images/logo.jpg' alt='Reelae'/></td>"
                            + "</tr>"
                            //<!-- header end--->
                            + "<tbody><tr><td style='height:30px;'>&nbsp;</td></tr>"
                            //<!-- starts content --->
                            + "<tr>"
                            + "<td style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'><p>Hi <strong>" + _name + "</strong>,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Your account has been created as " + role + ". Now you can access your account using below credentials: </p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Login Id: " + _loginid + "</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Password: " + _password + "</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Please click here for login <a href='" + path + "'>" + path + "</a></p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Thanks,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'> Customer Service Team Reelae </p>"
                            + "</tr>"
                            + "</tbody>"
                            + "</table>"
                            + "</td>"
                            + "</tr>"
                            //<!-- content end-->
                            + "</table>");
            return sbAdmin.ToString();

        }

        public static string GetApplicationPath()
        {
            return "http://" + System.Web.HttpContext.Current.Request.Url.Authority + System.Web.HttpContext.Current.Request.ApplicationPath;
        }

        public static string getAccountStatusHtml(string _name, string _email, string path, string status)
        {
            StringBuilder sbAdmin = new StringBuilder();


            sbAdmin.Append("<table cellpadding='0' cellspacing='0' border='0' width='600' style='margin:0px auto;'>"
                            + "<tr><td valign='top' style= 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color: #333333'>"
                            + "<table width='570' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>"
                            + "<tbody><tr>"
                            + "<td style='height:25px;'>&nbsp;</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td>"
                            //<!-- header start--->
                            + "<table width='520' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>");
            sbAdmin.Append("<tr>"
                            + "<td style='text-align:center'>"
                            + "<img src='" + GetApplicationPath() + "/Content/images/logo.jpg' alt='Reelae'/></td>"
                            + "</tr>"
                            //<!-- header end--->
                            + "<tbody><tr><td style='height:30px;'>&nbsp;</td></tr>"
                            //<!-- starts content --->
                            + "<tr>"
                            + "<td style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'><p>Hi <strong>" + _name + "</strong>,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>" + _email + " account has been   " + status + " by admin");
            if (status == "activated")
            {
                sbAdmin.Append("<p>To login please click on the following link: </p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>" + path + "</p>");
            }
            else
            {
                sbAdmin.Append(".");
            }
            sbAdmin.Append(
                        "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Thanks,</p>"
                        + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'> Customer Service Team Reelae </p>"
                        + "</tr>"
                        + "</tbody>"
                        + "</table>"
                        + "</td>"
                        + "</tr>"
                        //<!-- content end-->
                        + "</table>");
            return sbAdmin.ToString();

        }

    }
}