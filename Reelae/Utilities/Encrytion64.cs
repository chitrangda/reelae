﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Configuration;
using System.Web;
using Reelae.Utilities;

namespace ReelaeBusinessLayer.Utilities
{
    public class Encryption64
    {
        
        static string Cypher = ConfigurationManager.AppSettings["cryptString"].ToString();
        #region Fields
        private static byte[] key = { };
        private static byte[] IV = { 38, 55, 206, 48, 28, 64, 20, 16 };
        
        private static string stringKey = Cypher;
        #endregion

        #region Public Methods

        public static string Encrypt(string text)
        {
            try
            {

                key = Encoding.UTF8.GetBytes(stringKey.Substring(0, 8));

                DESCryptoServiceProvider des = new DESCryptoServiceProvider();

                Byte[] byteArray = Encoding.UTF8.GetBytes(text);

                MemoryStream memoryStream = new MemoryStream();

                CryptoStream cryptoStream = new CryptoStream(memoryStream, des.CreateEncryptor(key, IV), CryptoStreamMode.Write);

                cryptoStream.Write(byteArray, 0, byteArray.Length);

                cryptoStream.FlushFinalBlock();

                return Convert.ToBase64String(memoryStream.ToArray());
            }

            catch (Exception ex)
            {

                ErrorLogger.WriteErrorLog(ex, "Encryption64", "Encrypt");

            }

            return string.Empty;

        }


        public static string Decrypt(string text)
        {
            try
            {

                key = Encoding.UTF8.GetBytes(stringKey.Substring(0, 8));

                DESCryptoServiceProvider des = new DESCryptoServiceProvider();

                Byte[] byteArray = Convert.FromBase64String(text);

                MemoryStream memoryStream = new MemoryStream();

                CryptoStream cryptoStream = new CryptoStream(memoryStream,

                    des.CreateDecryptor(key, IV), CryptoStreamMode.Write);

                cryptoStream.Write(byteArray, 0, byteArray.Length);

                cryptoStream.FlushFinalBlock();

                return Encoding.UTF8.GetString(memoryStream.ToArray());

            }

            catch (Exception ex)
            {
                ErrorLogger.WriteErrorLog(ex, "Encryption64", "Decrypt");

            }



            return string.Empty;

        }



        #endregion


        #region - encrypt and decrypt querystrings -

        public static string encryptString(string _params)
        {
            string cryptString = @"" + _params;
            string returnString;
            // get cypher string from web.config file
            string cypher = ConfigurationManager.AppSettings["cryptString"];
            // encrypt string and then replace instances of '&' so that the string does not break
            #region Removing all Special Charters - Added By Sumit On 29-07-15
            // Current      Replaced
            // +            :
            // /            ^
            // =            | 
            #endregion

            returnString = Encrypt11(_params, cypher).Replace("&", "(~~)").Replace('=', '|').Replace('+', ':').Replace('/', '^');
            // return value
            returnString = HttpUtility.UrlEncode(returnString);
            return returnString;
        }
        public static string decryptString(string _params)
        {
            if (_params != "0" && _params != null && _params != "")
            {
                _params = HttpUtility.UrlDecode(_params);
                string cryptString = _params.ToString().Replace(" ", "+");
                string returnString;
                // get cypher string from web.config file
                string cypher = ConfigurationManager.AppSettings["cryptString"];
                // replace instances of "(~~)" with '&' to reverse what was done during encryption process
                #region Removing all Special Charters - Added By Sumit On 29-07-15
                // Current     Replaced
                // |            +
                // ^            /
                // -            = 
                returnString = Decrypt11(_params.Replace("(~~)", "&").Replace("|", "=").Replace(":", "+").Replace("^", "/"), cypher);
                #endregion

                // return value
                return returnString;
            }
            else
            {
                if (_params != "")
                {
                    return _params;
                }
                else
                {
                    return "0";
                }
            }
        }       

        #endregion

        #region - encrypt and decrypt methods -
        public static string Decrypt11(string stringToDecrypt, string sEncryptionKey)
        {
            byte[] key = { };
            byte[] IV = { 10, 20, 30, 40, 50, 60, 70, 80 };
            byte[] inputByteArray = new byte[stringToDecrypt.Length];
            MemoryStream ms = new MemoryStream();
            Encoding encoding = Encoding.UTF8;
            try
            {
                key = Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(stringToDecrypt);               
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
            }
            catch (System.Exception ex)
            {
                ErrorLogger.WriteErrorLog(ex, "Encryption64", "Decrypt11");
            }
            return encoding.GetString(ms.ToArray());
        }

        public static string Encrypt11(string stringToEncrypt, string sEncryptionKey)
        {
            byte[] key = { };
            byte[] IV = { 10, 20, 30, 40, 50, 60, 70, 80 };
            byte[] inputByteArray; //Convert.ToByte(stringToEncrypt.Length)
            MemoryStream ms = new MemoryStream();
            try
            {
                key = Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);
                
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
            }
            catch (System.Exception ex)
            {
                ErrorLogger.WriteErrorLog(ex, "Encryption64", "Encrypt11");

            }
            return Convert.ToBase64String(ms.ToArray());
        }
        #endregion

    }
}
