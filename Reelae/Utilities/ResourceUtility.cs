﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reelae.Utilities
{
    public class ResourceUtility
    {
        public string fileAbsoluteURL { get; set; }
        public string fileName { get; set; }
        public string fileType { get; set; }
        public string FileSize { get; set; }
    }
}