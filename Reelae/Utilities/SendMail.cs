using System;
using System.Data;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using System.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mime;
using System.Collections.Specialized;
using System.Collections;
using System.IO;
using System.Web.Security;
using System.Net.Configuration;

namespace Reelae.Utilities
{
    public class Mail
    {
        /// <summary>
        /// This method is responsible for sending mail.
        /// </summary>
        /// <param name="toEmail"></param>
        /// <param name="queryString"></param>
        /// <returns></returns>
        public static bool SendMail(string toEmail, string subject, string mailbody)
        {

            try
            {
                var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

                SmtpClient client = new SmtpClient();
                MailMessage message = new MailMessage();
                message.Subject = subject;
                if (toEmail.Contains(","))
                {
                    string[] recipents = toEmail.Split(',');
                    if(recipents.Length>0)
                    {
                        foreach(string rec in recipents)
                        {
                            message.To.Add(new MailAddress(rec));

                        }
                    }
                }
                else
                {
                    message.To.Add(new MailAddress(toEmail));
                }
                message.From = new MailAddress(smtpSection.From);
                message.Body = mailbody;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;


                client.Host = smtpSection.Network.Host; //---- client Host Details. 
                client.EnableSsl = smtpSection.Network.EnableSsl; //---- Specify whether host accepts SSL Connections or not.
                NetworkCredential NetworkCred = new NetworkCredential(smtpSection.Network.UserName, smtpSection.Network.Password);
                client.Credentials = NetworkCred;
                client.Port = smtpSection.Network.Port; //---- client Server port number. This varies from host to host. 
                client.EnableSsl = true;
                client.Send(message);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

    }
}
