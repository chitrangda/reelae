﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web;

namespace Reelae.Utilities
{
    public class CommonUtility
    {
        public static string ToTitleCase(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                StringBuilder sb = new StringBuilder("");
                input = input.Trim(' ');

                string[] words = input.Split(' ');
                foreach (var item in words)
                {
                    if (item == "")
                        continue;
                    sb.Append(item.First().ToString().ToUpper() + item.Substring(1));
                    sb.Append(" ");
                }
                return sb.ToString().Trim(' ');
            }
            else
            {
                return "";
            }

        }

        public static string GetGroupInitialName(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                StringBuilder sb = new StringBuilder("");
                input = input.Trim();

                string[] words = input.Split(' ');
                foreach (var item in words)
                {
                    sb.Append(item.First().ToString().ToUpper());
                }
                return sb.ToString();
            }
            else
            {
                return "DG";
            }

        }

        public static DateTime? GetDateTimeFromString(string stringDate, string format)
        {
            if (string.IsNullOrEmpty(stringDate))
                return null;
            string[] splitedDateParts = stringDate.Trim().Split(new char[] { '/', '-' });
            if (string.IsNullOrEmpty(splitedDateParts[0].Trim()) || string.IsNullOrEmpty(splitedDateParts[0].Trim()) || string.IsNullOrEmpty(splitedDateParts[0].Trim()))
                return null;
            if (MasterConstants.DATEFORMATMDY.ToLower() == format.ToLower())
            {
                return new DateTime(Convert.ToInt32(splitedDateParts[2].Trim()), Convert.ToInt32(splitedDateParts[0].Trim()), Convert.ToInt32(splitedDateParts[1].Trim()));
            }
            else
                return null;
        }

        public static string getTimeDuration(int? _duration)
        {
            string _time = _duration.ToString();
            if (_duration >= 60)
            {
                _time = Convert.ToString(_duration / 60);
                if (_time == "1")
                {
                    _time = _time + " Hour";
                }
                else
                {
                    _time = _time + " Hours";

                }
            }
            else if (_duration >= 1440)
            {
                _time = Convert.ToString(_duration / 1440);
                if (_time == "1")
                {
                    _time = _time + " Day";
                }
                else
                {
                    _time = _time + " Days";

                }

            }
            else if (_duration >= 43880)
            {
                _time = Convert.ToString(_duration / 43800);
                if (_time == "1")
                {
                    _time = _time + " Month";
                }
                else
                {
                    _time = _time + " Months";

                }

            }
            else if (_duration >= 525600)
            {
                _time = Convert.ToString(_duration / 525600);
                if (_time == "1")
                {
                    _time = _time + " Year";
                }
                else
                {
                    _time = _time + " Years";

                }

            }
            else
            {
                _time = _time + " Minutes";
            }
            return _time;
        }

        public static int PageSize
        {
            get { return int.Parse(System.Configuration.ConfigurationManager.AppSettings["PageSize"].ToString()); }
        }      

        public static string GetUserShortName(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                StringBuilder sb = new StringBuilder("");
                input = input.Trim();

                string[] words = input.Split(' ');
                foreach (var item in words)
                {
                    sb.Append(item.First().ToString().ToUpper());
                }
                return sb.ToString();
            }
            else
            {
                return "DU";
            }

        }
    }
    public class JsonTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<List<string>> data { get; set; }
    }
    public static class GetAssignmentStatus
    {
        public static string AssignmentStatus(this string assignmentId)
        {
            string status = string.Empty;
            switch (int.Parse(assignmentId))
            {
                case 0:
                    status = "Assigned";
                    break;
                case 1:
                    status = "In Progress";
                    break;
                case 2:
                    status = "Submit";
                    break;
                case 3:
                    status = "Review";
                    break;
                case 4:
                    status = "Archieve";
                    break;
                case 5:
                    status = "Cancelled";
                    break;
            }
            return status;
        }
    }
}