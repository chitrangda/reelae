﻿using ReelaeDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reelae.Models
{
    public class newTopicViewModel
    {
        public Forum forum { get; set; }
        public IEnumerable<Subject> SubjectList { get; set; }
    }
}