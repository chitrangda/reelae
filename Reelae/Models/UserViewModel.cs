﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Reelae.Utilities;
using ReelaeDataAccessLayer;

namespace Reelae.Models
{
    public class UserViewModel:User
    {
        public RolesMaster UserRole { get; set; }

        #region UserViewProps
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        #endregion

        public static bool Validate() {
            bool result = false;

            return result;
        }

    }
}