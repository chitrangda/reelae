﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReelaeDataAccessLayer;

namespace Reelae.Models
{
    public class MessageViewModal
    {
        public User _userinfo { get; set; }
        public IEnumerable<re_GetChatMembers_sp_Result> ActiveStudents { get; set; }
        public IEnumerable<re_GetChatMembers_sp_Result> ActiveTeachers { get; set; }
        public IEnumerable<re_GetChatMembers_sp_Result> Institutions { get; set; }

        public IEnumerable<re_getUserSingleChatUnreadMessagesCount_sp_Result> UesrsUnreadMessagesWithCount { get; set; }
    }

    public class ChatMessage
    {
        public string messageId { get; set; }
        public string messageToken { get; set; }
        public string type { get; set; }
        public string message { get; set; }
        public string sendType { get; set; }
        public string from { get; set; }
        public string sendTo { get; set; }
        public string attachmentUrl { get; set; }
        public string fromEmailId { get; set; }
        public string toEmailId { get; set; }
        public string messageDate { get; set; }
        public string userStatus { get; set; }
    }

    public class ChatUserDetails {
        public int UserID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string UserPicUrl { get; set; }
        public string UserType { get; set; }
        public string ChatStatus { get; set; }
        public bool IsOnline { get; set; }

    }

    public static class TaskConstants {
        public static string Offline = "offline";
        public static string Online = "online";
    }

}