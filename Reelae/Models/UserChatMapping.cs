﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reelae.Models
{
    public class UserChatMapping
    {
        private Dictionary<int, List<string>> CurrentMappingInformation;
        public UserChatMapping()
        {
            CurrentMappingInformation = new Dictionary<int, List<string>>();
        }

        public void AddUserId(int UserID)
        {
            if (!IsUserLoggedIn(UserID))
            {
                CurrentMappingInformation.Add(UserID, new List<string>());
            }
        }

        public void AddConnectionId(int ConnectionID, int UserID)
        {
            if (!IsConnectionIDExists(UserID, ConnectionID))
            {
                CurrentMappingInformation[UserID].Add(ConnectionID.ToString());
            }
        }
        public bool IsUserLoggedIn(int UserID)
        {
            return CurrentMappingInformation.ContainsKey(UserID);
        }
        public bool IsConnectionIDExists(int UserID, int ConnectionID)
        {
            if (IsUserLoggedIn(UserID))
            {
                return CurrentMappingInformation[UserID].Any(n => ConnectionID.ToString() == n.ToString());
            }
            else
                return false;
        }

        public void RemoveConnectionId(int ConnectionID, int UserID)
        {
            if (IsConnectionIDExists(UserID, ConnectionID))
            {
                CurrentMappingInformation[UserID].Remove(ConnectionID.ToString());
            }
        }

        public List<string> GetUserConnectionIds(int UserID)
        {
            if (IsUserLoggedIn(UserID))
            {
                return CurrentMappingInformation[UserID];
            }
            else
                return null;
        }
    }

    public class ConnectionMapping<T>
    {
        private readonly Dictionary<T, HashSet<string>> _connections =
            new Dictionary<T, HashSet<string>>();

        public int Count
        {
            get
            {
                return _connections.Count;
            }
        }

        public void Add(T key, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    connections = new HashSet<string>();
                    _connections.Add(key, connections);
                }

                lock (connections)
                {
                    connections.Add(connectionId);
                }
            }
        }

        public IEnumerable<string> GetConnections(T key)
        {
            HashSet<string> connections;

            if (_connections.TryGetValue(key, out connections))
            {
                return connections;
            }

            return Enumerable.Empty<string>();
        }

        public void Remove(T key, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    return;
                }

                lock (connections)
                {
                    connections.Remove(connectionId);

                    if (connections.Count == 0)
                    {
                        _connections.Remove(key);
                    }
                }
            }
        }

        public IEnumerable<string> GetOtherConnections(T key)
        {
            List<string> allConnections = new List<string>();
            foreach (var item in _connections)
            {
                if (!item.Key.Equals(key))
                {
                    allConnections.AddRange(item.Value);
                }
            }
            return allConnections;
        }

        public IEnumerable<T> GetOnlineConnectionsNames(T key)
        {
            if (_connections.Count > 0)
                return _connections.Keys.ToList().Where(n => !n.Equals(key));
            else
                return Enumerable.Empty<T>();
        }

        public IEnumerable<T> connectionNamesForConnectionId(IEnumerable<string> connectioIds, string userName)
        {
          return _connections.Where(n => n.Value.Any(y => connectioIds.Contains(y)) && !n.Key.Equals(userName)).Select(z => z.Key);

        }

    }


    public class ConnectionAssignmentGroupMapping<T>
    {
        private readonly Dictionary<T, HashSet<string>> _connections =
            new Dictionary<T, HashSet<string>>();

        public int Count
        {
            get
            {
                return _connections.Count;
            }
        }

        public void Add(T key, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    connections = new HashSet<string>();
                    _connections.Add(key, connections);
                }

                lock (connections)
                {
                    connections.Add(connectionId);
                }
            }
        }

        public IEnumerable<string> GetConnections(T key)
        {
            HashSet<string> connections;

            if (_connections.TryGetValue(key, out connections))
            {
                return connections;
            }

            return Enumerable.Empty<string>();
        }

        public void Remove(T key, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    return;
                }

                lock (connections)
                {
                    connections.Remove(connectionId);

                    if (connections.Count == 0)
                    {
                        _connections.Remove(key);
                    }
                }
            }
        }

        public IEnumerable<string> GetActiveAssignments(T key)
        {
            List<string> allConnections = new List<string>();
            foreach (var item in _connections)
            {
                if (!item.Key.Equals(key))
                {
                    allConnections.AddRange(item.Value);
                }
            }
            return allConnections;
        }

        public IEnumerable<T> GetOtherConnectionusers(T key)
        {
            if (_connections.Count > 0)
                return _connections.Keys.ToList().Where(n => !n.Equals(key));
            else
                return Enumerable.Empty<T>();
        }

        public IEnumerable<T> GetAssignmentMembers(string assignmentGroupName)
        {
            return _connections.Where(n => n.Value.Contains(assignmentGroupName)).Select(n => n.Key);
        }

    }
}