﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reelae.Models
{
    public class DashboardCustomizedItem
    {
        public int UserID { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public bool IsEnabled { get; set; }
    }
}