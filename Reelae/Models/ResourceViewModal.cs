﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReelaeDataAccessLayer;

namespace Reelae.Models
{
    public class ResourceViewModal
    {
        public User userInfo { get; set; }
        public Resource resourceInfo { get; set; }
        public IEnumerable<Resource> resourceList { get; set; }
    }

    public class DownloadActivity
    {
        string ResourceName { get; set; }
        string FileType { get; set; }
        public List<DownloadBy> DownloadBy { get; set; }
    }
    public class DownloadBy
    {
        string Name { get; set; }
        string PicURL { get; set; }
        int UserId { get; set; }
    }

    public class StudentResourceViewModal
    {
        public User userInfo { get; set; }
        public IEnumerable<re_getStudentResource_sp_Result> resourceList { get; set; }
    }
    public class EditResourceViewModal
    {
        public Resource resourceInfo { get; set; }
    }
    public class AddResourceViewModal
    {
        public Resource resourceInfo { get; set; }
    }

    public class AddTagResourceViewModel
    {
        public int ResourceId { get; set; }
        public Nullable<int> TagId { get; set; }
        public string TagName { get; set; }
        //resource Tags
        public IEnumerable<ReelaeDataAccessLayer.re_getResourceTags_sp_Result> ResourceTags { get; set; }
    }


    public class ResourceActivityChart
    {
        public string StudentName { get; set; }
        public Nullable<int> ActivityCount { get; set; }
        public string StudentPicUrl { get; set; }
    }

    public class ResourceVisitTimeByStudent
    {
        public string StudentName { get; set; }
        public Nullable<int> SpendTime { get; set; }
        public string StudentPicUrl { get; set; }
    }
}