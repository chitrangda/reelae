﻿using ReelaeDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reelae.Models
{
    public class ForumViewModel
    {
        public User _userinfo { get; set; }

        public IEnumerable<re_getForum_sp_Result> _forumTopics { get; set; }

        public IEnumerable<Subject> _subjects { get; set; }
       

    }
}