﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReelaeDataAccessLayer;
using Ninject;
using System.Data.Entity;
using System.Data;
using Reelae.Filters;

namespace Reelae.Repositories
{
    public interface IKeywordRepository
    {
        IEnumerable<KeyWord> Get(int id);
        //KeyWord Get(int id);
        int Add(string name, int institution, string user);
        void Remove(KeyWord entity);

        bool IsAlreadyExist(string name);

        void Remove(string keyword);

        #region [OthersMethods]
        IEnumerable<KeyWord> GetInstitutionKeywords(int? institutionID);
        #endregion
    }
    public class KeywordRepository : IKeywordRepository
    {
        [Inject]
         ReelaeEntities context { get; set; }
        public KeywordRepository(ReelaeEntities _context)
        {
            context = _context;
        }

        public int Add(string name, int institution, string user)
        {
            KeyWord entity = new KeyWord();
            entity.KeywordName = name;
            entity.InstitutionID = institution;
            entity.CreatedBy = user;
            entity.CreatedDate = DateTime.Now;
            context.KeyWords.Add(entity);
            context.SaveChanges();
            return entity.KeywordID;
        }

        public IEnumerable<KeyWord> Get(int id)
        {
            return context.KeyWords.Where(r => r.InstitutionID == id).ToList();
        }


        //public KeyWord Get(int id)
        //{
        //    return context.KeyWords.Find(id);
        //}

        public void Remove(KeyWord entity)
        {
            var obj = context.KeyWords.Find(entity.KeywordID);
            context.KeyWords.Remove(obj);
            context.SaveChanges();
        }

        #region [Other Methods]
        public IEnumerable<KeyWord> GetInstitutionKeywords(int? institutionID)
        {
            return context.KeyWords.Where(n => n.InstitutionID == institutionID).ToList();
        }

        #region [KeywordCreation]

        #endregion

        #endregion

        public bool IsAlreadyExist(string name)
        {
            if(context.KeyWords.Where(r=>r.KeywordName.ToLower()==name.Trim().ToLower()).ToList().Count>0 )
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Remove(string keyword)
        {
            var obj = context.KeyWords.Where(r => r.KeywordName == keyword).FirstOrDefault();
            if(obj != null)
            {
                context.KeyWords.Remove(obj);
                context.SaveChanges();
            }
        }

    }
}
