﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReelaeDataAccessLayer;
using Ninject;
using System.Data.Entity;
using System.Data;
using Reelae.Filters;

namespace Reelae.Repositories
{

    public interface IThemeRepository
    {
        int Add(Theme entity);
        IEnumerable<Theme> Get();
        Theme Get(int id);
        void Remove(Theme entity);

    }
    public class ThemeRepository : IThemeRepository
    {
        [Inject]
        public ReelaeEntities context { get; set; }
        public int Add(Theme entity)
        {
            context.Themes.Add(entity);
            context.Entry(entity).GetDatabaseValues();
            return entity.ThemeID;
        }

        public IEnumerable<Theme> Get()
        {
            return context.Themes.ToList();
        }

        public Theme Get(int id)
        {
            return context.Themes.Find(id);
        }

        public void Remove(Theme entity)
        {
            var obj = context.Themes.Find(entity.ThemeID);
            context.Themes.Remove(obj);
            context.SaveChanges();
        }
    }
}
