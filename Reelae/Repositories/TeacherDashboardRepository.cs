﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReelaeDataAccessLayer;
using Ninject;

namespace Reelae.Repositories
{
    public interface ITeacherDashboardRepository
    {
        IEnumerable<re_GetTeacherDashboardCount_sp_Result> GetDashbordCount(int institutionId, int id,DateTime? StartDate,DateTime? EndDate);
    }
    public class TeacherDashboardRepository : ITeacherDashboardRepository
    {
        [Inject]
        ReelaeEntities context { get; set; }
        public TeacherDashboardRepository(ReelaeEntities _context)
        {
            context = _context;
        }
        public IEnumerable<re_GetTeacherDashboardCount_sp_Result> GetDashbordCount(int institutionId, int id, DateTime? StartDate, DateTime? EndDate)
        {
            return context.re_GetTeacherDashboardCount_sp(institutionId, id,StartDate,EndDate);
        }
    }
}