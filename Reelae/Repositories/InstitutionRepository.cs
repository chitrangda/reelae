﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReelaeDataAccessLayer;
using Ninject;
using System.Data.Entity;
using System.Data;
using Reelae.Filters;

namespace Reelae.Repositories
{

    public interface IInstitutionRepository
    {
        IEnumerable<Institution> Get();
        Institution Get(int id);
        int Add(Institution entity);
        void Remove(Institution entity);

        void updateType(int id, string type);

        void update(Institution entity, string userid);
        //changes_04082017

        int[] GetTeacherStudentCount(int? institutionID, int? userID);

        bool IsInstitutionInitialNameAlereadyExists(string initialName,int? institutionid);
    }

    [LoggingFilterAttribute]
    public class InstitutionRepository : IInstitutionRepository
    {
        [Inject]
        ReelaeEntities context;
        //ReelaeEntities context = new ReelaeEntities();
        public InstitutionRepository(ReelaeEntities _context)
        {
            context = _context;
        }
        public int Add(Institution entity)
        {
            try
            {

                context.Institutions.Add(entity);
                context.SaveChanges();
                context.Entry(entity).GetDatabaseValues();
                return entity.InstitutionID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public IEnumerable<Institution> Get()
        {
            return context.Institutions.ToList();
        }

        public Institution Get(int id)
        {
            return context.Institutions.Find(id);
        }

        public void Remove(Institution entity)
        {
            var obj = context.Institutions.Find(entity.InstitutionID);
            context.Institutions.Remove(obj);
            context.SaveChanges();
        }




        public int[] GetTeacherStudentCount(int? institutionID, int? userID)
        {
            re_get_StudentteacherCount_InstitutionID_sp_Result result = context.re_get_StudentteacherCount_InstitutionID_sp(institutionID, userID).FirstOrDefault();
            int[] _Count = { 0, 0 };
            if (result != null)
            {
                _Count[0] = (result.StudentCount.HasValue) ? result.StudentCount.Value : 0;
                _Count[1] = (result.TeacherCount.HasValue) ? result.TeacherCount.Value : 0;
            }
            return _Count;
        }

        public void updateType(int id, string type)
        {
            try
            {
                var obj = context.Institutions.Find(id);
                obj.InstitutionType = type;
                context.SaveChanges();
            }
            catch
            {

            }
        }
        public void update(Institution entity,string userid)
        {
            try
            {
                //if (entity.ThemeID == null)
                //{
                //    Theme entityTheme = new Theme() { ThemeName = entity.Theme.ThemeName, ThemeDescription = "", ColorCode = entity.Theme.ColorCode };
                //    context.Themes.Add(entityTheme);
                //    context.SaveChanges();
                //    entity.ThemeID = entityTheme.ThemeID;
                //    entity.Theme.ThemeID = entityTheme.ThemeID;
                //}
                Institution _institution = context.Institutions.Find(entity.InstitutionID);
                if (_institution != null)
                {
                    _institution.ThemeID = context.Themes.Where(r => r.ColorCode == entity.Theme.ColorCode).Select(r => r.ThemeID).SingleOrDefault();
                    _institution.InstitutionName = entity.InstitutionName;
                    _institution.Logo = entity.Logo;
                    _institution.ModifiedBy = userid;
                    _institution.ModifiedDate = DateTime.Now;
                    _institution.IntialName = entity.IntialName;
                    context.SaveChanges();
                }

            }
            catch (Exception)
            {

            }
        }

        public bool IsInstitutionInitialNameAlereadyExists(string initialName,int? institutionid)
        {
            return context.Institutions.Any(n => n.IntialName.ToLower() == initialName && n.InstitutionID!= institutionid);
        }
    }
}
