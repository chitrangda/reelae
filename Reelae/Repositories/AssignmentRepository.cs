﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReelaeDataAccessLayer;
using Ninject;
using System.Data.Entity;
using System.Data;
using Reelae.Filters;

namespace Reelae.Repositories
{

    public interface IAssignmentRepository
    {
        IEnumerable<Assignment> Get();
        Assignment Get(int id);
        void Add(ref Assignment entity);
        void Remove(Assignment entity);

        #region [OthersMethods]
        IEnumerable<Assignment> GetStudentsFilteredActiveAssignments(int? studentID, DateTime? FilterSD, DateTime? FilterED);

        IEnumerable<User> GetAssignmentMembersProfile(int? studentid, int? assignmentid);

        #endregion

        IEnumerable<re_TeacherAssignmentList_sp_Result> GetAssignmentList(int? teacherid, int? groupid);
        IEnumerable<re_TeacherAssignmentList_sp_Result> GetAssignmentList(int? teacherid, int? groupid, int? pageNumber);
        IEnumerable<re_getStudentAssignment_sp_Result> GetStudentAssignmentList(int? StudentId, int? GroupId);
        IEnumerable<re_getStudentAssignment_sp_Result> GetStudentAssignmentList(int? StudentId, int? GroupId, int? pageNumber);
        IEnumerable<re_TeacherViewSubmission_sp_Result> GetTeacherViewSubmissionassignment(int? teacherid, int? groupid, int? pageNumber);
        void ArchiveAssignment(int? id);
        void CancelAssignment(int? id);
        void Update(ref Assignment entity);
        //workspace methods
        IEnumerable<re_getAssignmentWorkspaceContent_sp_Result> GetAssignmentWorkSpaceContent(int? assignmentId);

        void UpdateAssignmentWorkSpaceContent(int? assignmentId, int? studentId, string HtmlContent, string userEmail, int keyWordCount);
        IEnumerable<AssignmentWorkspace> GetAssignmentCountData(int? assignmentId);
        IEnumerable<Assignment> GetTeacherAssignmentList(int? teacherid);
        IEnumerable<Assignment> GetTeacherAssignmentList(int? teacherid, int? groupId);

        IEnumerable<re_getStudentKeywordInformation_sp_Result> GetAssignmentCountUsesData(int? assignmentId);

        void UpdateAssignmentWorkSpaceKeywordData(int? assignmentId, int? keywordId, int? studentId, int? count);

        IEnumerable<AssignmentWorkspace_Keywordcount> GetStudentKeywordWiseData(int? assignmentId, int? studentId);

    }

    [LoggingFilterAttribute]
    public class AssignmentRepository : IAssignmentRepository
    {
        [Inject]
        ReelaeEntities context { get; set; }
        public AssignmentRepository(ReelaeEntities _context)
        {
            context = _context;
        }
        public void Add(ref Assignment entity)
        {
            context.Assignments.Add(entity);
            context.SaveChanges();
        }

        public IEnumerable<Assignment> Get()
        {
            return context.Assignments.ToList();
        }

        public Assignment Get(int id)
        {
            return context.Assignments.Find(id);
        }

        public void Remove(Assignment entity)
        {
            var obj = context.Assignments.Find(entity.GroupID);
            context.Assignments.Remove(obj);
            context.SaveChanges();
        }

        public IEnumerable<Assignment> GetStudentsFilteredActiveAssignments(int? studentID, DateTime? FilterSD, DateTime? FilterED)
        {
            var studentGroups = context.GroupAssociations.Where(n => n.StudentID == studentID && n.IsActive == 1).Select(y => y.GroupID).ToList();
            if (FilterSD == null && FilterED == null)
            {
                return context.Assignments.Where(n => studentGroups.Any(y => y == n.GroupID) && n.AssignmentStatus == 1);
            }
            else
            {
                return context.Assignments.Where(n => studentGroups.Any(y => y == n.GroupID) && n.AssignmentStatus == 1 && ((n.AssignmentDate <= FilterSD && n.DueDate >= FilterSD) || (n.AssignmentDate <= FilterED && n.DueDate >= FilterED) || (n.AssignmentDate >= FilterSD && n.DueDate <= FilterED)));
            }
        }

        public IEnumerable<User> GetAssignmentMembersProfile(int? studentid, int? assignmentid)
        {
            var assignment = context.Assignments.Where(n => n.AssignmentID == assignmentid).FirstOrDefault();
            var studentIds = context.GroupAssociations.Where(n => n.GroupID == assignment.GroupID && n.IsActive == 1).Select(y => y.StudentID);
            return context.Users.Where(n => studentIds.Contains(n.UserID));
        }

        public IEnumerable<re_TeacherAssignmentList_sp_Result> GetAssignmentList(int? teacherid, int? groupid)
        {
            return context.re_TeacherAssignmentList_sp(teacherid, groupid, 10, 0);
        }

        //for paging only
        public IEnumerable<re_TeacherAssignmentList_sp_Result> GetAssignmentList(int? teacherid, int? groupid, int? pageNumber)
        {
            return context.re_TeacherAssignmentList_sp(teacherid, groupid, Utilities.CommonUtility.PageSize, pageNumber);
        }
        public IEnumerable<re_getStudentAssignment_sp_Result> GetStudentAssignmentList(int? StudentId, int? GroupId)
        {
            return context.re_getStudentAssignment_sp(StudentId, GroupId, Utilities.CommonUtility.PageSize, 0);
        }
        //student assignment
        public IEnumerable<re_getStudentAssignment_sp_Result> GetStudentAssignmentList(int? StudentId, int? GroupId, int? pageNumber)
        {
            return context.re_getStudentAssignment_sp(StudentId, GroupId, Utilities.CommonUtility.PageSize, pageNumber);
        }
        public IEnumerable<re_TeacherViewSubmission_sp_Result> GetTeacherViewSubmissionassignment(int? teacherid, int? groupid)
        {
            return context.re_TeacherViewSubmission_sp(teacherid, groupid, Utilities.CommonUtility.PageSize, 0);
        }

        public IEnumerable<re_TeacherViewSubmission_sp_Result> GetTeacherViewSubmissionassignment(int? teacherid, int? groupid, int? pageNumber)
        {
            if (groupid == -1)
            {
                return context.re_TeacherViewSubmission_sp(teacherid, 0, Utilities.CommonUtility.PageSize, pageNumber).Where(x => x.GroupName == "Individual");
            }
            return context.re_TeacherViewSubmission_sp(teacherid, groupid, Utilities.CommonUtility.PageSize, pageNumber);
        }


        //Archive Assignment
        public void ArchiveAssignment(int? id)
        {
            var result = context.Assignments.Find(id);
            if (result.AssignmentStatus != 1)
            {
                var originalAssignment = new Assignment_Archieve()
                {
                    AssignmentID = result.AssignmentID,
                    AssignmentName = result.AssignmentName,
                    Description = result.Description,
                    GradeID = result.GradeID,
                    GroupID = result.GroupID,
                    AssignmentDate = result.AssignmentDate,
                    DueDate = result.DueDate,
                    CreatedDate = result.CreatedDate,
                    CreatedBy = result.CreatedBy,
                    ModifiedDate = result.ModifiedDate,
                    ModifiedBy = result.ModifiedBy,
                    SubjectID = result.SubjectID,
                    SubmissionDate = result.SubmissionDate,
                    SubmissionBy = result.SubmissionBy,
                    ReviewDate = result.ReviewDate,
                    ReviewBy = result.ReviewBy,
                    AssignmentStatus = result.AssignmentStatus,
                    TeacherId = result.TeacherId,
                    LeaderID = result.LeaderID,
                };

                context.Assignment_Archieve.Add(originalAssignment);
                context.Assignments.Remove(result);
                context.SaveChanges();
            }
        }

        //Cancle Assignment
        public void CancelAssignment(int? id)
        {
            var result = context.Assignments.Find(id);
            result.AssignmentStatus = 5;
            context.SaveChanges();
        }
        public void Update(ref Assignment entity)
        {
            var assignments = this.Get(entity.AssignmentID);
            if (assignments != null)
            {
                assignments.AssignmentDate = entity.AssignmentDate;
                assignments.DueDate = entity.DueDate;
                assignments.GroupID = entity.GroupID;
                assignments.LeaderID = entity.LeaderID;
                assignments.AssignmentName = entity.AssignmentName;
                assignments.Description = entity.Description;
                assignments.ModifiedDate = entity.ModifiedDate;
                assignments.ModifiedBy = entity.ModifiedBy;
                assignments.WordsGoal = entity.WordsGoal;
                context.SaveChanges();
            }
        }

        public IEnumerable<re_getAssignmentWorkspaceContent_sp_Result> GetAssignmentWorkSpaceContent(int? assignmentId)
        {
            return context.re_getAssignmentWorkspaceContent_sp(assignmentId);
        }

        public void UpdateAssignmentWorkSpaceContent(int? assignmentId, int? studentId, string HtmlContent, string userEmail, int keyWordCount)
        {
            var curAssignWorkspace = context.AssignmentWorkspaces.Where(n => n.AssignmentId == assignmentId && n.StudentID == studentId).FirstOrDefault();
            if (curAssignWorkspace != null)
            {
                curAssignWorkspace.Content = HtmlContent;
                curAssignWorkspace.ModifiedBy = userEmail;
                curAssignWorkspace.ModifiedDate = DateTime.UtcNow;
                curAssignWorkspace.WordCount = keyWordCount;
            }
            else
            {
                curAssignWorkspace = new AssignmentWorkspace()
                {
                    Content = HtmlContent,
                    CreatedBy = userEmail,
                    CreatedDate = DateTime.UtcNow,
                    AssignmentId = assignmentId,
                    StudentID = studentId,
                    WordCount = keyWordCount
                };
                context.AssignmentWorkspaces.Add(curAssignWorkspace);
            }
            context.SaveChanges();

        }

        public IEnumerable<AssignmentWorkspace> GetAssignmentCountData(int? assignmentId)
        {
           return  context.AssignmentWorkspaces.Where(x => x.AssignmentId == assignmentId).ToList();         
        }
        public IEnumerable<Assignment> GetTeacherAssignmentList(int? teacherid)
        {
            return context.Assignments.Where(x => x.TeacherId == teacherid).ToList();
        }
        public IEnumerable<Assignment> GetTeacherAssignmentList(int? teacherid, int? groupId)
        {
            return context.Assignments.Where(x => x.TeacherId == teacherid && x.GroupID== groupId).ToList();
        }
        public IEnumerable<re_getStudentKeywordInformation_sp_Result> GetAssignmentCountUsesData(int? assignmentId)
        {
            return context.re_getStudentKeywordInformation_sp(assignmentId);
        }

        public void UpdateAssignmentWorkSpaceKeywordData(int? assignmentId, int? keywordId, int? studentId, int? count)
        {
            var keywordRec = context.AssignmentWorkspace_Keywordcount.Where(n => n.AssignmentId == assignmentId && n.KeywordId == keywordId && n.StudentId == studentId).FirstOrDefault();
            if (keywordRec != null)
            {
                keywordRec.KeywordCount = count;
            }
            else
            {
                context.AssignmentWorkspace_Keywordcount.Add(new AssignmentWorkspace_Keywordcount { AssignmentId = assignmentId, StudentId = studentId, KeywordId = keywordId, KeywordCount = count });
            }
            context.SaveChanges();
        }

        public IEnumerable<AssignmentWorkspace_Keywordcount> GetStudentKeywordWiseData(int? assignmentId, int? studentId)
        {
            return context.AssignmentWorkspace_Keywordcount.Where(n => n.AssignmentId == assignmentId && n.StudentId == studentId);
        }
    }
}
