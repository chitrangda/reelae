﻿using Ninject;
using ReelaeDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reelae.Repositories
{
    public interface IAttendanceRepository
    {
        void remove(List<re_getAttendance_sp_Result> _students);

        List<re_getAttendance_sp_Result> get(int? SubjectId, int? TeacherId, DateTime? AttendanceDate);

        void save(List<re_getAttendance_sp_Result> studentAttendence,DateTime? _date ,int? subjectId, int? teacherId, string username);

        IEnumerable<re_select_attendence_participatingrating_sp_Result> getAll(int? SubjectId, int? TeacherId, DateTime? StartDate, DateTime? EndDate);

        IEnumerable<re_getStudentAttendance_Result> Get(int studentId);
    }
    public class AttendanceRepository : IAttendanceRepository
    {
        [Inject]
        ReelaeEntities context { get; set; }

        public AttendanceRepository(ReelaeEntities _context)
        {
            context = _context;
        }

        public void remove(List<re_getAttendance_sp_Result> _students)
        {
            foreach(var _student in _students)
            {
                var obj = context.Attendences.Find(_student.AttendenceID);
                if(obj!=null)
                {
                    context.Attendences.Remove(obj);
                    context.SaveChanges();
                }
            }
        }

        public List<re_getAttendance_sp_Result> get(int? SubjectId, int? TeacherId, DateTime? AttendanceDate)
        {
            return context.re_getAttendance_sp(SubjectId, TeacherId, AttendanceDate).ToList();
        }

        public void save(List<re_getAttendance_sp_Result> studentAttendence, DateTime? _date,int? subjectId,int? teacherId,string username)
        {
            var _studentAttendence = studentAttendence.Where(r => r.AttendenceMarK != null).ToList();
            foreach(var _student in _studentAttendence)
            {
                if (_student.AttendenceID != null)
                {
                    var obj = context.Attendences.Find(_student.AttendenceID);
                    if (obj != null)
                    {
                        obj.AttendenceMarK = _student.AttendenceMarK;
                        obj.AttendenceTime = _student.AttendenceTime;
                        obj.Rating = _student.Rating;
                        obj.ModifiedBy = username;
                        obj.ModifiedDate = DateTime.Now;
                        context.SaveChanges();
                    }
                }
                else
                {
                    if (_student.AttendenceMarK != null)
                    {
                        Attendence entity = new Attendence() { AttendenceDate = _date, MemberID = _student.UserID, SubjectID = subjectId, TeacherID = teacherId, CreatedBy = username, CreatedDate = DateTime.Now, AttendenceMarK = _student.AttendenceMarK,Rating=_student.Rating,AttendenceTime=_student.AttendenceTime };
                        context.Attendences.Add(entity);
                        context.SaveChanges();
                    }

                }
            }
        }

        public IEnumerable<re_select_attendence_participatingrating_sp_Result> getAll(int? SubjectId, int? TeacherId,DateTime? StartDate,DateTime? EndDate)
        {
            return context.re_select_attendence_participatingrating_sp(SubjectId, TeacherId,StartDate,EndDate);
        }

        public IEnumerable<re_getStudentAttendance_Result> Get(int studentId)
        {
            return context.re_getStudentAttendance(studentId);
        }
    }
}