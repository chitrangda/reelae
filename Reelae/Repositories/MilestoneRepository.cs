﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReelaeDataAccessLayer;
using Ninject;
using Reelae.Filters;
using System.Data.Entity;

namespace Reelae.Repositories
{

    public interface IMilestoneRepository
    {
        void Add(MileStone entity, string username);

        void Update(MileStone entity, string username);

        void UpdateStatus(MileStone entity, string username);

        void Remove(int? id);

        MileStone GetMilestone(int? id);

        IEnumerable<MileStone> Get(int? AssignmentId);

        IEnumerable<re_getStudentAssignment_sp_Result> GetAssignments(int? StudentId);

        IEnumerable<re_getAssignmentMember_sp_Result> GetAssignmentMembers(int id);

        string getAssignmentName(int id);

        DateTime? getAssignmentDate(int id);

        IEnumerable<re_getMilestoneResource_sp_Result> getMilestoneStudents(DateTime startDate, DateTime endDate, int Assignmentd);

        IEnumerable<MileStone> getMilestoneData(DateTime startDate, DateTime endDate, int Assignmentd);

        IEnumerable<MileStone> getMilestones(DateTime date, int studentId);

    }

    [LoggingFilterAttribute]
    public class MilestoneRepository : IMilestoneRepository
    {
        [Inject]
        public ReelaeEntities context { get; set; }
        public void Add(MileStone entity, string username)
        {
            entity.CreatedBy = username;
            entity.CreatedDate = DateTime.Now;
            context.MileStones.Add(entity);
            context.SaveChanges();
        }

        public void Update(MileStone entity, string username)
        {
            MileStone existEntity = context.MileStones.Find(entity.MileStoneID);
            if (existEntity != null)
            {
                existEntity.MileStoneName = entity.MileStoneName;
                existEntity.TaskDescription = entity.TaskDescription;
                existEntity.AssignmentID = entity.AssignmentID;
                existEntity.StudentId = entity.StudentId;
                existEntity.StartDate = entity.StartDate;
                existEntity.endDate = entity.endDate;
                existEntity.StudentId = entity.StudentId;
                existEntity.ModifiedBy = username;
                existEntity.ModifiedDate = DateTime.Now;
                context.SaveChanges();
            }
        }

        public void UpdateStatus(MileStone entity, string username)
        {
            MileStone existEntity = context.MileStones.Find(entity.MileStoneID);
            if (existEntity != null)
            {
                existEntity.MilesStoneStatus = entity.MilesStoneStatus;
                existEntity.ModifiedBy = username;
                existEntity.ModifiedDate = DateTime.Now;
                context.SaveChanges();
            }
        }

        public void Remove(int? id)
        {
            context.MileStones.Remove(context.MileStones.Find(id));
            context.SaveChanges();
        }

        public MileStone GetMilestone(int? id)
        {
            return context.MileStones.Find(id);
        }

        public IEnumerable<MileStone> Get(int? AssignmentId)
        {
            return context.MileStones.Where(r => r.AssignmentID == AssignmentId);
        }

        public IEnumerable<re_getStudentAssignment_sp_Result> GetAssignments(int? StudentId)
        {
            return context.re_getStudentAssignment_sp(StudentId, null, 100, 0).Where(r => r.DueDate >= DateTime.Now);
        }

        public IEnumerable<re_getAssignmentMember_sp_Result> GetAssignmentMembers(int id)
        {
            return context.re_getAssignmentMember_sp(id);
        }

        public string getAssignmentName(int id)
        {
            return context.Assignments.Find(id).AssignmentName;
        }
        public DateTime? getAssignmentDate(int id)
        {
            return context.Assignments.Find(id).DueDate;

        }
        public IEnumerable<re_getMilestoneResource_sp_Result> getMilestoneStudents(DateTime startDate, DateTime endDate, int Assignmentd)
        {
            return context.re_getMilestoneResource_sp(Assignmentd, startDate, endDate);
            //return context.MileStones.Where(r => DbFunctions.TruncateTime(r.StartDate) >= startDate && DbFunctions.TruncateTime(r.endDate) <= endDate && r.AssignmentID == Assignmentd).Distinct();
        }

        public IEnumerable<MileStone> getMilestoneData(DateTime startDate, DateTime endDate, int Assignmentd)
        {
            return context.MileStones.Where(r => DbFunctions.TruncateTime(r.StartDate) >= startDate && DbFunctions.TruncateTime(r.endDate) <= endDate && r.AssignmentID == Assignmentd).Distinct();
        }

        public IEnumerable<MileStone> getMilestones(DateTime date, int studentId)
        {
            return context.MileStones.Where(r => r.StudentId == studentId && (date >= r.StartDate && date <= r.endDate));
        }

    }
}