﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReelaeDataAccessLayer;
using Ninject;
using System.Data.Entity;
using System.Data;
using Reelae.Filters;
using Reelae.Areas.Teacher.Models;

namespace Reelae.Repositories
{
    public interface INoticeBoardRepository
    {
        void Add(NoticeBoard entity, string username);

        void Add(newNoticeViewModel entity, string username);

        void AddComment(int noticeboardId, int? studentId, string comment, string username);

        void Update(NoticeBoard entity, string username);

        void Update(newNoticeViewModel entity, string username);

        void Update(StudentNoticeBoardAction entity, string username);

        IEnumerable<NoticeBoard> Get();

        IEnumerable<NoticeBoard> GetAll(int? id);

        NoticeBoard Get(int id);

        StudentNoticeBoardAction GetComment(int id);

        newNoticeViewModel GetNotice(int id);

        void Remove(int NoticeBoardID);

        void RemoveFile(int NoticeboardFileId);

        void RemoveComment(int id);

        //IEnumerable<re_select_noticeboardinfo_sp_Result> Get(int? groupId, int? teacherId);
        IEnumerable<re_select_noticeboardinfo_sp_Result> Get(string groupId, int? teacherId);
        IEnumerable<re_getNoticeComment_sp_Result> GetComments(string groupId, int? teacherId);

        IEnumerable<re_select_noticeboardinfo_sp_Result> Get(int? studentId);
        IEnumerable<re_getNoticeComment_sp_Result> GetComments(int? studentId);

        IEnumerable<StudentNoticeBoardAction> GetNoticeComments(List<int> noticeboardId);

        IEnumerable<StudentNoticeBoardAction> getAllComments(int? noticeBoardId);

        IEnumerable<re_getAllStudentNoticeComment_sp_Result> getAllCommentsTeachers(int? noticeBoardId);

        void Save_NoticeStartTime(int? studentId, string[] NoticeBoardIds, string username);

        void Save_NoticeEndTime(int? studentId);

        void SaveDownloadStatus(int NoticeBoardId, int? studentId);

        IEnumerable<NoticeBoardFile> getnoticeFiles(List<int?> noticeBoardIds);

        IEnumerable<NoticeBoardFile> getnoticeFiles(int noticeBoardId);


    }

    [LoggingFilterAttribute]
    public class NoticeBoardRepository : INoticeBoardRepository
    {
        [Inject]
        public ReelaeEntities context { get; set; }
        public void Add(NoticeBoard entity, string username)
        {
            entity.CreatedBy = username;
            entity.CreatedDate = DateTime.Now;
            context.NoticeBoards.Add(entity);
            context.SaveChanges();

        }

        public void Add(newNoticeViewModel entity, string username)
        {
            try
            {
                entity._noticeBoard.CreatedBy = username;
                entity._noticeBoard.CreatedDate = DateTime.Now;
                context.NoticeBoards.Add(entity._noticeBoard);
                context.SaveChanges();
                context.Entry(entity._noticeBoard).GetDatabaseValues();
                if (entity._noticeBoardFiles != null)
                {
                    entity._noticeBoardFiles.ForEach(r => r.NoticeBoardId = entity._noticeBoard.NoticeBoardID);
                    foreach (var _file in entity._noticeBoardFiles)
                    {
                        context.NoticeBoardFiles.Add(_file);
                        context.SaveChanges();
                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        public void AddComment(int noticeboardId, int? studentId, string comment, string username)
        {
            StudentNoticeBoardAction entity = new StudentNoticeBoardAction();
            entity.NoticeBoardID = noticeboardId;
            entity.StudentID = studentId;
            entity.Content = comment;
            entity.CreatedBy = username;
            entity.CreatedDate = DateTime.Now;
            context.StudentNoticeBoardActions.Add(entity);
            context.SaveChanges();

        }

        public void Update(NoticeBoard entity, string username)
        {
            var existEntity = context.NoticeBoards.Find(entity.NoticeBoardID);
            if (existEntity != null)
            {
                existEntity.Subject = entity.Subject;
                existEntity.PublishedDate = entity.PublishedDate;
                existEntity.Content = entity.Content;
                existEntity.url = entity.url;
                existEntity.ModifiedBy = username;
                existEntity.ModifiedDate = DateTime.Now;
                context.SaveChanges();

            }

        }

        public void Update(newNoticeViewModel entity, string username)
        {
            var existEntity = context.NoticeBoards.Find(entity._noticeBoard.NoticeBoardID);
            if (existEntity != null)
            {
                existEntity.Subject = entity._noticeBoard.Subject;
                existEntity.PublishedDate = entity._noticeBoard.PublishedDate;
                existEntity.Content = entity._noticeBoard.Content;
                existEntity.url = entity._noticeBoard.url;
                existEntity.ModifiedBy = username;
                existEntity.ModifiedDate = DateTime.Now;
                existEntity.PublishedDate = entity._noticeBoard.PublishedDate;
                context.SaveChanges();

            }
            if (entity._noticeBoardFiles != null)
            {
                entity._noticeBoardFiles.ForEach(r => r.NoticeBoardId = entity._noticeBoard.NoticeBoardID);
                foreach (var _file in entity._noticeBoardFiles)
                {
                    if (context.NoticeBoardFiles.Find(_file.NoticeBoardFileId) == null)
                    {
                        context.NoticeBoardFiles.Add(_file);
                        context.SaveChanges();
                    }
                }
            }
        }

        public void Update(StudentNoticeBoardAction entity, string username)
        {
            var _exixtEntity = context.StudentNoticeBoardActions.Find(entity.NoticeBoardActionID);
            if (_exixtEntity != null)
            {
                _exixtEntity.Content = entity.Content;
                _exixtEntity.ModifiedBy = username;
                _exixtEntity.ModifiedDate = DateTime.Now;
                context.SaveChanges();

            }
        }

        public newNoticeViewModel GetNotice(int id)
        {
            newNoticeViewModel model = new newNoticeViewModel();
            model._noticeBoard = context.NoticeBoards.Find(id);
            model._noticeBoardFiles = context.NoticeBoardFiles.Where(r => r.NoticeBoardId == id).ToList();
            return model;
        }

        public IEnumerable<NoticeBoard> Get()
        {
            return context.NoticeBoards.ToList();
        }

        public NoticeBoard Get(int id)
        {
            return context.NoticeBoards.Find(id);
        }

        public StudentNoticeBoardAction GetComment(int id)
        {
            return context.StudentNoticeBoardActions.Find(id);
        }

        public IEnumerable<NoticeBoard> GetAll(int? id)
        {
            return context.NoticeBoards.Where(r => r.User.InstitutionID == id).OrderByDescending(r => r.CreatedDate);
        }

        public void Remove(int NoticeBoardID)
        {
            if (context.StudentNoticeBoardActions.Where(r => r.NoticeBoardID == NoticeBoardID) != null)
            {
                context.StudentNoticeBoardActions.RemoveRange(context.StudentNoticeBoardActions.Where(r => r.NoticeBoardID == NoticeBoardID));
            }
            context.SaveChanges();
            if (context.NoticeBoardFiles.Where(r => r.NoticeBoardId == NoticeBoardID) != null)
            {
                context.NoticeBoardFiles.RemoveRange(context.NoticeBoardFiles.Where(r => r.NoticeBoardId == NoticeBoardID));
            }
            context.SaveChanges();
            var obj = context.NoticeBoards.Find(NoticeBoardID);
            context.NoticeBoards.Remove(obj);
            context.SaveChanges();
        }

        public void RemoveFile(int NoticeboardFileId)
        {
            context.NoticeBoardFiles.Remove(context.NoticeBoardFiles.Find(NoticeboardFileId));
            context.SaveChanges();
        }

        public void RemoveComment(int id)
        {
            context.StudentNoticeBoardActions.Remove(context.StudentNoticeBoardActions.Find(id));
            context.SaveChanges();
        }
        public IEnumerable<re_select_noticeboardinfo_sp_Result> Get(string groupId, int? teacherId)
        {
            return context.re_select_noticeboardinfo_sp(teacherId, groupId);
        }

        public IEnumerable<re_getNoticeComment_sp_Result> GetComments(string groupId, int? teacherId)
        {

            return context.re_getNoticeComment_sp(teacherId, groupId);

        }

        public IEnumerable<re_select_noticeboardinfo_sp_Result> Get(int? studentId)
        {
            return context.re_getStudentNotice_sp(studentId);
        }

        public IEnumerable<re_getNoticeComment_sp_Result> GetComments(int? studentId)
        {
            return context.re_getStudentNoticeComment_sp(studentId);


        }

        public IEnumerable<StudentNoticeBoardAction> GetNoticeComments(List<int> noticeboardId)
        {
            return context.StudentNoticeBoardActions.Where(r => noticeboardId.Contains(r.NoticeBoardID));


        }

        public IEnumerable<StudentNoticeBoardAction> getAllComments(int? noticeBoardId)
        {
            //return context.re_getAllStudentNoticeComment_sp(noticeBoardId);
            return context.StudentNoticeBoardActions.Where(r => r.NoticeBoardID == noticeBoardId && r.Content != null);
        }
        public IEnumerable<re_getAllStudentNoticeComment_sp_Result> getAllCommentsTeachers(int? noticeBoardId)
        {
            return context.re_getAllStudentNoticeComment_sp(noticeBoardId);
        }

        public void Save_NoticeStartTime(int? studentId, string[] NoticeBoardIds, string username)
        {
            var entity = context.StudentNoticeBoardActions.Where(r => r.StudentID == studentId).ToList();
            if (entity != null && entity.Count() > 0)
            {
                entity.ForEach(r => r.ActionStarttime = DateTime.Now);
                context.SaveChanges();
            }
            else
            {
                foreach (string _noticeId in NoticeBoardIds)
                {
                    StudentNoticeBoardAction newEntity = new StudentNoticeBoardAction();
                    newEntity.NoticeBoardID = Convert.ToInt32(_noticeId);
                    newEntity.StudentID = studentId;
                    newEntity.ActionStarttime = DateTime.Now;
                    newEntity.CreatedBy = username;
                    newEntity.CreatedDate = DateTime.Now;
                    context.StudentNoticeBoardActions.Add(newEntity);
                    context.SaveChanges();
                }

            }
        }

        public void Save_NoticeEndTime(int? studentId)
        {
            var entity = context.StudentNoticeBoardActions.Where(r => r.StudentID == studentId).ToList();
            entity.ForEach(r => r.ActionEndtime = DateTime.Now);
            context.SaveChanges();

        }

        public void SaveDownloadStatus(int NoticeBoardId, int? studentId)
        {
            var entity = context.StudentNoticeBoardActions.Where(r => r.StudentID == studentId && r.NoticeBoardID == NoticeBoardId).ToList();
            entity.ForEach(r => r.IsDownloaded = 1);
            context.SaveChanges();

        }

        public IEnumerable<NoticeBoardFile> getnoticeFiles(List<int?> noticeBoardIds)
        {
            return context.NoticeBoardFiles.Where(r => noticeBoardIds.Contains(r.NoticeBoardId)).ToList();
        }

        public IEnumerable<NoticeBoardFile> getnoticeFiles(int noticeBoardId)
        {
            return context.NoticeBoardFiles.Where(r => r.NoticeBoardId == noticeBoardId);
        }

    }

}
