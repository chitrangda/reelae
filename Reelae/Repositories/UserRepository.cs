﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using ReelaeDataAccessLayer;

namespace Reelae.Repositories
{

    //public interface IUserRepository<TEnt, in TPk> where TEnt : class
    //{
    //    IEnumerable<TEnt> Get();
    //    TEnt Get(string id);
    //    void Add(TEnt entity);
    //    void Remove(TEnt entity);
    //}


    public interface IUserRepository
    {
        IEnumerable<User> Get();
        User Get(string id);

        User Get(int id);
        int Add(User entity);
        void Remove(User entity);

        void Remove(string id);

        void Update(User entity, string username);

        IEnumerable<User> GetAll(int InstitutionId, string[] membershipId);

        IEnumerable<re_getInstituteMembers_sp_Result> GetAll(int InstitutionId, int role);

        re_getUserDetails_sp_Result getUserDetail(int id);


        #region TeacherMethods
        IEnumerable<Group> GetGroups(int teacherID);
        Group GetGroupByTeacher(int teacherID, int groupid);
        #endregion

        #region StudentMethods

        #endregion

        IEnumerable<re_getActivatedInstitueMembers_sp_Result> GetAll(int InstitutionId, string type, int subjectId);

        void EditUserNameAndEmail(int? userId, string name, string Email);

        void SetUserCodeBySp(int userID, int InstitutionID);

        IEnumerable<re_select_teachers_student_sp_Result> GetTeachersActiveMembers(int? teacherid);
        IEnumerable<re_GetChatMembers_sp_Result> GetInstituteActiveMembersByRoleId(int? institutionid, int? roleId);

        User getUserByEmailId(string userEmailId); 
    }

    public class UserRepository : IUserRepository
    {
        ReelaeEntities context { get; set; }
        public string MasterContant { get; private set; }

        public UserRepository(ReelaeEntities _context)
        {
            this.context = _context;
        }

        public int Add(User entity)
        {
            try
            {
                context.Users.Add(entity);
                context.SaveChanges();
                context.Entry(entity).GetDatabaseValues();
                //var _user = SetUserCodeBySp(entity.UserID, entity.InstitutionID);
                ////setUserCode(entity.UserID, entity.InstitutionID);
                ////entity.UserCode = GetUserCode(entity.UserID, entity.InstitutionID); 
                ////context.SaveChanges();
                //// SetUserCode(entity.UserID, entity.InstitutionID);
                //if (_user != null)

                //{
                //    string _code = _user.FirstOrDefault().UserCode;
                //}
                return entity.UserID;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    string headerMsg = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                         eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        headerMsg += string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return -1;
            }
        }

        public IEnumerable<User> Get()
        {
            return context.Users.ToList();

        }

        public User Get(string id)
        {
            return context.Users.Where(n => n.UserMemberShipID == id).FirstOrDefault();
        }

        public User Get(int id)
        {
            return context.Users.Where(n => n.UserID == id).FirstOrDefault();
        }

        public void Remove(User entity)
        {
            context.Users.Remove(context.Users.Where(n => n.UserID == entity.UserID).FirstOrDefault());
            context.SaveChanges();
        }
        public void Remove(string id)
        {
            context.Users.Remove(context.Users.Where(n => n.UserMemberShipID == id).FirstOrDefault());
            context.SaveChanges();
        }
        public IEnumerable<Group> GetGroups(int teacherID)
        {
            var curTeacher = context.Users.Where(n => n.UserID == teacherID).FirstOrDefault();
            if (curTeacher != null)
            {
                return curTeacher.Groups.ToList();
            }
            else
            {
                return new List<Group>();
            }
        }

        public void Update(User entity, string username)
        {
            User _user = context.Users.Find(entity.UserID);
            try
            {
                if (_user != null)
                {
                    _user.Name = entity.Name;
                    _user.Surname = entity.Surname;
                    _user.Email = entity.Email;
                    _user.UserPicUrl = entity.UserPicUrl;
                    _user.Location = entity.Location;
                    _user.Website = entity.Website;
                    _user.Bio = entity.Bio;
                    _user.ModifiedBy = username;
                    _user.ModifiedDate = DateTime.Now;
                    context.SaveChanges();
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    string headerMsg = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                         eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        headerMsg += string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        public IEnumerable<User> GetAll(int InstitutionId, string[] membershipId)
        {
            return context.Users.Where(r => r.InstitutionID == InstitutionId).Where(u => membershipId.Contains(u.UserMemberShipID));
        }

        public IEnumerable<re_getActivatedInstitueMembers_sp_Result> GetAll(int InstitutionId, string type, int subjectId)
        {
            return context.re_getActivatedInstitueMembers_sp(InstitutionId, type, subjectId);
        }

        public IEnumerable<re_getInstituteMembers_sp_Result> GetAll(int InstitutionId, int role)
        {
            return context.re_getInstituteMembers_sp(InstitutionId, role);

        }

        public re_getUserDetails_sp_Result getUserDetail(int id)
        {
            return context.re_getUserDetails_sp(id).FirstOrDefault();
        }

        #region [Teacher Methods]
        public Group GetGroupByTeacher(int teacherID, int groupid)
        {
            var teacher = context.Users.AsNoTracking().Where(n => n.UserID == teacherID).FirstOrDefault();
            if (teacher != null)
            {
                return teacher.Groups.Where(n => n.GroupID == groupid).FirstOrDefault();
            }
            return null;
        }

        public void EditUserNameAndEmail(int? userId, string name, string Email)
        {
            User _user = context.Users.Find(userId);
            if (_user != null)
            {
                _user.Name = name;
                _user.Email = Email;
                context.SaveChanges();
            }
        }

        private void setUserCode(int? userID, int? InstitutionID)
        {

            try
            {
                var useridParameter = userID.HasValue ?
                        new ObjectParameter("userid", userID) :
                        new ObjectParameter("userid", typeof(int));

                var institutionIDParameter = InstitutionID.HasValue ?
                    new ObjectParameter("InstitutionID", InstitutionID) :
                    new ObjectParameter("InstitutionID", typeof(int));
                var sql = "SELECT VALUE FROM ReelaeEntities.fn_GenerateUserCodeTV(@userid,@InstitutionID)";
                var query = ((IObjectContextAdapter)context).ObjectContext.CreateQuery<string>(sql, useridParameter, institutionIDParameter);
                foreach (var type in query)
                {

                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    string headerMsg = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                         eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        headerMsg += string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }

            }
        }

        #endregion

        #region PrivateMembers

        public void SetUserCodeBySp(int userID, int InstitutionID)
        {
            try
            {
                var _userCode = context.re_GenerateUserCode_sp(userID, InstitutionID).FirstOrDefault().UserCode;
                var obj = context.Users.Find(userID);
                if (obj != null)
                {
                    obj.UserCode = _userCode;
                    context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
            }
        }
        public IEnumerable<re_select_teachers_student_sp_Result> GetTeachersActiveMembers(int? teacherid)
        {
            return context.re_select_teachers_student_sp(teacherid);
        }

        public IEnumerable<re_GetChatMembers_sp_Result> GetInstituteActiveMembersByRoleId(int? institutionid, int? roleId)
        {
            return context.re_GetChatMembers_sp(institutionid, roleId);
        }

        public User getUserByEmailId(string userEmailId)
        {
            return context.Users.AsNoTracking().Where(n => n.Email.ToLower() == userEmailId.ToLower()).First();
        }
        #endregion

    }
}
