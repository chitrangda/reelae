﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReelaeDataAccessLayer;
using Ninject;
using System.Data.Entity;
using System.Data;
using Reelae.Filters;

namespace Reelae.Repositories
{
    [LoggingFilterAttribute]
    public class MessageRepository : IRepository<Message, int>
    {
        [Inject]
        public ReelaeEntities context { get; set; }
        public void Add(Message entity)
        {
            context.Messages.Add(entity);
        }

        public IEnumerable<Message> Get()
        {
            return context.Messages.ToList();
        }

        public Message Get(int id)
        {
            return context.Messages.Find(id);
        }

        public void Remove(Message entity)
        {
            var obj = context.Messages.Find(entity.MessageID);
            context.Messages.Remove(obj);
            context.SaveChanges();
        }
    }
}
