﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using ReelaeDataAccessLayer;

namespace Reelae.Repositories
{

    //public interface IUserRepository<TEnt, in TPk> where TEnt : class
    //{
    //    IEnumerable<TEnt> Get();
    //    TEnt Get(string id);
    //    void Add(TEnt entity);
    //    void Remove(TEnt entity);
    //}


    public interface ISubjectStudentRepository
    {
        IEnumerable<SubjectStudent> Get();
        IEnumerable<SubjectStudent> Get(int id);
        void Add(SubjectStudent entity, string username);
        void Remove(SubjectStudent entity);

        bool IsStudentMappedWithAnySubject(int studentId);

      
        void RemoveStudentFromSubject(int subjectID, int studentID);


        void removeStudentById(int studentID);

        IEnumerable<re_select_student_subject_sp_Result> GetStudents(int? id);
        IEnumerable<re_select_subject_studentwise_sp_Result> GetStudentSubjectList(int? studentId, int instituteId);
    }

    class SubjectStudentRepository : ISubjectStudentRepository
    {
        ReelaeEntities context { get; set; }
        public SubjectStudentRepository(ReelaeEntities _context) {
            this.context = _context;
        }

        public void Add(SubjectStudent entity,string username)
        {
            try
            {
                entity.CreatedBy = username;
                entity.CreatedDate = DateTime.Now;
                context.SubjectStudents.Add(entity);
                context.SaveChanges();
            }
            catch(Exception ex)
            {

            }
        }

        public IEnumerable<SubjectStudent> Get()
        {
           return context.SubjectStudents.ToList();
            
        }

        public IEnumerable<SubjectStudent> Get(int id)
        {
            return context.SubjectStudents.Where(n => n.SubjectID == id);
        }

        public void Remove(SubjectStudent entity)
        {
            context.SubjectStudents.Remove(context.SubjectStudents.Where(n => n.StudentID == entity.StudentID).FirstOrDefault());
            context.SaveChanges();
        }


      
        public void RemoveStudentFromSubject(int subjectID, int studentID)
        {
            var curStudent = context.SubjectStudents.Where(n => n.SubjectID == subjectID && n.StudentID == studentID).FirstOrDefault();
            if (curStudent != null) {
                context.SubjectStudents.Remove(curStudent);
                context.SaveChanges();

                context.re_deleteGroupStudent(subjectID, studentID);

            }
        } 
 

        public bool IsStudentMappedWithAnySubject(int studentId)
        {
            if (context.SubjectStudents.Where(n => n.StudentID == studentId).ToList().Count>0)
            {
                return true;
            }
            return false;
        }

        public void removeStudentById(int studentId)
        {
            context.SubjectStudents.RemoveRange(context.SubjectStudents.Where(n => n.StudentID == studentId).ToList());
            context.SaveChanges();
        }

        public IEnumerable<re_select_student_subject_sp_Result> GetStudents(int? id)
        {
            return context.re_select_student_subject_sp(id);
        }

        //for getting student subjects only
        public IEnumerable<re_select_subject_studentwise_sp_Result> GetStudentSubjectList(int? studentId,int instituteId)
        {
            return context.re_select_subject_studentwise_sp(instituteId, studentId);
        }
    }
}
