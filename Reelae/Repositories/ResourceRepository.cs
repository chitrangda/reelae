﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReelaeDataAccessLayer;
using Ninject;
using System.Data.Entity;
using System.Data;
using Reelae.Filters;
using System.IO;
using Reelae.Models;
using Reelae.Utilities;

namespace Reelae.Repositories
{
    public interface IResourceRepository
    {
        IEnumerable<Resource> GetAll(int? id);
        IEnumerable<re_select_teacher_resource_sp_Result> GetAllResourcePerSubject(int? subjectId, string docType, int? tag, string otherFilter, string sortOrder, int? labelId);
        void AddResource(AddResourceViewModal entity);
        FileUtility UploadResource(HttpPostedFileBase _HttpPostedFile);
        Resource Get(int? id);
        void SaveDownloadActivity(ResourceActivity entity);
        IEnumerable<re_select_download_resource_list_sp_Result> GetDownloadActivityResourceList(int? teacherId, int? subjectId);

        IEnumerable<re_select_download_resource_list_sp_Result> GetFileDetails(int? id);

        IEnumerable<re_getStudentResource_sp_Result> GetAllStudentResource(int? subjectId, string docType, int? tag, string otherFilter, string sortOrder,int? labelId);
        bool CheckresourceIsDownloadedByStudent(int? resourceId, int? downloadedBy);
        void EditResource(EditResourceViewModal entity);
        void RemoveResource(int? id);
        IEnumerable<re_select_subject_teacherwise_sp_Result> getSubjectsListTeacherId(int? _Teacherid, int? _institutionID);
        //Flag the resource
        void FlagResource(int? id, bool isFlaged);

        string GetTagName(int? id);

        List<ResourceTag> GetAllTags();
        void AddTagToSubjectResource(int? resourceId, int? tagId, string tagName);

        IEnumerable<re_getResourceTags_sp_Result> GetResourceTags();
        void UpdateDescription(int? resourceId, string description);
        IEnumerable<re_getResourceTags_sp_Result> GetStudentResourceTags();
        //update view activity
        void UpdateViewActivity(int? id);
        //restore resource file
        void RestoreResource(int? id);
        //get resource Activity LIST
        IEnumerable<re_getResourceActivityList_sp_Result> GetActivityList(int? resourceId);
        int AddLabelResource(ResourceLabel entity);
        //resource label list
        IEnumerable<ResourceLabel> GetResourceLabelList(int? subjectId);
        //get resourceLabel list
        IEnumerable<re_getResourceLabel_sp_Result> GetResourceLabelList(int? teacherId, int? studentId, int? subjectId);
        //save visit details
        int SaveVisitDetails(StudentActivity studentActivity);
        //visit data
        IEnumerable<StudentActivity> GetStudentVisitList(int? subjectId);
    }
    [LoggingFilterAttribute]
    public class ResourceRepository : IResourceRepository
    {
        [Inject]
        ReelaeEntities context;
        public ResourceRepository(ReelaeEntities _context)
        {
            context = _context;
        }

        public void AddResource(AddResourceViewModal entity)
        {
            try
            {
                var userId = HttpContext.Current.Session["UserName"] == null ? "0" : HttpContext.Current.Session["UserName"].ToString();
                Resource objFileInfo = new Resource()
                {
                    Title = entity.resourceInfo.Title,
                    URL = entity.resourceInfo.URL,
                    fileType = entity.resourceInfo.fileType,
                    FileSize = entity.resourceInfo.FileSize.ToString(),
                    Description = entity.resourceInfo.Description,
                    CreatedDate = DateTime.Now,
                    CreatedBy = userId.ToString(),
                    TeacherID = int.Parse(HttpContext.Current.Session["UserId"].ToString()),
                    SubjectID = entity.resourceInfo.SubjectID,
                    LabelId = entity.resourceInfo.LabelId
                };

                context.Resources.Add(objFileInfo);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                var exMessage = ex.Message;
            }
        }
        public FileUtility UploadResource(HttpPostedFileBase _HttpPostedFile)
        {
            try
            {
                string fileSavePath = string.Empty;
                string fileType = string.Empty;
                string folderName = string.Empty;
                string ContentType = _HttpPostedFile.ContentType;

                List<string> subStrings = new List<string> { "video", "officedocument", "compressed", "image", "audio" };
                switch (subStrings.FirstOrDefault(ContentType.Contains))
                {
                    case "video":
                        fileType = "video";
                        folderName = "VideoAndAudio";
                        break;
                    case "audio":
                        fileType = "audio";
                        folderName = "VideoAndAudio";
                        break;
                    case "officedocument":
                        fileType = "document";
                        folderName = "Docs";
                        break;
                    case "compressed":
                        fileType = "compressed";
                        folderName = "compressed";
                        break;
                    case "image":
                        fileType = "image";
                        folderName = "Photos";
                        break;
                    default:
                        fileType = "document";
                        folderName = "Docs";
                        break;
                }

                fileSavePath = HttpContext.Current.Server.MapPath(folderName);
                string fileExt = System.IO.Path.GetExtension(_HttpPostedFile.FileName);
                string orginalFileName = _HttpPostedFile.FileName;
                string fileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(_HttpPostedFile.FileName);

                long fileSize = _HttpPostedFile.ContentLength;
                var uid = Guid.NewGuid();
                // string renameFile = fileNameWithoutExtension + uid.ToString() + fileExt;
                string renameFile = uid.ToString() + fileExt;
                if (!Directory.Exists(fileSavePath))
                {
                    Directory.CreateDirectory(fileSavePath);
                }

                _HttpPostedFile.SaveAs(Path.Combine(fileSavePath, renameFile));

                string fileAbsoluteFilePath = folderName + "/" + renameFile;
                FileUtility _resourceUtility = new FileUtility();
                _resourceUtility.fileAbsoluteURL = fileAbsoluteFilePath;
                _resourceUtility.fileName = orginalFileName;
                _resourceUtility.FileSize = fileSize.ToString();
                _resourceUtility.fileType = fileType;

                return _resourceUtility;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public IEnumerable<Resource> GetAll(int? id)
        {
            int testcherId = Convert.ToInt32(HttpContext.Current.Session["UserId"].ToString());
            return context.Resources.Where(x => x.TeacherID == testcherId).ToList();
        }
        public Resource Get(int? id)
        {
            return context.Resources.Find(id);
        }
        public void Remove(Resource entity)
        {
            var obj = context.Resources.Find(entity.ResourceID);
            context.Resources.Remove(obj);
            context.SaveChanges();
        }
        public void SaveDownloadActivity(ResourceActivity entity)
        {
            try
            {
                int studentId = int.Parse(HttpContext.Current.Session["UserId"].ToString());
                var result = context.ResourceActivities.Where(x => (x.DownloadedBy == studentId || x.ViewedBy == studentId) && x.ResourceID == entity.ResourceID).FirstOrDefault();
                if (result != null)
                {
                    result.DownloadTime = DateTime.Now;
                    result.DownloadedBy = studentId;
                    result.IsDownloaded = 1;
                    result.ModifiedDate = DateTime.Now;
                }
                else
                {
                    context.ResourceActivities.Add(new ResourceActivity()
                    {
                        ResourceID = entity.ResourceID,
                        IsDownloaded = entity.IsDownloaded,
                        DownloadTime = entity.DownloadTime,
                        DownloadedBy = studentId,
                        ModifiedDate = DateTime.Now
                    });
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                var exMessage = ex.Message;
            }
        }
        public IEnumerable<re_select_download_resource_list_sp_Result> GetDownloadActivityResourceList(int? teacherId, int? subjectId)
        {
            teacherId = int.Parse(HttpContext.Current.Session["UserId"].ToString());
            return context.re_select_download_resource_list_sp(teacherId, null, subjectId).Take(5);
        }

        public IEnumerable<re_select_download_resource_list_sp_Result> GetFileDetails(int? id)
        {
            int teacherID = int.Parse(HttpContext.Current.Session["UserId"].ToString());
            var result = context.re_select_download_resource_list_sp(teacherID, id, null).ToList();
            if (result.Count > 0)
            {
                result[0].CreatedBy = (context.Users.AsEnumerable().Where(x => x.UserID == teacherID).Select(x => new { CreatedBy = x.Name + " " + x.Surname }).FirstOrDefault().CreatedBy).ToString();
            }
            return result;
        }
        public IEnumerable<re_getStudentResource_sp_Result> GetAllStudentResource(int? subjectId, string docType, int? tag, string otherFilter, string sortOrder, int? labelId)
        {
            int id = Convert.ToInt32(HttpContext.Current.Session["UserId"].ToString());
            docType = string.IsNullOrEmpty(docType) == true ? null : docType;
            var result = context.re_getStudentResource_sp(id, subjectId, docType, tag, labelId).ToList();
            if (!string.IsNullOrEmpty(otherFilter))
            {
                switch (otherFilter)
                {
                    case "Trash":
                        result = result.Where(x => x.Isdeleted == 1).ToList();
                        break;
                    case "Recent":
                        result = result.Where(x => x.CreatedDate.Value.Date == DateTime.Now.Date).ToList();
                        break;
                    case "Flaged":
                        result = result.Where(x => x.IsFlagged == true).ToList();
                        break;
                }
            }
            else
            {
                result = result.Where(x => x.Isdeleted == 0).ToList();
            }

            if (!string.IsNullOrEmpty(sortOrder))
            {
                if (sortOrder == "name_asc")
                {
                    result = result.OrderBy(x => x.Title).ToList();
                }
                else if (sortOrder == "name_desc")
                {
                    result = result.OrderByDescending(x => x.Title).ToList();
                }
            }
            return result;
        }
        //get filttered resource
        public IEnumerable<re_select_teacher_resource_sp_Result> GetAllResourcePerSubject(int? subjectId, string docType, int? tag, string otherFilter, string sortOrder, int? labelId)
        {
            int testcherId = Convert.ToInt32(HttpContext.Current.Session["UserId"].ToString());
            docType = string.IsNullOrEmpty(docType) == true ? null : docType;
            var result = context.re_select_teacher_resource_sp(subjectId, testcherId, docType, tag, labelId).ToList();
            if (!string.IsNullOrEmpty(otherFilter))
            {
                switch (otherFilter)
                {
                    case "Trash":
                        result = result.Where(x => x.Isdeleted == 1).ToList();
                        break;
                    case "Recent":
                        result = result.Where(x => x.CreatedDate.Value.Date == DateTime.Now.Date).ToList();
                        break;
                    case "Flaged":
                        result = result.Where(x => x.IsFlagged == true).ToList();
                        break;
                }
            }
            else
            {
                result = result.Where(x => x.Isdeleted == 0).ToList();
            }

            if (!string.IsNullOrEmpty(sortOrder))
            {
                if (sortOrder == "name_asc")
                {
                    result = result.OrderBy(x => x.Title).ToList();
                }
                else if (sortOrder == "name_desc")
                {
                    result = result.OrderByDescending(x => x.Title).ToList();
                }
            }

            return result;
        }
        //check resource is downloaded by student
        public bool CheckresourceIsDownloadedByStudent(int? resourceId, int? downloadedBy)
        {
            var result = context.ResourceActivities.Where(x => x.ResourceID == resourceId && x.DownloadedBy == downloadedBy && x.IsDownloaded == 1).Select(y => y.ResourceID);
            if (result.Count() > 0)
            {
                return true;
            }
            return false;
        }
        //Edit resource details      
        public void EditResource(EditResourceViewModal _model)
        {
            try
            {
                var entity = _model.resourceInfo;
                var userId = HttpContext.Current.Session["UserName"] == null ? "0" : HttpContext.Current.Session["UserName"].ToString();
                var result = context.Resources.Find(entity.ResourceID);
                if (result != null)
                {
                    result.Title = entity.Title;
                    result.URL = entity.URL;
                    result.fileType = entity.fileType;
                    result.FileSize = entity.FileSize.ToString();
                    result.Description = entity.Description;
                    result.ModifiedDate = DateTime.Now;
                    result.ModifiedBy = userId.ToString();
                    result.TeacherID = int.Parse(HttpContext.Current.Session["UserId"].ToString());
                    result.SubjectID = entity.SubjectID;
                    result.LabelId = entity.LabelId;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                var exMessage = ex.Message;
            }
        }
        //remove resource 
        public void RemoveResource(int? id)
        {
            //context.Resources.Remove(context.Resources.Find(id));
            //context.ResourceActivities.RemoveRange(context.ResourceActivities.Where(x=>x.ResourceID== id));
            //context.SaveChanges();
            //update isdeleted column
            var result = context.Resources.Find(id);
            if (result != null)
            {
                result.Isdeleted = 1;
                result.ModifiedBy = HttpContext.Current.Session["UserName"].ToString();
                result.ModifiedDate = DateTime.Now;
                context.SaveChanges();
            }
        }
        //get subject list on the basis of teacher id
        public IEnumerable<re_select_subject_teacherwise_sp_Result> getSubjectsListTeacherId(int? _Teacherid, int? _institutionID)
        {
            return context.re_select_subject_teacherwise_sp(_institutionID, _Teacherid);
        }

        public void FlagResource(int? id, bool isFlaged)
        {
            int UserId = int.Parse(HttpContext.Current.Session["UserId"].ToString());
            if (id != null)
            {
                var result = context.FlaggedResources.Where(x => x.ResourceId == id && x.UserId == UserId).FirstOrDefault();
                if (result != null)
                {
                    context.FlaggedResources.Remove(result);
                }
                else
                {
                    context.FlaggedResources.Add(new FlaggedResource()
                    {
                        IsFlagged = true,
                        UserId = UserId,
                        ResourceId = id,
                        CreatedBy = HttpContext.Current.Session["UserName"].ToString(),
                        CreatedDate = DateTime.Now
                    });
                }
                context.SaveChanges();
            }
        }

        public string GetTagName(int? id)
        {
            return context.ResourceTags.Find(id).TagName.ToString();
        }

        public List<ResourceTag> GetAllTags()
        {
            return context.ResourceTags.ToList();
        }
        //saved tag 
        public void AddTagToSubjectResource(int? resourceId, int? tagId, string tagName)
        {
            var userId = HttpContext.Current.Session["UserName"] == null ? "0" : HttpContext.Current.Session["UserName"].ToString();
            var checkExistance = context.ResourceTags.Where(x => x.TagName == tagName).ToList();
            if (checkExistance.Count == 0)
            {
                ResourceTag obj = new ResourceTag()
                {
                    TagName = tagName,
                    CreatedBy = userId,
                    CreatedDate = DateTime.Now
                };
                context.ResourceTags.Add(obj);
                context.SaveChanges();
                tagId = obj.TagId;
            }
            var result = context.Resources.Find(resourceId);
            result.TagId = tagId == null ? checkExistance[0].TagId : tagId;
            context.SaveChanges();
        }
        //get tags listing
        public IEnumerable<re_getResourceTags_sp_Result> GetResourceTags()
        {
            int TeacherID = int.Parse(HttpContext.Current.Session["UserId"].ToString());
            return context.re_getResourceTags_sp(null, TeacherID, null, null, null);
        }
        //update description
        public void UpdateDescription(int? resourceId, string description)
        {
            var result = context.Resources.Find(resourceId);
            result.Description = description;
            context.SaveChanges();
        }

        //get tags listing
        public IEnumerable<re_getResourceTags_sp_Result> GetStudentResourceTags()
        {
            int studentId = int.Parse(HttpContext.Current.Session["UserId"].ToString());
            return context.re_getResourceTags_sp(null, null, null, null, studentId);
        }
        //update view activity
        public void UpdateViewActivity(int? id)
        {
            if (id != null)
            {
                int studentId = int.Parse(HttpContext.Current.Session["UserId"].ToString());
                var result = context.ResourceActivities.Where(x => (x.DownloadedBy == studentId || x.ViewedBy == studentId) && x.ResourceID == id).FirstOrDefault();
                if (result != null)
                {
                    result.ViewTime = DateTime.Now;
                    result.isViewed = true;
                    result.ViewedBy = studentId;
                    result.ModifiedDate = DateTime.Now;
                }
                else
                {
                    context.ResourceActivities.Add(new ResourceActivity()
                    {
                        isViewed = true,
                        ViewedBy = studentId,
                        ViewTime = DateTime.Now,
                        ResourceID = id.Value,
                        ModifiedDate= DateTime.Now
                    });
                }
                context.SaveChanges();
            }
        }
        //restore resource file
        public void RestoreResource(int? id)
        {
            var result = context.Resources.Find(id);
            if (result != null)
            {
                result.Isdeleted = 0;
                result.ModifiedBy = HttpContext.Current.Session["UserName"].ToString();
                result.ModifiedDate = DateTime.Now;
                context.SaveChanges();
            }
        }
        //get resource Activity LIST
        public IEnumerable<re_getResourceActivityList_sp_Result> GetActivityList(int? resourceId)
        {
            int studentId = int.Parse(HttpContext.Current.Session["UserId"].ToString());
            return context.re_getResourceActivityList_sp(resourceId);
        }
        //Add resource label
        public int AddLabelResource(ResourceLabel entity)
        {
            entity.CreatedDate = DateTime.Now;
            entity.CreatedBy = HttpContext.Current.Session["UserName"].ToString();
            context.ResourceLabels.Add(entity);
            context.SaveChanges();
            return entity.ResourceLabelId;
        }
        public IEnumerable<ResourceLabel> GetResourceLabelList(int? subjectId)
        {
            int teacherId = int.Parse(HttpContext.Current.Session["UserId"].ToString());
            return context.ResourceLabels.Where(x => x.SubjectId == subjectId && x.TeacherId == teacherId).ToList();
        }
        //get resourceLabel list
        public IEnumerable<re_getResourceLabel_sp_Result> GetResourceLabelList(int? teacherId, int? studentId, int? subjectId)
        {
            return context.re_getResourceLabel_sp(studentId, subjectId, teacherId);
        }
        //save visit time 
        public int SaveVisitDetails(StudentActivity studentActivity)
        {
            var result = context.StudentActivities.Where(x=>x.StudentId==studentActivity.StudentId && x.SubjectId== studentActivity.SubjectId).FirstOrDefault();
            if (result != null)
            {
                result.StartDateTime = studentActivity.StartDateTime;
                result.EndDateTime = studentActivity.EndDateTime;
            }
            else
            {
                context.StudentActivities.Add(studentActivity);
            }
            context.SaveChanges();
            return studentActivity.ActivityId;
        }

        public IEnumerable<StudentActivity> GetStudentVisitList(int? subjectId)
        {
            return context.StudentActivities.Where(x=>x.Page=="Resource" && x.SubjectId== subjectId);
        }
    }
}