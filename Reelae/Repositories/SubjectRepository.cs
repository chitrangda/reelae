﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReelaeDataAccessLayer;
using Ninject;
using System.Data.Entity;
using System.Data;
using Reelae.Filters;

namespace Reelae.Repositories
{
    public interface ISubjectRepository<TEnt, in TPk> where TEnt : class
    {
        IEnumerable<TEnt> Get();

        TEnt Get(int? id);

        int Add(TEnt entity, string institution, string userid);

        void Remove(int? id);

        IEnumerable<re_select_studenttecher_subject_sp_Result> GetSubjectMembers(int? _subjectId);

        IEnumerable<re_select_studenttecher_subject_sp_Result> GetSubjectSearchedMembers(int? _subjectId, string Email);

        IEnumerable<re_select_count_member_studentwise_sp_Result> getSubjectList(int? _institutionID);

        IEnumerable<re_select_count_member_teacherwise_sp_Result> getSubjectsListTeacherId(int? _Teacherid, int? _institutionID);

        IEnumerable<re_select_count_member_studentwise_sp_Result> getLatestSubjectList(int? _institutionID);

        IEnumerable<re_select_count_member_studentwise_sp_Result> getArchiveSubjectList(int? _institutionID);

        IEnumerable<SubjectTeacher> GetSubjectTeacher(int? teacherId);

        IEnumerable<SubjectStudent> GetSubjecStudent(int? studentId);




        #region OtherMethods
        IEnumerable<TEnt> GetByInstitution(int institutionID);

        bool IsSubjectExistsByInstitution(int institutionID, string subjectName, int curSubjectID);

        //changes_01092017
        int GetMaxSubjectCountByInstitution(int institutionId);
        #endregion

    }
    [LoggingFilterAttribute]
    public class SubjectRepository : ISubjectRepository<Subject, int>
    {
        [Inject]
        ReelaeEntities context;
        //ReelaeEntities context = new ReelaeEntities();
        public SubjectRepository(ReelaeEntities _context)
        {
            context = _context;
        }

        public int Add(Subject entity, string institution, string userid)
        {
            try
            {
                Subject _exitingEntity = context.Subjects.Find(entity.SubjectID);
                if (_exitingEntity == null)
                {
                    entity.CreatedDate = DateTime.Now;
                    entity.InstitutionID = Convert.ToInt32(institution);
                    entity.CreatedBy = userid;
                    entity.SubjectCode = GetSubjectCode(entity.InstitutionID);
                    context.Subjects.Add(entity);
                    context.SaveChanges();
                    context.Entry(entity).GetDatabaseValues();
                    return entity.SubjectID;

                }
                else
                {
                    _exitingEntity.SubjectName = entity.SubjectName;
                    _exitingEntity.Description = entity.Description;
                    _exitingEntity.StartDate = entity.StartDate;
                    _exitingEntity.EndDate = entity.EndDate;
                    _exitingEntity.ModifiedBy = userid;
                    _exitingEntity.ModifiedDate = DateTime.Now;
                    context.SaveChanges();
                    return _exitingEntity.SubjectID;
                }
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public IEnumerable<Subject> Get()
        {
            return context.Subjects.ToList();

        }

        public Subject Get(int? id)
        {
            return context.Subjects.Find(id);
        }

        public void Remove(int? id)
        {
            var obj = context.Subjects.Find(id);
            if (obj != null)
            {
                obj.inactive = 1;
                context.SaveChanges();
            }
        }
        public IEnumerable<re_select_studenttecher_subject_sp_Result> GetSubjectMembers(int? _subjectId)
        {
            return context.re_select_studenttecher_subject_sp(_subjectId);
        }
        public IEnumerable<re_select_count_member_studentwise_sp_Result> getSubjectList(int? _institutionID)
        {
            return context.re_select_count_member_studentwise_sp(_institutionID, null);
        }
        public IEnumerable<re_select_count_member_teacherwise_sp_Result> getSubjectsListTeacherId(int? _Teacherid, int? _institutionID)
        {
            return context.re_select_count_member_teacherwise_sp(_institutionID, _Teacherid);
        }

        #region OtherMethods
        public IEnumerable<Subject> GetByInstitution(int institutionID)
        {
            return context.Subjects.Where(n => n.InstitutionID == institutionID).AsNoTracking().ToList();
        }

        public bool IsSubjectExistsByInstitution(int institutionID, string subjectName, int curSubjectID)
        {
            return context.Subjects.Any(n => n.InstitutionID == institutionID && n.SubjectName.ToLower() == subjectName.ToLower() && n.SubjectID != curSubjectID && n.inactive==0);
        }
        #endregion

        public IEnumerable<re_select_count_member_studentwise_sp_Result> getLatestSubjectList(int? _institutionID)
        {
            var res = context.re_select_count_member_studentwise_sp(_institutionID, null).Where(x => x.EndDate != null);
            return res.Where(x => x.EndDate.Value.Date >= DateTime.Now.Date);
        }

        public IEnumerable<re_select_count_member_studentwise_sp_Result> getArchiveSubjectList(int? _institutionID)
        {
            var res = context.re_select_count_member_studentwise_sp(_institutionID, null).Where(x => x.EndDate != null);
            return res.Where(x => x.EndDate.Value.Date < DateTime.Now.Date);
        }
        public IEnumerable<re_select_studenttecher_subject_sp_Result> GetSubjectSearchedMembers(int? _subjectId, string Email)
        {
            return context.re_select_studenttecher_subject_sp(_subjectId).Where(r => r.Email == Email).ToList();
        }

        public int GetMaxSubjectCountByInstitution(int institutionId)
        {
            return context.Subjects.Where(n => n.InstitutionID == institutionId).Count();
        }

        public IEnumerable<SubjectTeacher> GetSubjectTeacher(int? teacherId)
        {
            return context.SubjectTeachers.Where(r => r.TeacherID == teacherId && r.Subject.inactive==0).Distinct().ToList().OrderByDescending(r => r.CreatedDate);
        }

        public IEnumerable<SubjectStudent> GetSubjecStudent(int? studentId)
        {
            return context.SubjectStudents.Where(r => r.StudentID == studentId.Value && r.Subject.inactive == 0).Distinct().ToList().OrderByDescending(r => r.CreatedDate);

        }

        #region [PrivateMembers]
        private string GetSubjectCode(int InstitutionID)
        {
            try
            {
                var MaxSubjectCount = context.Subjects.Where(n => n.InstitutionID == InstitutionID).Count();
                var InstitutionCode = context.Institutions.Find(InstitutionID).IntialName;
                string newSubjectCode = "";
                if (!string.IsNullOrEmpty(InstitutionCode))
                {
                    int newCodeNumber = MaxSubjectCount + 1;
                    if (newCodeNumber > 99)
                        newSubjectCode = string.Format("{0}/{1}/{2}", InstitutionCode, "Sub", newCodeNumber.ToString());
                    else
                        newSubjectCode = string.Format("{0}/{1}/{2}", InstitutionCode, "Sub", newCodeNumber.ToString().PadLeft(3, '0'));

                }
                return newSubjectCode;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        #endregion
    }
}
