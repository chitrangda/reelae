﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReelaeDataAccessLayer;
using Ninject;
using System.Data.Entity;
using System.Data;
using Reelae.Filters;

namespace Reelae.Repositories
{

    public interface IForumRepository
    {
        IEnumerable<re_getForum_sp_Result> GetForTeacher(int id,string _subjectIds);

        IEnumerable<re_getForum_sp_Result> GetForStudent(int id, string _subjectIds);

        IEnumerable<Forum> GetAll(int? id);

        Forum Get(int id);

        IEnumerable<re_getForumUsersReply_sp_Result> GetAllReply(int forumId);

        IEnumerable<Subject> GetSubjects(string role, int? userId, int? instituteId);

        ForumUserAction GetReply(int id);


        void Add(Forum entity, string username);

        void Add(ForumUserAction entity, string username);

        void Remove(int id);

        void RemoveReply(int id);

        void Update(Forum entity, string username);

        void Update(ForumUserAction entity, string username);


    }

    public class ForumRepository : IForumRepository
    {
        [Inject]
        public ReelaeEntities context { get; set; }
        public void Add(Forum entity, string username)
        {
            entity.CreatedBy = username;
            entity.CreatedDate = DateTime.Now;
            context.Fora.Add(entity);
            context.SaveChanges();
        }

        public void Add(ForumUserAction entity, string username)
        {
            entity.CreatedBy = username;
            entity.CreatedDate = DateTime.Now;
            context.ForumUserActions.Add(entity);
            context.SaveChanges();
        }

        public IEnumerable<Forum> GetAll(int? id)
        {
            return context.Fora.Where(r => r.User.InstitutionID == id).OrderByDescending(r => r.CreatedDate);
        }

        public IEnumerable<re_getForum_sp_Result> GetForTeacher(int id, string subjectIds)
        {
            if (subjectIds == null)
            {
                var _subjectIds = context.SubjectTeachers.Where(r => r.TeacherID == id).Select(r => r.SubjectID).Distinct().ToArray();
                string joinSubjectIds = string.Join(",", _subjectIds);
                return context.re_getForum_sp(joinSubjectIds);
            }
            else
            {
                return context.re_getForum_sp(subjectIds);
            }


        }

        public IEnumerable<re_getForum_sp_Result> GetForStudent(int id, string subjectIds)
        {
            if (subjectIds == null)
            {
                var _subjectIds = context.SubjectStudents.Where(r => r.StudentID == id).Select(r => r.SubjectID).ToArray();
                string joinSubjectIds = string.Join(",", _subjectIds);
                return context.re_getForum_sp(joinSubjectIds);
            }
            else
            {
                return context.re_getForum_sp(subjectIds);

            }
        }

        public IEnumerable<Subject> GetSubjects(string role, int? userId, int? instituteId)
        {
            if (role == "Teacher")
            {
                return context.re_select_subject_teacherwise_sp(instituteId, userId).Select(r => new Subject() { SubjectID = r.SubjectID, SubjectName = r.SubjectName }).ToList();

            }
            else
            {
                return context.re_select_subject_studentwise_sp(instituteId, userId).Select(r => new Subject() { SubjectID = r.SubjectID, SubjectName = r.SubjectName }).ToList();

            }
        }

        public IEnumerable<re_getForumUsersReply_sp_Result> GetAllReply(int forumId)
        {
            return context.re_getForumUsersReply_sp(forumId);
        }

        public Forum Get(int id)
        {
            return context.Fora.Find(id);
        }

        public ForumUserAction GetReply(int id)
        {
            return context.ForumUserActions.Find(id);
        }

        public void Remove(int id)
        {
            var _actions = context.ForumUserActions.Where(r => r.ForumID == id);
            if (_actions != null && _actions.Count() > 0)
            {
                context.ForumUserActions.RemoveRange(_actions);
                context.SaveChanges();

            }
            var obj = context.Fora.Find(id);
            if (obj != null)
            {
                context.Fora.Remove(obj);
                context.SaveChanges();
            }
        }

        public void RemoveReply(int id)
        {
            context.ForumUserActions.Remove(context.ForumUserActions.Find(id));
            context.SaveChanges();
        }

        public void Update(Forum entity, string username)
        {
            var _existingEntity = context.Fora.Find(entity.ForumID);
            if(_existingEntity != null)
            {
                _existingEntity.Topic = entity.Topic;
                _existingEntity.Title = entity.Title;
                _existingEntity.PublishedDate = entity.PublishedDate;
                _existingEntity.Content = entity.Content;
                _existingEntity.ModifiedDate = DateTime.Now;
                _existingEntity.ModifiedBy = username;
                context.SaveChanges();
            }
        }

        public void Update(ForumUserAction entity, string username)
        {
            var existingEntity = context.ForumUserActions.Find(entity.ForumUserActionID);
            if(existingEntity!=null)
            {
                existingEntity.ResponseDescription = entity.ResponseDescription;
                existingEntity.ModifiedBy = username;
                existingEntity.ModifiedDate = DateTime.Now;
                context.SaveChanges();

            }
        }



    }
}
