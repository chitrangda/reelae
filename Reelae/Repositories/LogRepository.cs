﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;
using ReelaeDataAccessLayer;

namespace Reelae.Repositories
{
    public interface ILogRepository
    {
        IEnumerable<ErrorLog> Get();
        ErrorLog Get(int id);
        void Add(ErrorLog entity);
        void Remove(ErrorLog entity);

        #region [OthersMethods]
        
        #endregion
    }

    public class LogRepository : ILogRepository
    {
        [Inject]
        ReelaeEntities context { get; set; }
        public LogRepository(ReelaeEntities _context)
        {
            context = _context;
        }
        public void Add(ErrorLog entity)
        {
            context.ErrorLogs.Add(entity);
            context.SaveChanges();
        }

        public IEnumerable<ErrorLog> Get()
        {
            return context.ErrorLogs.ToList();
        }

        public ErrorLog Get(int id)
        {
            return context.ErrorLogs.Find(id);
        }

        public void Remove(ErrorLog entity)
        {
            var obj = context.ErrorLogs.Find(entity.ErrorLogID);
            context.ErrorLogs.Remove(obj);
            context.SaveChanges();
        }
    }
}