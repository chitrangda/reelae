﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using ReelaeDataAccessLayer;

namespace Reelae.Repositories
{

    //public interface IUserRepository<TEnt, in TPk> where TEnt : class
    //{
    //    IEnumerable<TEnt> Get();
    //    TEnt Get(string id);
    //    void Add(TEnt entity);
    //    void Remove(TEnt entity);
    //}


    public interface ISubjectTeacherRepository
    {
        IEnumerable<SubjectTeacher> Get();
        IEnumerable<SubjectTeacher> Get(int id);
        IEnumerable<re_getTeacherSubject_sp_Result> Get(int id, int instituteId);
        IEnumerable<re_getTeacherSubject_sp_Result> GetLatestSubject(int id, int instituteId);//latest subject
        IEnumerable<re_getTeacherSubject_sp_Result> GetArchiveSubject(int id, int instituteId);//archive subject
        void Add(SubjectTeacher entity, string username);
        void Remove(SubjectTeacher entity);

        bool IsTeacherMappedWithAnySubject(int teacherId);

        #region [Other Methods]
        void RemoveTeacherFromSubject(int subjectID, int teacherID);
        #endregion

        void removeTeacherById(int techerId); 
    }

    class SubjectTeacherRepository : ISubjectTeacherRepository
    {
        ReelaeEntities context { get; set; }
        public SubjectTeacherRepository(ReelaeEntities _context)
        {
            this.context = _context;
        }

        public void Add(SubjectTeacher entity, string username)
        {
            try
            {
                entity.CreatedBy = username;
                entity.CreatedDate = DateTime.Now;
                context.SubjectTeachers.Add(entity);
                context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public IEnumerable<SubjectTeacher> Get()
        {
            return context.SubjectTeachers.ToList();

        }

        public IEnumerable<SubjectTeacher> Get(int id)
        {
            return context.SubjectTeachers.Where(n => n.SubjectID == id);
        }

        public IEnumerable<re_getTeacherSubject_sp_Result> Get(int id, int instituteId)
        {
            //return context.re_getTeacherSubject_sp(id, instituteId);
            var result = context.re_getTeacherSubject_sp(id, instituteId).Where(x => x.EndDate != null);
            return result.Where(x => x.EndDate.Value.Date >= DateTime.Now.Date);
        }

        public IEnumerable<re_getTeacherSubject_sp_Result> GetLatestSubject(int id, int instituteId)
        {
            // return context.re_getTeacherSubject_sp(id, instituteId);
            var result = context.re_getTeacherSubject_sp(id, instituteId).Where(x => x.EndDate != null);
            return result.Where(x => x.EndDate.Value.Date >= DateTime.Now.Date);
        }
        public IEnumerable<re_getTeacherSubject_sp_Result> GetArchiveSubject(int id, int instituteId)
        {
            //return context.re_getTeacherSubject_sp(id, instituteId);
            var result = context.re_getTeacherSubject_sp(id, instituteId).Where(x => x.EndDate != null);
            return result.Where(x => x.EndDate.Value.Date < DateTime.Now.Date);
        }
        public void Remove(SubjectTeacher entity)
        {
            context.SubjectTeachers.Remove(context.SubjectTeachers.Where(n => n.TeacherID == entity.TeacherID).FirstOrDefault());
            context.SaveChanges();
        }


        #region Other Methods
        public void RemoveTeacherFromSubject(int subjectID, int teacherID)
        {
            var curTeacher = context.SubjectTeachers.Where(n => n.SubjectID == subjectID && n.TeacherID == teacherID).FirstOrDefault();
            if (curTeacher != null)
            {
                context.SubjectTeachers.Remove(curTeacher);
                context.SaveChanges();
            }
        }
        #endregion

        public bool IsTeacherMappedWithAnySubject(int teacherId)
        {
            if (context.SubjectTeachers.Where(n => n.TeacherID == teacherId).ToList().Count > 0)
            {
                return true;
            }
            return false;
        }

        public void removeTeacherById(int teacherId)
        {
            context.SubjectTeachers.RemoveRange(context.SubjectTeachers.Where(n => n.TeacherID == teacherId).ToList());
            context.SaveChanges();
        }
    }
}
