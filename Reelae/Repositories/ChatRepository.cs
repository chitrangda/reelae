﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReelaeDataAccessLayer;

namespace Reelae.Repositories
{
    public interface IChatRepository
    {
        int Add(Message message, ChatUser user);
        void UpdateMessageDeliveredStatus(int? id, int? userId);
        void UpdateMessageReceivedStatus(int? id, int? userId);
        void UpdateMessageReadStatus(int? id, int? userId);
        void UpdateMessageDeleted(int? id, int? userId);
        IEnumerable<re_GetSingleChatBody_sp_Result> GetIndividualChatMessages(int? from, int? to);
        void UpdateUserMessageReadStatus(int? from, int? to);

        IEnumerable<re_getUserSingleChatUnreadMessagesCount_sp_Result> GetUserSingleChatUnreadMessagesCount(int? toUserID);

        int? UserUnreadMessagesCount(int? from, int? to);

        void UpdateUserMessageDeletedStatus(int? from, int? to,int? messageId,bool issent);

    }
    public class ChatRepository : IChatRepository
    {
        ReelaeEntities context { get; set; }
        public ChatRepository(ReelaeEntities _context)
        {
            this.context = _context;
        }
        public ChatRepository()
        {
            this.context = new ReelaeEntities();
        }
        public int Add(Message message, ChatUser user)
        {
            context.Messages.Add(message);
            context.SaveChanges();
            user.MessageID = message.MessageID;
            context.ChatUsers.Add(user);
            context.SaveChanges();
            return message.MessageID;
        }

        public void UpdateMessageDeleted(int? id, int? userId)
        {
            throw new NotImplementedException();
        }

        public void UpdateMessageDeliveredStatus(int? id, int? userId)
        {
            throw new NotImplementedException();
        }

        public void UpdateMessageReadStatus(int? id, int? userId)
        {
            throw new NotImplementedException();
        }

        public void UpdateMessageReceivedStatus(int? id, int? userId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<re_GetSingleChatBody_sp_Result> GetIndividualChatMessages(int? from, int? to)
        {
            //UpdateUserMessageStatus(from, to);
            return context.re_GetSingleChatBody_sp(from, to);
        }

        public void UpdateUserMessageReadStatus(int? from, int? to)
        {
            //update message online status

            //var messagesIds = context.ChatUsers.Where(n => n.FromUserID == from && n.ToUserID == to && n.GroupID == null).Select(y => y.MessageID).ToList();
            //if (messagesIds != null)
            //{
            //    var messages = context.Messages.Where(z => messagesIds.Contains(z.MessageID) && z.IsOnline == 0);
            //    foreach (var item in messages)
            //    {
            //        item.IsOnline = 1;
            //    }
            //    context.SaveChanges();
            //}

            //update message read status

            var chatUsers = context.ChatUsers.Where(n => n.FromUserID == from && n.ToUserID == to && n.IsRead == 0 && n.GroupID == null);
            if (chatUsers != null)
            {
                foreach (var item in chatUsers)
                {
                    item.IsRead = 1;
                }
                context.SaveChanges();
            }
        }

        public IEnumerable<re_getUserSingleChatUnreadMessagesCount_sp_Result> GetUserSingleChatUnreadMessagesCount(int? toUserID)
        {
            return context.re_getUserSingleChatUnreadMessagesCount_sp(toUserID);
        }

        public int? UserUnreadMessagesCount(int? from, int? to)
        {
            return context.ChatUsers.Where(n => n.FromUserID == from && n.ToUserID == to && n.IsRead == 0 && n.GroupID == null).Count();
        }

        public void UpdateUserMessageDeletedStatus(int? from, int? to, int? messageId, bool issent)
        {
            var chatUser = context.ChatUsers.Where(n => n.FromUserID == from && n.ToUserID == to && n.GroupID == null && n.MessageID == messageId).FirstOrDefault();
            if (chatUser != null) {
                if (issent)
                    chatUser.IsMsgRemovedFrom = 1;
                else
                    chatUser.IsMsgRemovedTo = 1;
                context.SaveChanges();
            }
        }
    }
}