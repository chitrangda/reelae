﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReelaeDataAccessLayer;
using Ninject;
using System.Data.Entity;
using System.Data;
using Reelae.Filters;

namespace Reelae.Repositories
{
    [LoggingFilterAttribute]

    public class GradeRepository : IRepository<Grade, int>
    {
        [Inject]
        public ReelaeEntities context { get; set; }
        public void Add(Grade entity)
        {
            context.Grades.Add(entity);
        }

        public IEnumerable<Grade> Get()
        {
            return context.Grades.ToList();
        }

        public Grade Get(int id)
        {
            return context.Grades.Find(id);
        }

        public void Remove(Grade entity)
        {
            var obj = context.Grades.Find(entity.GradeID);
            context.Grades.Remove(obj);
            context.SaveChanges();
        }
    }
}
