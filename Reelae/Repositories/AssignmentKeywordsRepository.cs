﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;
using ReelaeDataAccessLayer;

namespace Reelae.Repositories
{
    public interface IAssignmentKeywordsRepository
    {
        IEnumerable<AssignmentKeyword> Get();
        AssignmentKeyword Get(int id);
        void Add(ref AssignmentKeyword entity);
        void Remove(AssignmentKeyword entity);

        #region [OthersMethods]
        IEnumerable<AssignmentKeyword> GetAssignmentKeywordsByAssignmentID(int? AssignmentID);

        void RemoveKeywordOfAssignment(int? AssignmentId, int? KeywordId);
        #endregion
    }
    public class AssignmentKeywordsRepository : IAssignmentKeywordsRepository
    {
        [Inject]
        ReelaeEntities context { get; set; }
        public AssignmentKeywordsRepository(ReelaeEntities _context)
        {
            context = _context;
        }
        public void Add(ref AssignmentKeyword entity)
        {
            int? AssignmentId = entity.AssignmentID;
            int? KeywordId = entity.KeywordID;
            var obj = context.AssignmentKeywords.Where(n => n.AssignmentID == AssignmentId && n.KeywordID == KeywordId).FirstOrDefault();
            if (obj == null)
            {
                context.AssignmentKeywords.Add(entity);
                context.SaveChanges();
            }
        }

        public IEnumerable<AssignmentKeyword> Get()
        {
            return context.AssignmentKeywords.AsNoTracking().ToList();
        }

        public AssignmentKeyword Get(int id)
        {
            return context.AssignmentKeywords.Find(id);
        }

        public void Remove(AssignmentKeyword entity)
        {
            var obj = context.AssignmentKeywords.Find(entity.AssignmentKeywordID);
            context.AssignmentKeywords.Remove(obj);
            context.SaveChanges();
        }

        #region OtherMethods
        public IEnumerable<AssignmentKeyword> GetAssignmentKeywordsByAssignmentID(int? AssignmentID)
        {
            return context.AssignmentKeywords.Where(n => n.AssignmentID == AssignmentID.Value).ToList();
        }

        public void RemoveKeywordOfAssignment(int? AssignmentId, int? KeywordId)
        {
            var obj = context.AssignmentKeywords.Where(n => n.AssignmentID == AssignmentId && n.KeywordID == KeywordId).FirstOrDefault();
            if (obj != null)
            {
                context.AssignmentKeywords.Remove(obj);
                context.SaveChanges();
            }
        }
        #endregion
    }
}