﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReelaeDataAccessLayer;
using Ninject;
using System.Data.Entity;
using System.Data;
using Reelae.Filters;

namespace Reelae.Repositories
{

    public interface IGroupRepository
    {
        IEnumerable<Group> Get();
        Group Get(int id);
        int Add(Group entity);
        void Remove(int? id);

        IEnumerable<Group> GetBySubjectID(int subjectID);

        IEnumerable<Group> GetBySubjectStudents(int subjectID, int StudentID);

        int AddGroupByTeacher(Group group, int teacherID);

        IEnumerable<re_getGroupMemberCount_sp_Result> getGroupWithMemberCountByTeacher(int? teacherid);

        bool IsGroupNameAlreadyExistsByTeacher(int teacherID, string groupName, int curGroupID);

        IEnumerable<re_getGroupMembers_sp_Result> getGroupMembers(int? groupid);

        IEnumerable<Group> GetStudentGroupsByTeacher(int studentId, int teacherId);

        IEnumerable<Group> GetTeacherGroups(int? teacherid);

        void StudentAddToGroup(int? studentid, int? teacherid, IEnumerable<string> curGroupIds);

        void AddStudentsToGroup(int? groupid, int? teacherid, IEnumerable<string> studentsIds);

        void StudentRemoveFromGroup(int? studentid, int? teacherid, int? GroupId);

        IEnumerable<re_getGroupMembers_sp_Result> GetGroupSearchedMember(int? groupid, string Email);

        IEnumerable<re_getTeacherGroup_sp_Result> getTeacherGroup(int TeacherId);


        void UpdateGroupSubject(string groupIds, int subjectId);

        IEnumerable<Group> GetSubjectGroups(int? subjectid, int? teacherid);
        void RemoveGroupFromSubject(int groupId, int subjectId);

        IEnumerable<re_GetStudentGroup_sp_Result> GetStudentGroups(int? subjectid);

        IEnumerable<re_getStudent_AddtoGroup_sp_Result> getStudentsAddtoGroup(int? GroupId, int? InstitueId);
    }

    [LoggingFilterAttribute]
    public class GroupRepository : IGroupRepository
    {
        [Inject]
        public ReelaeEntities context { get; set; }
        public GroupRepository(ReelaeEntities _context)
        {
            this.context = _context;
        }

        public int Add(Group entity)
        {
            try
            {
                context.Groups.Add(entity);
                context.SaveChanges();
                context.Entry(entity).GetDatabaseValues();
                return entity.GroupID;
            }
            catch (Exception)
            {
                return -1;
            }

        }

        public IEnumerable<Group> Get()
        {
            return context.Groups.ToList();
        }

        public Group Get(int id)
        {
            return context.Groups.Find(id);
        }

        public void Remove(int? id)
        {
            var obj = context.Groups.Find(id);
            obj.InActive = 1;
            context.SaveChanges();
        }

        public IEnumerable<Group> GetBySubjectID(int subjectID)
        {
            //var groupAssociations = context.GroupAssociations.Where(n => n.SubjectID == subjectID).AsNoTracking().ToList();
            return context.Groups.Where(n => n.SubjectID == subjectID).ToList();
        }

        public IEnumerable<Group> GetBySubjectStudents(int subjectID, int StudentID)
        {
            var groupAssociations = context.GroupAssociations.Where(n => n.IsActive.HasValue && n.IsActive.Value == 1 && n.StudentID == StudentID).AsNoTracking().ToList();
            return context.Groups.Where(n => groupAssociations.Any(y => y.GroupID == n.GroupID) && n.SubjectID == subjectID).ToList();
        }

        public int AddGroupByTeacher(Group group, int teacherID)
        {
            if (group.GroupID == 0)
            {
                //AddGroup
                group.CreatedBy = teacherID.ToString();
                group.TeacherID = teacherID;
                group.CreatedDate = DateTime.UtcNow;
                context.Groups.Add(group);
                context.SaveChanges();
                return group.GroupID;
            }
            else
            {
                //edit group
                var curGroup = context.Groups.Find(group.GroupID);
                curGroup.GroupName = group.GroupName;
                curGroup.GroupIntialName = group.GroupIntialName;
                curGroup.Description = group.Description;
                curGroup.ModifiedDate = DateTime.UtcNow;
                curGroup.SubjectID = group.SubjectID;
                curGroup.ModifiedBy = teacherID.ToString();
                context.SaveChanges();
                return curGroup.GroupID;
            }

        }

        public IEnumerable<re_getGroupMemberCount_sp_Result> getGroupWithMemberCountByTeacher(int? teacherid)
        {
            return context.re_getGroupMemberCount_sp(teacherid);
        }

        public bool IsGroupNameAlreadyExistsByTeacher(int teacherID, string groupName, int curGroupID)
        {
            return context.Groups.AsNoTracking().Any(n => n.TeacherID == teacherID && n.GroupName.ToLower() == groupName.ToLower() && n.GroupID != curGroupID && n.InActive == 0);
        }
        public IEnumerable<re_getGroupMembers_sp_Result> getGroupMembers(int? groupid)
        {
            return context.re_getGroupMembers_sp(groupid);
        }

        public IEnumerable<Group> GetStudentGroupsByTeacher(int studentId, int teacherId)
        {
            var groupAssociations = context.GroupAssociations.Where(n => n.IsActive.HasValue && n.IsActive.Value == 1 && n.StudentID == studentId).AsNoTracking();
            if (groupAssociations != null)
            {
                return context.Groups.Where(n => groupAssociations.Any(y => y.GroupID == n.GroupID) && n.TeacherID.Value == teacherId).ToList();
            }
            else
            {
                return new List<Group>();
            }

        }

        public IEnumerable<Group> GetTeacherGroups(int? teacherid)
        {
            return context.Groups.AsNoTracking().Where(n => n.TeacherID == teacherid && n.InActive == 0);
        }

        public void StudentAddToGroup(int? studentid, int? teacherid, IEnumerable<string> curGroupIds)
        {
            var teacherGroups = context.Groups.Where(n => n.TeacherID == teacherid);
            var groupAssociations = context.GroupAssociations.Where(n => !teacherGroups.Any(y => curGroupIds.ToList().Contains(y.GroupID.ToString())) && n.StudentID == studentid).ToList();
            var curGroupIdsToAdd = curGroupIds.Where(n => !groupAssociations.Any(y => y.GroupID.Value.ToString() == n)).ToList();

            foreach (var item in groupAssociations)
            {
                item.ModifiedBy = teacherid.ToString();
                item.ModifiedDate = DateTime.UtcNow;
                item.IsActive = 0;

            }
            foreach (var item in curGroupIdsToAdd)
            {
                var newAssociation = new GroupAssociation();
                newAssociation.IsActive = 1;
                newAssociation.CreatedBy = teacherid.ToString();
                newAssociation.CreatedDate = DateTime.UtcNow;
                newAssociation.GroupID = Convert.ToInt32(item);
                newAssociation.StudentID = studentid;
                context.GroupAssociations.Add(newAssociation);
            }
            context.SaveChanges();
        }

        public void StudentRemoveFromGroup(int? studentid, int? teacherid, int? GroupId)
        {
            var groupAssociation = context.GroupAssociations.Where(n => n.GroupID == GroupId && n.StudentID == studentid && n.IsActive == 1).FirstOrDefault();
            if (groupAssociation != null)
            {
                groupAssociation.IsActive = 0;
                groupAssociation.ModifiedBy = teacherid.ToString();
                groupAssociation.ModifiedDate = DateTime.Now;
                context.SaveChanges();
            }
        }

        public void AddStudentsToGroup(int? groupid, int? teacherid, IEnumerable<string> studentsIds)
        {
            foreach (var item in studentsIds)
            {
                int id = Convert.ToInt32(item);
                var exitingEntity = context.GroupAssociations.Where(r => r.StudentID == id && r.GroupID==groupid).FirstOrDefault();
                if (exitingEntity != null)
                {
                    exitingEntity.IsActive = 1;
                    context.SaveChanges();
                }
                else
                {
                    var newAssociation = new GroupAssociation();
                    newAssociation.IsActive = 1;
                    newAssociation.CreatedBy = teacherid.ToString();
                    newAssociation.CreatedDate = DateTime.UtcNow;
                    newAssociation.GroupID = groupid;
                    newAssociation.StudentID = Convert.ToInt32(item);
                    context.GroupAssociations.Add(newAssociation);
                    context.SaveChanges();
                }
            }
        }

        public IEnumerable<re_getGroupMembers_sp_Result> GetGroupSearchedMember(int? groupid, string Email)
        {
            return context.re_getGroupMembers_sp(groupid).Where(r => r.Email == Email).ToList();

        }
        public IEnumerable<re_getTeacherGroup_sp_Result> getTeacherGroup(int TeacherId)
        {
            return context.re_getTeacherGroup_sp(TeacherId);
        }

        public void UpdateGroupSubject(string groupIds, int subjectId)
        {
            IEnumerable<string> curGroupIds = new List<string>();
            if (!string.IsNullOrEmpty(groupIds))
            {
                curGroupIds = groupIds.Trim().Split(',');
            }
            foreach (string _groupId in curGroupIds)
            {
                var entity = context.Groups.Find(Convert.ToInt32(_groupId));
                if (entity != null)
                {
                    entity.SubjectID = subjectId;
                    context.SaveChanges();
                }
            }
        }

        public IEnumerable<Group> GetSubjectGroups(int? subjectid, int? teacherid)
        {
            return context.Groups.AsNoTracking().Where(n => n.SubjectID == subjectid && n.TeacherID == teacherid && n.InActive == 0);
        }
        public void RemoveGroupFromSubject(int groupId, int subjectId)
        {
            var curGroup = context.Groups.Where(x => x.GroupID == groupId).FirstOrDefault();
            curGroup.SubjectID = null;
            context.SaveChanges();
        }

        public IEnumerable<re_GetStudentGroup_sp_Result> GetStudentGroups(int? subjectid)
        {
            return context.re_GetStudentGroup_sp(subjectid);
        }

        public IEnumerable<re_getStudent_AddtoGroup_sp_Result> getStudentsAddtoGroup(int? GroupId, int? InstitueId)
        {
            return context.re_getStudent_AddtoGroup_sp(GroupId, InstitueId);
        }

    }
}
