﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReelaeDataAccessLayer;
using Ninject;
using System.Data.Entity;
using System.Data;
using Reelae.Filters;
using Reelae.Areas.Teacher.Models;

namespace Reelae.Repositories
{
    public interface IPinUpBoardRepository
    {
        void Add(PinUpBoard entity, string username);

        void Add(PinUpBoardFile entity);

        void Update(PinUpBoard entity, string username);

        void Update(PinUpBoardFile entity, string username);

        IEnumerable<PinUpBoard> GetAll(int? institueId);

        IEnumerable<re_getPinUpBoard_sp_Result> Get(int? studentId);

        IEnumerable<re_GetStudentGroup_sp_Result> getGroups(int? StudentId);

        IEnumerable<re_getPublicPinUpBoard_sp_Result> GetPublicPinUps(int? InstitutionId, int? StudentId);

        IEnumerable<re_getGroupPinUpBoard_sp_Result> GetGroupPinUps(int? InstitutionId, int? StudentId);

        PinUpBoard Get(int id);

        PinUpBoardFile GetFile(int id);

        IEnumerable<PinUpBoardFile> getFiles(int id);

        void Remove(int PinUpBoardId);

        void RemoveFile(int PinUpBoardFileId);

    }

    [LoggingFilterAttribute]
    public class PinUpBoardRepository : IPinUpBoardRepository
    {
        [Inject]
        public ReelaeEntities context { get; set; }
        public void Add(PinUpBoard entity, string username)
        {
            entity.CreatedBy = username;
            entity.CreatedDate = DateTime.Now;
            context.PinUpBoards.Add(entity);
            context.SaveChanges();

        }

        public void Add(PinUpBoardFile entity)
        {
            context.PinUpBoardFiles.Add(entity);
            context.SaveChanges();
        }

        public void Update(PinUpBoard entity, string username)
        {
            var existEntity = context.PinUpBoards.Find(entity.PinUpBoardID);
            if (existEntity != null)
            {
                existEntity.Visibilty = entity.Visibilty;
                existEntity.GroupId = entity.GroupId;
                existEntity.Content = entity.Content;
                existEntity.ModifiedBy = username;
                existEntity.ModifiedDate = DateTime.Now;
                context.SaveChanges();

            }

        }

        public IEnumerable<PinUpBoard> GetAll(int? institueId)
        {
            return context.PinUpBoards.Where(r => r.InstitutionId == institueId).OrderByDescending(r => r.CreatedDate);
        }

        public PinUpBoard Get(int id)
        {
            return context.PinUpBoards.Find(id);
        }

        public IEnumerable<re_GetStudentGroup_sp_Result> getGroups(int? StudentId)
        {
            return context.re_GetStudentGroup_sp(StudentId);

        }

        public IEnumerable<re_getPinUpBoard_sp_Result> Get(int? studentId)
        {
            return context.re_getPinUpBoard_sp(studentId);
        }

        public IEnumerable<re_getPublicPinUpBoard_sp_Result> GetPublicPinUps(int? InstitutionId, int? StudentId)
        {
            return context.re_getPublicPinUpBoard_sp(InstitutionId, StudentId);
        }

        public IEnumerable<re_getGroupPinUpBoard_sp_Result> GetGroupPinUps(int? InstitutionId, int? StudentId)
        {
            var _groups = context.re_GetStudentGroup_sp(StudentId).Select(r => r.GroupID).ToArray();
            string joinGroups = string.Join(",", _groups);
            return context.re_getGroupPinUpBoard_sp(InstitutionId, joinGroups);
        }

        public IEnumerable<PinUpBoardFile> getFiles(int id)
        {
            return context.PinUpBoardFiles.Where(r => r.PinUpBoardID == id).OrderByDescending(r => r.CreatedDate).ToList();
        }

        public PinUpBoardFile GetFile(int id)
        {
            return context.PinUpBoardFiles.Find(id);
        }

        public void Update(PinUpBoardFile entity, string username)
        {
            var existEntity = context.PinUpBoardFiles.Find(entity.PinUpBoardFileID);
            if (existEntity != null)
            {
                existEntity.FileUrl = entity.FileUrl;
                existEntity.FileName = entity.FileName;
                existEntity.FileSize = entity.FileSize;
                existEntity.FileType = entity.FileType;
                existEntity.ModifiedDate = DateTime.Now;
                existEntity.ModifiedBy = username;
                context.SaveChanges();
            }
        }

        public void Remove(int PinUpBoardId)
        {
            context.PinUpBoardFiles.RemoveRange(context.PinUpBoardFiles.Where(r => r.PinUpBoardID == PinUpBoardId));
            context.PinUpBoards.Remove(context.PinUpBoards.Find(PinUpBoardId));
            context.SaveChanges();
        }

        public void RemoveFile(int PinUpBoardFileId)
        {
            context.PinUpBoardFiles.Remove(context.PinUpBoardFiles.Find(PinUpBoardFileId));
            context.SaveChanges();
        }

    }

}
