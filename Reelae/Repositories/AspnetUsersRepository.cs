﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;
using ReelaeDataAccessLayer;

namespace Reelae.Repositories
{
    public interface IAspnetUsersRepository
    {
        bool update(string id,bool isEnabled);
        bool update(string id, string Name);
    }
    public class AspnetUsersRepository : IAspnetUsersRepository
    {
        [Inject]
        ReelaeEntities context { get; set; }
        public AspnetUsersRepository(ReelaeEntities _context)
        {
            context = _context;
        }
        public bool update(string id, bool isEnabled)
        {
            var curAspUsers = context.AspNetUsers.Find(id);
            if (curAspUsers != null)
            {
                curAspUsers.isEnabled = isEnabled;
                context.SaveChanges();
                return true;
            }
            else
                return false;
        }
        public bool update(string id, string Name)
        {
            var curAspUsers = context.AspNetUsers.Find(id);
            if (curAspUsers != null)
            {
                curAspUsers.Name = Name;
                context.SaveChanges();
                return true;
            }
            else
                return false;
        }
    }
}