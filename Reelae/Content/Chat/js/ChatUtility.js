﻿var GlobalChatObject = function (chatConfigObject) {

    //jQueryMessageBox, jQueryChatArea, jQueryNotificationArea, jQuerySendButton, jQueryMembersLists, _userToken, _userName
    if (!chatConfigObject) {
        return null;
    }

    /* globals and defaults */
    var MessageType = { "File": { "Document": "doc", "PDF": "pdf", "Image": "image", "ZIP": "zip", "Video": "video", "Audio": "audio", "Text": "txt" }, "Text": "text", "Emoticons": "emoticons", "System": "system" };
    var SendType = { "Group": "group", "Single": "single" };
    var chatStatus = { Online: "online", Offline: "offline" };
    /* end */

    var noSupportMessage = "Your browser cannot support WebSocket!";
    var ws;
    var messageBoxElement = chatConfigObject.jQueryMessageBox || null;
    var chatAreaElement = chatConfigObject.jQueryChatArea || null;
    var notificationAreaElement = chatConfigObject.jQueryNotificationArea || null;
    var selUserInfo = { userToken: null, userName: null, chatType: SendType.Single, status: chatStatus.Offline, userEmailId: null };
    var userToken = chatConfigObject._userToken || null;
    var userName = chatConfigObject._userName || null;
    var userEmailId = chatConfigObject._userEmailId || null;
    var sendButton = chatConfigObject.jQuerySendButton || null;
    var membersLists = chatConfigObject.jQueryMembersLists || null;
    var curUserHeaderEle = chatConfigObject.jQueryUserHeaderEle || null;
    var chat = $.connection.chattingHub || null;
    var messagesList = [];

    function ChatObject(_userToken, _userName) {
        this.userToken = _userToken;
        this.userName = _userName;
        this.isObjectReady = true;
        curChatType = SendType.Single;
    }
    /* Public Methods */

    //create message format object
    ChatObject.prototype.createObjectToSend = function (type, message, sendType, sendTo, toEmailId, userStatus) {
        return new ObjectToSend(type, message, sendType, sendTo, toEmailId, userStatus);
    }
    //send message
    ChatObject.prototype.onFileSend = function (fileName, filePath, fileType) {
        debugger
        var messageObj = currentChatObject.createObjectToSend(GetTypeByInputFileType(fileType), fileName, selUserInfo.chatType, selUserInfo.userToken, selUserInfo.userEmailId, selUserInfo.status);
        messageObj.attachmentUrl = filePath;
        currentChatObject.sendMessage(messageObj);
    }
    ChatObject.prototype.sendMessage = function (MessageObj) {
        if (MessageObj instanceof ObjectToSend) {
            if (MessageObj.type && MessageObj.message && MessageObj.sendType && MessageObj.sendTo) {
                saveMessageBeforeSend(MessageObj);
                if (selUserInfo.chatType == SendType.Single) {
                    var isReceived = false;
                    if (MessageObj.type == "text") {
                        //set text message
                        setMessageToMessageBox(MessageObj, isReceived);
                    }
                    else {
                        //set other messages type
                        setFileObjectToChatArea(MessageObj, MessageObj.type, isReceived);
                    }
                    chat.server.sendIndividualMessage(JSON.stringify(MessageObj));
                }
                else {
                    chat.server.sendGroupMessage(MessageObj);
                }
            }
            else {
                throw new Error("Some Object Keys Are Missing.");
            }
        }
        else {
            throw new Error("Message Object is not Valid.");
        }
    }
    ChatObject.prototype.setChatType = function (_chatType) {
        if (_chatType)
            curChatType = _chatType;

    }
    ChatObject.prototype.onUserSelect = function (curMemberSection) {
        if (curMemberSection) {
            //if (sendButton.hasClass('btn-danger')) {
            //    sendButton.removeClass('btn-danger').addClass('btn-success');
            //}
            membersLists.find('.active').removeClass('active');
            curMemberSection.addClass('active');

            selUserInfo.chatType = curMemberSection.attr('cT');
            selUserInfo.userName = curMemberSection.find('.chat-user_name h2.name').attr('title').trim();
            selUserInfo.userToken = curMemberSection.attr('userToken');
            selUserInfo.status = getUserChatStatus(curMemberSection);
            selUserInfo.userEmailId = curMemberSection.attr('uEId');
            //clear notification on click
            var notiEle = curMemberSection.find('span.bg-noti');
            notiEle.text('');
            notiEle.hide();

            //update selected user Header Info
            updateChatBoxHeader("");
        }
        else {
            selUserInfo = { userToken: null, userName: null, chatType: SendType.Single, status: chatStatus.Offline, userEmailId: null };
        }
    }
    ChatObject.prototype.OnMessageSend = function () {
        messageBoxElement.empty();
    }
    ChatObject.prototype.updateMessageId = function (msgId, messageToken) {
        debugger
        var curEle = chatAreaElement.find('div[tS="' + messageToken + '"]');
        if (curEle.length)
            curEle.attr('msgToken', msgId);
    }
    ChatObject.prototype.OnMessageReceived = OnMessageReceived;
    ChatObject.prototype.OnMessageRead = OnMessageRead;
    ChatObject.prototype.setPresenseStatus = setPresenseStatus;
    ChatObject.prototype.setOthersPresenseStatus = setOthersPresenseStatus;
    ChatObject.prototype.sendConnectionStatusChangeMessage = function (message) {
        chat.server.sendConnectionStatusChange(message);
    }
    //wrapper functions

    //connection change functions

    //chat client messages
    //chat.client.receiveConnectionStatusChangeMessage = function (_userToken, status) {
    //    alert('received');
    //    setPresenseStatus(_userToken, status);
    //}
    //chat.client.getUserOnlineStatus = function (_userToken, status) {
    //    setPresenseStatus(_userToken, status);
    //}
    //chat.client.receiveIndividualMessage = function (messageReceived) {
    //    alert(messageReceived);
    //    OnMessageReceived(messageReceived);
    //}
    //chat.client.receiveHello = function (message) {
    //    alert("messageRecived");
    //}
    /* End */

    /* Private Members */
    function getCurrentUser() {
        var _user = userToken;
        return _user;
    }
    function getUserBoxElement(uEId) {
        if (uEId) {
            return membersLists.find('.user_chat-detail[uEId="' + uEId + '"]');
        }
        else
            return null;
    }
    function ObjectToSend(type, message, sendType, sendTo, toEmailId, userStatus) {
        this.from = userToken;
        this.type = type;
        this.message = message;
        this.sendType = SendType.Single;
        this.sendTo = sendTo;
        this.messageToken = getCurMessageToken();
        this.messageId = "";
        this.fromEmailId = userEmailId;
        this.toEmailId = toEmailId;
        this.messageDate = "";
        this.attachmentUrl = "";
        this.userStatus = userStatus;
    }
    function getCurMessageToken() {
        return (new Date()).valueOf() + '_' + userToken;
    }
    function getFileType() {


    }
    function getUserChatStatus(chatDetailEle) {
        if (chatDetailEle) {
            var _cS = chatDetailEle.find('.user_status>i').hasClass('txt_on') ? chatStatus.Online : chatStatus.Offline;
            return _cS;
        }
        else
            return chatStatus.Offline;
    }
    function saveMessageBeforeSend(curMessage) {
        var _getMessagesLists = localStorage.getItem('messagesList');
        if (!_getMessagesLists) {
            _getMessagesLists = [];
        }
        else {
            _getMessagesLists = JSON.parse(_getMessagesLists);
        }
        _getMessagesLists.push(curMessage);
        localStorage.setItem('messagesList', JSON.stringify(_getMessagesLists));
    }
    /* End */

    /* Events */
    function scrollToBottom(scrollSource, scrollTo) {
        //target.animate({ scrollTop: target[0].scrollHeight });
        try {
            if (!scrollSource.data('mCS')) {
                scrollSource.mCustomScrollbar({
                    theme: "dark-3",
                    autoHideScrollbar: false
                });
            }
            //else {
            //    scrollSource.mCustomScrollbar('destroy');
            //    scrollSource.mCustomScrollbar({
            //        theme: "dark-3",
            //        autoHideScrollbar: false
            //    });
            //}
            scrollSource.mCustomScrollbar('scrollTo', scrollTo);
        } catch (e) {

        }
    }
    function OnMessageReceived(serializedObj) {
        debugger
        if (serializedObj) {
            var parsedMessageObj = null;
            try {
                parsedMessageObj = JSON.parse(serializedObj);
            } catch (e) {
                parsedMessageObj = eval(serializedObj);
            }

            if (parsedMessageObj.type && parsedMessageObj.type != "system") {
                var isReceived = true;
                if (parsedMessageObj.type == "pdf" || parsedMessageObj.type == "doc" || parsedMessageObj.type == "image" || parsedMessageObj.type == "zip") {
                    var fileVar = parsedMessageObj.message.split('`');
                    //setFileObjectToImageBox(fileVar[0], fileVar[1], parsedMessageObj.from);
                    setFileObjectToChatArea(parsedMessageObj, parsedMessageObj.type, isReceived);
                }
                else {

                    if (selUserInfo.userEmailId == parsedMessageObj.fromEmailId)
                        setMessageToMessageBox(parsedMessageObj, isReceived);
                    else
                        updateIndividualMsgReceivedCounter(parsedMessageObj.fromEmailId);
                }
            }
            else {

            }
        }
    }
    function OnMessageRead(messageId) {

    }
    function setMessageToMessageBox(MessageObject, isreceived) {
        if (chatAreaElement && MessageObject) {
            var currentHtml = "";
            var curDate=(new Date());
            var msgDeliveredStatus = (MessageObject.userStatus == chatStatus.Online) ? "" + curDate.toLocaleDateString()
+ " " + curDate.toLocaleTimeString()
 : "Not Delivered";
            if (isreceived)
                currentHtml = '<div msgToken="' + MessageObject.messageId + '" class="bubble_chat"><i title="remove this message" class="material-icons cancel_chat">cancel</i><p>' + MessageObject.message + '</p><span class="time">Sent ' + MessageObject.messageDate + '</span></div>';
            else
                currentHtml = '<div tS="' + MessageObject.messageToken + '" msgToken="' + MessageObject.messageId + '" class="bubble_chat bubble_chat-alt white"><i title="remove this message" class="material-icons cancel_chat">cancel</i><p>' + MessageObject.message + '</p><span class="time">' + msgDeliveredStatus + '</span></div>';

            if (chatAreaElement.data('mCS'))
                chatAreaElement.find('.mCSB_container').first().append($(currentHtml));
            else
                chatAreaElement.append($(currentHtml));

            scrollToBottom(chatAreaElement, chatAreaElement.find('div.bubble_chat:last'));
        }
    }
    function setFileObjectToImageBox(fileName, FilePath, pName) {
        if (chatAreaElement) {
            var currentHtml = "";
            if (isreceived)
                currentHtml = '<div msgToken="' + MessageObject.messageId + '" class="bubble_chat"><p><img src="' + MessageObject.message + '" alt="..." class="img-responsive"></p></div>';
            else
                currentHtml = '<div msgToken="' + MessageObject.messageId + '" class="bubble_chat bubble_chat-alt green"><p><img src="' + MessageObject.message + '" alt="..." class="img-responsive"></p></div>';
            chatAreaElement.append($(currentHtml));
        }

        //if (messageBoxElement) {
        //    var url = "Downloader.ashx?fP=" + encodeURIComponent(FilePath) + "&fN=" + encodeURIComponent(fileName) + "";
        //    var ach = $('<a>');
        //    ach.attr('href', url);
        //    ach.html(fileName);
        //    ach.attr('target', '_blank');
        //    messageBoxElement.append("<br/># " + pName + " " + ach[0].outerHTML);
        //    ach.trigger('click');
        //}
    }
    function updateIndividualMsgReceivedCounter(fromUserEmailID) {
        if (fromUserEmailID) {
            var fromUserEle = getUserBoxElement(fromUserEmailID);
            if (fromUserEle) {
                var notiEle = fromUserEle.find('span.bg-noti');
                var preCounter = notiEle.text();
                if (preCounter.trim())
                    notiEle.text(+preCounter + 1);
                else
                    notiEle.text("1");
                notiEle.show();
            }
        }
    }
    function setPresenseStatus(_userToken, status, lastActiveTime) {
        debugger
        if (_userToken && membersLists.length) {
            var curUser = membersLists.find('.user_chat-detail[uEId="' + _userToken + '"]');
            if (curUser.length == 0)
                return false;
            if (status == chatStatus.Online) {
                curUser.find('div.user_status>i').removeClass('txt_off').addClass('txt_on');
            }
            else {
                curUser.find('div.user_status>i').removeClass('txt_on').addClass('txt_off');
            }
            if (selUserInfo.userEmailId == _userToken) {
                selUserInfo.status = status;
                updateChatBoxHeader(lastActiveTime);
                updateSentMessageDeliveredStatus();
            }
        }
    }
    function setOthersPresenseStatus(_userTokenList, status, lastActiveTime) {
        debugger
        if (_userTokenList.length && membersLists.length) {
            _userTokenList.forEach(function (element, index) {
                var curUser = membersLists.find('.user_chat-detail[uEId="' + element + '"]');
                if (curUser.length) {
                    if (status == chatStatus.Online)
                        curUser.find('div.user_status>i').removeClass('txt_off').addClass('txt_on');
                    else
                        curUser.find('div.user_status>i').removeClass('txt_on').addClass('txt_off');
                }

                if (selUserInfo.userEmailId == element) {
                    selUserInfo.status = status;
                    updateChatBoxHeader(lastActiveTime);
                    updateSentMessageDeliveredStatus();
                }
            });
        }
    }
    function updateChatNotificationCounter() {
        var curCounter = notificationAreaElement.find('span.badge').text();
        if (curCounter.trim() && !isNan(curCounter.trim())) {
            notificationAreaElement.find('span.badge.bg-chat').text(parseInt(curCounter, 10) + 1);
        }
        else
            notificationAreaElement.find('span.badge.bg-chat').text(1);
    }
    function updateChatBoxHeader(lastActiveTime) {
        //change header also if user is selected
        var curStatusClass = "";
        var curStatusText = "";
        var lastActiveTime = "";
        if (selUserInfo.status == chatStatus.Online) {
            curStatusClass = 'txt_on';
            curStatusText = "Online";
        }
        else {
            curStatusClass = 'txt_off';
            curStatusText = "Offline";
            lastActiveTime = (lastActiveTime) ? lastActiveTime : "";
        }
        var curuserChatHeaderHtml = "";
        if (memberArea == "Student") {
            curuserChatHeaderHtml = '<h2 style="float:none !important;" class="name">' + selUserInfo.userName + '<div class="clearfix"></div><small><i class="fa fa-circle ' + curStatusClass + '" aria-hidden="true"></i> &nbsp;' + curStatusText + '</small><div class="clearfix"></div><small>' + lastActiveTime + '</small></h2>';
        }
        else {
            curuserChatHeaderHtml = '<h2 class="name">' + selUserInfo.userName + '<div class="clearfix"></div><small><i class="fa fa-circle ' + curStatusClass + '" aria-hidden="true"></i> &nbsp;' + curStatusText + '</small><div class="clearfix"></div><small>' + lastActiveTime + '</small></h2>';
        }
        curUserHeaderEle.html(curuserChatHeaderHtml);
    }
    function htmlEncode(value) {
        var encodedValue = $('<div />').text(value).html();
        return encodedValue;
    }
    function updateSentMessageDeliveredStatus() {
        if (chatAreaElement) {
            curDate = new Date();
            chatAreaElement.find('div.bubble_chat-alt span.time').html(curDate.toLocaleDateString()
+ " " + curDate.toLocaleTimeString());
        }
    }
    /* End */

    /*Set file Types Ui functions*/
    function setFileObjectToChatArea(MessageObject, fileType, isreceived) {
        var innerHtml = "";
        var addonClass = (isreceived) ? "" : " bubble_chat-alt white";
        var curDate = new Date();
        var msgDeliveredStatus = (MessageObject.userStatus == chatStatus.Online) ? ""+curDate.toLocaleDateString()
+ " " + curDate.toLocaleTimeString() : "Not Delivered";
        var msgDate = (isreceived) ? "Sent " + MessageObject.messageDate : msgDeliveredStatus;
        var downloadUrl = "/Chat/DownloadFile?fP=" + encodeURIComponent(MessageObject.attachmentUrl) + "&fN=" + encodeURIComponent(MessageObject.message) + "";

        if (fileType == "image" || fileType == "img") {
            innerHtml = '<div tS="' + MessageObject.messageToken + '" msgToken="' + MessageObject.messageId + '" class="bubble_chat' + addonClass + '"><i title="remove this message" class="material-icons cancel_chat">cancel</i><a target="_blank" class="fileView" href="' + downloadUrl + '"><p><img src="' + MessageObject.attachmentUrl + '" alt="..." class="img-responsive"></p><span class="time">' + msgDate + '</span></a></div>';
        }
        else if (fileType == "video") {
            innerHtml = '<div tS="' + MessageObject.messageToken + '" msgToken="' + MessageObject.messageId + '" class="bubble_chat' + addonClass + '"><i title="remove this message" class="material-icons cancel_chat">cancel</i><a class="fileView" target="_blank" href="' + downloadUrl + '"><p><video class="img-responsive" id="thumb"><source src="' + MessageObject.attachmentUrl + '" /></video></p><span class="time">' + msgDate + '</span></a></div>';
        }
        else if (fileType == "audio") {
            innerHtml = '<div tS="' + MessageObject.messageToken + '" msgToken="' + MessageObject.messageId + '" class="bubble_chat' + addonClass + '"><i title="remove this message" class="material-icons cancel_chat">cancel</i><a class="fileView" target="_blank" href="' + downloadUrl + '"><p><audio  controls><source src="' + MessageObject.attachmentUrl + '" /></audio></p><span class="time">' + msgDate + '</span></a></div>';
        }
        else if (fileType == "doc" || fileType == "pdf" || fileType == "zip") {
            var fileNameToShow = (MessageObject.message.length > 35) ? (MessageObject.message.substring(0, 32) + "...") : MessageObject.message;
            innerHtml = '<div tS="' + MessageObject.messageToken + '" msgToken="' + MessageObject.messageId + '" class="bubble_chat' + addonClass + '"><i title="remove this message" class="material-icons cancel_chat">cancel</i><p><a title="' + MessageObject.message + '" class="fileView  chat_attachment bg_org" target="_blank" href="' + downloadUrl + '"><i class="material-icons">attachment</i><span class="attachment_txt">' + fileNameToShow + '</span></a></p><span class="time">' + msgDate + '</span></div>';
        }
        if (chatAreaElement.data('mCS'))
            chatAreaElement.find('.mCSB_container').first().append($(innerHtml));
        else
            chatAreaElement.append($(innerHtml));
        scrollToBottom(chatAreaElement, chatAreaElement.find('div.bubble_chat:last'));
    }

    /*End*/

    /* Handler functions */

    function OnSendEvent(event) {
        if (!selUserInfo.userToken) {
            showAlertBox("Information Alert Message", "Please select any member to chat with.", gInfo.alertMsgType.information);
            return false;
        }
        var message = parseEmoticons(messageBoxElement.val().trim());
        if (!message) {
            showAlertBox("Information Alert Message", "Message should not be empty.", gInfo.alertMsgType.information);
            return false;
        }
        var messageObj = currentChatObject.createObjectToSend(MessageType.Text, message, selUserInfo.chatType, selUserInfo.userToken, selUserInfo.userEmailId, selUserInfo.status);
        //empty input area
        messageBoxElement.val('');
        currentChatObject.sendMessage(messageObj);
        messageBoxElement.focus();
    }

    function OnEnterKeySendEvent(event) {
        if (event.keyCode == 13) {
            if (!selUserInfo.userToken) {
                showAlertBox("Information Alert Message", "Please select any member to chat with.", gInfo.alertMsgType.information);
                return false;
            }
            var message = parseEmoticons(messageBoxElement.val().trim());
            if (!message) {
                showAlertBox("Information Alert Message", "Message should not be empty.", gInfo.alertMsgType.information);
                return false;
            }
            var messageObj = currentChatObject.createObjectToSend(MessageType.Text, message, selUserInfo.chatType, selUserInfo.userToken, selUserInfo.userEmailId, selUserInfo.status);
            //empty input area
            messageBoxElement.val('');
            currentChatObject.sendMessage(messageObj);
            event.preventDefault();
            event.stopPropagation();
            messageBoxElement.focus();
        }
    }
    //select user on click
    $(document).on('click', '#' + membersLists.attr('id') + ' .user_chat-detail', function (event) {
        currentChatObject.onUserSelect($(event.currentTarget));
        var unreadCallback = function () {
            if (chatAreaElement.find('.time_bar.unread').length) {
                UpdateUserUnreadMessagesToRead(selUserInfo.userToken, chatAreaElement);
            }
        }
        GetIndividualChatBody(selUserInfo.userToken, chatAreaElement, unreadCallback);
    });
    //on send button click
    $(document).on('click', '#' + sendButton.attr('id') + '', OnSendEvent);
    $(document).on('click', '#chkSent', function (event) {
        if ($(this).find('i').hasClass('fa-square-o')) {
            $(this).find('i.fa-square-o').removeClass('fa-square-o');
            $(this).find('i').addClass('txt_green');
            $(document).on('keydown', '#' + messageBoxElement.attr('id'), OnEnterKeySendEvent)
        }
        else {
            $(this).find('i').addClass('fa-square-o');
            $(this).find('i').removeClass('txt_green');
            $(document).off('keydown', '#' + messageBoxElement.attr('id'), OnEnterKeySendEvent)
        }
    });

    $(document).on('click', '#' + chatAreaElement.attr('id') + ' .material-icons.cancel_chat', function (event) {
        var curMsgDiv = $(this).parent();
        var callbackFunction = function () {
            curMsgDiv.remove();
        };
        UpdateIndividualMessageDeletedStatus(curMsgDiv.attr('msgToken'), (curMsgDiv.hasClass('bubble_chat-alt')) ? true : false, selUserInfo.userToken, callbackFunction);
    });
    //smilies handler
    $(document).on('click', '.smiley-box>img[title]', function (event) {
        var _Code = $(this).attr('title');
        if (_Code) {
            var existingVal = messageBoxElement.val();
            var curCursorposition = messageBoxElement[0].selectionStart;
            if (curCursorposition && curCursorposition > -1) {
                var updatedValue = "";
                if (curCursorposition == existingVal.length) {
                    updatedValue = existingVal + "" + _Code;
                }
                else {
                    updatedValue = existingVal.substr(0, curCursorposition) + _Code + existingVal.substr(curCursorposition);
                }
                messageBoxElement.val(updatedValue);
            }
            else {
                messageBoxElement.val(existingVal + " " + _Code);
            }

            $('#addSmiliesLnk').trigger('click');
        }
    });

    $(document).on('keydown', '#txtChatMemberFilter', function (eve) {
        if (eve.keyCode == 13) {
            OnChatMembersSearch($(eve.currentTarget));
            eve.stopPropagation();
            eve.preventDefault();
        }
    });

    //chat export
    $(document).on('click', '#chatExportForm [name="btnExportChat"]', function (eve) {
        debugger
        if (selUserInfo.userToken) {
            $('#chatExportForm [name="userId"]').val(selUserInfo.userToken);
            $('#chatExportForm [name="userName"]').val(selUserInfo.userName);
            $('#chatExportForm [name="btnSubmit"]').trigger('click');
        }
        else {
            showAlertBox("Information Alert Message", "Please select any member to export chat.", gInfo.alertMsgType.information);
            return false;
        }
    });
    $(document).on('click', '#chatExportForm [name="btnExportChatCsv"]', function (eve) {
        debugger
        if (selUserInfo.userToken) {
            var callbackBackFunc = function (result) {
                if (result) {
                    export_table_to_csv(result.Data, "ChatHistory_" + (new Date()).toDateString() + ".csv");
                    $("#divLoader").hide();
                }
            }
            ExportChatCsv(selUserInfo.userToken, selUserInfo.userName, callbackBackFunc);
        }
        else {
            showAlertBox("Information Alert Message", "Please select any member to export chat.", gInfo.alertMsgType.information);
            return false;
        }
    });

    /* */
    /* End Chat Hub Functionality*/

    /*Ajax function Calls*/

    /*End Ajax Calls */
    var currentChatObject = new ChatObject();
    currentChatObject.messageType = MessageType;
    currentChatObject.sendType = SendType;
    currentChatObject.messageBox = messageBoxElement;
    return currentChatObject;
};