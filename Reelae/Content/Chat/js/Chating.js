﻿//global Info
var roleData = ["Admin", "Teacher", "Student"];
var curSearchStr = "";
//var validExtentions = ["png", "jpg", "jpeg", "mp3", "mp4", "wmv", "avi", "gif", "tif", "zip", "xls", "xlsx", "doc", "docx", "pdf", "txt", "rtf", "rar", "flv", "acc"];

// This optional function html-encodes messages for display in the page.
function htmlEncode(value) {
    var encodedValue = $('<div />').text(value).html();
    return encodedValue;
}
//function isValidExtention(inputfile) {
//    if (!inputfile) {
//        showAlertBox("Failure Alert Message", "Its is not a valid type of file.", gInfo.alertMsgType.information);
//        return false;
//    }
//    var filePart = inputfile.name.trim().split('.');
//    if (filePart.length < 2) {
//        showAlertBox("Failure Alert Message", "Its is not a valid type of file.", gInfo.alertMsgType.information);
//        return false;
//    }
//    var fileExtension = filePart[filePart.length - 1];
//    if (inputfile && validExtentions.indexOf(fileExtension.trim()) > -1) {
//        return true;
//    }
//    else {
//        showAlertBox("Failure Alert Message", "Its is not a valid type of file.", gInfo.alertMsgType.information);
//        return false;
//    }
//}
//resumable upload work
var r = new Resumable({
    target: "/Chat/ResumableUploader"
});
r.on('fileSuccess', function (file, responseText) {
    if (responseText && responseText.indexOf("filePath") > -1) {
        //sendFileMessage(file.fileName, responseText.split("filePath=")[1], file.file.type);
        window.chatgInfo.onFileSend(file.fileName, responseText.split("filePath=")[1], file.file.type);
    }
    console.debug('fileSuccess', file);
});
r.on('fileProgress', function (file) {
    console.debug('fileProgress', file);
});
r.on('fileAdded', function (file, event) {
    debugger
    if (isValidExtention(file.file))
        r.upload();
    else {
        event.preventDefault();
        event.stopPropagation();
        return false;
    }
    console.debug('fileAdded', event);
});
r.on('filesAdded', function (array) {
    r.upload();
    console.debug('filesAdded', array);
});
r.on('fileRetry', function (file) {
    console.debug('fileRetry', file);
});
r.on('fileError', function (file, message) {
    console.debug('fileError', file, message);
});
r.on('uploadStart', function () {
    console.debug('uploadStart');
});
r.on('complete', function () {
    console.log(arguments);
    console.debug('complete');
});
r.on('progress', function () {
    console.debug('progress');
});
r.on('error', function (message, file) {
    console.debug('error', message, file);
});
r.on('pause', function () {
    console.debug('pause');
});
r.on('cancel', function () {
    console.debug('cancel');
});
function iniFileUploading() {

}

function GetTypeByInputFileType(fileType) {
    if (fileType.indexOf("pdf") > -1)
        return window.chatgInfo.messageType.File.PDF;
    else if (fileType.indexOf("doc") > -1 || fileType.indexOf("docx") > -1)
        return window.chatgInfo.messageType.File.Document;
    else if (fileType.indexOf("image") > -1)
        return window.chatgInfo.messageType.File.Image;
    else if (fileType.indexOf("video") > -1)
        return window.chatgInfo.messageType.File.Video;
    else if (fileType.indexOf("audio") > -1)
        return window.chatgInfo.messageType.File.Audio;
    else if (fileType.indexOf("zip") > -1)
        return window.chatgInfo.messageType.File.ZIP;
    else
        return "";

}
//uploading functions

//emoticons message
var emoticonData = {
    ":aln": "alien1", ":thk": "tease", ":ang": "angle", ":slp": "sleep", ":blnk": "blanco",
    ":zip": "zip_it", ":bor": "boring", ":brb": "brb", ":bsy": "busy", ":cell": "cellphone",
    ":tp": "clock", ":cool": "cool", ":czy": "crazy", ":cry": "cry", ":dvl": "devil",
    ":blush": "blush", ":stop": "dnd", ":flwr": "flower", ":heart": "heart", ":geek": "geek",
    ":gift": "gift", ":ill": "ill", ":love": "in_love", ":file": "text_file", ":kiss": "kissy",
    ":laugh": "laugh", ":mail": "mail", ":music2": "music2", ":whst": "not_guilty", ":please": "please",
    ":info": "info", ":sad": "sad", ":silly": "silly", ":lol": "", ":slps": "speechless",
    ":srpd": "surprised", ":tease": "tease", ":music": "music1", ":wink": "wink", ":grin": "xd"
};
function parseEmoticons(txtmessage) {
    if (!txtmessage) {
        return "";
    }
    else {
        var updatedString = txtmessage;
        var emoticonPat = /(\:{1,1}[a-z]+)/g;
        if (emoticonPat.test(txtmessage)) {
            var matchWords = txtmessage.match(emoticonPat);
            for (var i = 0; i < matchWords.length; i++) {
                var msg = getImageHtmlByEmoticonCode(matchWords[i]);
                if (msg != matchWords[i])
                    updatedString = updatedString.replace(matchWords[i], msg);
            }
        }
        return updatedString;
    }
}

function getImageHtmlByEmoticonCode(emoticonCode) {
    if (emoticonData[emoticonCode.toLowerCase()]) {
        var ele = $("<img>");
        ele.prop("src", "\\Content\\emoticons\\" + emoticonData[emoticonCode.toLowerCase()] + ".png");
        ele.attr('code', emoticonCode);
        return ele[0].outerHTML;
    }
    else
        return emoticonCode;
}

//Ui Methods
function OnChatMembersSearch(filterEle) {
    debugger
    if (filterEle && filterEle.length) {
        var filterStr = filterEle.val().trim();
        if (filterStr || curSearchStr) {
            var colapDiv = $('#chatAccordion .panel>.panel-collapse.collapse.in');
            if (colapDiv.length) {
                GetChatFiteredMembers(filterStr, colapDiv.attr('rId'), colapDiv.find('.panel-body'));
            }
        }
        else {
            showAlertBox("Information Alert Message", "Please enter search string first.", gInfo.alertMsgType.information);
            return false;
        }
    }
}
function GetIndividualChatBody(fromId, messageBodyEle, callbackfunction) {
    if (!fromId)
        return false;
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/" + memberArea + "/Message/GetIndividualChatBody/",
        dataType: "html",
        data: JSON.stringify({ "from": fromId }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result && result.trim()) {
                    try {
                        messageBodyEle.mCustomScrollbar('destroy');
                    } catch (e) {

                    }
                    messageBodyEle.html(result.trim());
                    
                    messageBodyEle.mCustomScrollbar({
                        theme: "dark-3",
                        autoHideScrollbar: false
                    });

                    if (messageBodyEle.find('.time_bar.unread,.clearfix').length) {
                        setTimeout(function () { messageBodyEle.mCustomScrollbar('scrollTo', messageBodyEle.find('.time_bar.unread ~ div.bubble_chat:first')); }, 2000);
                    }
                    else {
                        setTimeout(function () { messageBodyEle.mCustomScrollbar('scrollTo', 'bottom'); }, 500);
                    }
                    //callback function for update read status for messages
                    if ($.isFunction(callbackfunction)) {
                        setTimeout(callbackfunction, 500);
                    }
                }
                else {
                    try {
                        messageBodyEle.mCustomScrollbar('destroy');
                    } catch (e) {

                    }
                    messageBodyEle.empty();
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred. Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });

}

function UpdateUserUnreadMessagesToRead(fromId, messageBodyEle) {
    if (!fromId)
        return false;
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/" + memberArea + "/Message/UpdateUserUnreadMessagesToRead/",
        dataType: "html",
        data: JSON.stringify({ "from": fromId }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                messageBodyEle.find('.time_bar.unread,.clearfix').fadeOut(3000, function () {
                    console.log(arguments);
                    messageBodyEle.find('.time_bar.unread,.clearfix').remove();
                });
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            showAlertBox("Failure Alert Message", "Some Problem Occurred. Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });

}

function UpdateIndividualMessageDeletedStatus(messageId, isSent, counterUserId, callbackFunction) {
    if (!messageId || !counterUserId)
        return false;
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/" + memberArea + "/Message/UpdateIndividualMessageDeletedStatus/",
        dataType: "json",
        data: JSON.stringify({ "messageId": messageId, "isSent": isSent, "messageTo": counterUserId }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if ($.isFunction(callbackFunction)) {
                    callbackFunction();
                    showAlertBox("Success Alert Message", "Message Deleted Successfully.", gInfo.alertMsgType.success);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            showAlertBox("Failure Alert Message", "Some Problem Occurred. Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function GetChatFiteredMembers(_filterString, curRole, curContainer) {
    $("#divLoader").show();
    var dataToSend = { "filterString": "", "role": "" };
    dataToSend.filterString = _filterString;
    dataToSend.role = curRole;
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/" + memberArea + "/Message/GetFilteredMembersList/",
        dataType: "html",
        data: JSON.stringify(dataToSend),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                curSearchStr = _filterString;
                if (result && result.trim()) {
                    try {
                        curContainer.mCustomScrollbar('destroy');
                    } catch (e) {

                    }
                    curContainer.html(result.trim());
                    curContainer.mCustomScrollbar({
                        theme: "dark-3",
                        autoHideScrollbar: false
                    });
                }
                else {
                    try {
                        curContainer.mCustomScrollbar('destroy');
                    } catch (e) {

                    }
                    curContainer.empty();
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred. Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });

}

function ExportChatCsv(userId,userName, callbackFunction) {
    if (!userId || !userName)
        return false;
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Chat/ExportCsvChat/",
        dataType: "json",
        data: JSON.stringify({ "userId": userId, "userName": userName }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if ($.isFunction(callbackFunction))
                    callbackFunction(result);
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred. Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

//not used yet
function UpdateIndividualMessageSentStatus(messageId, messageFromId, messageFromEmailId, callbackFunction) {
    if (!messageId || !messageFromId)
        return false;
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/" + memberArea + "/Message/UpdateIndividualMessageSentStatus/",
        dataType: "json",
        data: JSON.stringify({ "messageId": fromId, "messageFrom": messageFromId }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if ($.isFunction(callbackFunction))
                    callbackFunction(messageId, messageFromId, messageFromEmailId);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            showAlertBox("Failure Alert Message", "Some Problem Occurred. Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function UpdateIndividualMessageReadStatus(messageId, messageFromId, messageFromEmailId, callbackFunction) {
    if (!messageId || !messageFromId)
        return false;
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/" + memberArea + "/Message/UpdateIndividualMessageReadStatus/",
        dataType: "json",
        data: JSON.stringify({ "messageId": fromId, "messageFrom": messageFromId }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if ($.isFunction(callbackFunction))
                    callbackFunction(messageId, messageFromId, messageFromEmailId);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            showAlertBox("Failure Alert Message", "Some Problem Occurred. Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
////
function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], { type: "text/csv" });

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = $(html).find('tr');

    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++)
            row.push(cols[j].innerText);

        csv.push(row.join(","));
    }

    // Download CSV
    download_csv(csv.join("\n"), filename);
}