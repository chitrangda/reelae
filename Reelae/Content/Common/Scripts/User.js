﻿function addUserByInstitution(url) {
    var container = $('#addUserModal');
    var dataToSend = { "Email": $('#userEmail').val(), "Password": $('#userName').val(), "Name": $('#userPassword').val(), "Role": $("#userRole").val() };
    //validate
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: url,
        data: JSON.stringify(DataToSend),
        dataType: "json",
        async: false,
        success: function (result) {
            //OnSuccess
            container.find('button["data-dismiss"="modal"]').trigger('click');
            resetAddUserModal();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function ValidateUser(dataToSend) {
    if (!dataToSend.Email) {
        alert("Email Id is incorrect");
        return false;
    }
    if (!dataToSend.Name) {
        alert("Name Is Required");
        return false;
    }
    if (!dataToSend.Password) {
        alert("Password Is Required");
        return false;
    }
    if (!dataToSend.Role) {        
        return false;
    }
    return true;
}
function resetAddUserModal() {
    $('#userEmail').val('');
    $('#userName').val('');
    $('#userPassword').val('');
    $('#userConPassword').val('');
    $('#userRole').val('');
}
