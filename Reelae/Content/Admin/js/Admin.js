﻿
//open function of model pop-up onlcik of subject
function openSubjectModal() {
    if (!checkforCode())
        return false;
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Admin/GetSubjectView/",
        dataType: "html",
        data: JSON.stringify({ "id": $("#SubjectId").val().trim() }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#divAddSubject').html(result);
                    initDatePickers();
                    //set dates for edit mode
                    if ($('#hdn_SubjectStartDate').val())
                        $("#txt_SubjectStartDate").data('daterangepicker').setStartDate(new Date($('#hdn_SubjectStartDate').val()));
                    else
                        $("#txt_SubjectStartDate").data('daterangepicker').setStartDate(new Date());
                    if ($('#txt_SubjectEndDate').val())
                        $("#txt_SubjectEndDate").data('daterangepicker').setStartDate(new Date($('#hdn_SubjectEndDate').val()));
                    else
                        $("#txt_SubjectEndDate").data('daterangepicker').setStartDate(new Date());
                    $('#SubjectModelBox').modal('show');
                    $("#divLoader").hide();

                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
//Success Method for Adding the subject
function onSubjectSucess(response) {
    $("#divLoader").hide();
    if (!isSessionOutResponse(response)) {
        if (response.EnableError) {
            showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
        } else if (response.EnableSuccess) {
            var callbackFunc = function () {
                $("#divLoader").show();
                var _curSubjectId = $("#SubjectId").val();
                OnSubjectModalReset();
                $('#SubjectModelBox').modal('hide');
                window.location.href = "/Admin/Admin/dashboard/" + _curSubjectId;
            }
            showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
        }

    }
}

//Failure Method of Subjects When an error Occurred in adding the subjects
function onSubjectFailure(response) {
    $("#divLoader").hide();
    showAlertBox("Failure Alert Message", "Some Problem Occurred.", gInfo.alertMsgType.failure);
}
function onSubjectBegin() {
    //$('#SubjectModelBox').modal('show');
    
    //var substartdate = new Date($("#txt_SubjectStartDate").val());
    //var subenddate = new Date($("#txt_SubjectEndDate").val());

    var substartdate = moment($('#txt_SubjectStartDate').data('daterangepicker').startDate.format("MM/DD/YYYY"), "MM/DD/YYYY");
    var subenddate = moment($('#txt_SubjectEndDate').data('daterangepicker').startDate.format("MM/DD/YYYY"), "MM/DD/YYYY");

    if ($("#txt_SubjectName").val().trim() == "") {
        //alert("Please enter subject name");
        showAlertBox("Information Alert Message", "Please enter subject name.", gInfo.alertMsgType.information);
        $("#txt_SubjectName").focus();
        return false;
    } //Validation for startdate must be less than enddate
    else if (substartdate >= subenddate) {
        $("#txt_SubjectEndDate").data('daterangepicker').setStartDate(new Date());
        showAlertBox("Information Alert Message", "Start Date must be less than End Date.", gInfo.alertMsgType.information);
        return false;
    }
    else {
        $("#divLoader").show();
        return true;
    }
}
//before subject save click 
function OnSubjectSaveClick() {
    //set value to hdnfield
    $("#hdn_SubjectStartDate").val($("#txt_SubjectStartDate").data('daterangepicker').startDate.format("MM/DD/YYYY"));
    $("#hdn_SubjectEndDate").val($("#txt_SubjectEndDate").data('daterangepicker').endDate.format("MM/DD/YYYY"));
    //
    if (onSubjectBegin()) {
        $('#addSubjectModalSubmit').trigger('click');
    }
}
//setDefaultDateForAddSubject
function openModalForAddSubject() {
    if (!checkforCode())
        return false;
    $("#txt_SubjectStartDate").data('daterangepicker').setStartDate(new Date());
    $("#txt_SubjectEndDate").data('daterangepicker').setStartDate(new Date());
    $("#txt_SubjectStartDate").data('assignSD', $("#txt_SubjectStartDate").data('daterangepicker').startDate.format("MM/DD/YYYY"));
    $("#txt_SubjectEndDate").data('assignSD', $("#txt_SubjectEndDate").data('daterangepicker').startDate.format("MM/DD/YYYY"));
    $('#SubjectModelBox').modal('show');

}

//reset subject modal 
function OnSubjectModalReset() {
    try {
        $("#txt_SubjectName").val('');
        $("#txt_descr").val('');
        $("#hdn_subid").val('0');
        $("#txt_SubjectStartDate").data('daterangepicker').setStartDate(new Date());
        $("#txt_SubjectEndDate").data('daterangepicker').setStartDate(new Date());
        $("#hdn_SubjectStartDate").data('daterangepicker').setStartDate(new Date());
        $("#hdn_SubjectEndDate").data('daterangepicker').setStartDate(new Date());
    } catch (e) {

    }
}

function deleteSubjectConfirmation(_subjectId, _subjectName) {
    var message = "Are you sure, do you want to remove <strong>" + _subjectName + "</strong>.";
    $('#message').html(message);
    $('#removeConfirmationModalBox').modal('show');
    $('#submitAlert').attr("onclick", "deleteSubject('" + _subjectId + "')");
}

function deleteSubject(_subjectId) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Admin/deleteSubject/",
        dataType: "json",
        data: JSON.stringify({ "id": _subjectId }),
        async: true,
        success: function (result) {
            $("#divLoader").hide();
            $('#removeConfirmationModalBox').modal('hide');
            if (!isSessionOutResponse(result)) {
                if (result) {
                    if (result.EnableSuccess) {
                        var callbackFunc = function () {
                            $("#divLoader").show();
                            window.location.href = "/Admin/Admin/Dashboard";
                        }
                        showAlertBox("Success Alert Message", result.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
                    }
                    else {
                        showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);

                    }

                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });


}
//Method For clean the div
//function CleanDivHtml(_this) {
//    //$("#namespace Reelae.").empty();
//    $(_this).parent().parent().find('>li.active').removeClass('active');
//    $(_this).parent().parent().find('>li a span.bg_active').removeClass('bg_active').addClass('bg_light')

//    $(_this).parent().addClass('active');
//    $(_this).find('span.bg_light').removeClass('bg_light').addClass('bg_active');
//}
//function CleanDivHtml(_this) {
//    //$("#namespace Reelae.").empty();
//    $(_this).parent().parent().find('.active').removeClass('active');// $(_this).parent().parent().find('>li.active').removeClass('active');
//    $(_this).parent().parent().find('>li a span.bg_active').removeClass('bg_active').addClass('bg_light')

//    $(_this).addClass('active');//$(_this).parent().addClass('active');
//    $(_this).find('span.bg_light').removeClass('bg_light').addClass('bg_active');

//    console.log(_this);
//}
function CleanDivHtml(_this) {
    //$("#namespace Reelae.").empty();
    $(_this).parents('#mainHeaderSection').first().find('.active').removeClass('active');// $(_this).parent().parent().find('>li.active').removeClass('active');
    $(_this).parents('#mainHeaderSection').first().find('li a span.bg_active').removeClass('bg_active').addClass('bg_light')

    $(_this).addClass('active');//$(_this).parent().addClass('active');
    $(_this).parent().addClass('active');
    $(_this).find('span.bg_light').removeClass('bg_light').addClass('bg_active');

}

function OnSuccessSelectSubject(result) {
    if (!isSessionOutResponse(result)) {
        initMadSelect();
        setContainersHeight();
    }
}

function OnSuccessSelectArchiveSubject(result) {
    if (!isSessionOutResponse(result)) {
        initMadSelect();
        setContainersHeight();
        $('.group').find('*').unbind("click");
    }
}
// Method for record the attendance of Student.
/* Student Attendance Section */
var groupColours = ["bg-blue", "bg-green", " bg-red", "bg-orange", "bg-purple", "bg-blue-sky"];
function init_daterangepicker_rangeCustom(ContainerEle) {
    if ("undefined" != typeof $.fn.daterangepicker && ContainerEle.length > 0) {

        console.log("init_daterangepicker_range");
        var a = function (a, b, c) {
            console.log(a.toISOString(), b.toISOString(), c), ContainerEle.find('span').html(a.format("MMMM D, YYYY") + " - " + b.format("MMMM D, YYYY"))
        },
            b = {
                startDate: moment().subtract(29, "days"),
                endDate: moment(),
                minDate: "01/01/2012",
                maxDate: "12/31/2015",
                dateLimit: {
                    days: 60
                },
                showDropdowns: !0,
                showWeekNumbers: !0,
                timePicker: !1,
                timePickerIncrement: 1,
                timePicker12Hour: !0,
                ranges: {
                    Today: [moment(), moment()],
                    Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
                    "Last 7 Days": [moment().subtract(6, "days"), moment()],
                    "Last 30 Days": [moment().subtract(29, "days"), moment()],
                    "This Month": [moment().startOf("month"), moment().endOf("month")],
                    "Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
                },
                opens: "left",
                buttonClasses: ["btn btn-default"],
                applyClass: "btn-small btn-primary",
                cancelClass: "btn-small",
                format: "MM/DD/YYYY",
                separator: " to ",
                locale: {
                    applyLabel: "Submit",
                    cancelLabel: "Clear",
                    fromLabel: "From",
                    toLabel: "To",
                    customRangeLabel: "Custom",
                    daysOfWeek: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
                    monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                    firstDay: 1
                }
            };
        ContainerEle.find('span').html(moment().subtract(29, "days").format("MMMM D, YYYY") + " - " + moment().format("MMMM D, YYYY")), ContainerEle.daterangepicker(b, a), ContainerEle.on("show.daterangepicker", function () {
            console.log("show event fired")
        }), ContainerEle.on("hide.daterangepicker", function () {
            console.log("hide event fired")
        }), ContainerEle.on("apply.daterangepicker", function (a, b) {
            console.log("apply event fired, start/end dates are " + b.startDate.format("MMMM D, YYYY") + " to " + b.endDate.format("MMMM D, YYYY"))
            //customize
            ContainerEle.data('assignSD', b.startDate.format("MM/DD/YY"));
            ContainerEle.data('assignED', b.endDate.format("MM/DD/YY"));

        }), ContainerEle.on("cancel.daterangepicker", function (a, b) {
            console.log("cancel event fired")
        }), $("#options1").click(function () {
            ContainerEle.data("daterangepicker").setOptions(b, a)
        }), $("#options2").click(function () {
            ContainerEle.data("daterangepicker").setOptions(optionSet2, a)
        }), $("#destroy").click(function () {
            ContainerEle.data("daterangepicker").remove()
        })
    }
}

function initSignalDatePicker(containerEle) {
    containerEle.daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        drops: "up",
        locale: {
            format: 'MM/DD/YYYY'
        }
    },
    function (start, end, label) {
        //var curDate = new Date(start);
        containerEle.data('assignSD', start.format('MM/DD/YYYY'));
    });

    containerEle.on("apply.daterangepicker", function (a, b) {
        console.log("apply event fired, start/end dates are " + b.startDate.format("MMMM D, YYYY") + " to " + b.endDate.format("MMMM D, YYYY"))
        //customize
        containerEle.data('assignSD', b.startDate.format('MM/DD/YYYY'));
        containerEle.data('assignED', b.endDate.format('MM/DD/YYYY'));
    });
}
//Success Method for Student Attendance view
function OnSuccessGetStudentAttandanceView(result) {
    if (!isSessionOutResponse(result) && result) {
        if ($('#StudentAttandanceModel').length > 0) {
            try {
                $('#ddlSAGroups').selectpicker();
                initSignalDatePicker($('#stuAttandanceDate'));
            } catch (e) {

            }
            $('#StudentAttandanceModel').modal('show');
        }
    }
}

// Failure method if there is any error to record the Student attendance.
function OnFailureGetStudentAttandanceView(error) {

}

//Method to Record the Subject of Student as per Subject.
function OnSubjectChangeByStudentAttandance() {
    var subjectID = "0";
    var containerEle = $('#ddlSAGroups');
    GetGroupsOfSubject(subjectID, containerEle);
}

//Method for Get the Group By Subjects
function GetGroupsOfSubject(subjectID, containerEle) {
    if (!keywordString || containerEle.length) {
        return false;
    }
    $("#divLoader").show();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Assignments/GetGroupsBySubject/" + subjectID,
        dataType: "json",
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result.status.toLowerCase() == "success") {
                    var data = JSON.parse(result.Data);
                    BindDropDownData(containerEle, data);
                    containerEle.selectpicker('refresh')
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

//Method to bind the Dropdown Groupwise.
function BindDropDownData(containerEle, Data) {
    if (!containerEle || containerEle.length == 0)
        return false;
    containerEle.html('');
    if (!Data || !Data instanceof Array)
        return false;
    var _index = 0;
    for (var i = 0; i < Data.length; i++) {
        var option = $('<option>');
        if (containerEle.attr('id') == "ddlSAGroups") {
            if (i == groupColours.length - 1)
                _index = 0;
            else
                _index++;
            var dataContentData = "<option value='" + Data[i].Id + "' data-content='<span class='group_icn " + groupColours[_index] + "></span> " + Data[i].Value + "'></option>";
            option.attr('data-content', '')

        }
        else {
            option.attr('value', Data[i].Id);
            option.html(Data[i].Value);
        }
        containerEle.append(option);
    }
}


// Success Method Of Attendance for adding the Student.
function OnSuccessAddStudentAttandance(result) {
    if (!isSessionOutResponse(result)) {
        if (result && result.hasOwnProperty("status") && result.status.toLowerCase() == "success") {
            //alert(result.message);
            showAlertBox("Success Alert Message", result.message, gInfo.alertMsgType.success);
            $('#StudentAttandanceModel').modal("hide");
            $('#modalFinishStudentAttandance').modal("show");
        }
        else {
            //alert(result);
        }
    }
}

// Failure Method Of Attendance for adding the Student.
function OnFailureAddStudentAttandance() {
    //alert("Some Problem Occurred");
}
function OnBeginAddStudentAttandance() {
    //alert("Some Problem Occurred");
}

function putValueforModel() {
    $('#hdnSASubID').val();
    $('#hdnSADate').val();
    $('#hdnSAGrpID').val();
    return true;
}

//Method to Validate the inputs for Adding the Student Attendance.
function validateAddStudentAttandance() {
    putValueforModel();
    var subject = $('#hdnSASubID').val();
    var date = $('#hdnSADate').val();
    var group = $('#hdnSAGrpID').val();

    if (!subject) {
        //alert("Subject is not selected");
        showAlertBox("Information Alert Message", "Subject is not selected", gInfo.alertMsgType.information);
        return false;
    }
    if (!group) {
        //alert("Group is not selected");
        showAlertBox("Information Alert Message", "Group is not selected", gInfo.alertMsgType.information);
        return false;
    }
    if (!date) {
        //alert("Attandance Date is not selected");
        showAlertBox("Information Alert Message", "Attandance Date is not selected", gInfo.alertMsgType.information);
        return false;
    }
    $('#submitAddStudentAttandance').trigger('click');
    return true;
}

/* End Student Attandance Section */


/* Student Assignment View */
//Success Method for Student Assignment View
function OnSuccessGetStudentAssignmentView(result) {
    if (result) {
        if ($('#StudentAssignmentsModel').length > 0) {
            try {
                $('#ddlSAssignGroups').selectpicker();
            } catch (e) {

            }
            $('#StudentAssignmentsModel').modal('show');
        }
    }
}
//Failure Method for Student Assignment View
function OnFailureGetStudentAssignmentView(error) {

}

// Success Method Of Student Assignment
function OnStudentAssignmentNext() {
    //check selected subject id or group id
    var subjectID = "";
    var groupID = "";
    if (!subjectID || groupID) {
        return false;
    }
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Assignments/GetStudentAssignmentList",
        dataType: "json",
        data: JSON.stringify({ "SubjectID": subjectID, "GroupID": groupID }),
        async: true,
        success: function (result) {
            if (!result.hasOwnProperty("status")) {
                $('#assignlstContainer').html(result);
                $('#StudentAssignmentsModel').modal('hide');
                $('#StudentAssignmentsModelStep2').modal('show');
            }
            else {
                //alert(result.message);
                showAlertBox("Failure Alert Message", result.message, gInfo.alertMsgType.failure);
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
//Method of Subject according to SubjectId
function OnSubjectChangeByStudentAssignments() {
    var subjectID = "0";
    var containerEle = $('#ddlSAssignGroups');
    GetGroupsOfSubject(subjectID, containerEle);
}
/* End Student Assignment View */

/* Taecher Info View */
function OpenTeacherInfoView(tid) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Admin/TeacherInfoView/",
        dataType: "html",
        data: JSON.stringify({ "teacherID": tid }),
        async: false,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#teacherInfoContainer').html(result);
                    $('#teacherInfoModal').modal('show');
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
/* End Taecher Info View */

/* Member Info View */
function OpenMemberInfoView(tid, type) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Admin/MemberInfoView",
        dataType: "html",
        data: JSON.stringify({ "_id": tid,"role":type }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#teacherInfoContainer').html(result);
                    $("#exampleAccordion").mCustomScrollbar({
                        theme: "dark-3",
                        autoHideScrollbar: true
                    });
                    $('#teacherInfoModal').modal('show');
                }
                if (type.toLowerCase() == "t") {
                    $('#memberInfoModalLabel').html("Teacher Information");
                    $("#MemberPic").attr("src", "../../../Content/images/teacher.jpg");

                }
                else {
                    $('#memberInfoModalLabel').html("Student Information");
                    $("#MemberPic").attr("src", "../../../Content/images/student.jpg");

                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
/* Student Attandance Info View */
function OpenStudentAttandanceView(sid) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Admin/StudentAttandanceAdminView/",
        dataType: "html",
        data: JSON.stringify({ "studentID": sid }),
        async: false,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#studentAttandanceContainer').html(result);
                    $('#stuAttandanceViewModal').modal('show');
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").show();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
/* End Student Attandance Info View */

//reset subject popup
function resetSubjectModal() {
    $('#txt_SubjectName').val('');
    $('#txt_SubjectName').focus();
    $('#txt_descr').val('');
}

var _isArchiveSelect = "no";
function latestAndArchiveBtnClick() {
    $('#archive').click(function () {
        _isArchiveSelect = "yes";
    });

    $('#latest').click(function () {
        _isArchiveSelect = "no";
    });
}

function removeButnHide() {
    if (_isArchiveSelect == "yes") {
        $('.text-center').find('.t_group').remove();
    }
}