﻿function getMembersGridView(id, _this) {
    CleanDivHtml(_this);
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Member/getMembersGridView/",
        dataType: "html",
        data: JSON.stringify({ "id": id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#pan_hide').addClass("col-xs-12").empty().html(result);
                    var dyH=(returnPageHeight() - $('#divMembersGrid').prev().height() - 30);
                    $('#divMembersGrid').css('height', dyH);
                    try {
                        $('#tblMemberGrid').DataTable({
                            "searching": true,
                            "paging": false,
                            "info": false,
                            "order": [[1, "asc"]],
                            "scrollY":dyH-135, //'61vh',
                            "scrollX": false,
                            language: {
                                search: "<i class='material-icons'>search</i> _INPUT_",
                                searchPlaceholder: "Search members..."
                            }
                        });
                    } catch (e) {

                    }
                    $("#divLoader").hide();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });

}
function getEditMembersView(id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Member/getEditMembersView/",
        dataType: "html",
        data: JSON.stringify({ "id": id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#divEditMember').html(result);
                    $('#editMemberModalBox').modal('show');
                    $("#divLoader").hide();

                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });

}
function onEditSucess(response) {
    if (!isSessionOutResponse(response)) {
        if (response.EnableError) {
            if (response.ErrorMsg.indexOf('This user is mapped with a subject') > -1) {
                var res = response.ErrorMsg.split("#");
                //var message = "This " + res[3] + " is already mapped with a subject. Still, do you want to inactivate.";
                var message = "This " + res[3] + " is already mapped with the subject, still do you want to inactivate";
                $('#message').text(message);
                $('#submitAlert').attr("onclick", "yesClick('" + res[1] + "', '" + res[2] + "', '" + res[3] + "')");

                $('#removeConfirmationModalBox').modal('show');
            }
            else {
                showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
            }
        } else if (response.EnableSuccess) {
            $('#editMemberModalBox').modal('hide');
            var message = response.SuccessMsg.split("Ω");
            var rolePara = (message[1].toLowerCase() == "teacher") ? 1 : 2;
            var callbackFunction = function () { getMembersGridView(rolePara) };
            showAlertBox("Success Alert Message", message[0], gInfo.alertMsgType.success, callbackFunction);
        }
        $("#divLoader").hide();
    }
}
function yesClick(userId, uId, type) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Member/SaveEditMemberActiveOrInactive/",
        dataType: "html",
        data: JSON.stringify({ "id": userId, "uId": uId, "type": type }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#editMemberModalBox').modal('hide');
                    $('#removeConfirmationModalBox').modal('hide');
                    window.location.reload();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

//Failure Method of Subjects When an error Occurred in adding the subjects
function onEditFailure(response) {
    showAlertBox("Failure Alert Message", "Some Problem Occurred.", gInfo.alertMsgType.failure);
}
function onEditBegin() {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!$('#editmemberName').val().trim()) {
        showAlertBox("Information Alert Message", "Please Enter Name.", gInfo.alertMsgType.information);
        return false;
    }
    else if (!$('#editmemberEmail').val().trim()) {
        showAlertBox("Information Alert Message", "Please Enter Email.", gInfo.alertMsgType.information);
        return false;
    }
    else if (!regex.test($('#editmemberEmail').val())) {
        showAlertBox("Information Alert Message", "Invalid email address.", gInfo.alertMsgType.information);
        return false;
    }
    $('#divLoader').show();
    return true;

}
function returnPageHeight() {
    var totalPageHeight = $('.pageContainer').height();
    var navigationMenuHeight = $('.top_nav .nav_menu').height();
    var remainingHeight = totalPageHeight - navigationMenuHeight;
    return remainingHeight;
}