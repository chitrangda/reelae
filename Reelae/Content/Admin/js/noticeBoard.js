﻿$(document).ready(function () {
    $('#tblForum').DataTable({
        "searching": true,
        "paging": false,
        "info": false,
        "order": [[1, "asc"]],
        "scrollY": '62vh',
        "scrollX": false,
        language: {
            search: "<i class='material-icons'>search</i> _INPUT_",
            searchPlaceholder: "Search topic..."
        }

    });
})

function getComments(_noticeBoardId) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/NoticeBoard/getComments/",
        dataType: "html",
        data: JSON.stringify({ "id": _noticeBoardId }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#noticeCommentdiv').html(result);
                    $('#comment').modal('show');
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function RemoveNoticeConfirmation(_noticeBoardId, _noticeTitle) {
    var message = "Are you sure, do you want to remove the notice '<strong>" + _noticeTitle + "'</strong>.";
    $('#message').html(message);
    $('#removeConfirmationModalBox').modal('show');
    $('#submitAlert').attr("onclick", "RemoveNotice('" + _noticeBoardId + "')");
}

function RemoveNotice(_noticeBoardId) {
    $('#removeConfirmationModalBox').modal('hide');
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/NoticeBoard/RemoveNotice/",
        dataType: "json",
        data: JSON.stringify({ "id": _noticeBoardId }),
        async: true,
        success: function (response) {
            $("#divLoader").hide();
            if (!isSessionOutResponse(response)) {
                if (response.EnableError) {
                    showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
                } else if (response.EnableSuccess) {
                    var callbackFunc = function () {
                        $("#divLoader").show();
                        window.location.reload();
                    }
                    showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function RemoveCommentConfirmation(_id) {
    var message = "Are you sure, do you want to remove the comment?";
    $('#message').html(message);
    $('#removeConfirmationModalBox').modal('show');
    $('#submitAlert').attr("onclick", "RemoveComment('" + _id + "')");
}

function RemoveComment(_id) {
    $('#removeConfirmationModalBox').modal('hide');
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/NoticeBoard/RemoveComment/",
        dataType: "json",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (response) {
            $("#divLoader").hide();
            if (!isSessionOutResponse(response)) {
                if (response.EnableError) {
                    showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
                } else if (response.EnableSuccess) {
                    var callbackFunc = function () {
                        $("#divLoader").show();
                        window.location.reload();
                    }
                    showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function getFiles(_noticeBoardId) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/NoticeBoard/getFiles/",
        dataType: "html",
        data: JSON.stringify({ "id": _noticeBoardId }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#notieFilesDiv').html(result);
                    $("#btnfile_2010").html("" + $("#hdn_totalFiles").val() + " Files <i class=\"fa fa-paperclip\" aria-hidden=\"true\"></i>")
                    $('#noticeFilesModal').modal('show');
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function deleteNoticeFileConfirmation(_id, _name, _noticeBoardId,_url) {
    var message = "Are you sure, do you want to remove the file <strong>" + _name + " ?</strong>";
    $('#message').html(message);
    $('#removeConfirmationModalBox').modal('show');
    $('#submitAlert').attr("onclick", "DeleteNoticeFile('" + _id + "','" + _noticeBoardId + "','"+_url+"')");
}

function DeleteNoticeFile(_id, _noticeBoardId,_url) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/NoticeBoard/DeleteNoticeFile/",
        dataType: "json",
        data: JSON.stringify({ "id": _id, "fileUrl": _url }),
        async: true,
        success: function (result) {
            $('#removeConfirmationModalBox').modal('hide');
            $('#noticeFilesModal').modal('hide');
            $("#divLoader").hide();
            if (!isSessionOutResponse(result)) {
                if (result) {
                    var callbackFunc = function () {
                        $("#divLoader").show();
                        getFiles(_noticeBoardId)

                    }
                    showAlertBox("Success Alert Message", result.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

