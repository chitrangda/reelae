﻿function OpenaddUserModal(userRole) {
    if (!checkforCode())
        return false;
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Admin/GetAddUserView/",
        dataType: "html",
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#addUserContainer').html(result);
                    var heading = "Add " + userRole;
                    $("#addUserModalLabel").html(heading);
                    $('#addUserModal').modal('show');
                    $("#Role").val(userRole);
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

//Success Method For Add user
function OnSuccessAddUser(response) {
    if (!isSessionOutResponse(response)) {
        if (response.EnableError) {
            showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
        } else if (response.EnableSuccess) {
            //$("#btnCloseModal").click(OnCloseBtnClick);
            var callbackFunc = function () {
                $('#addUserModal').modal("hide");
                if (response.Role == "Teacher") {
                    window.location.href = "/Admin/Admin/Dashboard?id=t"
                } else
                {
                    window.location.href = "/Admin/Admin/Dashboard?id=s"

                }
                //window.location.reload();
            };
            showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
        }
        $("#divLoader").hide();
    }
}

// Failure method for Add user
function OnFailureAddUser(result) {
    //alert('Oops!Some Problem Occurred.Please Try Again.');
    $("#divLoader").hide();
    showAlertBox("Failure Alert Message", "Oops!Some Problem Occurred.Please Try Again.", gInfo.alertMsgType.failure);
}

function resetAddUserModal() {
    $('#userEmail').val('');
    $('#userPassword').val('');
    $('#userConPassword').val('');
    $('#userName').val('');
    $('#userRole').val('');
    $('#subjectId').val('');
    $('#addUserModalLabel').html('');
}
function validateAddUser() {
    var dataToSend = { "Email": $('#userEmail').val(), "Name": $('#userName').val(), "Password": $('#userPassword').val(), "Role": $('#userRole').val(), "ConPassword": $('#userConPassword').val(), "subjectID": $('#subjectId').val() }
    if (!dataToSend.Name) {
        showAlertBox("Information Alert Message", "Name is required.", gInfo.alertMsgType.information);
        //alert("Name is required.");
        return false;
    }
    else if (!dataToSend.Email) {
        //alert("Email Id is required.");
        showAlertBox("Information Alert Message", "Email Id is required.", gInfo.alertMsgType.information);
        return false;
    }
    else if (!dataToSend.Password) {
        //alert("Password is required.");
        showAlertBox("Information Alert Message", "Password is required.", gInfo.alertMsgType.information);
        return false;
    }
    else if (dataToSend.Password != dataToSend.ConPassword) {
        //alert("Password and confirm password does not match.");
        showAlertBox("Information Alert Message", "Password and confirm password does not match.", gInfo.alertMsgType.information);
        return false;
    }
    else {
        $("#divLoader").show();
        return true;
    }
}

//register click on ddlAddUser


