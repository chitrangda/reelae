﻿$(document).ready(function () {
    $('#tblForum').DataTable({
        "searching": true,
        "paging": false,
        "info": false,
        "order": [[1, "asc"]],
        "scrollY": '62vh',
        "scrollX": false,
        language: {
            search: "<i class='material-icons'>search</i> _INPUT_",
            searchPlaceholder: "Search topic..."
        }

    });
  
})

function RemoveForumTopicConfirmation(_id, _title) {
    var message = "Are you sure, do you want to remove the forum topic '<strong>" + _title + "'</strong>.";
    $('#message').html(message);
    $('#removeConfirmationModalBox').modal('show');
    $('#submitAlert').attr("onclick", "RemoveForumTopic('" + _id + "')");
}

function RemoveForumTopic(_id) {
    $('#removeConfirmationModalBox').modal('hide');
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Forum/RemoveForumTopic/",
        dataType: "json",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (response) {
            $("#divLoader").hide();
            if (!isSessionOutResponse(response)) {
                if (response.EnableError) {
                    showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
                } else if (response.EnableSuccess) {
                    var callbackFunc = function () {
                        $("#divLoader").show();
                        window.location.reload();
                    }
                    showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function getAllReply(_id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Forum/getAllReply/",
        dataType: "html",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#newtopicDiv').html(result);
                    $(".modal-body").mCustomScrollbar({
                        theme: "dark-3",
                        autoHideScrollbar: true
                    });
                    $('#allReplyModelBox').modal('show');
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function removeReplyConfirmation(_id, _forumId) {
    var message = "Are you sure, do you want to remove ?";
    $('#message').html(message);
    $('#removeConfirmationModalBox').modal('show');
    $('#submitAlert').attr("onclick", "removeReply(" + _id + "," + _forumId + ")");
}

function removeReply(_id, _forumId) {
    $('#removeConfirmationModalBox').modal('hide');
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Forum/removeReply/",
        dataType: "json",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (response) {
            $("#divLoader").hide();
            if (!isSessionOutResponse(response)) {
                if (response.EnableError) {
                    showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
                } else if (response.EnableSuccess) {
                    var callbackFunc = function () {
                        $('#allReplyModelBox').modal('hide');
                        getAllReply(_forumId);
                    }
                    showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}