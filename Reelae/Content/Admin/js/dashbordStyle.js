﻿var _curLogo = '';
var _curInstituteName = ''
//Method for get the Dashboard style view
function OpenDashboardStyleModal() {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Admin/GetDashboardStyleView/",
        dataType: "html",
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#divDashboardStyle').html(result);
                    _curLogo = $("#hdnLogoPath").val();
                    _curInstituteName = $("#InstitutionName").val().trim();
                    $('#dashboardSetupModal').modal('show');
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

//Method for begin the setup
function onSetupBegin() {
    var scriptPat = /[<,>]/g
    if ($("#InitialName").val().trim() == "") {
        showAlertBox("Information Alert Message", "Please enter institution's initial name.", gInfo.alertMsgType.infomation);
        $("#InitialName").focus();
        return false;
    }
    if ($("#InstitutionName").val().trim() == "") {
        //alert("Please enter institution name");
        showAlertBox("Information Alert Message", "Please enter institution name.", gInfo.alertMsgType.infomation);
        $("#InstitutionName").focus();
        return false;
    }
    else if (scriptPat.test($("#InstitutionName").val().trim())) {
        //alert('<,> are not allowed .It can be harmless.');
        showAlertBox("Information Alert Message", "<,> are characters not allowed.", gInfo.alertMsgType.information);
        $("#InstitutionName").val(_curInstituteName);
        return false;
    }
    else {
        $("#divLoader").show();
        return true;
    }
}

//method for event of setup complete
function onSetupComplete(result) {
    if (!isSessionOutResponse(result.responseJSON)) {
        $('#divLoader').hide();
        if (result.responseJSON.hasOwnProperty("EnableError")) {
            showAlertBox("Failure Alert Message", result.responseJSON.ErrorMsg, gInfo.alertMsgType.failure);
        }
        else {
            var cssClass = "nav-md admin-page theme-sel-" + $("#bodythemeClass").val();
            $("body").addClass(cssClass);
            $('#dashboardSetupModal').modal('hide');
            //$("#divLoader").hide();
            location.reload();
        }
    }
}

// Method to set the Theme colors
function setThemeColor(ColorCode, ThemeName, themeClass, _this) {
    $("#divColors").find("a").css("border", "1px solid #e6e9ed");
    $(_this).css("border", "1px solid blue");
    $("#Theme_ColorCode").val(ColorCode);
    $("#Theme_ThemeName").val(ThemeName);
    $("#bodythemeClass").val(themeClass);
}

// Method for File Upload trigger
function triggerFileUpload() {
    $('#FileUpload1').trigger('click');
}

// Method for Upload the Logo
function UploadLogo() {
    if (window.FormData !== undefined) {

        var fileUpload = $("#FileUpload1").get(0);
        var files = fileUpload.files;

        // Create FormData object  
        var fileData = new FormData();
        var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];

        // Looping over all files and add it to FormData object  
        for (var i = 0; i < files.length; i++) {
            var fileType = files[i].type;

            if ($.inArray(fileType, ValidImageTypes) < 0) {
                //alert('Invalid file type, please upload file type of image.')
                showAlertBox("Information Alert Message", "Invalid file type, please upload file type of image.", gInfo.alertMsgType.information);
                return false;
            }
            //check file size in MB
            var size = (files[i].size / (1024 * 1024)).toFixed(2);
            if (size > 1) {
                //alert("Image is too big, please upload small image.");
                showAlertBox("Information Alert Message", "Image is too big, please upload small image.", gInfo.alertMsgType.information);
                return false;
            }
            fileData.append(files[i].name, files[i]);
        }

        // Adding one more key to FormData object  
        $("#divLoader").show();
        $.ajax({
            url: '/Admin/Admin/UploadLogo',
            type: "POST",
            contentType: false, // Not to set any content header  
            processData: false, // Not to process data  
            data: fileData,
            success: function (result) {
                if (!isSessionOutResponse(result)) {
                    if (result != "") {
                        $(".upload_logo").find("span").remove();
                        var logopath = "../../../InstituteLogo/" + result;
                        $("#hdnLogoPath").val(logopath);
                        $("#imgLogo").attr("src", logopath);
                        $("#btnDelete").show();
                        $("#btnLogoupload").hide();
                    }
                    $("#divLoader").hide();
                }
            },
            error: function (err) {
                //alert(err.statusText);
                $("#divLoader").hide();
                showAlertBox("Failure Alert Message", err.statusText, gInfo.alertMsgType.failure);
                
            }
        });
    }
    else {
        alert("FormData is not supported.");
    }
}

// method for deleting the logo
function DeleteLogo() {
    //if (_curLogo == '') {
    //    $(".upload_logo").append("<span>L</span>");
    //    $("#imgLogo").attr("src", "").attr("alt", "");

    //    $("#hdnLogoPath").val("");
    //} else {
    //    $("#imgLogo").attr("src", _curLogo).attr("alt", "");
    //    $("#hdnLogoPath").val(_curLogo);
    //}
    $("#hdnLogoPath").val("");
    $(".upload_logo").append("<span>L</span>");
    $("#imgLogo").attr("src", "").attr("alt", "");
    $("#btnDelete").hide();
    $("#btnLogoupload").show();
}


