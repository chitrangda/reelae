﻿/*global Variable*/
var gInfo = {};
gInfo.alertMsgType = { success: "success", failure: "failure", information: "info" };

/*these function for setting scroll dynamically and set height dynamically*/
function returnPageHeight() {
    var totalPageHeight = $('.pageContainer').height();
    var navigationMenuHeight = $('.top_nav .nav_menu').height();
    var remainingHeight = totalPageHeight - navigationMenuHeight;
    return remainingHeight;
}
function returnSubjectContainerWidth() {
    var totalPageHeight = $('.pageContainer').height();
    var navigationMenuHeight = $('.top_nav .nav_menu').height();
    var remainingHeight = totalPageHeight - navigationMenuHeight;
    var teacherStudentMenu = $('.main_menu2 .menu_section').first().height();
    return remainingHeight - teacherStudentMenu;
}
//call on load or any action
function setContainersHeight() {
    $('#divSubjectMembers').css('height', (returnPageHeight() - $('#divSubjectMembers').prev().height() - 30));
    var subContentHeight = returnSubjectContainerWidth() - 130;
    $('.main_menu2 .menu_section:eq(1) div[data-simplebar="init"]').css('height', subContentHeight);
    if (subContentHeight < $('#allSublst').height()) {
        $('#allSublst span.badge').addClass('right_0');
    }
    else {
        $('#allSublst span.badge.right_0').removeClass('right_0');
    }
}
/*End */
/*Generic Popup Alert Functionality*/
function showAlertBox(HeaderMsg, BodyMsg, alertType, callback) {
    var alertEle = $('#genericAlertBox');
    if (!alertEle.length) {
        alert("Sorry! Generic Alert Box is not available");
        return false;
    }
    switch (alertType) {
        case "success":
            alertType = "alert-success";
            break;
        case "failure":
            alertType = "alert-danger";
            break;
        case "info":
            alertType = "alert-info"
            break;
        default:
            alertType = "alert-info";
            break;
    }
    $('.modal-header', alertEle).removeClass("alert-success alert-danger alert-info");
    $('.modal-header', alertEle).addClass(alertType);
    HeaderMsg = HeaderMsg || "Alert Message";
    BodyMsg = BodyMsg || "No messages to show";
    $('#genericAlertBoxModalLabel', alertEle).html(HeaderMsg);
    $('.popup_txt', alertEle).html(BodyMsg);
    if (callback && $.isFunction(callback))
        $("#btnCloseModal").one('click', callback);
    alertEle.modal('show');
}
/*End Generic Popup Alert Functionality*/
/*Check sessionOut Respose*/
function isSessionOutResponse(result) {
    var typeOf = typeof result;
    if (result) {
        if (!$.isPlainObject(result)) {
            try {
                result = JSON.parse(result);
            } catch (e) {
                result = {};
            }
        }
    }
    else
        result = {};
    if (result.hasOwnProperty("LogOutKey") && result.LogOutKey.toLowerCase() == "sessionout") {
        $('#divLoader').hide();
        var callbackFunction = function () { location.href = gInfo.LogoutUrl; }
        showAlertBox("Information Alert Message", "Oops!! Your session has been expired.You are going to redirect to the login page.", gInfo.alertMsgType.information, callbackFunction);
        //location.href = gInfo.LogoutUrl;
        return true;
    }
    else
        return false;
}
/*End Check sessionOut Respose*/
//Alert in case of harmfull input
function onChangeValidate(_this) {
    var currentVal = $(_this).val();
    var scriptPat = /[<>]/g
    if (scriptPat.test(currentVal)) {
        //alert('<,> are not allowed .It can be harmfull.');
        showAlertBox("Information Alert Message", "<,> characters are not allowed.", gInfo.alertMsgType.information)
        $(_this).val('');
    }
    if ($(_this).attr('id') == "InstitutionName") {
        if (!$('#InitialName').prop('readonly') && $('#InitialName').val().trim() == "" && $(_this).val().trim())
            setInstitutionInitialName($(_this).val().trim())
    }
}
//et initial date pickers
function initDatePickers() {
    initSignalDatePicker($("#txt_SubjectStartDate"));
    initSignalDatePicker($("#txt_SubjectEndDate"));
}

function setInstitutionInitialName(institutionName) {
    var dataToSave = [];
    var words = institutionName.split(' ');
    for (var i = 0; i < words.length && i < 6; i++) {
        if (words[i].trim())
            dataToSave.push(words[i][0].toUpperCase());
    }
    $('#InitialName').val(dataToSave.join(''));
}

