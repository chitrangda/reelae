﻿$(document).ready(function () {
    $('#tblPinupBoard').DataTable({
        "searching": true,
        "paging": false,
        "info": false,
        "order": [[1, "asc"]],
        "scrollY": '62vh',
        "scrollX": false,
        language: {
            search: "<i class='material-icons'>search</i> _INPUT_",
            searchPlaceholder: "Search ..."
        }

    });

})

function deletePinupConfirmation(_id) {
    var message = "Are you sure, do you want to remove?";
    $('#message').html(message);
    $('#removeConfirmationModalBox').modal('show');
    $('#submitAlert').attr("onclick", "deletePinup('" + _id + "')");
}

function deletePinup(_id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/PinUpBoard/deletePinup/",
        dataType: "json",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (result) {
            $("#divLoader").hide();
            $('#removeConfirmationModalBox').modal('hide');
            if (!isSessionOutResponse(result)) {
                if (result) {
                    var callbackFunc = function () {
                        $("#divLoader").show();
                        window.location.reload();
                    }
                    showAlertBox("Success Alert Message", result.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function getAttachment(_id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/PinUpBoard/getAttachment/",
        dataType: "html",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#newtopicDiv').html(result);
                    $("#filesModelBox").modal("show");
                    $('#tblFile').DataTable({
                        "searching": true,
                        "paging": false,
                        "info": false,
                        "order": [[1, "asc"]],
                        "scrollY": '30vh',
                        "scrollX": false,
                        language: {
                            search: "<i class='material-icons'>search</i> _INPUT_",
                            searchPlaceholder: "Search ..."
                        }

                    });
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function deletePinupFileConfirmation(_id, _name) {
    var message = "Are you sure, do you want to remove the file <strong>" + _name + " ?</strong>";
    $('#message').html(message);
    $('#removeConfirmationModalBox').modal('show');
    $('#submitAlert').attr("onclick", "deleteFile('" + _id + "')");
}

function deleteFile(_id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/PinUpBoard/deleteFile/",
        dataType: "json",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (result) {
            $("#divLoader").hide();
            if (!isSessionOutResponse(result)) {
                if (result) {
                    var callbackFunc = function () {
                        $("#divLoader").show();
                        window.location.reload();
                    }
                    showAlertBox("Success Alert Message", result.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}