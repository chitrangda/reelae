﻿/* Subject Member View */
var _serachedMember = '';
var _searchedMemberName = '';
var _sortOrder = "asc";
function OpenAssignMemberModal(_userRole) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Admin/getAssignMemberView/",
        dataType: "html",
        data: JSON.stringify({ "userRole": _userRole, "subjectId": $("#SubjectId").val() }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#divAsignMember').html(result);
                    $("#hdn_userRole").val(_userRole);
                    $("#hdn_subjectId").val($("#SubjectId").val());
                    if (_userRole.toLowerCase() == "teacher") {
                        $('#assignMemberModal #addUserModalLabel').html("Teacher Assignment");
                        $("#lblMsz").html("No more teachers are found.");
                    }
                    else {
                        $('#assignMemberModal #addUserModalLabel').html("Student Assignment");
                        $("#lblMsz").html("No more students are found.");
                    }
                    $('#tblmembers').DataTable({
                        "searching": true,
                        "paging": false,
                        "info": false,
                        "order": [[1, "asc"]],
                        "scrollY": '50vh',
                        "scrollX": false,
                        language: {
                            search: "<i class='material-icons'>search</i> _INPUT_",
                            searchPlaceholder: "Search members..."
                        }

                    });

                    $('#assignMemberModal').modal('show');

                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
function initAutoComplete(_lookupArray) {
    _serachedMember = '';
    _searchedMemberName = '';
    $('#autocomplete').autocomplete({
        lookup: _lookupArray,
        onSelect: function (suggestion) {
            _serachedMember = suggestion.data;
            _searchedMemberName = suggestion.value;
        }
    });
}
function searchMember(e) {
    if (e.keyCode == 13) {
        e.preventDefault();
        getsearchMember()
        return false;
    }
}
function getsearchMember() {
    if ($("#autocomplete").val().trim() != _searchedMemberName.trim()) {
        _serachedMember = "-1";
    }
    if ($("#autocomplete").val().trim() == "") {
        _serachedMember = "";
    }
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Admin/getsearchMember/",
        dataType: "html",
        data: JSON.stringify({ "id": $("#hdn_subjectId").val(), "userid": _serachedMember, "role": $("#hdn_userRole").val() }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                $("#divMembers").empty().html(result);
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
function onAssignsuccess(response) {
    if (!isSessionOutResponse(response)) {
        if (response.EnableError) {
            //alert(response.ErrorMsg);
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
        } else if (response.EnableSuccess) {
            //alert(response.SuccessMsg);
            $('#assignMemberModal').modal('hide');
            $("#divLoader").hide();
            var callbackFunc = function () {
                var _curSubjectId = $("#SubjectId").val();
                window.location.href = "/Admin/Admin/dashboard/" + _curSubjectId;
            }
            showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
        }


    }
}

//Failure Method of Subjects When an error Occurred in adding the subjects
function onAssignFailure(response) {
    $("#divLoader").hide();
    showAlertBox("Failure Alert Message", "Some Problem Occurred.", gInfo.alertMsgType.failure);
}
//reset dropen value on model popup close
function resetDropdown() {
    $("#ddlOption").val("1 sel");
    $("#ddlAddUser li").removeClass("selected");
    $("#ddlAddUser li:first").addClass("selected");
}
//validate and submit assign member to subjects
function onValidateAssignMember(_this) {
    if ($('#assignMemberModal input[type="checkbox"]:checked').length > 0) {
        $("#divLoader").show();
        $('#btnAssignMemberSubmit').trigger('click');
    }
    else
        return showAlertBox("Infomartion Alert Message", "Select atleast one member from list", gInfo.alertMsgType.information);
}

function onAssignMemberHeaderCheckChange(_this) {
    if ($(_this).is(':checked')) {
        $('#assignMemberModal input[type="checkbox"]').prop('checked', 'checked');
    }
    else {
        $('#assignMemberModal input[type="checkbox"]').prop('checked', false);
    }
}

function onAssignMemberChildCheckChange() {
    var totalCheckbox = $('#assignMemberModal input[type="checkbox"]:not(.hdChk)').length;
    var totalCheckedCheckbox = $('#assignMemberModal input[type="checkbox"]:not(.hdChk):checked').length;
    if (totalCheckbox > totalCheckedCheckbox) {
        $('#assignMemberModal input[type="checkbox"].hdChk').prop('checked', false);
    }
    else {
        $('#assignMemberModal input[type="checkbox"].hdChk').prop('checked', 'checked');
    }
}

/*Deactivate Member from Subject*/
function DeactivateMemberFromSubject(uid, uType, _this) {
    var subid = $('#SubjectId').val();
    if (!uid || !subid || !uType) {
        //alert("Required Information is not completed for this request.");
        showAlertBox("Information Alert Message", "Required Information is not completed for this request.", gInfo.alertMsgType.information);
        return false;
    }
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Admin/DeactivateMemberFromSubject/",
        dataType: "html",
        data: JSON.stringify({ "uid": uid, "utype": uType, "subid": subid }),
        async: true,
        success: function (result) {
            try {
                result = JSON.parse(result);
            } catch (e) {
                result = eval(result);
            }
            if (!isSessionOutResponse(result)) {
                $("#divLoader").hide();
                if (result.status.toLowerCase() == "success") {
                    var callbackFunc = function () {
                        var _curSubjectId = $("#SubjectId").val();
                        window.location.href = "/Admin/Admin/dashboard/" + _curSubjectId;
                    }
                    showAlertBox("Success Alert Message", "Member is removed successfully", gInfo.alertMsgType.success, callbackFunc);
                    //alert("Member is removed Successfully");
                    if (_this) {
                        $('#modalDeactivationMemberAlert').modal('hide');
                    }
                }
                else {
                    //alert("There is some problem in removing member from giving subject.");
                    showAlertBox("Failure Alert Message", "There is some problem in removing member from given subject.", gInfo.alertMsgType.failure);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function getMemberGroupAssociation(uid, uType, _this) {
    var subid = $('#SubjectId').val();
    if (!uid || !subid || !uType) {
        //alert("Required Information is not completed for this request");
        showAlertBox("Information Alert Message", "Invalid arguments for this request", gInfo.alertMsgType.information);
        return false;
    }
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Assignments/GetStudentsGroups/",
        dataType: "html",
        data: JSON.stringify({ "uid": uid, "utype": uType, "subid": subid }),
        async: true,
        success: function (result) {
            try {
                result = JSON.parse(result);
            } catch (e) {
                result = eval(result);
            }
            if (!isSessionOutResponse(result)) {
                var member = (uType == "t") ? "teacher" : "student";
                var subjectName = $('#pan_hide .subjectname').text();
                if ($('#pan_hide .subjectname').attr('title')) {
                    subjectName = $('#pan_hide .subjectname').attr('title');
                }
                var memberName = $(_this).parents('.user_box').first().find('.user_name .memberName').text();
                var alertMessage = "Do you want to remove " + member + " <b>" + memberName + "</b> from subject <b>" + subjectName + "</b>";
                if (result.status.toLowerCase() == "success") {
                    if (result.Data && result.length > 0) {
                        $('#modalDeactivationMemberAlert .popup_txt').html("This Member is associated with the following groups <b>" + result.Data.join() + "</b>.<br/>" + alertMessage + "");
                        $('#submitLink').attr('onclick', "DeactivateMemberFromSubject('" + uid + "','" + uType + "',this)")
                        $('#modalDeactivationMemberAlert').modal('show');
                    }
                    else {
                        $('#modalDeactivationMemberAlert .popup_txt').html("" + alertMessage + ".");
                        $('#submitLink').attr('onclick', "DeactivateMemberFromSubject('" + uid + "','" + uType + "',this)")
                        $('#modalDeactivationMemberAlert').modal('show');
                    }
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
/*Deactivate Member from Subject */

/*Sorting*/
function MemberSort(_column) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Admin/MemberSort/",
        dataType: "html",
        data: JSON.stringify({ "id": $("#hdn_subjectId").val(), "userid": _serachedMember, "role": $("#hdn_userRole").val(), "_column": _column, "_order": _sortOrder }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                $("#divMembers").empty().html(result);
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

//new function for assigning members in Admin Area
function onClickSubmitButton(e) {
    var selectedItem = [];
    var parentEle = $('#tblmembers').first();
    var res = parentEle.find('input[type="checkbox"]:not(.hdChk):checked');
    $(res).each(function (index, value) {
        var valueItem = value.value;
        if ($.isNumeric(valueItem)) {
            selectedItem.push(value.value);
        }
    });
    $('#hdn_selectedItem').val(selectedItem);
    if (selectedItem.length <= 0) {
        e.preventDefault();
        return showAlertBox("Infomartion Alert Message", "Select atleast one member from list", gInfo.alertMsgType.information);
    }
}