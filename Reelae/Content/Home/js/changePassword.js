﻿function OpenChangePasswordView() {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/UserProfile/GetChangePasswordView",
        dataType: "html",
        async: false,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#ChangePassword').html(result);
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
function onPasswordSucess(response) {
    if (!isSessionOutResponse(response)) {
        if (response.EnableError) {
            //alert(response.ErrorMsg);
            showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
        } else if (response.EnableSuccess) {
            //alert(response.SuccessMsg);
            showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success);
        }
        $("#OldPassword").val("");
        $("#NewPassword").val("");
        $("#ConfirmPassword").val("");
    }
}
function onPasswordFailure(response) {
    //alert("Error Occurred.");
    showAlertBox("Failure Alert Message", "Oops!!Some Error Occurred", gInfo.alertMsgType.failure);
    $('#ChnagePasswordModelBox').modal('hide');
}
function ValidateFields() {
    var regP = new RegExp("^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{6,}$");
    if ($("#OldPassword").val().trim() == "") {
        //alert("Please enter current password.");
        showAlertBox("Information Alert Message", "Please enter current password.", gInfo.alertMsgType.information);
        $("#OldPassword").focus();
        return false;
    }
    else if ($("#NewPassword").val().trim() == "") {
        //alert("Please enter new password.");
        showAlertBox("Information Alert Message", "Please enter new password.", gInfo.alertMsgType.information);
        $("#NewPassword").focus();
        return false;
    }
    //else if (regP.test($("#NewPassword").val().trim())) {
    //    alert("Password must be combination of Lower,Upper case and Special Characters.");
    //    $("#NewPassword").focus();
    //    return false;
    //}
    else if ($("#ConfirmPassword").val().trim() != $("#NewPassword").val().trim()) {
        //alert("The new password and confirmation password do not match.");
        showAlertBox("Information Alert Message", "The new password and confirmation password do not match.", gInfo.alertMsgType.information);
        $("#ConfirmPassword").focus();
        return false;
    }
    else if ($("#OldPassword").val().trim() == $("#NewPassword").val().trim()) {
        //alert("The new and old password should not be same.");
        showAlertBox("Information Alert Message", "The new and old password should not be same.", gInfo.alertMsgType.information);
        $("#ConfirmPassword").focus();
        return false;
    }
    else {
        return true;
    }
}