﻿function triggerFileUpload() {
    $('#FileUpload1').trigger('click');
}
function OpenProfileSetupView() {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/UserProfile/GetProfileSetupView",
        dataType: "html",
        async: false,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#profile').html(result);

                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
function onProfileSucess(response) {
    $("#divLoader").hide();
    if (!isSessionOutResponse(response)) {
        if (response.EnableError) {
            //alert(response.ErrorMsg);
            showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
        } else if (response.EnableSuccess) {
            //alert(response.SuccessMsg);
            var callbackFunc = function () {
                window.location.reload();
            }
            showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
        }
    }
}
function onProfileFailure(response) {
    //alert("Error Occurred.");
    $("#divLoader").hide();
    showAlertBox("Failure Alert Message", "Oops! Some Problem Occurred.", gInfo.alertMsgType.failure);
}
function ValidateProfileFields() {
    var email_regex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var urlRegex = '^(?!mailto:)(?:(?:http|https|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[0-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))|localhost)(?::\\d{2,5})?(?:(/|\\?|#)[^\\s]*)?$';
    var _website = "";
    if ($("#Website").val().trim() != "http://")
    {
        _website = $("#Website").val().trim();
    }
    var url = new RegExp(urlRegex, 'i');
    var profileDiv = $('#divUserProfile');
    var profileName = $('input[name="Name"]', profileDiv).val();
    if (!profileName.trim()) {
        showAlertBox("Information Alert Message", "Name Is Required.", gInfo.alertMsgType.information);
        return false;
    }
    if ($("#Email").val().trim() != "") {
        if (!email_regex.test($("#Email").val().trim())) {
            showAlertBox("Information Alert Message", "Invalid email address.", gInfo.alertMsgType.information);
            return false;

        }
    }
    if (_website!="")
    {
        if (!url.test(_website)) {
            showAlertBox("Information Alert Message", "Invalid website url", gInfo.alertMsgType.information);
            return false;
        }
    }
    $("#divLoader").show();
    return true;
}
function UploadPic() {
    if (window.FormData !== undefined) {

        var fileUpload = $("#FileUpload1").get(0);
        var files = fileUpload.files;

        // Create FormData object  
        var fileData = new FormData();
        var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
        // Looping over all files and add it to FormData object  
        for (var i = 0; i < files.length; i++) {

            var fileType = files[i].type;
            if ($.inArray(fileType, ValidImageTypes) < 0) {
                //alert('Invalid file type, please upload file type of image.')
                showAlertBox("Information Alert Message", "Invalid file type, please upload file type of image.", gInfo.alertMsgType.information);
                return false;
            }
            var size = (files[i].size / (1024 * 1024)).toFixed(2);
            if (size > 4) {
                //alert("Image is too big, please upload small image.");
                showAlertBox("Information Alert Message", "Image is too big, please upload small image.", gInfo.alertMsgType.information);
                return false;
            }
            fileData.append(files[i].name, files[i]);
        }

        // Adding one more key to FormData object  
        $("#divLoader").show();
        $.ajax({
            url: '/UserProfile/UploadPic',
            type: "POST",
            contentType: false, // Not to set any content header  
            processData: false, // Not to process data  
            data: fileData,
            success: function (result) {
                if (!isSessionOutResponse(result)) {
                    if (result != "") {
                        var picPath = "~/UserPic/" + result.filename;
                        //$("#UserPicUrl").val(logopath);
                        $("#UserPicUrl").val(picPath);
                        $("#imgUserPic").attr("src", result.path);
                    }
                    $("#divLoader").hide();
                }
            },
            error: function (err) {
                showAlertBox("Failure Alert Message", err.statusText, gInfo.alertMsgType.failure)
                //alert(err.statusText);
                $("#divLoader").hide();
            }
        });
    }
    else {
        //alert("FormData is not supported.");
        showAlertBox("Browser Functionality Alert Message", "FormData is not supported in this current version of browser.Please Upgrade newer version", gInfo.alertMsgType.failure)
    }
}

function DeletePic() {
    $("#UserPicUrl").val("");
    $("#imgUserPic").attr("src", "");
}
//Alert in case of harmfull input
