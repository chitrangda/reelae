﻿var _serachedTagId = '';
var __serachedTagName = '';


function onAddResourceSucess(response) {
    if (!isSessionOutResponse(response)) {
        if (response.EnableError) {
            showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
        } else if (response.EnableSuccess) {
            $('#addResourceModal').modal('hide');
            $('#editResourceModal').modal('hide');
            var callbackFunc = function () {
                clearData();
                // window.location.reload();
                callLabelFilter($('#labelId').val());
            }
            showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
        }
        $("#divLoader").hide();
    }
}

//Failure Method of Subjects When an error occured in adding the subjects
function onAddResourceFailure(response) {
    showAlertBox("Failure Alert Message", "Some Problem Occured.", gInfo.alertMsgType.failure);
}
function onAddResourceBegin() {
    //if ($.trim($("#resourceInfo_SubjectID").val()) == "") {
    //    showAlertBox("information alert message", "Please select a subject.", gInfo.alertMsgType.information);
    //    $("#txt_subjectName").focus();
    //    return false;
    //}
   //else 
    if ($.trim($("#txt_subjectDescription").val()) == "") {
        showAlertBox("information alert message", "Please enter description.", gInfo.alertMsgType.information);
        $("#txt_subjectDescription").focus();
        return false;
    }
        //else if ($("#_HttpPostedResourceFile").val() == '') {
    else if ($("#resourceInfo_URL").val() == '') {
        showAlertBox("information alert message", "Please select a file.", gInfo.alertMsgType.information);
        $("#txt_subjectDescription").focus();
        return false;
    }
    else {
        $("#divloader").show();
        return true;
    }
}

//before subject save click 
function OnAddResourceSaveClick() {

}
//
function clearData() {
    $('#addResourceModal').modal('hide');
}
//reset subject modal 
function OnAddResourceModalReset() {
    try {
        if ($("#resourceInfo_URL").val() != '') {
            //$('.ddlAddSubject li').first().addClass("selected").siblings("li").removeClass("selected");
            //$('.#subjectDDl>.mad-select-drop li').first().addClass("selected").siblings("li").removeClass("selected");
            $('#resourceInfo_SubjectID').val('');
            $('#txt_subjectDescription').val('');
            DeleteResourceFile();
        }
        else {
            clearData();
        }
    } catch (e) {
    }
}

function UploadResourceFile() {
    if (window.FormData !== undefined) {
        var fileUpload = $("#_HttpPostedResourceFile").get(0);
        var files = fileUpload.files;
        var fileData = new FormData();
        if (isValidExtention(files[0])) {
            fileData.append(files[0].name, files[0]);
        }
        else {
            return false;
        }     
        // Adding one more key to FormData object  
        $("#divLoader").show();
        $.ajax({
            url: '/Resource/UploadResourceFile',
            type: "POST",
            contentType: false, // Not to set any content header  
            processData: false, // Not to process data  
            data: fileData,
            success: function (result) {
                if (!isSessionOutResponse(result)) {
                    if (result != "") {
                        setUploadedResourcePreview(result.fileType, result.fileAbsoluteURL);
                        $('#btnDelete').show();
                        $('#btnResourceupload').hide();
                        $('#resourceInfo_URL').val(result.fileAbsoluteURL);
                        $('#resourceInfo_fileType').val(result.fileType);
                        $('#resourceInfo_FileSize').val(result.FileSize);
                        $('#resourceInfo_Title').val(result.fileName);
                    }
                    $("#divLoader").hide();
                }
            },
            error: function (err) {
                showAlertBox("Failure Alert Message", err.statusText, gInfo.alertMsgType.failure)
                $("#divLoader").hide();
            }
        });
    }
    else {
        showAlertBox("Browser Functionality Alert Message", "FormData is not supported in this current version of browser.Please Upgrade newer version", gInfo.alertMsgType.failure)
    }
}

function DeleteResourceFile() {
    $("#divLoader").show();
    $.ajax({
        url: '/Resource/DeleteResourceFile?fileUrl=' + $("#resourceInfo_URL").val(),
        type: "POST",
        contentType: false, // Not to set any content header  
        processData: false, // Not to process data  
        data: {},
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result != "" && result == 'True') {
                    $('#_HttpPostedResourceFile').val('');
                    $('#resourceInfo_fileType').val('');
                    $('#resourceInfo_FileSize').val('');
                    $('#resourceInfo_Title').val('');
                    $('#resourceInfo_URL').val('');
                    $('.upload_logo').empty();
                    $("#btnDelete").hide();
                    $("#btnResourceupload").show();
                }
                $("#divLoader").hide();
            }
        },
        error: function (err) {
            showAlertBox("Failure Alert Message", err.statusText, gInfo.alertMsgType.failure)
            $("#divLoader").hide();
        }
    });
}

function triggerFileUpload() {// for trigger file upload control
    $('#_HttpPostedResourceFile').trigger('click');
}

function showFileDetails(id, _this) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Resource/GetReourceFileDetails/",
        dataType: "html",
        data: JSON.stringify({ "id": id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('.side_panel').show();
                    $('#fileDetails').empty().html(result);
                    $('.prod_box').removeClass('active');
                    $(_this).parent().addClass('active');
                    $("#divLoader").hide();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

//for setting resource preview
function setUploadedResourcePreview(fileType, URL) {
    var fileExtension = URL.split(".");
    fileExtension = fileExtension[fileExtension.length - 1];
    var html = "";
    switch (fileType) {
        case "document":
            if (fileExtension == "doc" || fileExtension == "docx") {
                html = "<div class='prod_wrap_add text-center'>";
                html += " <i class='material-icons file_icn'>insert_drive_file</i></div>";
            }
            else if (fileExtension == "xls" || fileExtension == "xlsx") {
                html = "<div class='prod_wrap_add text-center'>";
                html += " <i class='material-icons file_icn'>insert_drive_file</i></div>";
            }
            else {
                html = "<div class='prod_wrap_add text-center'>";
                html += " <i class='material-icons file_icn'>insert_drive_file</i></div>";
            }
            break;
        case "video":
            html = "<video id='thumb' heght='50px' width='50px'><source heght='50px' width='50px' src='" + URL + "' /> </video>";
            break;
        case "audio":
            html = " <audio controls heght='50px' width='50px'> <source src='" + URL + "' /> </audio>";
            break;
        case "compressed":
            html = "<div class='prod_wrap_add text-center'>";
            html += "<i class='material-icons file_icn'>archive</i></div>";
            break;
        case "image":
            html = "<img src=" + URL + " id='imgResource'>";
            break;
    }
    $('.upload_logo').empty();
    $('.upload_logo').append(html);
}

function getDownloadResourceActivityList(sortOrder) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Resource/GetDownloadActivity?subjectId=" + $('#subjectId').val() + "&sortOrder=" + sortOrder,
        dataType: "html",
        //data: JSON.stringify({ "id": id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#downloadActivity').empty().html(result);
                    $(".parentActivity").mCustomScrollbar({
                        theme: "dark-3",
                        autoHideScrollbar: true
                    });
                    $("#divLoader").hide();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

//for student 
function showFileDetail(id, _this) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/Resource/GetStudentReourceFileDetails/",
        dataType: "html",
        data: JSON.stringify({ "id": id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('.side_panel').show();
                    $('#fileDetails').empty().html(result);
                    $('.prod_box').removeClass('active');
                    $(_this).parent().addClass('active');
                    $("#divLoader").hide();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

//student resource listing
function getResourcesGridViewStudent(sortOrder) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/Resource/GetResources/",
        dataType: "html",
        data: JSON.stringify({ "subjectId": $('#subjectId').val(), "sortOrder": sortOrder, "docType": $('#docType').val(), "otherFilter": $('#otherFilterType').val(), "tag": $('#selectedTag').val(), "labelId": $('#labelId').val() }),
        //data: JSON.stringify({ "subjectId": $('#subjectId').val(), "sortOrder": sortOrder }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#fileDetails').hide();
                    $('#resourceList').empty().html(result);

                    $(".resourceparent").mCustomScrollbar({
                        theme: "dark-3",
                        updateOnContentResize: true
                    });

                    $('#resourceList').children().first().find('#itemId').trigger('click');
                    $("#divLoader").hide();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
//Teacher resource listing
function getResourcesGridView(sortOrder) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Resource/GetResources/",
        dataType: "html",
        data: JSON.stringify({ "subjectId": $('#subjectId').val(), "sortOrder": sortOrder, "docType": $('#docType').val(), "otherFilter": $('#otherFilterType').val(), "tag": $('#selectedTag').val(), "labelId":  $('#labelId').val()}),
        async: true,
        
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#fileDetails').hide();
                    $('#resourceList').empty().html(result);
                    //for adding scroll bar
                    $(".resourceparent").mCustomScrollbar({
                        theme: "dark-3",
                        updateOnContentResize: true
                    });
                    //end
                    $('#resourceList').children().first().find('#itemId').trigger('click');
                    $("#divLoader").hide();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
//for student download resource
function downloadFilesStudent(resourceId) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/Resource/Download/",
        dataType: "html",
        data: JSON.stringify({ "resourceId": resourceId }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $("#divLoader").show();
                    window.location = "/Student/Resource/DownloadFile?resourceId=" + resourceId;
                    $("#divLoader").hide();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
//for teacher download resource
function downloadFilesTeacher(resourceId) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Resource/Download/",
        dataType: "html",
        data: JSON.stringify({ "resourceId": resourceId }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $("#divLoader").show();
                    window.location = "/Resource/DownloadFile?resourceId=" + resourceId;
                    // var url = "/Resource/DownloadFile?resourceId=" + resourceId;
                    // $('#dwl').attr('src', url);
                    // window.location.reload();
                    // getDownloadResourceActivityList();
                    $("#divLoader").hide();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
//edit resource details
function editSubjectResource(id, _this) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Resource/EditSubjectResource/",
        dataType: "html",
        data: JSON.stringify({ "id": id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#divAddSubjectResource').empty();
                    $('#divEditSubjectResource').html(result);
                    $('#editResourceModal').modal('show');
                    initMadSelect($('.ddlAddSubject').parent());
                    setUploadedResourcePreview($('#resourceInfo_fileType').val(), $('#resourceInfo_URL').val());
                    $("#btnDelete").show();
                    $("#btnResourceupload").hide();
                    $("#divLoader").hide();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
//add resource details
function addSubjectResource() {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Resource/AddResourceDetails/",
        dataType: "html",
        //data: {},
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#divEditSubjectResource').empty();
                    $('#divAddSubjectResource').empty().html(result);
                    $('#addResourceModal').modal('show');
                    // initMadSelect($('.ddlAddSubject').parent());
                    $('#resourceInfo_SubjectID').val($('#subjectId').val());
                    $('#resourceInfo_LabelId').val($('#labelId').val());

                    $("#divLoader").hide();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
function resetEditResourceModel() {
    $('#editResourceModal').modal('hide');
}
//delete resource details
function deleteSubjectResourceConfirmation(id, resourceName) {
    $('#removeConfirmationModalBox').modal('show');
    var message = "Are you sure, Do you want delete <strong>" + resourceName + "</strong> resource.";
    $('#message').html(message);
    $('#submitAlert').attr('onclick', 'deleteSubjectResource(' + id + ')');
}

function deleteSubjectResource(id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Resource/DeleteSubjectResource/",
        dataType: "html",
        data: JSON.stringify({ "id": id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $("#removeConfirmationModalBox").modal('hide');
                    getDownloadResourceActivityList("");
                    getResourcesGridView("");
                    $("#divLoader").hide();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
//auto complete of existing tags
function initAutoComplete_tags() {
    _serachedTagId = '';
    __serachedTagName = '';
    $('#TagName').autocomplete({
        lookup: arrayResourceTag,
        onSelect: function (suggestion) {
            _serachedTagId = suggestion.data;
            __serachedTagName = suggestion.value;
            $('#selectedTagId').val(suggestion.data);
        }
    });
}
//add addTagToResource
function addTagToResource(resourceId) {
    var selectedText = $('#TagName').val();
    if (selectedText.length > 0) {
        $("#divLoader").show();
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Resource/AddTagToSubjectResource/",
            dataType: "json",
            data: JSON.stringify({ "resourceId": resourceId, "tagId": $('#selectedTagId').val(), "tagName": selectedText }),
            async: true,
            success: function (result) {
                if (!isSessionOutResponse(result)) {
                    $('#addResourceTag').modal('hide');
                    $('#divAddTagResource').empty();
                    bindTags(null);
                    showAlertBox("Success Alert Message", "Tag Added Successfully.", gInfo.alertMsgType.success, null);
                    $("#divLoader").hide();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#divLoader").hide();
                showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
                return false;
            }
        });
    }
    else {
        showAlertBox("information alert message", "Please Fill Tag.", gInfo.alertMsgType.information);
        $("#TagName").focus();
    }
}
//flage section 
function flagResource(id, isFlaged) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Resource/FlagSubjectResource/",
        dataType: "html",
        data: JSON.stringify({ "id": id, "isFlaged": isFlaged }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    getDownloadResourceActivityList("");
                    getResourcesGridView("");
                    $("#divLoader").hide();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
//tag the resource
function tagResource(id, Title) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Resource/TagSubjectResource/",
        dataType: "html",
        data: JSON.stringify({ "id": id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#divAddTagResource').empty().html(result);
                    $('#addResourceTag').modal('show');
                    $("#divLoader").hide();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

//view student description
function viewDescription(id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/Resource/ViewResourceDescription/",
        dataType: "html",
        data: JSON.stringify({ "id": id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#resourceViewDescriptionDiv').empty().html(result);
                    $('#viewResourceDescription').modal('show');
                    $("#divLoader").hide();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

//bind student resource tags
function bindStudentResourceTags(id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/Resource/GetStudentResourceTags/",
        dataType: "html",
        data: JSON.stringify({ "id": id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#resourceTags').empty();
                    $('#resourceTags').html(result);
                    $("#divLoader").hide();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
//bind teacher resource tags
//for binding of taging
function bindTags(id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Resource/GetResourceTags/",
        dataType: "html",
        data: JSON.stringify({ "id": id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#resourceTags').empty();
                    $('#resourceTags').html(result);
                    $("#divLoader").hide();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
//add description
function addDescription(id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Resource/AddResourceDescription/",
        dataType: "html",
        data: JSON.stringify({ "id": id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#divAddDescriptionResource').empty().html(result);
                    $('#addResourceDescription').modal('show');
                    $("#divLoader").hide();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
//update resource description
function updateResourceDescription(resourceId) {
    var selectedText = $('#Description').val();
    if (selectedText.length > 0) {
        $("#divLoader").show();
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Resource/UpdateResourceDescription/",
            dataType: "json",
            data: JSON.stringify({ "resourceId": resourceId, "description": selectedText }),
            async: true,
            success: function (result) {
                if (!isSessionOutResponse(result)) {
                    $('#addResourceDescription').modal('hide');
                    $('#divAddDescriptionResource').empty();
                    showAlertBox("Success Alert Message", "Description Saved Successfully.", gInfo.alertMsgType.success, null);
                    $("#divLoader").hide();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#divLoader").hide();
                showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
                return false;
            }
        });
    }
    else {
        showAlertBox("information alert message", "Please Fill Description.", gInfo.alertMsgType.information);
        $("#Description").focus();
    }
}
//listing of right panel resource activity
function getActivityContent(id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Resource/GetActivityDetailList/",
        dataType: "html",
        data: JSON.stringify({ "id": id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#activityDetails').empty().html(result);
                    $("#divLoader").hide();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
//student flag resource
function flagStudentResource(id, isFlaged) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Resource/FlagSubjectResource/",
        dataType: "html",
        data: JSON.stringify({ "id": id, "isFlaged": isFlaged }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    getResourcesGridViewStudent('');
                    $("#divLoader").hide();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
//update view activity
function updateViewActivity(resourceId){
        $("#divLoader").show();
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Student/Resource/UpdateViewActivity/",
            dataType: "html",
            data: JSON.stringify({ "id": resourceId }),
            async: true,
            success: function (result) {
                if (!isSessionOutResponse(result)) {
                    if (result) {
                        $("#divLoader").hide();
                    }
                }
                $("#divLoader").hide();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#divLoader").hide();
                showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
                return false;
            }
        });
}
//restore the resource file
function restoreResourceFile(id)//Resource restore successfully.
{
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Resource/RestoreResourceFile/",
        dataType: "html",
        data: JSON.stringify({ "id": id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $("#removeConfirmationModalBox").modal('hide');
                    getDownloadResourceActivityList("");
                    getResourcesGridView("");
                    $("#divLoader").hide();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
///////////////////////////////////Resource Label Section/////////////////////////////////
//open the view of adding the label
 function addLabel(subjectId)
    {
        $("#divLoader").show();
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Resource/AddResourceLabel/",
            dataType: "html",
            data: JSON.stringify({ "id": 1 }),
            async: true,
            success: function (result) {
                if (!isSessionOutResponse(result)) {
                    if (result) {
                        $('#divAddSubjectResource').empty();
                        $('#divAddSubjectResource').html(result);
                        $('#addResourceLabel').modal('show');
                        $("#divLoader").hide();
                    }
                }
                $("#divLoader").hide();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#divLoader").hide();
                showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
                return false;
            }
        });
    }
 //listing of labels
    function resourceLabelList(subjectId,userType) {
        $("#divLoader").show();
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Resource/ResourceLableList/",
            dataType: "html",
            data: JSON.stringify({ "subjectId": subjectId }),
            async: true,
            success: function (result) {
                if (!isSessionOutResponse(result)) {
                    if (result) {
                        $('#resourceLabelList').empty();
                        $('#resourceLabelList').html(result);                      
                        checklabelIsAdded();

                        //$("#resourceLabelList").mCustomScrollbar({
                        //    theme: "dark-3",
                        //    updateOnContentResize: true
                        //});



                        //if ($('#labelId').val()=='')
                        //{
                             $('#labelList').children().first().find('a').trigger('click');
                        //}                                            
                        $("#divLoader").hide();
                    }
                }
                $("#divLoader").hide();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#divLoader").hide();
                showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
                return false;
            }
        });
    }
//Save the label details
    function saveLabel(resourceId) {
        var startdate = moment($('#labeldateRange').data('daterangepicker').startDate.format("MM/DD/YYYY"), "MM/DD/YYYY");
        var enddate = moment($('#labeldateRange').data('daterangepicker').endDate.format("MM/DD/YYYY"), "MM/DD/YYYY");

        var txtLabel = $('#txtLabel').val();
        if (txtLabel.length > 0) {
            $("#divLoader").show();
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Resource/AddLabelToSubjectResource/",
                dataType: "json",
                data: JSON.stringify({ "subjectId": $('#subjectId').val(), "startDate": startdate, "endDate": enddate, "labelName": txtLabel }),
                async: true,
                success: function (result) {
                    if (!isSessionOutResponse(result)) {                  
                        $('#addResourceLabel').modal('hide');
                        $('#divAddSubjectResource').empty();
                        resourceLabelList($('#subjectId').val());
                        showAlertBox("Success Alert Message", "Label Added Successfully.", gInfo.alertMsgType.success, null);
                        $("#divLoader").hide();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#divLoader").hide();
                    showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
                    return false;
                }
            });
        }
        else {
            showAlertBox("information alert message", "Please Fill Label.", gInfo.alertMsgType.information);
            $("#txtLabel").focus();
        }
    }

    function callLabelFilter(labelId) {
        //$('#resourceLeftMenu ul li').removeClass('active');
        //$('#resourceLeftMenu ul li').first('a').addClass('active');

        //$('#selectedTag').val('');
        //$('#otherFilterType').val('');
        //$('#docType').val('');

        $('#labelId').val(labelId);
        if ($('#userType').val() == 'T') {
            getResourcesGridView("");
        }
        else {
            getResourcesGridViewStudent("");
        }
    }

    function checklabelIsAdded() {
        if ($('#labelList').length) {
            $('#emptyMessage').hide();
            $('#scrollBody').addClass('resource_scroll-body');
            $('#fileDetails').show();
            $('#home').show();
        }
        else {
            $('#scrollBody').removeClass('resource_scroll-body');
            $('#fileDetails').hide();
            $('#home').hide();
            $('#emptyMessage').show();
        }
    }

    //for displaying activity details
    function showActiveFileDetail() {
        $('#profile').removeClass('active');
        $('#homeFileDetails').addClass('active');
    }
    function showResourceActivityGraph() {
        $("#divLoader").show();
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Resource/ResourceActivityGraph/",
            dataType: "html",
            //data: JSON.stringify({ "resourceId": 1 }),
            async: true,
            success: function (result) {
                if (!isSessionOutResponse(result)) {
                    if (result) {
                        $('#divAddSubjectResource').empty();
                        $('#divAddSubjectResource').html(result);
                        $('#resourceActivityGraph').modal('show');
                        $("#divLoader").hide();
                    }
                }
                $("#divLoader").hide();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#divLoader").hide();
                showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
                return false;
            }
        });
    }

    function showResourceVisitGraph() {
        $("#divLoader").show();
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Resource/ResourceVisitGraph/",
            dataType: "html",
            //data: JSON.stringify({ "resourceId": 1 }),
            async: true,
            success: function (result) {
                if (!isSessionOutResponse(result)) {
                    if (result) {
                        $('#divAddSubjectResource').empty();
                        $('#divAddSubjectResource').html(result);
                        $('#resourceVisitGraph').modal('show');
                        $("#divLoader").hide();
                    }
                }
                $("#divLoader").hide();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#divLoader").hide();
                showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
                return false;
            }
        });
    }
//update visit time
    function updateVisitTime(startTime, endTime) {
        $("#divLoader").show();
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Student/Resource/SaveResorceVisitTime/",
            dataType: "html",
            data: JSON.stringify({ "startDate": startTime, "endTime": endTime, "subjectId": $('#subjectId').val() }),
            async: true,
            success: function (result) {
                if (!isSessionOutResponse(result)) {
                    if (result) {
                        $("#divLoader").hide();
                    }
                }
                $("#divLoader").hide();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#divLoader").hide();
                showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
                return false;
            }
        });
    }