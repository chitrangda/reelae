﻿var _subArr = [];
$(document).ready(function () {
    $('#ddlSubjects').multiselect({
        disableIfEmpty: true,
        maxHeight: 200,
        buttonWidth: '170px',
        //enableFiltering: true,
        nonSelectedText: 'Select Subject',
        includeSelectAllOption: true,
        selectAllJustVisible: false,
        enableCaseInsensitiveFiltering: false,
        allSelectedText: 'All Subjects are selected',
        onChange: function (option, checked, select) {
            if (checked) {
                _subArr.push($(option).val());

            } else {
                var item = $.inArray($(option).val(), _subArr);
                _subArr.splice($.inArray($(option).val(), _subArr), 1);
            }
            getForumTopics(_subArr.join());
        },
        onSelectAll: function () {
            _subArr = [];
        },
        onDeselectAll: function () {
            _subArr = [];
        }
    });
})

$(document).ready(function () {
    $(".fourm_txt").mCustomScrollbar({
        theme: "dark-3",
        autoHideScrollbar: true
    });
})

function getForumTopics(_id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Forum/getForumTopics/",
        dataType: "html",
        data: JSON.stringify({ "subjectIds": _id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#divTopics').html(result);
                    $("#divLoader").hide();

                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function getNewTopicView(_count) {
    if (_count > 0) {
        $("#divLoader").show();
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Forum/getNewTopicView/",
            dataType: "html",
            async: true,
            success: function (result) {
                if (!isSessionOutResponse(result)) {
                    if (result) {
                        $('#newtopicDiv').html(result);
                        initForumDatePicker($('#forumTopicDate'));
                        $('#newTopicModelBox').modal('show');
                        $('#txt_descr').summernote({
                            toolbar: [
                                ['misc', ['undo', 'redo']],
                                ['style', ['bold', 'italic', 'underline', 'clear']],
                                ['font', ['strikethrough', 'superscript', 'subscript']],
                                ['fontsize', ['fontsize']],
                                ['color', ['color']],
                                ['para', ['ul', 'ol', 'paragraph']],
                                ['Insert', ['picture', 'link', 'video', 'table', 'hr']]
                            ],
                            height: 100
                        });
                        initMadSelect($(".mad-select"), false);
                        $(".modal-body").mCustomScrollbar({
                            theme: "dark-3",
                            autoHideScrollbar: true
                        });
                        $("#divLoader").hide();
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#divLoader").hide();
                showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
                return false;
            }
        });
    }
    else
    {
        showAlertBox("Information Alert Message", "Please add subject first", gInfo.alertMsgType.information);

    }
}

function onTopicBegin() {
    var urlRegex = '^(?!mailto:)(?:(?:http|https|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[0-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))|localhost)(?::\\d{2,5})?(?:(/|\\?|#)[^\\s]*)?$';
    var url = new RegExp(urlRegex, 'i');
    var scriptPat = /[<>]/g;
    if ($("#txt_topic").val().trim() == "") {
        showAlertBox("Information Alert Message", "Please enter Topic.", gInfo.alertMsgType.information);
        $("#txt_title").focus();
        return false;
    }
    else if (scriptPat.test($("#txt_topic").val().trim())) {
        showAlertBox("Information Alert Message", "<,> characters are not allowed.", gInfo.alertMsgType.information);
        $("#txt_title").val('');
        return false;

    }
    else if ($("#txt_title").val().trim() == "") {
        showAlertBox("Information Alert Message", "Please enter Title.", gInfo.alertMsgType.information);
        $("#txt_title").focus();
        return false;
    }
    else if (scriptPat.test($("#txt_title").val().trim())) {
        showAlertBox("Information Alert Message", "<,> characters are not allowed.", gInfo.alertMsgType.information);
        $("#txt_title").val('');
        return false;

    }
    else if ($("#txt_descr").val().trim() == "") {
        showAlertBox("Information Alert Message", "Please enter Content.", gInfo.alertMsgType.information);
        $("#txt_topic").focus();
        return false;
    }
        //else if (scriptPat.test($("#txt_descr").val().trim())) {
        //    showAlertBox("Information Alert Message", "<,> characters are not allowed.", gInfo.alertMsgType.information);
        //    $("#txt_descr").val('');
        //    return false;

        //}
    else {
        $("#divLoader").show();
        return true;
    }
}

function onTopicSucess(response) {
    $("#divLoader").hide();
    $('#newTopicModelBox').modal('hide');
    if (!isSessionOutResponse(response)) {
        if (response.EnableError) {
            showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
        } else if (response.EnableSuccess) {
            var callbackFunc = function () {
                $("#divLoader").show();
                window.location.reload();
            }
            showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
        }

    }
}

function setSubjectId(_subjectId) {
    $("#selSubjectID").val(_subjectId);
}

function editForumTopic(_id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Forum/getEditView/",
        dataType: "html",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#newtopicDiv').html(result);
                    initForumDatePicker($('#forumTopicDate'));
                    $('#newTopicModelBox').modal('show');
                    $('#txt_descr').summernote({
                        toolbar: [
                           ['misc', ['undo', 'redo']],
                           ['style', ['bold', 'italic', 'underline', 'clear']],
                           ['font', ['strikethrough', 'superscript', 'subscript']],
                           ['fontsize', ['fontsize']],
                           ['color', ['color']],
                           ['para', ['ul', 'ol', 'paragraph']],
                           ['Insert', ['picture', 'link', 'video', 'table', 'hr']]
                        ],
                        height: 100
                    });
                    initMadSelect($(".mad-select"), false);
                    $(".modal-body").mCustomScrollbar({
                        theme: "dark-3",
                        autoHideScrollbar: true
                    });
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function RemoveForumTopicConfirmation(_id, _title) {
    var message = "Are you sure, do you want to remove the forum topic '<strong>" + _title + "'</strong>.";
    $('#message').html(message);
    $('#removeConfirmationModalBox').modal('show');
    $('#submitAlert').attr("onclick", "RemoveForumTopic('" + _id + "')");
}

function RemoveForumTopic(_id) {
    $('#removeConfirmationModalBox').modal('hide');
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Forum/RemoveForumTopic/",
        dataType: "json",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (response) {
            $("#divLoader").hide();
            if (!isSessionOutResponse(response)) {
                if (response.EnableError) {
                    showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
                } else if (response.EnableSuccess) {
                    var callbackFunc = function () {
                        $("#divLoader").show();
                        window.location.reload();
                    }
                    showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function initForumDatePicker(containerEle) {
    var start = moment().add(29, 'days');

    if ($("#hdn_Date").val() != "") {
        start = moment($("#hdn_Date").val());
    }
    else {
        $("#hdn_Date").val(start.format('MM/DD/YYYY'));
    }
    containerEle.daterangepicker({
        startDate: start,
        singleDatePicker: true,
        showDropdowns: true,
        drops: "up",
        locale: {
            format: 'MM/DD/YYYY'
        }
    },
    function (start, end, label) {
        var curDate = new Date();
        curDate.setHours(0, 0, 0, 0);
        var endDate = new Date(end);
        endDate.setHours(0, 0, 0, 0);
        if (endDate < curDate) {
            alert('Inactive date can not be past date.');
            return false;
        }
        $("#hdn_Date").val(start.format('MM/DD/YYYY'));
        containerEle.data('assignSD', start.format('MM/DD/YYYY'));

    });
    $("#hdn_Date").val(start.format('MM/DD/YYYY'));
}

function ReadMore(_id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Forum/getContent/",
        dataType: "html",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#newtopicDiv').html(result);
                    $(".modal-body").mCustomScrollbar({
                        theme: "dark-3",
                        autoHideScrollbar: true
                    });
                    $('#ReadMoreModelBox').modal('show');
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function Reply(_id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Forum/getReplyView/",
        dataType: "html",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#newtopicDiv').html(result);
                    $(".modal-body").mCustomScrollbar({
                        theme: "dark-3",
                        autoHideScrollbar: true
                    });
                    $('#txt_descr').summernote({
                        toolbar: [
                                ['misc', ['undo', 'redo']],
                                ['style', ['bold', 'italic', 'underline', 'clear']],
                                ['font', ['strikethrough', 'superscript', 'subscript']],
                                ['fontsize', ['fontsize']],
                                ['color', ['color']],
                                ['para', ['ul', 'ol', 'paragraph']],
                                ['Insert', ['picture', 'link', 'video', 'table', 'hr']]
                        ],
                        height: 200
                    });
                    $('#replyModelBox').modal('show');
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function onReplyBegin() {
    if ($("#txt_descr").val().trim() == "") {
        showAlertBox("Information Alert Message", "Please enter reply.", gInfo.alertMsgType.information);
        $("#txt_descr").focus();
        return false;
    }
    else {
        $("#divLoader").show();
        return true;
    }
}

function onReplySucess(response) {
    $("#divLoader").hide();
    $('#replyModelBox').modal('hide');
    if (!isSessionOutResponse(response)) {
        if (response.EnableError) {
            showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
        } else if (response.EnableSuccess) {
            var callbackFunc = function () {
                $("#divLoader").show();
                window.location.reload();
            }
            showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
        }

    }
}

function getAllReply(_id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Forum/getAllReply/",
        dataType: "html",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#newtopicDiv').html(result);
                    $(".modal-body").mCustomScrollbar({
                        theme: "dark-3",
                        autoHideScrollbar: true
                    });
                    $('#allReplyModelBox').modal('show');
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function editReply(_id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Forum/getEditReplyView/",
        dataType: "html",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#newtopicDiv').html(result);
                    $(".modal-body").mCustomScrollbar({
                        theme: "dark-3",
                        autoHideScrollbar: true
                    });
                    $('#txt_descr').summernote({ height: 200 });
                    $('#replyModelBox').modal('show');
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function removeReplyConfirmation(_id, _forumId) {
    var message = "Are you sure, do you want to remove ?";
    $('#message').html(message);
    $('#removeConfirmationModalBox').modal('show');
    $('#submitAlert').attr("onclick", "removeReply(" + _id + "," + _forumId + ")");
}

function removeReply(_id, _forumId) {

    $('#removeConfirmationModalBox').modal('hide');
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Forum/removeReply/",
        dataType: "json",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (response) {
            $('#allReplyModelBox').modal('hide');
            $("#divLoader").hide();
            if (!isSessionOutResponse(response)) {
                if (response.EnableError) {
                    showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
                } else if (response.EnableSuccess) {
                    var callbackFunc = function () {
                        getAllReply(_forumId);
                    }
                    showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
