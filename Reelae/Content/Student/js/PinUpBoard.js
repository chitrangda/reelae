﻿var _groupIdArr = [];
$(document).ready(function () {
    $("#liPrivate").find("a").trigger("click");
})

function setActiveTab(_tab) {
    var _url = "";
    if (_tab == "#PrivateDiv") {
        _url = "/Student/PinUpBoard/getPrivatePinups/";
    }
    else if (_tab == "#PublicDiv") {
        _url = "/Student/PinUpBoard/getPublicPinups/";
    }
    else if (_tab == "#GroupDiv") {
        _url = "/Student/PinUpBoard/getGroupPinups/";
    }
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: _url,
        dataType: "html",
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $(_tab).html(result);
                    $(".tab-content").find("div").removeClass("active");
                    $(_tab).addClass("active");
                    $(_tab).find(".column-count .col-md-4 .notice_board .attach_link:first").find("a").trigger("click");
                    $(".notice_content_wrap").mCustomScrollbar({
                        theme: "dark-3",
                        autoHideScrollbar: true
                    });
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function getNewNoteView() {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/PinUpBoard/getNewNoteView/",
        dataType: "html",
        //data: JSON.stringify({ "id": _groupIdArr.join() }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#noteDiv').html(result);
                    $("#hdn_visibilty").val($("#tabList").find(".active").find("a").html());
                    $('#ddlVisibilty li[data-value="' + $("#tabList").find(".active").find("a").html() + '"]').addClass("selected");
                    if ($("#tabList").find(".active").find("a").html() == "Group") {
                        showGroupList(true);
                    }
                    initMadSelect($(".mad-select"), false);
                    initMultiSelectGroup();
                    //$('#ddlVisibilty li[data-value="' + $("#tabList").find(".active").find("a").html() + '"]').trigger('click');
                    $('#newNoteModelBox').modal('show');
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function onNoteSucess(response) {
    $("#divLoader").hide();
    $('#newNoteModelBox').modal('hide');
    if (!isSessionOutResponse(response)) {
        if (response.EnableError) {
            showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
        } else if (response.EnableSuccess) {
            var callbackFunc = function () {
                $("#divLoader").show();
                window.location.reload();
            }
            showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
        }

    }
}

function onNoteBegin() {
    var scriptPat = /[<>]/g;
    if ($("#txt_descr").val().trim() == "") {
        showAlertBox("Information Alert Message", "Please enter note.", gInfo.alertMsgType.information);
        $("#txt_descr").focus();
        return false;

    }
    else if (scriptPat.test($("#txt_descr").val().trim())) {
        showAlertBox("Information Alert Message", "<,> characters are not allowed.", gInfo.alertMsgType.information);
        $("#txt_descr").val('');
        return false;

    }
    else if ($("#hdn_visibilty").val() == "Group" && _groupIdArr.length == 0) {
        showAlertBox("Information Alert Message", "Please select group.", gInfo.alertMsgType.information);
        $('#ddlGroup_note').focus();
        return false;
    }
    else {
        $("#divLoader").show();
        return true;
    }
}

function onNoteFailure() {
    $("#divLoader").hide();
    alert('Some problem occured!')
}

function showGroupList(opt) {
    if (opt == true) {
        $("#divGroup").show();
    }
    else {
        $("#divGroup").hide();

    }
}

function initMultiSelectGroup() {
    $('#ddlGroup_note').multiselect({
        disableIfEmpty: true,
        buttonWidth: '200px',
        //enableFiltering: true,
        nonSelectedText: 'Select Group',
        includeSelectAllOption: true,
        selectAllJustVisible: false,
        enableCaseInsensitiveFiltering: false,
        maxHeight: 600,
        dropUp: true,
        allSelectedText: 'All groups are selected',
        onChange: function (option, checked, select) {
            if (checked) {
                _groupIdArr.push($(option).val());

            } else {
                var item = $.inArray($(option).val(), _groupIdArr);
                _groupIdArr.splice($.inArray($(option).val(), _groupIdArr), 1);
            }
            $("#hdn_groupId").val(_groupIdArr.join());
        },
        onSelectAll: function () {
            _groupIdArr = [];
        },
        onDeselectAll: function () {
            _groupIdArr = [];
        }

    });
}

function editNote(_id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/PinUpBoard/editNote/",
        dataType: "html",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#noteDiv').html(result);
                    initMadSelect($(".mad-select"), false);
                    initMultiSelectGroup();
                    $('#newNoteModelBox').modal('show');
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function deleteNoteConfirmation(_id) {
    var message = "Are you sure, do you want to remove this note";
    $('#message').html(message);
    $('#removeConfirmationModalBox').modal('show');
    $('#submitAlert').attr("onclick", "deleteNote('" + _id + "')");
}

function deleteNote(_id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/PinUpBoard/deleteNote/",
        dataType: "json",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (result) {
            $("#divLoader").hide();
            if (!isSessionOutResponse(result)) {
                if (result) {
                    var callbackFunc = function () {
                        $("#divLoader").show();
                        window.location.reload();
                    }
                    showAlertBox("Success Alert Message", result.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function getAttachment(_id, _divId) {
    if (_divId) {
        _divId = "#" + _divId;
        $('#noticeListDiv .column-count-item [id^="div_"]').removeClass("active-note");
        $(_divId).addClass("active-note");
    }
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/PinUpBoard/getAttachment/",
        dataType: "html",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {

                    $('#attachmentDiv').html(result);
                    $(".side_over").mCustomScrollbar({
                        theme: "dark-3",
                        autoHideScrollbar: true
                    });
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function triggerFileUpload() {// for trigger file upload control
    $('#_HttpPostedNoteFile').trigger('click');
}

function UploadNoteFile(_id) {
    if (window.FormData !== undefined) {
        var fileUpload = $("#_HttpPostedNoteFile").get(0);
        var files = fileUpload.files;
        if (files.length > 5) {
            alert("You can only upload a maximum of 5 files");
            return false;
        }
        // Create FormData object  
        var fileData = new FormData();
        //fileData.append(files[0].name, files[0]);
        for (var i = 0; i < files.length; i++) {
            if (isValidExtention(files[i])) {
                fileData.append(files[i].name, files[i]);
            }
            else {
                return false;
            }
        }
        fileData.append("id", _id);
        // Adding one more key to FormData object  
        $("#divLoader").show();
        $.ajax({
            url: '/Student/PinUpBoard/UploadNoteFile',
            type: "POST",
            contentType: false, // Not to set any content header  
            processData: false, // Not to process data  
            data: fileData,
            success: function (result) {
                if (!isSessionOutResponse(result)) {
                    if (result != "") {
                        //$("#attachmentDiv").empty().html(result);
                        getAttachment(_id);
                        $("#divLoader").hide();
                    }
                    $("#divLoader").hide();
                }
            },
            error: function (err) {
                showAlertBox(err.status, err.msg, gInfo.alertMsgType.failure)
                $("#divLoader").hide();
            }
        });
    }
    else {
        showAlertBox("Browser Functionality Alert Message", "FormData is not supported in this current version of browser.Please Upgrade newer version", gInfo.alertMsgType.failure)
    }
}

function editFile(_fileUploadId) {
    _fileUploadId = "#" + _fileUploadId;
    $(_fileUploadId).trigger('click');
}

function UploadEditNoteFile(_id, _fileUploadId, _pinupBoardId) {
    if (window.FormData !== undefined) {
        _fileUploadId = "#" + _fileUploadId;
        var fileUpload = $(_fileUploadId).get(0);
        var _file = fileUpload.files[0];
        // Create FormData object  
        var fileData = new FormData();
        if (isValidExtention(_file)) {
            fileData.append(_file.name, _file);
        }
        else {
            return false;
        }
        fileData.append("id", _id);
        // Adding one more key to FormData object  
        $("#divLoader").show();
        $.ajax({
            url: '/Student/PinUpBoard/UploadEditNoteFile',
            type: "POST",
            contentType: false, // Not to set any content header  
            processData: false, // Not to process data  
            data: fileData,
            success: function (result) {
                $("#divLoader").hide();
                if (!isSessionOutResponse(result)) {
                    if (result != "") {
                        getAttachment(_pinupBoardId);
                        $("#divLoader").hide();
                    }
                }
            },
            error: function (err) {
                showAlertBox(err.status, err.msg, gInfo.alertMsgType.failure)
                $("#divLoader").hide();
            }
        });
    }
    else {
        showAlertBox("Browser Functionality Alert Message", "FormData is not supported in this current version of browser.Please Upgrade newer version", gInfo.alertMsgType.failure)
    }
}

function deleteFileConfirmation(_id, _fileUrl, _pinupbardId, _fileName) {
    var message = "Are you sure, do you want to remove the file <strong>" + _fileName + "</strong>";
    $('#message').html(message);
    $('#removeConfirmationModalBox').modal('show');
    $('#submitAlert').attr("onclick", "deleteFile('" + _id + "','" + _fileUrl + "','" + _pinupbardId + "')");
}

function deleteFile(_id, _fileUrl, _pinupbardId) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/PinUpBoard/deleteFile/",
        dataType: "json",
        data: JSON.stringify({ "id": _id, "fileUrl": _fileUrl }),
        async: true,
        success: function (result) {
            $("#divLoader").hide();
            if (!isSessionOutResponse(result)) {
                if (result) {
                    var callbackFunc = function () {
                        $("#divLoader").show();
                        getAttachment(_pinupbardId, null)
                    }
                    showAlertBox("Success Alert Message", result.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}