﻿var arrayStudentAssignments = [];

function assignmentStudentGrid(id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/Assignment/StudentAssignmentGrid/" + id,
        dataType: "html",
        data: "",
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#gridResult').empty().html(result);
                  //  $('#count').text($('#gridRowCount').val());
                    bindstudentAssignment(id);
                   // initAutoCompleteStudentAssignment();
                   // $("#divLoader").hide();
                }
                else
                {
                    $("#divLoader").hide();
                }
              //  $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred. Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
function bindstudentAssignment(id) {
    var dataTable = $('#studentAssignmentGrid').DataTable({
        "responsive": true,
        "paging": true,
        "ordering": true,
        //"info": false,
        "oLanguage": {
            "sEmptyTable": "No Record(s) Found."
        },
        // "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "processing": false,
        "serverSide": true,
        "searching": true,
        "scroll": true,
        "ajax": {
            url: "/Student/Assignment/BindStudentAssignmentDetails?id=" + id, // json datasource
            type: "Post",  // method  , by default get
            error: function () {  // error handling
                jQuery("#tbAdmin").append('<tbody class="business-user-grid-error"><tr><th colspan="8">No data found in the server</th></tr></tbody>');
            }
        },
        "columnDefs": [
             {
                 "targets": 0,
                 "sClass": "mx-w-300",
             },
              {
                  "targets": 1,
                  "sClass": "width_130",
              },
               {
                   "targets": 2,
                   "sClass": "width_130",
               },
                {
                    "targets": 3,
                    "sClass": "t-min-w_205",
                },
            {
                "targets": 4,
                "sClass": "t-min-w_115",
            },
            {
                "targets": 5,
                "orderable": false,
                "sClass": "dataTableCenter",
            },

        {
            "targets": 6,
            "orderable": false,
            "sClass": "dataTableCenter",
        },

        {
            "targets": 7,
            "orderable": false,
            "sClass": "dataTableCenter",
        }
        ],

        "initComplete": function (settings, json) {
            $("#divLoader").hide();
        },
        "fnDrawCallback": function () {
            initMadSelect($('.ddlAction').parent());

            if ($('#studentAssignmentGrid tbody tr td').hasClass('dataTables_empty')) {
                $('#count').empty().text(0);
            }
            else {
                $('#count').empty().text($('#studentAssignmentGrid tbody tr').length);
            }
        }
    });
    $("#autocompleteCreateAssignment").keyup(function () {
        dataTable.search($(this).val()).draw();
    });
}

function initMadSelect(containerEle, putDisable) {
    containerEle.each(function () {
        var $input = $(this).find("input"),
            $ul = $(this).find("> ul"),
            $ulDrop = $ul.clone().addClass("mad-select-drop");
        $(this)
          .append('<i class="fa fa-chevron-down" aria-hidden="true" style="cursor:pointer"></i>', $ulDrop)
          .on({
              hover: function () { madSelectHover ^= 1; },
              click: function () {
                  if (!putDisable)
                      $ulDrop.toggleClass("show");
              }
          });
        // PRESELECT
        $ul.add($ulDrop).find("li[data-value='" + $input.val() + "']").addClass("selected");
        // MAKE SELECTED
        $ulDrop.on("click", "li", function (evt) {
            evt.stopPropagation();
            $input.val($(this).data("value")); // Update hidden input value
            $ul.find("li").eq($(this).index()).add(this).addClass("selected")
              .siblings("li").removeClass("selected");
        });
        // UPDATE LIST SCROLL POSITION
        if (!putDisable) {
            $ul.on("click", function () {
                var liTop = $ulDrop.find("li.selected").position().top;
                $ulDrop.scrollTop(liTop + $ulDrop[0].scrollTop);
            });
        }
    });
}
////////////////Searching of Assignments/////////////////////////////////////////////////
//var _serachedAssignment = '';
//var _searchedAssignmentName = '';

//function initAutoCompleteStudentAssignment() {
//    $('#autocompleteStudentAssignment').autocomplete({
//        lookup: arrayStudentAssignments,
//        onSelect: function (suggestion) {
//            _serachedAssignment = suggestion.data;
//            _searchedAssignmentName = suggestion.value;
//        }
//    });
//}

//function searchAssignmentStudent(e) {
//    if (e.keyCode == 13) {
//        //e.preventDefault();
//        // detect the enter key
//        getsearchStudentAssignment();
//        return false;
//    }
//}

//function getsearchStudentAssignment() {
//    if ($("#autocompleteStudentAssignment").val().trim() != _searchedAssignmentName) {
//        _searchedAssignmentName = $("#autocompleteStudentAssignment").val().trim();
//    }
//    if ($("#autocompleteStudentAssignment").val().trim() == "") {
//        _serachedCreateAssignment = "";
//    }  
//    if ($("#autocompleteStudentAssignment").val().trim() == "") {
//        assignmentStudentGrid(null);
//    }
//    else {
//        $("#divLoader").show();
//        $.ajax({
//            type: "POST",
//            contentType: "application/json; charset=utf-8",
//            url: "/Student/Student/getsearchAssignment/",
//            dataType: "html",
//            data: JSON.stringify({ "groupId": $('#selectedGroupId').val(), "assignmentName": _searchedAssignmentName }),
//            async: true,
//            success: function (result) {
//                if (!isSessionOutResponse(result)) {

//                    $('#gridResult').empty().html(result);
//                    $('#count').text($('#gridRowCount').val());
//                    $("#divLoader").hide();
//                }
//            },
//            error: function (xhr, ajaxOptions, thrownError) {
//                $("#divLoader").hide();
//                showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
//                return false;
//            }
//        });
//    }
//}