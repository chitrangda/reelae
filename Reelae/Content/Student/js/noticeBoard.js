﻿$(document).ready(function () {
    getNotices(true);
    window.onbeforeunload = function () {
        SaveEndTime();
    };
})

function SaveStartTime() {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/NoticeBoard/SaveStartTime/",
        data:JSON.stringify({id:arrNoticeIds}),
        dataType: "json",
        async: true,
        success: function (result) {
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}

function SaveEndTime() {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/NoticeBoard/SaveEndTime/",
        dataType: "json",
        async: true,
        success: function (result) {
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}

function SaveDownloadStatus(_noticeBoardId)
{
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/NoticeBoard/SaveDownloadStatus/",
        data:JSON.stringify({"id":_noticeBoardId}),
        dataType: "json",
        async: true,
        success: function (result) {
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}

function getNotices(_isFirst) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/NoticeBoard/getNotices/",
        dataType: "html",
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#divNoticeList').html(result);
                    if (_isFirst) {
                        SaveStartTime();
                    }
                    $("#divLoader").hide();

                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function SaveComment(_noticeBoardId, e, _this) {
    if (e.keyCode == 13) {
        e.preventDefault();
        var scriptPat = /[<>]/g;
        if ($(_this).val().trim() == "") {
            showAlertBox("Information Alert Message", "Please enter comment.", gInfo.alertMsgType.information);
            $(_this).focus();
            return false;

        }
        else if (scriptPat.test($(_this).val().trim())) {
            showAlertBox("Information Alert Message", "<,> characters are not allowed.", gInfo.alertMsgType.information);
            $(_this).val('');
            return false;

        }
        $("#divLoader").show();
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Student/NoticeBoard/SaveComment/",
            dataType: "json",
            data: JSON.stringify({ "id": _noticeBoardId, "_comment": $(_this).val().trim() }),
            async: true,
            success: function (response) {
                if (!isSessionOutResponse(response)) {
                    if (response) {
                        $("#divLoader").hide();
                        if (response.EnableError) {
                            showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
                        } else if (response.EnableSuccess) {
                            var callbackFunc = function () {
                                $("#divLoader").show();
                                getNotices(false);
                            }
                            showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
                        }
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#divLoader").hide();
                showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
                return false;
            }
        });
    }
}

function getMoreComments(_noticeBoardId) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/NoticeBoard/getMoreComments/",
        dataType: "html",
        data: JSON.stringify({ "id": _noticeBoardId }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#noticeCommentdiv').html(result);
                    $(".modal-body").mCustomScrollbar({
                        theme: "dark-3",
                        autoHideScrollbar: true
                    });
                    $('#comment').modal('show');
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function EditComment(_id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/NoticeBoard/EditComment/",
        dataType: "html",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#editComment').html(result);
                    $(".modal-body").mCustomScrollbar({
                        theme: "dark-3",
                        autoHideScrollbar: true
                    });
                    $('#editCommentModelBox').modal('show');
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function RemoveCommentConfirmation(_id) {
    var message = "Are you sure, do you want to remove the comment?";
    $('#message').html(message);
    $('#removeConfirmationModalBox').modal('show');
    $('#submitAlert').attr("onclick", "RemoveComment('" + _id + "')");
}

function RemoveComment(_id) {
    $('#removeConfirmationModalBox').modal('hide');
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/NoticeBoard/RemoveComment/",
        dataType: "json",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (response) {
            $("#divLoader").hide();
            if (!isSessionOutResponse(response)) {
                if (response.EnableError) {
                    showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
                } else if (response.EnableSuccess) {
                    var callbackFunc = function () {
                        $("#divLoader").show();
                        window.location.reload();
                    }
                    showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function oneditSucess(response) {
    $("#divLoader").hide();
    $('#editCommentModelBox').modal('hide');
    if (!isSessionOutResponse(response)) {
        if (response.EnableError) {
            showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
        } else if (response.EnableSuccess) {
            var callbackFunc = function () {
                window.location.reload();
            }
            showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
        }

    }
}

function oneditBegin() {
    var scriptPat = /[<>]/g;
    if ($("#txt_content").val().trim() == "") {
        showAlertBox("Information Alert Message", "Please enter comment.", gInfo.alertMsgType.information);
        $("#txt_title").focus();
        return false;
    }
    else if (scriptPat.test($("#txt_content").val().trim())) {
        showAlertBox("Information Alert Message", "<,> characters are not allowed.", gInfo.alertMsgType.information);
        $("#txt_title").val('');
        return false;

    }
    else {
        $("#divLoader").show();
        return true;
    }
}

function setActive(_this) {
    if ($(_this).hasClass("active")) {
        $(_this).removeClass("active");

    }
    else {
        $(_this).addClass("active");
    }
}