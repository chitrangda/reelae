﻿var resourcesData;
var milestonedata = [];
var _pickerStartDate;
var _pickerEndDate;
var MilesStoneStatusData = [
    {
        text: "New",
        id: 1,
        color: "#ea8436"
    }, {
        text: "In Progress",
        id: 2,
        color: "#6C71EF"
    },
    {
        complete: "Done",
        id: 3,
        color: "#14c673"
    }
];


$(document).ready(function () {
    _pickerStartDate = moment().startOf('isoWeek');
    _pickerEndDate = moment().endOf('isoWeek');
    initMilestoneDateRangePicker($('#dateFilterEle'), OnSelectFilerDate);
    getStudents(moment().startOf('isoWeek'), moment().endOf('isoWeek'), $("#selAssignment").val());


});

function OnSelectFilerDate(startDate, endDate) {
    _pickerStartDate = startDate._d;
    _pickerEndDate = endDate._d;
    getStudents(startDate._d, endDate._d, $("#selAssignment").val());

}

function onDateClear() {
    getStudents(moment().startOf('isoWeek'), moment().endOf('isoWeek'), $("#selAssignment").val());

}

function getStudents(_startDate, _endDate, _id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/MileStones/getStudents/",
        dataType: "json",
        data: JSON.stringify({ "startDate": _startDate, "endDate": _endDate, "assignmentId": _id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    if (result.length > 0) {
                        resourcesData = result;
                        getData(_startDate, _endDate, _id);
                    }
                    else {
                        $("#divLoader").hide();
                        $("#scheduler").html("<div class='no-data'>No data found</div>")
                    }

                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function getData(_startDate, _endDate, _id) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/MileStones/getData/",
        dataType: "json",
        data: JSON.stringify({ "startDate": _startDate, "endDate": _endDate, "assignmentId": _id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    milestonedata = [];
                    for (var i = 0; i < result.length; i++) {
                        var _status = ''
                        if (result[i].MilesStoneStatus == 1) {
                            _status = "New";
                        }
                        else if (result[i].MilesStoneStatus == 2) {
                            _status = "In Progress";
                        }
                        else {
                            _status = "Done";
                        }
                        milestonedata.push({
                            studentId: result[i].studentId,
                            text: result[i].text,
                            startDate: new Date(result[i].startDateYear, result[i].startDateMonth, result[i].startDateDay, result[i].startDateHour, result[i].startDateMin, 0),
                            endDate: new Date(result[i].endDateYear, result[i].endDateMonth, result[i].endDateDay, result[i].endDateHour, result[i].endDateMin),
                            pic: result[i].pic,
                            milestoneId: result[i].milestoneId,
                            startDateInfo: result[i].startDateInfo,
                            endDateInfo: result[i].endDateInfo,
                            TaskDescription: result[i].TaskDescription,
                            MilesStoneStatus: result[i].MilesStoneStatus,
                            Status: _status

                        })
                    }
                    $("#scheduler").dxScheduler({
                        dataSource: milestonedata,
                        views: [],
                        currentView: "timelineWeek",
                        currentDate: new Date(_pickerStartDate),
                        firstDayOfWeek: 1,
                        startDayHour: 8,
                        endDayHour: 22,
                        cellDuration: 60,
                        groups: ["studentId"],
                        resources: [{
                            fieldExpr: "studentId",
                            allowMultiple: true,
                            dataSource: resourcesData,
                            label: "Name",
                        }, {
                            fieldExpr: "MilesStoneStatus",
                            allowMultiple: false,
                            dataSource: MilesStoneStatusData,
                            label: "MilesStone Status",
                            useColorAsDefault: true

                        }],
                        height: "93.9vh",
                        noDataText: '<h1>No Data!!!</h1>',
                        editing: {
                            allowAdding: false,
                            allowDeleting: false,
                            allowDragging: false,
                            allowResizing: false,
                            allowUpdating: false
                        },
                        dataCellTemplate: function (itemData, itemIndex, itemElement) {
                            //itemElement.append("<input type='hidden' value='" + itemData.startDate + "' id='hdn_" + itemIndex + "'>");
                            //itemElement.addClass("done");
                        },
                        appointmentTooltipTemplate: function (appointmentData, contentElement) {
                            return $("<div class='msPopup'><div class='left-col col-xs-7 pad_lr-0'>" + appointmentData.text + "</div><div class='right-col col-xs-5 pad_lr-0 text-right'><i class='material-icons delete_icn' onclick='RemoveMilestoneConfirmation(" + appointmentData.milestoneId + ",\"" + appointmentData.text + "\")' title='Remove Milestone'>delete</i><i class='material-icons edit_icn' title='Edit Milestone' onclick='editMilestone(" + appointmentData.milestoneId + ")'>mode_edit</i></div><div class='clearfix'></div><small style='font-weight:normal'>" + appointmentData.startDateInfo + "-" + appointmentData.endDateInfo + "</small><div class='clearfix'></div><span style='font-size:13px'>" + appointmentData.TaskDescription + "</span></div>");
                        },
                        resourceCellTemplate: function (data, index, element) {
                            element.append("<span style='text-align:left;display:inline-block;width:100%;padding-top: 15px; padding-left:10px;padding-right:10px;'><img src='" + data.data.pic + "' class='img-circle' style='float:left;' height='50' width='50'><span class='milestone_name'><span>" + data.data.name + "<div class='clearfix'></div><small>5h tracked</small></span><div class='clearfix'></div><div class='progress'><div class='progress-bar' role='progressbar' aria-valuenow='70' aria-valuemin='0' aria-valuemax='100' style='width:70%'></div></div></span></span>");
                        },
                        appointmentTemplate: function (data) {
                            //return $("<img src='" + data.pic + "' class='img-circle' height='50' width='50'><span>" + data.text + "</span>");
                            return $("<div class='msInfo'><div class='msuserpic'><img src='" + data.pic + "' class='img-circle' width='50' height='50'></div><div class='msuserInfo'><div class='msName'>" + data.text + "</div><div class='ms-date'>" + data.startDateInfo + " - " + data.endDateInfo + "</div></div><div style='float:right;padding: 10px 10px;'>" + data.Status + "</div></div>")
                        },
                        onAppointmentClick: null,
                        onAppointmentDblClick: function (e) {
                            e.cancel = true;
                            if (e.appointmentData.studentId == _CurstudentId) {
                                getEditMilestoneStatusView(e.appointmentData.milestoneId);
                            }
                        },
                        onCellClick: function (e) {
                            debugger;
                            e.cancel = true;
                            getCellMilestones(e.cellElement[0].cellIndex, e.cellData.startDate, e.cellData.groups.studentId,e.cellElement);
                            //if (_count > 0) {
                              e.cellElement.addClass("active");
                            //}
                        }
                    });
                    DevExpress.viz.currentTheme("generic.light");
                    $(".dx-scheduler-navigator").hide();
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });

}

function getNewMilestoneView(_count) {
    if (_count > 0) {
        $("#divLoader").show();
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Student/MileStones/getNewMilestoneView/",
            dataType: "html",
            data: JSON.stringify({ "id": $("#selAssignment").val() }),
            async: true,
            success: function (result) {
                if (!isSessionOutResponse(result)) {
                    if (result) {
                        $('#noteDiv').html(result);
                        initDateTimePicker('#txt_sdate', moment(), moment($("#AssignmentDueDate").val()), initDueDate);
                        initDueDate(moment(), moment());
                        initMadSelect($("#MemberSelect"), false);
                        $('#newMilestoneModelBox').modal('show');
                        $("#divLoader").hide();
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#divLoader").hide();
                showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
                return false;
            }
        });
    }
    else {
        showAlertBox("Information Alert Message", "No assignment found", gInfo.alertMsgType.information);

    }
}

function initDueDate(_date1, _date2) {
    initDateTimePicker('#txt_edate', _date1, _date2);
}

function onMilestoneSucess(response) {
    $("#divLoader").hide();
    $('#newMilestoneModelBox').modal('hide');
    if (!isSessionOutResponse(response)) {
        if (response.EnableError) {
            showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
        } else if (response.EnableSuccess) {
            var callbackFunc = function () {
                $("#divLoader").show();
                window.location.href = "/Student/MileStones/Index/" + $("#selAssignment").val()
            }
            showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
        }

    }
}

function onMilestoneBegin() {
    var scriptPat = /[<>]/g;
    if ($("#txt_name").val().trim() == "") {
        showAlertBox("Information Alert Message", "Please enter milestone name.", gInfo.alertMsgType.information);
        $("#txt_descr").focus();
        return false;

    }
    else if (scriptPat.test($("#txt_name").val().trim())) {
        showAlertBox("Information Alert Message", "<,> characters are not allowed.", gInfo.alertMsgType.information);
        $("#txt_descr").val('');
        return false;

    }
    else if ($("#txt_descr").val().trim() == "") {
        showAlertBox("Information Alert Message", "Please enter task description.", gInfo.alertMsgType.information);
        $("#txt_descr").focus();
        return false;

    }
    else if (scriptPat.test($("#txt_descr").val().trim())) {
        showAlertBox("Information Alert Message", "<,> characters are not allowed.", gInfo.alertMsgType.information);
        $("#txt_descr").val('');
        return false;

    }
    else {
        $("#divLoader").show();
        return true;
    }
}

function onMilestoneFailure() {
    $("#divLoader").hide();
    alert('Some problem occured!')
}

function milestone(_id) {
    getStudents(_pickerStartDate, _pickerEndDate, _id);
}


function editMilestone(_id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/MileStones/geteditMilestoneView/",
        dataType: "html",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#noteDiv').html(result);
                    initDateTimePicker('#txt_sdate', moment(), moment($("#AssignmentDueDate").val()), initDueDate);
                    initDateTimePicker('#txt_edate', moment($("#txt_sdate").val()), moment($("#txt_sdate").val()).add('days', 5));
                    initMadSelect($("#MemberSelect"), false);
                    $('#newMilestoneModelBox').modal('show');
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function RemoveMilestoneConfirmation(_id, _name) {
    var message = "Are you sure, do you want to remove the <strong>" + _name + "</strong>";
    $('#message').html(message);
    $('#removeConfirmationModalBox').modal('show');
    $('#submitAlert').attr("onclick", "deleteMilestone('" + _id + "')");
}

function deleteMilestone(_id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/MileStones/deleteMilestone/",
        dataType: "json",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (response) {
            if (!isSessionOutResponse(response)) {
                if (response) {
                    if (response.EnableError) {
                        showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
                    } else if (response.EnableSuccess) {
                        var callbackFunc = function () {
                            $("#divLoader").show();
                            window.location.href = "/Student/MileStones/Index/" + $("#selAssignment").val()
                        }
                        showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
                    }
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function getEditMilestoneStatusView(_id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/MileStones/getEditMilestoneStatusView/",
        dataType: "html",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#noteDiv').html(result);
                    initMadSelect($("#StatusSelect"), false);
                    $('#milestoneStatusModelBox').modal('show');
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function onSaveStatusSucess(response) {
    $("#divLoader").hide();
    $('#milestoneStatusModelBox').modal('hide');
    if (!isSessionOutResponse(response)) {
        if (response.EnableError) {
            showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
        } else if (response.EnableSuccess) {
            var callbackFunc = function () {
                $("#divLoader").show();
                window.location.href = "/Student/MileStones/Index/" + $("#selAssignment").val()
            }
            showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
        }

    }
}


function initDateTimePicker(_id, _date1, _date2, CallBack) {
    $(_id).daterangepicker({
        "singleDatePicker": true,
        "timePicker": true,
        "locale": {
            "format": "MM/DD/YYYY hh:mm A",
        },
        "minDate": _date1 || moment(),
        "maxDate": _date2 || moment(),
        drops: "up"

    }, function (start, end, label) {
        console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");

    }).on('apply.daterangepicker', function (ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        if (CallBack && $.isFunction(CallBack)) {
            CallBack(picker.startDate, moment(picker.endDate).add('days', 5));
        }
    });
}


function initMilestoneDateRangePicker(ContainerEle, callBack, cancelCallBack) {
    if ("undefined" != typeof $.fn.daterangepicker && ContainerEle.length > 0) {
        console.log("init_daterangepicker_range");
        var a = function (a, b, c) {
            console.log(a.toISOString(), b.toISOString(), c), ContainerEle.find('span').html(a.format("MMMM D, YYYY") + " - " + b.format("MMMM D, YYYY"))
        },
            b = {
                startDate: moment().startOf('isoWeek'),
                endDate: moment().endOf('isoWeek'),
                minDate: "01/01/2001",
                maxDate: "12/31/2020",
                dateLimit: {
                    days: 7
                },
                showDropdowns: !0,
                showWeekNumbers: !0,
                timePicker: !1,
                timePickerIncrement: 1,
                timePicker12Hour: !0,
                ranges: {
                    "Last 7 Days": [moment().subtract(6, "days"), moment()]
                },
                opens: "right",
                buttonClasses: ["btn btn-default"],
                applyClass: "btn-small btn-primary",
                cancelClass: "btn-small",
                format: "MM/DD/YYYY",
                separator: " to ",
                locale: {
                    applyLabel: "Submit",
                    cancelLabel: "Clear",
                    fromLabel: "From",
                    toLabel: "To",
                    customRangeLabel: "Custom",
                    daysOfWeek: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
                    monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                    firstDay: 1,
                    format: "MM/DD/YYYY"
                }
            };
        ContainerEle.find('span').html(moment().startOf('isoWeek').format("MMMM D, YYYY") + " - " + moment().endOf('isoWeek').format("MMMM D, YYYY")),
        ContainerEle.daterangepicker(b, a),
        ContainerEle.on("show.daterangepicker", function () {
            console.log("show event fired")
        }),
        ContainerEle.on("hide.daterangepicker", function () {
            console.log("hide event fired")
        }),
        ContainerEle.on("apply.daterangepicker", function (a, b) {
            if (!b) {
                b = ContainerEle.data('daterangepicker')
                ContainerEle.find('span').html(b.startDate.format("MMMM D, YYYY") + " - " + b.endDate.format("MMMM D, YYYY"))
            }
            else {
                console.log("apply event fired, start/end dates are " + b.startDate.format("MMMM D, YYYY") + " to " + b.endDate.format("MMMM D, YYYY"))
                //customize
                ContainerEle.data('assignSD', b.startDate.format("MM/DD/YYYY"));
                ContainerEle.data('assignED', b.endDate.format("MM/DD/YYYY"));
                if (callBack && $.isFunction(callBack)) {
                    callBack(b.startDate, b.endDate);
                }
            }

        }), ContainerEle.on("cancel.daterangepicker", function (a, b) {
            console.log("cancel event fired");
            ContainerEle.data('assignSD', moment().startOf('isoWeek').format("MMMM D, YYYY"));
            ContainerEle.data('assignED', moment().endOf('isoWeek').format("MMMM D, YYYY"));
            if (cancelCallBack && $.isFunction(cancelCallBack)) {
                cancelCallBack();
            }
        }), $("#options1").click(function () {
            ContainerEle.data("daterangepicker").setOptions(b, a)
        }), $("#options2").click(function () {
            ContainerEle.data("daterangepicker").setOptions(optionSet2, a)
        }), $("#destroy").click(function () {
            ContainerEle.data("daterangepicker").remove()
        })
    }

}


function getCellMilestones(_index, _startDate, _studentId, cellElement) {
    var _count = 0;
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/MileStones/getCellMilestones/",
        dataType: "html",
        data: JSON.stringify({ "date": _startDate, "studentId": _studentId }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $("#markDoneMilestone").html(result);
                    _count = $("#hdn_count").val();
                    if (_count > 1) {
                        $("input.flat").iCheck({
                            checkboxClass: "icheckbox_flat-green",
                            radioClass: "iradio_flat-green"
                        });
                        $('table input').on('ifChecked', function () {
                            checkState = '',
                            $(this).parent().parent().parent().addClass('selected'),
                            countChecked()
                        }),
                        $('table input').on('ifUnchecked', function () {
                            checkState = '',
                            $(this).parent().parent().parent().removeClass('selected'),
                            countChecked()
                        });
                        $('#markDoneModelBox').modal('show');
                    }
                    if (_count == 1)
                    {
                        cellElement.addClass("done");
                    }
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
    return _count;
}