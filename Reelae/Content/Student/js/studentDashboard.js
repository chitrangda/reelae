﻿/*Student Dashboard Scripts*/

//Ready Function
$(function () {
    init_daterangepicker_rangeCustom($('#dateFilterEle'), OnSelectFilerDate);
    initDashboard();
});
function initDashboard() {
    //set filter date
    $("#dateFilterEle").data('daterangepicker').setStartDate(new Date(gInfo.DashboardObj.FilterStartDate));
    $("#dateFilterEle").data('daterangepicker').setEndDate(new Date(gInfo.DashboardObj.FilterEndDate));
    $("#dateFilterEle").trigger('apply.daterangepicker');

}
function OnSelectFilerDate(startDate, endDate) {
    console.log("OnSelect: " + startDate.format('MM/DD/YYYY') + ":" + endDate.format('MM/DD/YYYY'));
    GetStudentsActiveAssignments(startDate.format('MM/DD/YYYY'), endDate.format('MM/DD/YYYY'));
}
function GetStudentsActiveAssignments(filterSD, filterED) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/Student/GetStudentsActiveAssignments/",
        dataType: "html",
        data: JSON.stringify({ "FilterSD": filterSD, "FilterED": filterED }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#divActiveAssignmentsContainer').html(result);
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });

}
/*End Ready Function*/