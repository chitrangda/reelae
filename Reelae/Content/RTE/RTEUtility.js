﻿var GlobalRTEObject = function (RTEConfigObject) {
    if (!RTEConfigObject) {
        return null;
    }
    /* set value to variables*/
    /*set globals and defaults*/
    var userStatus = { Online: "online", Offline: "offline" };
    //var membersHeadersColors = ["bg-green-w", "bg-blue-w", "bg-megenta-w"];
    //var membersSideNamesCls = ["user2", "user3", "user4"];
    var membersHeadersColors = ["bg-blackCow-w", "bg-battleShipGrey-w", "bg-slateBlue-w", "bg-seaTurtleGreen-w", "bg-antiqueWhite-w", "bg-cooperGreen-w", "bg-darkOrange-w", "bg-grapeFruitRed-w", "bg-plumVelvet-w", "bg-neonPink-w"];
    var membersSideNamesCls = ["user1", "user2", "user3", "user4", "user5", "user6", "user7", "user8", "user9", "user10"];

    /*defaults*/
    //ui jquery elements
    var membersList = RTEConfigObject._jQMembersList || null;
    var editorControls = RTEConfigObject._jQEditorControls || null;
    var editorArea = RTEConfigObject._jQEditorArea || null;
    var editorInputArea = RTEConfigObject._jQEditorInputArea || null;
    var lastUpdationFooterEle = RTEConfigObject._jQLastUpdationFooterEle || null;
    var wordsGoalHeaderEle = RTEConfigObject._jQToalWordsGoalHeaderEle || null;
    //
    var selUserInfo = { userToken: null, userName: null, userEmailId: null };
    var curUserInfo = {
        userToken: RTEConfigObject._userToken || null, userName: RTEConfigObject._userName || null, userEmailId: RTEConfigObject._userEmailId || null, userShortName: RTEConfigObject._userShortName || null, userAssignmentData: "", lastUkeywordWiseDatapdatedTime: "", usedKeywordsCount: 0, userAssignmentDataHtml: "", wordsCount: 0, membersKeywordWiseCount: {}
    };
    var assignmentToken = RTEConfigObject._assignmentToken || null;
    var isLeader = RTEConfigObject._isLeader || null;
    var isGroupAssignment = RTEConfigObject._isGroupAssignment || null;
    var groupToken = RTEConfigObject._groupToken || null;
    var keywordData = RTEConfigObject._keywordData.trim().split(',') || [];
    //assignment object
    var assignmentHub = $.connection.assignmentHub || null;
    //local variables
    var totalAssignmentMembersList = RTEConfigObject._assignmentMembersList.trim().split(',') || [];
    var membersKeywordCount = RTEConfigObject._userUsedKeywords.trim().split(',') || [];
    var membersWordCount = RTEConfigObject._userWordCount.trim().split(',') || [];
    var memberKeywordUsageData = JSON.parse(RTEConfigObject._userKeywordUsageData) || [];
    
    //changes
    var membersKeywordWiseCount = [];

    var activeMembers = [curUserInfo.userEmailId];
    var isEditorAreaActive = false;
    var idContentModified = false;
    //buffer queue for Data
    var cursorStart = 0;
    var cursorEnd = 0;
    var contentSendInterval = null;
    var contentBufferQueueForNotify = [];
    var historyStack = [];
    var LastUpdateBy = RTEConfigObject._lastUpdatedBy;
    var LastUpdatedOn = RTEConfigObject._lastUpdatedOn;
    var TotalWordsGoal = RTEConfigObject._totalWordsGoal;
    var perWordsGoal = RTEConfigObject._perWordsGoal;

    /* end */
    function OnInit() {
        updateUpdatedTimeFooter();
    }



    function LocalContentObj(_cursorStart, _cursorEnd, _content, _isSaved, _timestamp, _finalContent, _htmlContent) {
        this.CursorStart = _cursorStart;
        this.CursorEnd = _cursorEnd;
        this.Content = _content;
        this.TimeStamp = _timestamp;
        this.FinalContent = _finalContent;
        this.FilnalHtmlContent = _htmlContent;
        this.IsSaved = _isSaved;
    }

    function AssignmentObject(_assignToken, _groupToken, _keywords, _members, _activeMembers, _isLeader, _isGroupAssignment) {
        this.assignmentToken = _assignToken || null;
        this.groupToken = _groupToken || '';
        this.keywords = _keywords || [];
        this.members = _members || [];
        this.activeMembers = _activeMembers || [];
        this.isLeader = _isLeader || false;
        this.isGroupAssignment = _isGroupAssignment || true;
        //call init
        OnInit();
    }
    function MembersObject() {
        this.Name = "";
        this.PicUrl = "";
        this.ShortName = "";
        this.Content = "";
        this.LastUpdated = "";
        this.UsedKeywordsCount = "";
    }
    function SendRTEData(_curUserInfo, _content, _assignmentToken, _selectionStart, _selectionEnd, _updateTime, _currentGroupId) {
        this.from = _curUserInfo.userToken;
        this.fromEmailId = _curUserInfo.userEmailId;
        this.content = _content;
        this.assignmentToken = _assignmentToken;
        this.selectionStart = _selectionStart;
        this.selectionEnd = _selectionEnd;
        this.actualContent = _curUserInfo.userAssignmentData;
        this.contentToken = "";
        this.updateTime = _updateTime;
        this.currentGroupId = _currentGroupId;
        this.actualContentHtml = _curUserInfo.userAssignmentDataHtml;
    }

    /*Public Methods*/

    AssignmentObject.prototype.createObjectToSend = function (_curUserInfo, _content, _assignmentToken, _selectionStart, _selectionEnd, _updateTime, _currentGroupId) {
        return new SendRTEData(_curUserInfo, _content, _assignmentToken, _selectionStart, _selectionEnd, _updateTime, _currentGroupId);
    }
    AssignmentObject.prototype.sendAssignmentData = sendAssignmentData;
    AssignmentObject.prototype.OnDataReceived = OnDataReceived;
    AssignmentObject.prototype.setPresenseStatus = setPresenseStatus;
    AssignmentObject.prototype.setOthersPresenseStatus = setOthersPresenseStatus;
    AssignmentObject.prototype.callOnOpenAssignmentWorkSpace = callOnOpenAssignmentWorkSpace;
    /*End Public Methods*/

    /*Private Members*/
    function setPresenseStatus(_userToken, status, lastActiveTime) {
        debugger
        if (_userToken && membersList.length) {
            var curUser = membersList.find('li[uEId="' + _userToken + '"]');
            var that = this;
            if (curUser.length == 0 && status == userStatus.Online) {
                //add user with his data
                var _callbackFunc = function (result) {
                    var colorPos = getBgColorIndexForUser();
                    var eleToAdd = $(result);
                    var uid = eleToAdd.attr('sid');
                    var userSName = eleToAdd.find('[name="hdnUserSN"]').val();
                    var userName = eleToAdd.find('.userName').text();
                    var userPicUrl = eleToAdd.find('[name="hdnUserPicUrl"]').val();

                    var memberListHtml = '<li uEId="' + _userToken + '" uId="' + uid + '" class="' + membersHeadersColors[colorPos] + '"><a title="' + userName + '" href="#-" style="padding:15px !important">' + userSName + '</a></li>';
                    if (userPicUrl) {
                        memberListHtml = '<li uEId="' + _userToken + '" uId="' + uid + '" class="' + membersHeadersColors[colorPos] + '"><a title="' + userName + '" href="#-" style="padding:1px !important"><img src="' + userPicUrl + '" alt=""></a></li>';
                    }
                    $(memberListHtml).insertBefore(membersList.find('li:first'));
                    $(result).insertBefore(editorArea.find('.usereditwrap:first'));
                    editorArea.find('.usereditwrap:first').removeClass('user1').addClass(membersSideNamesCls[colorPos]);
                    //changes_23112017
                    var stuWordsCount = editorArea.find('.usereditwrap:first small').text();
                    editorArea.find('.usereditwrap:first small').html(+stuWordsCount + "/" + perWordsGoal);

                    if (activeMembers.indexOf(_userToken) == -1) {
                        activeMembers.push(_userToken);
                    }
                }
                GetAssignmentWorkSpaceContentOfStudent(_userToken, assignmentToken, _callbackFunc);
            }
            if (curUser.length != 0 && status == userStatus.Offline) {
                //remove user with editor area
                var userId = curUser.attr('uId');
                curUser.remove();
                editorArea.find('.usereditwrap[sid="' + userId + '"]').remove();
                if (activeMembers.indexOf(_userToken) > -1) {
                    activeMembers.splice(activeMembers.indexOf(_userToken), 1);
                }
            }
        }
    }
    function setOthersPresenseStatus(users, status) {
        debugger
        for (var i = 0; i < users.length; i++) {
            if (users[i] == curUserInfo.userEmailId)
                continue;
            var _userToken = users[i];
            var that = this;
            if (_userToken && membersList.length) {
                var curUser = membersList.find('li[uEId="' + _userToken + '"]');
                if (curUser.length == 0 && status == userStatus.Online) {
                    //add user with his data
                    var _callbackFunc = function (result) {
                        var colorPos = getBgColorIndexForUser();
                        var eleToAdd = $(result);
                        var uid = eleToAdd.attr('sid');
                        var userName = eleToAdd.find('.userName').text();
                        var userSName = eleToAdd.find('[name="hdnUserSN"]').val();
                        var userPicUrl = eleToAdd.find('[name="hdnUserPicUrl"]').val();

                        var memberListHtml = '<li uEId="' + _userToken + '" uId="' + uid + '" class="' + membersHeadersColors[colorPos] + '"><a title="' + userName + '" href="#-" style="padding:15px !important">' + userSName + '</a></li>';
                        if (userPicUrl) {
                            memberListHtml = '<li uEId="' + _userToken + '" uId="' + uid + '" class="' + membersHeadersColors[colorPos] + '"><a title="' + userName + '" href="#-" style="padding:1px !important"><img src="' + userPicUrl + '" alt=""></a></li>';
                        }
                        $(memberListHtml).insertBefore(membersList.find('li:first'));
                        $(result).insertBefore(editorArea.find('.usereditwrap:first'));
                        editorArea.find('.usereditwrap:first').removeClass('user2').addClass(membersSideNamesCls[colorPos]);
                        //changes_23112017
                        var stuWordsCount = editorArea.find('.usereditwrap:first small').text();
                        editorArea.find('.usereditwrap:first small').html(+stuWordsCount + "/" + perWordsGoal);
                        if (activeMembers.indexOf(_userToken) == -1) {
                            activeMembers.push(_userToken);
                        }
                    }
                    GetAssignmentWorkSpaceContentOfStudent(_userToken, assignmentToken, _callbackFunc);
                }
                if (curUser.length != 0 && status == userStatus.Offline) {
                    //remove user with editor area
                    var userId = curUser.attr('uId');
                    curUser.remove();
                    editorArea.find('.usereditwrap[sid="' + userId + '"]').remove();
                    if (activeMembers.indexOf(_userToken) > -1) {
                        activeMembers.splice(activeMembers.indexOf(_userToken), 1);
                    }
                }
            }
        }
    }
    function sendAssignmentData(AssignmentObject) {
        if (AssignmentObject instanceof SendRTEData) {
            if (AssignmentObject.content) {
                //var isRecived = false;
                //setDataToAssignmentContainer(AssignmentObject, isRecived);
                assignmentHub.server.sendAssignmentData(JSON.stringify(AssignmentObject));
                updatedLastUpdationFooter(AssignmentObject.from, AssignmentObject.updateTime);
            }
        }
        else {
            throw new Error("Assignment Object is not Valid.");
        }
    }
    function OnDataReceived(serializedObj) {
        if (serializedObj) {
            var parsedMessageObj = null;
            try {
                parsedMessageObj = JSON.parse(serializedObj);
            } catch (e) {
                parsedMessageObj = eval(serializedObj);
            }
            var isReceived = true;
            setDataToAssignmentContainer(parsedMessageObj, isReceived);
            updatedLastUpdationFooter(parsedMessageObj.from, parsedMessageObj.updateTime);
            updateUserKeywordData(parsedMessageObj.fromEmailId, parsedMessageObj.from);
            updateUIWordCount(parsedMessageObj.from);
            //save local track records
        }
        else {
            throw new Error("Invalid Data");
        }
    }

    function updateUserKeywordData(fromEmailId, userToken) {
        var userIndex = totalAssignmentMembersList.indexOf(fromEmailId);
        if (userIndex > -1) {
            var retObject = parseKeywordWiseWholeData(userToken);
            membersKeywordCount[userIndex] = retObject["KeywordCount"];
            membersKeywordWiseCount[userIndex] = retObject["keywordWiseData"];
            membersWordCount[userIndex] = getWordsCount(userToken);
        }
    }
    function setDataToAssignmentContainer(assignmentObject, isreceived) {
        var curUserEditorArea = editorArea.find('.usereditwrap[sid="' + assignmentObject.from + '"]');
        curUserEditorArea.find('[name="lastUpdatedOn"]').val(assignmentObject.updateTime);
        curUserEditorArea.find('.editableCon>p').html(assignmentObject.actualContentHtml);
    }
    function callOnOpenAssignmentWorkSpace() {
        //register self into this assignment group
        assignmentHub.server.onOpenAssignmentWorkSpace(isGroupAssignment, assignmentToken, groupToken);
    }
    function OnEditorDataSend() {
        var eletoSend = contentBufferQueueForNotify.shift();
        if (eletoSend) {
            curUserInfo.userAssignmentData = eletoSend.FinalContent;
            curUserInfo.userAssignmentDataHtml = eletoSend.FilnalHtmlContent;
            curUserInfo.lastUpdatedTime = eletoSend.TimeStamp;
            //curUserInfo.usedKeywordsCount = parseKeywordData(curUserInfo.userToken);
            curUserInfo.usedKeywordsCount = getWordsCount(curUserInfo.userToken);
            var assignmentData = new SendRTEData(curUserInfo, eletoSend.Content, assignmentToken, eletoSend.CursorStart, eletoSend.CursorEnd, eletoSend.TimeStamp, groupToken);
            sendAssignmentData(assignmentData);
            historyStack.push(eletoSend);
        }
    }
    function UpdateWorkSpaceData() {
        var curUserEditorArea = editorArea.find('.usereditwrap[sid="' + curUserInfo.userToken + '"] .editableCon>p:first');
        var content = curUserEditorArea.html();
        var callbackFunc = function () {
            idContentModified = false;
            curUserEditorArea.removeAttr('placeholder');
        }
        if (idContentModified)
            UpdateAssignmentWorkSpaceContent(assignmentToken, curUserInfo.userToken, content, curUserInfo.wordsCount, memberKeywordUsageData, callbackFunc);
    }
    /**/
    //private helper  functions  
    function LoadSelectedEditorArea() {


    }
    function parseKeywordData(stuid) {
        var userKeywordCount = 0;
        if (keywordData || stuid) {
            var curUserEditorArea = editorArea.find('.usereditwrap[sid="' + stuid + '"] .editableCon>p');
            var _content = curUserEditorArea.text();
            for (var i = 0; i < keywordData.length; i++) {
                var curPat = new RegExp(keywordData[i], "ig");
                var matchList = _content.match(curPat);
                userKeywordCount += (matchList) ? matchList.length : 0;
            }
        }
        return userKeywordCount;
    }
    function parseKeywordWiseWholeData(stuid) {
        var userKeywordCount = 0;
        var userKeywordsWiseCount = [];
        if (keywordData || stuid) {
            var curUserEditorArea = editorArea.find('.usereditwrap[sid="' + stuid + '"] .editableCon>p');
            var _content = curUserEditorArea.text();
            for (var i = 0; i < keywordData.length; i++) {
                var curPat = new RegExp(keywordData[i], "ig");
                var matchList = _content.match(curPat);
                userKeywordCount += (matchList) ? matchList.length : 0;
                var curObj = {};
                curObj[keywordData[i]] = (matchList) ? matchList.length : 0;
                userKeywordsWiseCount.push(curObj);
                //if he is the curuser means self then update KeywordWiseUsageData
                if (stuid == curUserInfo.userToken) {
                    var eleIndex = memberKeywordUsageData.findIndex(function (ele) { return ele.KeyWordName == keywordData[i] });
                    if (eleIndex > -1)
                        memberKeywordUsageData[eleIndex]["Count"] = (matchList) ? matchList.length : 0;
                }
            }
        }
        return { "KeywordCount": userKeywordCount, "keywordWiseData": userKeywordsWiseCount };
    }

    function getWordsCount(stuid) {
        var wordsCount = 0;
        if (keywordData || stuid) {
            var curUserEditorArea = editorArea.find('.usereditwrap[sid="' + stuid + '"] .editableCon>p');
            var _content = curUserEditorArea.text();
            var WordsCountPat = /\b(\w|')+\b/gim
            wordsCount = _content.match(WordsCountPat).length;
        }
        return wordsCount;
    }

    function getBgColorIndexForUser() {
        for (var i = 0; i < membersSideNamesCls.length; i++) {
            if (!editorArea.find('.usereditwrap').hasClass(membersSideNamesCls[i]))
                return i;
        }
        return -1;
    }
    function getWorkSpaceHtml(groupKeywordCount, yourKeywordCount) {
        var html = '<div class="workspace"><div class="workLeft"><h3>Workspace</h3></div><div class="workRight"><h3>Words contributed</h3><div class="group">Group<span>' + groupKeywordCount + '</span></div><div class="you">You<span>' + yourKeywordCount + '</span></div></div></div>';
        return html;
    }
    function updateUIWordCount(sid) {
        var ele = editorArea.find('.usereditwrap[sid="' + sid + '"] .userName>small');
        if (ele.length) {
            ele.html(curUserInfo.wordsCount + "/" + perWordsGoal);
            var totalWordCount = 0;
            membersWordCount.map(function (data) {
                totalWordCount += (+data);
            });
            wordsGoalHeaderEle.html(totalWordCount + "/" + TotalWordsGoal);
        }
    }

    function toggleTimeHandlerForSendContent(jQeditorEle) {
        var javascriptEle = jQeditorEle[0];
        if (isEditorAreaActive) {
            javascriptEle.addEventListener("DOMCharacterDataModified", OnEditorDataChangeListener, false);
            //javascriptEle.addEventListener("input", OnEditorInputListener, false);
            contentSendInterval = setInterval(OnEditorDataSend, 1000);
        }
        else {
            javascriptEle.removeEventListener("DOMCharacterDataModified", OnEditorDataChangeListener, false);
            //javascriptEle.removeEventListener("input", OnEditorInputListener, false);
            clearInterval(contentSendInterval);
            contentSendInterval = null;
        }
    }
    function updatedLastUpdationFooter(sid, lastUpdatedTime) {
        LastUpdateBy = membersList.find('li[uid="' + sid + '"]>a[title]').attr('title');
        LastUpdatedOn = lastUpdatedTime;
    }

    //changes_23112017
    function getContentByStudentID(sid) {
        if (sid) {
            var ele = editorArea.find('.usereditwrap[sid="' + userId + '"]');
            
        }
    }

    //Time Interval For LastUpdation Footer Label 
    setInterval(updateUpdatedTimeFooter, 60 * 1000);
    function updateUpdatedTimeFooter() {
        if (LastUpdateBy && LastUpdatedOn) {
            var showTimeBeforeNow = moment(LastUpdatedOn).fromNow();
            lastUpdationFooterEle.find('small').html(LastUpdateBy + "&nbsp;&nbsp;" + showTimeBeforeNow);
            LastUpdateBy = '';
            LastUpdatedOn = '';
        }
    }
    //Time Interval For KeywordCount Change
    setInterval(updateKeywordCount, 1000);
    function updateKeywordCount() {
        if (isEditorAreaActive) {
            editorArea.find('.usereditwrap[sid="' + curUserInfo.userToken + '"] .editableCon>.workspace .you>span').html(curUserInfo.wordsCount);
            var groupCount = 0;
            membersKeywordCount.map(function (data) {
                groupCount += (+data);
            });
            editorArea.find('.usereditwrap[sid="' + curUserInfo.userToken + '"] .editableCon>.workspace .group>span').html(groupCount);
        }
    }

    //
    /*Ui Handelers And Events*/
    //active edit area
    $(document).on('click', '#' + editorArea.attr('id') + ' .editableCon', function (event) {
        //load keyword and groupCount
        var _stuid = $(this).parents('.usereditwrap').first().attr('sid');
        if (_stuid == curUserInfo.userToken) {
            if (!$(this).hasClass('eidtwrap')) {
                $(this).addClass('eidtwrap');

                var groupCount = 0;
                membersWordCount.map(function (data) {
                    groupCount += (+data);
                });
                var KeywordCount = [groupCount, 0];
                curUserInfo.usedKeywordsCount = 0;
                $(this).find('>p').attr('contenteditable', true);
                if ($(this).find('p[placeholder]').length) {
                    $(this).find('>p[contenteditable]').empty();
                }
                else {
                    var wholeUserData = parseKeywordWiseWholeData(_stuid);
                    curUserInfo.wordsCount = getWordsCount(_stuid);
                    curUserInfo.usedKeywordsCount = wholeUserData["KeywordCount"];
                    curUserInfo.keywordWiseData = wholeUserData["keywordWiseData"];
                }
                $(getWorkSpaceHtml(KeywordCount[0], curUserInfo.wordsCount)).insertBefore($(this).find('>p[contenteditable]'));
                isEditorAreaActive = true;
                toggleTimeHandlerForSendContent($(this).find('>p[contenteditable]'));
            }
        }
        else {
            //showAlertBox("Information Alert Message", "You are not allowed to see detailed infomartion of other student's ", gInfo.alertMsgType.information);
        }
    });
    //disable edit area
    $(document).on('click', '#' + editorArea.attr('id') + ' .workspace', function (event) {
        isEditorAreaActive = false;
        toggleTimeHandlerForSendContent($(this).parent().find('>p[contenteditable]'));
        $(this).parent().removeClass('eidtwrap');
        $(this).remove();
        $(this).parent().find('>p[contenteditable]').removeAttr('contenteditable');
        UpdateWorkSpaceData();
        event.stopPropagation();
    });
    //editor content change
    $(document).on('change', '#' + editorArea.attr('id') + ' .editableCon>p[contenteditable]', function (event) {
        //load keyword and groupCount
        var _stuid = $(this).parents('.usereditwrap').first().attr('sid');
        var jQueryEle = $(this);

    });

    function OnEditorDataChangeListener(eve) {
        var curEle = $(eve.currentTarget);
        var updationTimeStamp = (new Date()).valueOf();
        contentBufferQueueForNotify.push(new LocalContentObj(cursorStart, cursorEnd, eve.newValue, false, updationTimeStamp, curEle.text(), curEle.html()));
        idContentModified = true;
        LastUpdateBy = curUserInfo.userToken;
        LastUpdatedOn = updationTimeStamp;
        curUserInfo.usedKeywordsCount = parseKeywordData(curUserInfo.userToken);
        curUserInfo.wordsCount = getWordsCount(curUserInfo.userToken);
        updateUserKeywordData(curUserInfo.userEmailId, curUserInfo.userToken);
        updateUIWordCount(curUserInfo.userToken);
    }

    function OnEditorInputListener(eve) {

    }
    function OnEditorDOMInsertedListener(eve) {


    }
    function OnEditorDOMRemovedListener(eve) {


    }

    var currentRTEObject = new AssignmentObject(assignmentToken, groupToken, keywordData, totalAssignmentMembersList, activeMembers, isLeader, isGroupAssignment);
    return currentRTEObject;
    /* End */
}