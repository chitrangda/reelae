﻿//Ui Ajax Methods
function UpdateAssignmentWorkSpaceContent(assignId, studentId, contentHtml, wordCount, usedKeywordsData, callbackFunction) {
    if (!assignId || !studentId)
        return false;
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/RealTimeAssignment/UpdateAssignmentWorkSpaceContent/",
        dataType: "json",
        data: JSON.stringify({ "assignId": assignId, "studentId": studentId, "contentHtml": contentHtml, "wordCount": wordCount, "keywordWiseData":JSON.stringify(usedKeywordsData) }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if ($.isFunction(callbackFunction)) {
                    callbackFunction();
                    showAlertBox("Success Alert Message", "Assignment Content Saved Successfully.", gInfo.alertMsgType.success);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            showAlertBox("Failure Alert Message", "Some Problem Occurred. Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
function GetAssignmentWorkSpaceContentOfStudent(userEmailId, assignmentId, callbackFunction) {
    if (!userEmailId || !assignmentId)
        return false;
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Student/RealTimeAssignment/GetAssignmentWorkSpaceContentOfStudent/",
        dataType: "html",
        data: JSON.stringify({ "userEmailId": userEmailId, "assignmentId": assignmentId }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if ($.isFunction(callbackFunction)) {
                    callbackFunction(result);
                    //showAlertBox("Success Alert Message", "Message Deleted Successfully.", gInfo.alertMsgType.success);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            showAlertBox("Failure Alert Message", "Some Problem Occurred. Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}