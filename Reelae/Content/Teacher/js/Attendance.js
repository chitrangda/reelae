﻿var d = new Date();
var _curDate = new Date();
var weekday = new Array(7);
weekday[0] = "Sunday";
weekday[1] = "Monday";
weekday[2] = "Tuesday";
weekday[3] = "Wednesday";
weekday[4] = "Thursday";
weekday[5] = "Friday";
weekday[6] = "Saturday";
$(document).ready(function (e) {
    init_attendanceCalendar();
    $('#markOption').on('changed.bs.select', function (event, clickedIndex, newValue, oldValue) {
        if (newValue && clickedIndex == 1) {
            MarkAllStudents('A');
        }
        else if (newValue && clickedIndex == 2) {
            MarkAllStudents('P');
        }
    });
    $('#ddlSubject').on('changed.bs.select', function (event, clickedIndex, newValue, oldValue) {
        if (newValue) {
            getAddStudentView($("#ddlSubject").val());
        }
    });
    $(".ddlatte").on('changed.bs.select', function (event, clickedIndex, newValue, oldValue) {
        //alert($(this).id);
    });

});

function init_attendanceCalendar() {
    if ("undefined" != typeof $.fn.fullCalendar) {
        $('#attendanceCalendar').fullCalendar({
            header: {
                left: "prev,next,today",
                center: "title",
                right: "month,agendaWeek,agendaDay,listMonth"
            },
            dayClick: function (date, jsEvent, view) {
                if (date > _curDate) {
                    alert('Selected date can not be future date.');
                    return false;
                }
                d = date;
                var selectedDate = new Date(date.format());
                $("#datehdn").val(d.format());
                $(".fc-day").removeAttr("style");
                $(this).css('background-color', '#fbc42b');
                $("#lblattDate").html(" Attendance For " + weekday[selectedDate.getDay()] + " " + selectedDate.format("M d, Y"));
                if ($('#_subjectIdhdn').val()) {
                    getAddStudentView($('#_subjectIdhdn').val());
                }
            },
        });
    }


}

function getAddStudentView(_subjectId) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/Attendance/getStudentsView/",
        dataType: "html",
        data: JSON.stringify({ "id": _subjectId, "_date": d }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#tblStudents').html(result);
                    $('#_subjectIdhdn').val(_subjectId);
                    //initMadSelect($(".mad-select"));
                    //$("#datehdn").val(d.format('Y-m-d'));
                    $("#divLoader").hide();
                    $("select[id^='ddlattedance_']").selectpicker({ 'render': true });
                    if ($("#tblStudents tbody tr").length > 3) {
                        $("select[id^='ddlattedance_']").last().selectpicker({ 'dropupAuto': false, 'refresh': true });
                        $("select[id^='ddlattedance_']").eq($("select[id^='ddlattedance_']").length - 2).selectpicker({ 'dropupAuto': false, 'refresh': true });
                    }
                    if ($("#tblStudents tbody tr").length > 2) {
                        $("select[id^='ddlattedance_']").last().selectpicker({ 'dropupAuto': false, 'refresh': true });
                    }
                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function setActive(_this, _btnId, _hdnId, _attendance, _timelabel, _ratingId, _rating) {
    _btnId = "#" + _btnId;
    _hdnId = "#" + _hdnId
    _timelabel = "#" + _timelabel;
    _ratingId = "#" + _ratingId;
    $(_this).removeClass("gred_bg").addClass("bt_green");
    $(_btnId).removeClass("bt_green").addClass("gred_bg");
    $(_hdnId).val(_attendance);
    if (_attendance == "P") {
        $(_timelabel).prop('disabled', false);
        $(_ratingId).rateYo("option", "readOnly", false);
        $(_ratingId).rateYo("option", "rating", _rating);


    }
    else {
        $(_timelabel).prop('disabled', true);
        $(_ratingId).rateYo("option", "readOnly", true);
        $(_ratingId).rateYo("option", "rating", 0);


    }
    $(_timelabel).selectpicker('refresh');

}

function onMarkAttendanceBegin() {
    $("#divLoader").show();

}

function onMarkAttendanceSucess(response) {
    $("#divLoader").hide();
    if (!isSessionOutResponse(response)) {
        if (response.EnableSuccess) {
            var callbackFunc = function () {
                getAddStudentView($('#_subjectIdhdn').val());
            }
            showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);

        }
        else if (response.EnableError) {
            showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
        }
    }
}

function onMarkAttendanceComplete() {

}

function onMarkAttendanceFailure() {

}

function deleteA() {
    $("#_actionhdn").val("d")
    $("#btnSumit").trigger('click');
}

function saveA() {
    $("#_actionhdn").val("s");
    $("#btnSumit").trigger('click');

}

function MarkAllStudents(_mark) {
    if (_mark == 'P') {
        $("a[id^='btnP_']").removeClass("attendance-button gred_bg mml-0").addClass("attendance-button bt_green mrg-l0");
        $("a[id^='btnA_']").removeClass("attendance-button bt_green mrg-l0").addClass("attendance-button gred_bg mml-0");
        $("input[id^='hdn_']").val('P');
        $("select[id^='ddlattedance_']").prop('disabled', false);
        $("div[id^='rate_'").rateYo("option", "readOnly", false);

    }
    else if (_mark == 'A') {
        $("a[id^='btnA_']").removeClass("attendance-button gred_bg mml-0").addClass("attendance-button bt_green mrg-l0");
        $("a[id^='btnP_']").removeClass("attendance-button bt_green mrg-l0").addClass("attendance-button gred_bg mml-0");
        $("input[id^='hdn_']").val('A');
        $("select[id^='ddlattedance_']").prop('disabled', true);
        $("div[id^='rate_'").rateYo("option", "readOnly", true);
        $("div[id^='rate_'").rateYo("option", "rating", 0);
    }
    $("select[id^='ddlattedance_']").selectpicker('refresh');

}

function ClearTable() {
    $("#tblStudents").empty();
    disableMarkOptionDropdown(true);

}

function disableMarkOptionDropdown(_isDisabled) {
    $('#markOption').prop('disabled', _isDisabled);
    $('#markOption').selectpicker('refresh');
    $('#markOption').selectpicker('val', 'Mark All As');
}

function setSelected(_this) {
    if ($(_this).hasClass("select")) {
        $(_this).removeClass('select')
    }
    else {
        $(_this).addClass("select");
    }
}

function setRating(_id, _rate, _mark) {
    _id = "#" + _id;
    $(_id).rateYo({
        numStars: 5,
        rating: _rate,
        starWidth: "20px",
        halfStar: true,

    }).on("rateyo.set", function (e, data) {
        var _hdnid = "#ratehdn_" + $(this).attr("id").split("_")[1];
        $(_hdnid).val(data.rating);
    });
    if (_mark == 'A') {
        $(_id).rateYo("option", "readOnly", true);
    }
    else {
        $(_id).rateYo("option", "readOnly", false);

    }
}

function setTime(_id, _Timelabel, _timeId) {
    _timeId = "#" + _timeId;
    $(_timeId).val(_Timelabel);
}