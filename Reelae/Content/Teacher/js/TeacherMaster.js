﻿/*Loading Teacher Dashboard*/
$(function () {
    //global
    var gTeacher = {};
    window.gTeacherObject = {};
});

/* Update Profile Data */
function UpdateTeacherProfileChanges(url, DataToSend) {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: url,
        data: JSON.stringify(DataToSend),
        dataType: "json",
        async: false,
        success: function (result) {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            return false;
        }
    });
}
function OnSuccessTeacherProfileChanges(result) {


}
/* Teacher Master Data */
function LoadTeacherMasterNotification(url) {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: url,
        dataType: "json",
        async: false,
        success: function (result) {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            return false;
        }
    });
}
function OnSuccessLoadTeacherMasterNotification(result) {

}
/* Teacher Initial Data */
function LoadDashBoardInitialData(url) {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: url,
        dataType: "json",
        async: false,
        success: function (result) {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            return false;
        }
    });
}
function OnLoadDashBoardInitialDataSuccess(result) {

}
/* Loading Teacher Message Section*/
function LoadTeacherMessagesSection(url) {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: url,
        dataType: "json",
        async: false,
        success: function (result) {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            return false;
        }
    });
}
function OnSuccessLoadTeacherMessagesSection(result) {


}
/* Loading Teacher Group Members Section*/
function LoadTeacherGroupMembersSection(url) {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: url,
        dataType: "json",
        async: false,
        success: function (result) {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            return false;
        }
    });
}
function OnSuccessLoadTeacherGroupMembersSection(result) {


}

function AddMembersToGroup(url, DataToSend) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: url,
        data: JSON.stringify(DataToSend),
        dataType: "json",
        async: false,
        success: function (result) {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            return false;
        }
    });
}
function OnSuccessAddMembersToGroup(result) {


}

function RemoveMembersFromGroup(url, DataToSend) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: url,
        data: JSON.stringify(DataToSend),
        dataType: "json",
        async: false,
        success: function (result) {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            return false;
        }
    });
}
function OnSuccessRemoveMembersFromGroup(result) {


}

function LoadSubjectGroupMembers(url, DataToSend) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: url,
        data: JSON.stringify(DataToSend),
        dataType: "json",
        async: false,
        success: function (result) {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            return false;
        }
    });
}
function OnSuccessLoadSubjectGroupMembers(result) {


}

function LoadGroupMembers(url, DataToSend) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: url,
        data: JSON.stringify(DataToSend),
        dataType: "json",
        async: false,
        success: function (result) {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            return false;
        }
    });
}
function OnSuccessLoadGroupMembers(result) {

}

/* Loading Teacher Assignments Section */
function LoadTeacherAssignmentsSection(url) {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: url,
        dataType: "json",
        async: false,
        success: function (result) {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            return false;
        }
    });
}
function OnSuccessLoadTeacherAssignmentsSection(result) {


}
/* Loading Teacher View Subbmissions Section */
function LoadTeacherViewSubmissionsSection(url) {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: url,
        dataType: "json",
        async: false,
        success: function (result) {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            return false;
        }
    });
}
function OnSuccessLoadTeacherViewSubmissionsSection(result) {


}
/* Loading Teacher Keyword Master Section */
function LoadTeacherKeywordMasterSection(url) {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: url,
        dataType: "json",
        async: false,
        success: function (result) {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            return false;
        }
    });
}
function OnSuccessLoadTeacherKeywordMasterSection(result) {


}
/* Loading Teacher Forum Section */
function LoadTeacherForumSection(url) {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: url,
        dataType: "json",
        async: false,
        success: function (result) {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            return false;
        }
    });
}
function OnSuccessLoadTeacherForumSection(result) {


}
/* Loading Teacher Resource Section */
function LoadTeacherResourceSection(url) {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: url,
        dataType: "json",
        async: false,
        success: function (result) {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            return false;
        }
    });
}
function OnSuccessLoadTeacherResourceSection(result) {


}
/* Loading Teacher NoticeBoard Section */
function LoadTeacherNoticeBoardSection(url) {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: url,
        dataType: "json",
        async: false,
        success: function (result) {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            return false;
        }
    });
}
function OnSuccessLoadTeacherNoticeBoardSection(result) {


}