﻿var _groupIdArr = [];
var _grouparr = [];
var urlRegex = '^(?!mailto:)(?:(?:http|https|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[0-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))|localhost)(?::\\d{2,5})?(?:(/|\\?|#)[^\\s]*)?$';
var urlformat = new RegExp(urlRegex, 'i');
var youtubeRegex = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
var youtubeFormat = new RegExp(youtubeRegex, 'i');
$(document).ready(function () {
    $('#ddlGroup_notice').multiselect({
        disableIfEmpty: true,
        maxHeight: 200,
        buttonWidth: '170px',
        //enableFiltering: true,
        nonSelectedText: 'Select Group',
        includeSelectAllOption: true,
        selectAllJustVisible: false,
        enableCaseInsensitiveFiltering: false,
        allSelectedText: 'All groups are selected',
        onChange: function (option, checked, select) {
            if (checked) {
                _groupIdArr.push($(option).val());
                _grouparr.push($(option)[0].innerHTML);

            } else {
                var item = $.inArray($(option).val(), _groupIdArr);
                _groupIdArr.splice($.inArray($(option).val(), _groupIdArr), 1);
                _grouparr.splice($.inArray($(option)[0].innerHTML, _grouparr), 1);

            }
            getNotices(_groupIdArr.join());
        },
        onSelectAll: function () {
            _groupIdArr = [];
        },
        onDeselectAll: function () {
            _groupIdArr = [];
        }
    });
    getNotices(null);
})

function getNewNoticeView() {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/NoticeBoard/getNewNoticeView/",
        dataType: "html",
        data: JSON.stringify({ "id": _groupIdArr.join(), "groupName": _grouparr.join()}),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#addNewNoticeDiv').html(result);
                    initNoticeDatePicker($('#noticeDate'));
                    $('#newNoticeModelBox').modal('show');
                    initTagInput();
                    $(".modal-body").mCustomScrollbar({
                        theme: "dark-3",
                        autoHideScrollbar: true
                    });
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function onNoticeSucess(response) {
    $("#divLoader").hide();
    $('#newNoticeModelBox').modal('hide');
    if (!isSessionOutResponse(response)) {
        if (response.EnableError) {
            showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
        } else if (response.EnableSuccess) {
            var callbackFunc = function () {
                $("#divLoader").show();
                getNotices(_groupIdArr.join());
                //window.location.reload();
            }
            showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
        }

    }
}

function onNoticeBegin() {
    var urlRegex = '^(?!mailto:)(?:(?:http|https|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[0-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))|localhost)(?::\\d{2,5})?(?:(/|\\?|#)[^\\s]*)?$';
    var url = new RegExp(urlRegex, 'i');
    var scriptPat = /[<>]/g;
    if ($("#txt_title").val().trim() == "") {
        showAlertBox("Information Alert Message", "Please enter Title.", gInfo.alertMsgType.information);
        $("#txt_title").focus();
        return false;
    }
    else if (scriptPat.test($("#txt_title").val().trim())) {
        showAlertBox("Information Alert Message", "<,> characters are not allowed.", gInfo.alertMsgType.information);
        $("#txt_title").val('');
        return false;

    }
        //else if ($("#videoUrl").val().trim() != "" && !url.test($("#videoUrl").val().trim())) {
        //    showAlertBox("Information Alert Message", "Invalid url", gInfo.alertMsgType.information);
        //    $("#videoUrl").focus();
        //    return false;
        //}
    else if (scriptPat.test($("#txt_descr").val().trim())) {
        showAlertBox("Information Alert Message", "<,> characters are not allowed.", gInfo.alertMsgType.information);
        $("#txt_descr").val('');
        return false;

    }
    else {
        $("#divLoader").show();
        return true;
    }
}

function chkValidUrl(url) {
    var youtube = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/,
        yt = url.match(yt);

    if (yt) {
        return true;
    }
    return false;
}

function OnSelectFilerDate(startDate, endDate) {
    console.log("OnSelect: " + startDate.format('MM/DD/YYYY') + ":" + endDate.format('MM/DD/YYYY'));
}

function triggerFileUpload() {// for trigger file upload control
    $('#_HttpPostedNoticeFile').trigger('click');
}

function UploadNoticeFile() {
    if (window.FormData !== undefined) {
        var fileUpload = $("#_HttpPostedNoticeFile").get(0);
        var files = fileUpload.files;
        if (files.length > 5) {
            alert("You can only upload a maximum of 5 files");
            return false;
        }
        // Create FormData object  
        var fileData = new FormData();
        //fileData.append(files[0].name, files[0]);
        for (var i = 0; i < files.length; i++) {
            if (isValidExtention(files[i])) {
                fileData.append(files[i].name, files[i]);
            }
            else {
                return false;
            }
        }
        // Adding one more key to FormData object  
        $("#divLoader").show();
        $.ajax({
            url: '/Teacher/NoticeBoard/UploadNoticeFile',
            type: "POST",
            contentType: false, // Not to set any content header  
            processData: false, // Not to process data  
            data: fileData,
            success: function (result) {
                if (!isSessionOutResponse(result)) {
                    if (result != "") {
                        $("#FilesDiv").empty().html(result);
                        $("#divLoader").hide();
                    }
                    $("#divLoader").hide();
                }
            },
            error: function (err) {
                showAlertBox(err.status, err.msg, gInfo.alertMsgType.failure)
                $("#divLoader").hide();
            }
        });
    }
    else {
        showAlertBox("Browser Functionality Alert Message", "FormData is not supported in this current version of browser.Please Upgrade newer version", gInfo.alertMsgType.failure)
    }
}

function DeleteNoticeFile(_id, _fileUrl, _div, _noticeBoardId) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Teacher/NoticeBoard/DeleteNoticeFile',
        dataType: "json",
        data: JSON.stringify({ "id": _id, "fileUrl": _fileUrl }),
        async: true,
        success: function (response) {
            $("#divLoader").hide();
            if (!isSessionOutResponse(response)) {
                if (response.EnableError) {
                    showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
                } else if (response.EnableSuccess) {
                    var callbackFunc = function () {
                        $("#divLoader").show();
                        getNoticeFiles(_noticeBoardId);
                    }
                    showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
                    $("#" + _div).remove();
                }

            }
        },
        error: function (err) {
            showAlertBox("Failure Alert Message", err.statusText, gInfo.alertMsgType.failure)
            $("#divLoader").hide();
        }
    });
}

function setUploadedNoticePreview(fileType, URL) {
    var fileExtension = URL.split(".");
    fileExtension = fileExtension[fileExtension.length - 1];
    var html = "";
    switch (fileType) {
        case "document":
            if (fileExtension == "doc" || fileExtension == "docx") {
                html = "<div class='prod_wrap_add text-center'>";
                html += " <i class='material-icons file_icn'>insert_drive_file</i></div>";
            }
            else if (fileExtension == "xls" || fileExtension == "xlsx") {
                html = "<div class='prod_wrap_add text-center'>";
                html += " <i class='material-icons file_icn'>insert_drive_file</i></div>";
            }
            else {
                html = "<div class='prod_wrap_add text-center'>";
                html += " <i class='material-icons file_icn'>insert_drive_file</i></div>";
            }
            break;
        case "video":
            html = "<video id='thumb' heght='50px' width='50px'><source heght='50px' width='50px' src='" + URL + "' /> </video>";
            break;
        case "audio":
            html = " <audio controls heght='50px' width='50px'> <source src='" + URL + "' /> </audio>";
            break;
        case "compressed":
            html = "<div class='prod_wrap_add text-center'>";
            html += "<i class='material-icons file_icn'>archive</i></div>";
            break;

    }
    if (fileType.search("image") != -1) {
        html = "<img src=" + URL + " id='imgNotice'>";
    }
    $('.upload_logo').empty();
    $('.upload_logo').append(html);
    $('#btnDelete').show();
    $('#btnNoticeupload').hide();
}

function getNotices(_groupId) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/NoticeBoard/getNotices/",
        dataType: "html",
        data: JSON.stringify({ "groupIds": _groupId }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#divNoticeList').html(result);
                    $("#divLoader").hide();

                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function getNoticeFiles(_id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/NoticeBoard/getNoticeBoardFiles/",
        dataType: "html",
        data: JSON.stringify({ "id": _id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#FilesDiv').empty().html(result);
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}


function getMoreComments(_noticeBoardId) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/NoticeBoard/getMoreComments/",
        dataType: "html",
        data: JSON.stringify({ "id": _noticeBoardId }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#noticeCommentdiv').html(result);
                    $(".modal-body").mCustomScrollbar({
                        theme: "dark-3",
                        autoHideScrollbar: true
                    });
                    $('#comment').modal('show');
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function editNotice(_noticeBoardId) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/NoticeBoard/getEditNoticeView/",
        dataType: "html",
        data: JSON.stringify({ "id": _noticeBoardId }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#addNewNoticeDiv').html(result);
                    initNoticeDatePicker($('#noticeDate'));
                    initTagInput();
                    $(".modal-body").mCustomScrollbar({
                        theme: "dark-3",
                        autoHideScrollbar: true
                    });
                    $('#newNoticeModelBox').modal('show');
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function RemoveNoticeConfirmation(_noticeBoardId, _noticeTitle) {
    var message = "Are you sure, do you want to remove the notice '<strong>" + _noticeTitle + "'</strong>.";
    $('#message').html(message);
    $('#removeConfirmationModalBox').modal('show');
    $('#submitAlert').attr("onclick", "RemoveNotice('" + _noticeBoardId + "')");
}

function RemoveNotice(_noticeBoardId) {
    $('#removeConfirmationModalBox').modal('hide');
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/NoticeBoard/RemoveNotice/",
        dataType: "json",
        data: JSON.stringify({ "id": _noticeBoardId }),
        async: true,
        success: function (response) {
            $("#divLoader").hide();
            if (!isSessionOutResponse(response)) {
                if (response.EnableError) {
                    showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
                } else if (response.EnableSuccess) {
                    var callbackFunc = function () {
                        $("#divLoader").show();
                        getNotices(_groupIdArr.join());
                    }
                    showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function chkUrl(_url) {
    return urlformat.test(_url);
}

function initTagInput() {
    $('#urls').tagsInput({
        allowDuplicates: false,
        onAddTag: function (item) {
            if (!chkUrl(item)) {
                alert('Invalid url');
                $('#videoUrl').removeTag(item);
                return false;
            }
            if (youtubeFormat.test(item)) {
                if (item.search("=") != -1) {
                    var videoUrl = item.split("=");
                    $('#urls').removeTag(item);
                    item = 'https://www.youtube.com/embed/' + videoUrl[1];
                    $('#urls').addTag(item);
                }

            }
            //if ($('input[name=videoUrl]').val().trim() == "") {
            //    $('input[name=videoUrl]').val(item);
            //}
            //else {
            //    var _appendUrl = $('input[name=videoUrl]').val().trim() + "," + item;
            //    $('input[name=videoUrl]').val(_appendUrl);

            //}

        }
    });
}

function updateCount(_count) {
    $("#noticecount").html(_count + " Notices");
}

function setActive(_this) {
    if ($(_this).hasClass("active")) {
        $(_this).removeClass("active");

    }
    else {
        $(_this).addClass("active");
    }
}


function initNoticeDatePicker(containerEle) {
    var start = moment().add(29, 'days');

    if ($("#hdn_Date").val() != "") {
        start = moment($("#hdn_Date").val());
    }
    else {
        $("#hdn_Date").val(start.format('MM/DD/YYYY'));
    }

    containerEle.daterangepicker({
        startDate: start,
        singleDatePicker: true,
        showDropdowns: true,
        drops: "up",
        locale: {
            format: 'MM/DD/YYYY'
        }
    },
    function (start, end, label) {
        var curDate = new Date();
        curDate.setHours(0, 0, 0, 0);
        var endDate = new Date(end);
        endDate.setHours(0, 0, 0, 0);
        if (endDate < curDate) {
            alert('Inactive date can not be past date.');
            return false;
        }
        $("#hdn_Date").val(start.format('MM/DD/YYYY'));
        containerEle.data('assignSD', start.format('MM/DD/YYYY'));

    });
}