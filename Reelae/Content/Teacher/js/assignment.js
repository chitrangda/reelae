﻿
var _newAssignmentKeywords = [];
var _existingKeywords = [];
var _existingKeywordsData = [];
var curMemberDataTableInstance = null;
var AssignType = 1;
var LeaderId = "";
var keywordArray = [];
var arrayAssignments = [];
var arrayCreateAssignments = [];
var _deletedKeywords = [];


function init_daterangepicker_rangeCustomForAssignment(ContainerEle, callBack) {
    if ("undefined" != typeof $.fn.daterangepicker && ContainerEle.length > 0) {

        console.log("init_daterangepicker_range");
        var a = function (a, b, c) {
            console.log(a.toISOString(), b.toISOString(), c), ContainerEle.find('span').html(a.format("MMMM D, YYYY") + " - " + b.format("MMMM D, YYYY"))
        },
            b = {
                startDate: moment(),
                endDate: moment().add(30, "days"),
                minDate: "01/01/2001",
                maxDate: "12/31/2020",
                dateLimit: {
                    days: 60
                },
                showDropdowns: !0,
                showWeekNumbers: !0,
                timePicker: !1,
                timePickerIncrement: 1,
                timePicker12Hour: !0,
                ranges: {
                    Today: [moment(), moment()]
                    //Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
                    //"Last 7 Days": [moment().subtract(6, "days"), moment()],
                    //"Last 30 Days": [moment().subtract(29, "days"), moment()],
                    //"This Month": [moment().startOf("month"), moment().endOf("month")],
                    //"Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
                },
                opens: "left",
                buttonClasses: ["btn btn-default"],
                applyClass: "btn-small btn-primary",
                cancelClass: "btn-small",
                format: "MM/DD/YYYY",
                separator: " to ",
                drops: "up",
                locale: {
                    applyLabel: "Submit",
                    cancelLabel: "Clear",
                    fromLabel: "From",
                    toLabel: "To",
                    customRangeLabel: "Custom",
                    daysOfWeek: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
                    monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                    firstDay: 1,
                    format: "MM/DD/YYYY"
                }
            };
        ContainerEle.find('span').html(moment().format("MMMM D, YYYY") + " - " + moment().add(29, "days").format("MMMM D, YYYY")), ContainerEle.daterangepicker(b, a), ContainerEle.on("show.daterangepicker", function () {
            console.log("show event fired")
        }), ContainerEle.on("hide.daterangepicker", function () {
            console.log("hide event fired")
        }), ContainerEle.on("apply.daterangepicker", function (a, b) {
            //console.log("apply event fired, start/end dates are " + b.startDate.format("MMMM D, YYYY") + " to " + b.endDate.format("MMMM D, YYYY"))
            //customize
            if (!b) {
                b = ContainerEle.data('daterangepicker')
                ContainerEle.find('span').html(b.startDate.format("MMMM D, YYYY") + " - " + b.endDate.format("MMMM D, YYYY"))
            }
            else {
                console.log("apply event fired, start/end dates are " + b.startDate.format("MMMM D, YYYY") + " to " + b.endDate.format("MMMM D, YYYY"))
                //customize
                ContainerEle.data('assignSD', b.startDate.format("MM/DD/YYYY"));
                ContainerEle.data('assignED', b.endDate.format("MM/DD/YYYY"));
                if (callBack && $.isFunction(callBack)) {
                    callBack(b.startDate, b.endDate);
                }
            }

        }), ContainerEle.on("cancel.daterangepicker", function (a, b) {
            console.log("cancel event fired")
        }), $("#options1").click(function () {
            ContainerEle.data("daterangepicker").setOptions(b, a)
        }), $("#options2").click(function () {
            ContainerEle.data("daterangepicker").setOptions(optionSet2, a)
        }), $("#destroy").click(function () {
            ContainerEle.data("daterangepicker").remove()
        })
    }
}
function inputCurrentSuggesstion(_this) {
    //$('#txtAddKeyWord').data('keyID', $(this).val());
    //$('#txtAddKeyWord').val($(_this).text());
    //AddKeysToInputArea($(_this));
}

function initAutocompleteCustom(containerEle, inputArray, callback) {
    if (containerEle.length) {
        containerEle.autocomplete({
            lookup: inputArray,
            onSelect: function (suggestion) {
                if (callback && $.isFunction(callback)) {
                    callback(suggestion);
                }
            }
        });
    }
}
function OnSuggestionSelect(suggestion) {
    //suggesstion contains .data and .value
    $('#txtAddKeyWord').data('CurAssignmentkeyID', suggestion);
}
function findKeywords() {
    var curValue = $('#txtAddKeyWord').val();
    GetKeywordsFromString(curValue);
}
function AddKeywordsCustom(keywordInputEle, ExistingKeywordEle) {
    var currentVal = keywordInputEle.val().trim();
    var scriptPat = /[<>]/g
    ExistingKeywordEle = $('.tagsinput[data-tagsinput-init]', $('#createAssignmentModalStep3'));
    if (currentVal == "") {
        showAlertBox("Information Alert Message", "Keyword name is required.", gInfo.alertMsgType.information);
        keywordInputEle.val('');
        if (keywordInputEle.data('CurAssignmentkeyID'))
            keywordInputEle.removeData('CurAssignmentkeyID');
        keywordInputEle.focus();
        return false;
    }
    else if (scriptPat.test(currentVal)) {
        showAlertBox("Information Alert Message", "<,> characters are not allowed.", gInfo.alertMsgType.information)
        if (keywordInputEle.data('CurAssignmentkeyID'))
            keywordInputEle.removeData('CurAssignmentkeyID');
        keywordInputEle.val('');
        return false;
    }
    else {
        if (!ExistingKeywordEle.tagExist(keywordInputEle.val().trim())) {
            ExistingKeywordEle.addTag(keywordInputEle.val().trim());
            var lastSuggestedKeyword = keywordInputEle.data('CurAssignmentkeyID');
            if (lastSuggestedKeyword && lastSuggestedKeyword.value.toLowerCase() == keywordInputEle.val().trim().toLowerCase()) {
                if ($.isArray(_existingKeywords))
                    _existingKeywords.push(lastSuggestedKeyword.data);
                if ($('.tagsinput:not([data-tagsinput-init])', $('#createAssignmentModalStep3')).find('span.tag:last').length) {
                    $('.tagsinput:not([data-tagsinput-init])', $('#createAssignmentModalStep3')).find('span.tag:last').find('a[title]').attr('onclick', "removeKeyword(this,'E','" + keywordInputEle.data('CurAssignmentkeyID') + "')");
                }
                keywordInputEle.removeData('CurAssignmentkeyID');
            }
            else {
                if ($.isArray(_newAssignmentKeywords))
                    _newAssignmentKeywords.push(keywordInputEle.val().trim());
                $('.tagsinput:not([data-tagsinput-init])', $('#createAssignmentModalStep3')).find('span.tag:last').find('a[title]').attr('onclick', "removeKeyword(this,'N')");
            }
            keywordInputEle.val("");
        }
        else {
            showAlertBox("Alert Message", "Already exists", gInfo.alertMsgType.success);
        }
    }
}

function removeKeyword(_this, keywordType, keyId) {
    if (keywordType == 'N') {
        _newAssignmentKeywords.splice(_newAssignmentKeywords.indexOf($(_this).prev().text().trim()), 1);
    }
    else if (keywordType == 'E') {
        //for edit mode
        var eleIndex = _existingKeywords.indexOf(+keyId);
        if ($('#hdnAssignmentId').val()) {
            if (keyId > -1)
                _deletedKeywords.push(_existingKeywords[eleIndex]);
        }
        _existingKeywords.splice(eleIndex, 1);
    }
    $(_this).parents('span.tag').first().remove();
}


function onOpenCreateAssignment() {
    resetAddCreateModal();
    OpenaddAssignmentModal(null);
}
function OpenaddAssignmentModal(id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/Assignment/GetCreateAssignmentView/" + id,
        dataType: "html",
        data: "",
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    OnAddAssignmentViewSuccess(result);

                    //for edit assignment only                    
                    if (id) {
                        var StartDate = $('#txtStartDate').val();
                        var EndDate = $('#txtEndDate').val();
                        if (StartDate && EndDate) {
                            $("#assignmentdateRange").data('daterangepicker').setStartDate(new Date(StartDate));
                            $("#assignmentdateRange").data('daterangepicker').setEndDate(new Date(EndDate));
                            $("#assignmentdateRange").trigger('apply.daterangepicker');
                        }
                        LeaderId = $('#txtLeaderID').val();
                        var AssigneeID = $('#txtAssigneeID').val();
                        var onEditInit = true;
                        if (AssigneeID && AssigneeID != "0") {
                            AssignType = "1";
                            $('#ddlGroups_Assignment').val(AssigneeID);
                            $('#ddlGroups_Assignment').selectpicker('refresh');
                            GetGroupMembers(AssigneeID, onEditInit);
                        }
                        else {
                            AssignType = "2";
                            $('#ddlGroups_Assignment').data('selectpicker').$button.prop('disabled', "disabled");
                            GetTeachersStudents(onEditInit);
                        }
                        //loadkeywords
                        LoadExistingKeywordsCustom($('#txtAddKeyWord'), $('#txtAssinmentKeys'));
                        $('#txtAddKeyWord').removeData('CurAssignmentkeyID');
                        //end of edit assignment only
                    }
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}



function OnAddAssignmentViewSuccess(result) {
    LeaderId = "";
    AssignType = "1";
    _newAssignmentKeywords = [];
    _existingKeywords = [];
    _deletedKeywords = [];
    curMemberDataTableInstance = null;
    if (!$('#hdnAssignmentId').val())
        _existingKeywordsData = [];
    try {
        $('#txtAssinmentKeys').tagsinput('refresh');
    } catch (e) {

    }
    $('#leaderGroup label').html('Select Leader');
    $('#addAssignmentContainer').html(result);
    $('[name="iCheck"]', $('#createAssignmentModalStep2')).iCheck({
        checkboxClass: "icheckbox_flat-green",
        radioClass: "iradio_flat-green"
    })
    $('[name="iCheck"]', $('#createAssignmentModalStep2')).on('ifChecked', function (response) {
        OnAssignneTypeChange($(response.currentTarget).val());
    })
    init_daterangepicker_rangeCustomForAssignment($('#assignmentdateRange'));
    initMadSelect($('#ddlAssignmentType').parent());
    initAutocompleteCustom($('#txtAddKeyWord'), keywordArray, OnSuggestionSelect);

    try {
        $('#txtAssinmentKeys').tagsInput('destroy');
    } catch (e) {

    }
    //resetAddCreateModal();
    $('#createAssignmentModalStep1').modal('show');
    $('#ddlGroups_Assignment').selectpicker();

}


function validateCreateAssignmentStep1() {
    var assignType = $('#ddlAssignmentTypeValue').val();
    var assignTitle = $('#txt_AssignmentName').val().trim();
    var assignNotes = $('#txt_AssignmentDescription').val().trim();
    var startdate = moment($('#assignmentdateRange').data('daterangepicker').startDate.format("MM/DD/YYYY"), "MM/DD/YYYY");
    var enddate = moment($('#assignmentdateRange').data('daterangepicker').endDate.format("MM/DD/YYYY"), "MM/DD/YYYY");
    if (assignType == "0") {
        showAlertBox("Information Alert Message", "Please Select Assignment Type.", gInfo.alertMsgType.information);
        return false;
    }
    if (!assignTitle) {
        showAlertBox("Information Alert Message", "Please Enter Assignment Title.", gInfo.alertMsgType.information);
        return false;
    }
    if (!assignNotes) {
        showAlertBox("Information Alert Message", "Please Enter Assignment Notes.", gInfo.alertMsgType.information);
        return false;
    }
    if (!$('#hdnAssignmentId').val() && moment().subtract(1, "days") > startdate) {
        $("#assignmentdateRange").data('daterangepicker').setStartDate(new Date());
        showAlertBox("Information Alert Message", "Start Date must not be less than Today's Date.", gInfo.alertMsgType.information);
        return false;
    }
    if (startdate > enddate) {
        $("#assignmentdateRange").data('daterangepicker').setStartDate(new Date());
        $("#assignmentdateRange").data('daterangepicker').setEndDate(new Date());
        showAlertBox("Information Alert Message", "Start Date must be less than End Date.", gInfo.alertMsgType.information);
        return false;
    }
    //$('#submitCreateGroup').trigger('click');
    if (AssignType == "1" && $('#ddlGroups_Assignment').val() != "0") {
        $('#leaderGroup').show();
        $('#example').show();
        if ($('#example_wrapper').length)
            $('#example_wrapper').show();
    }
    else if (AssignType == "1" && $('#ddlGroups_Assignment').val() == "0") {
        $('#leaderGroup').hide();
        $('#example').hide();
        if ($('#example_wrapper').length)
            $('#example_wrapper').hide();

    }

    $('#createAssignmentModalStep1').modal('hide');
    $('#createAssignmentModalStep2').modal('show');
    return true;
}

function validateCreateAssignmentStep2() {
    var assignGroup = $('#ddlGroups_Assignment').val();
    if (AssignType == "1" && (!assignGroup || assignGroup == "0")) {
        showAlertBox("Information Alert Message", "Please Select Group or Individual First.", gInfo.alertMsgType.information);
        return false;
    }
    if ($('#example').find('tbody tr.noData').length) {
        showAlertBox("Information Alert Message", "There is no Leader/Individual .There should be at least one Leader/Individual.", gInfo.alertMsgType.information);
        return false;
    }
    if (!LeaderId) {
        showAlertBox("Information Alert Message", "Please select at least one Leader/Individual.", gInfo.alertMsgType.information);
        return false;
    }
    //$('#submitCreateGroup').trigger('click');
    $('#createAssignmentModalStep2').modal('hide');
    //edit assignment only 
    //LoadExistingKeywordsCustom($('#txtAddKeyWord'), $('#txtAssinmentKeys'));
    $('#createAssignmentModalStep3').modal('show');
    return true;
}

function OnBeginCreateAssignment() {

}
function OnSuccessCreateAssignment(result) {
    if (!isSessionOutResponse(result)) {
        $("#divLoader").hide();
        if (result && result.hasOwnProperty("status") && result.status.toLowerCase() == "success") {
            showAlertBox("Success Alert Message", result.msg, gInfo.alertMsgType.success, function () {
                location.reload();
            });
            $('#createAssignmentModalStep1').modal("hide");
            $('#createAssignmentModalStep2').modal("hide");
            $('#createAssignmentModalStep3').modal("hide");
        }
        else {
            showAlertBox("Failure Alert Message", "Some Problem Occurred. Please Try Again Later", gInfo.alertMsgType.failure);
        }
    }
}
function OnFailureCreateAssignment(result) {
    if (!isSessionOutResponse(result)) {
        $("#divLoader").hide();
        showAlertBox("Failure Alert Message", "Some Problem Occurred. Please Try Again Later", gInfo.alertMsgType.failure);
    }
}

function resetAddCreateModal() {
    $('#txt_AssignmentName').val('');
    $('#txt_AssignmentDescription').val('');
    $('#txt_StartDate').val('');
    $('#txtEndDate').val('');
    $('#txtAssigneeID').val('');
    $('#txtKeywordsID').val('');
    $('#txtNewKeywordsID').val('');
    if ($('#ddlGroups_Assignment>option', $('#createAssignmentModalStep2')).length > 0) {
        $('#ddlGroups_Assignment>option:eq(0)', $('#createAssignmentModalStep2')).attr('selected', 'selected');
    }
    //$('#txtAssinmentKeys_tagsinput').html('');
}
function PutValueforAssignments() {
    $('#txtKeywordsID').val(_existingKeywords.join(','));
    $('#txtNewKeywords').val(_newAssignmentKeywords.join(','));
    $('#txtdelKeywords').val(_deletedKeywords.join(','));

    //groupdata$('#ddlGroups_Assignment>option')
    if (AssignType == "2")
        $('#txtAssigneeID').val("0");
    else
        $('#txtAssigneeID').val($('#ddlGroups_Assignment', $('#createAssignmentModalStep2')).val());
    //AssignmentType
    $('#hdnAssignmentType').val($('#ddlAssignmentTypeValue').val());
    //name
    $('#hdnAssignmentName').val($('#txt_AssignmentName').val());
    //desc
    $('#hdnAssignmentDesc').val($('#txt_AssignmentDescription').val());
    //startDate
    //var assignDateRange = $('#assignmentdateRange .fa-calendar').text();
    $('#txtStartDate').val($('#assignmentdateRange').data('daterangepicker').startDate.format("MM/DD/YYYY"));
    //duedate
    $('#txtEndDate').val($('#assignmentdateRange').data('daterangepicker').endDate.format("MM/DD/YYYY"));
    $('#txtLeaderID').val(LeaderId);
    $('#hdnAssignmentWordCount').val($('#txt_AssignmentWordCount').val());
    return true;
}
function validateAddAssignment() {
    PutValueforAssignments();
    //var assignKeywords = $('#txtKeywordsID').val();
    //if (_existingKeywords.length == 0 && _newAssignmentKeywords.length == 0) {
    //    var callbackFunc = function () {
    //        $('#submitAddAssignment').trigger('click');
    //        $('#divLoader').show();
    //    }
    //    showAlertBox("Information Alert Message", "There is no Keywords added for this Assignment. Going to create Assignment with blank keyword.", gInfo.alertMsgType.information, callbackFunc);
    //}
    //else {
    //    $('#submitAddAssignment').trigger('click');
    //    $('#divLoader').show();
    //}
    $('#submitAddAssignment').trigger('click');
    $('#divLoader').show();
    return true;
}

//Keyword
function GetKeywordsFromString(keywordString) {
    if (!keywordString) {
        return false;
    }
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/Assignment/GetKeywords/" + keywordString,
        dataType: "json",
        async: true,
        success: function (result) {
            if (result.status == "success") {
                var data = JSON.parse(result.Data);
                BindDropDownData($('#ddlKeywords'), data);
                $('#ddlKeywords').css('display', 'block');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            return false;
        }
    });
}

function BindDropDownData(containerEle, Data) {
    if (!containerEle || containerEle.length == 0)
        return false;
    containerEle.html('');
    if (!Data || !Data instanceof Array)
        return false;
    for (var i = 0; i < Data.length; i++) {
        var option = $('<option>');
        option.attr('value', Data[i].Id);
        option.html(Data[i].Value);
        if (containerEle.attr('id') == "ddlKeywords") {
            option.attr('onclick', 'inputCurrentSuggesstion(this)');
        }
        containerEle.append(option);
    }
}
//previous and next click events

//On AssigneeType Change
//onchange = "OnAssignneTypeChange()"
function OnAssignneTypeChange(assigneeType) {
    //var assigneeType = $('input[name="rdb_AssigneeType"]').val();
    AssignType = assigneeType;
    if (assigneeType == "2") {
        $('#ddlGroups_Assignment').data('selectpicker').$button.prop('disabled', "disabled");
        $('#leaderGroup label').html('Select Student');
        GetTeachersStudents();
    } else if (assigneeType == "1") {
        $('#ddlGroups_Assignment').data('selectpicker').$button.prop('disabled', false);
        $('#leaderGroup label').html('Select Leader');
        var groupid = $('#ddlGroups_Assignment').val();
        if (groupid && groupid != "0")
            GetGroupMembers(groupid);
        else {
            $('#example').find('tbody').empty();
            $('#example').find('tbody').html('<tr class=".noData"><td colspan="2" class="text-center" style="width: 50px;"><div class="radio dis_in"><label>No Members Found.</label></div></td></tr>');
            $('#leaderGroup').hide();
            $('#example').hide();
            if ($('#example_wrapper').length)
                $('#example_wrapper').hide();

        }
    }
}

function OnGroupChange(_this) {
    var groupid = $(_this).val();
    if (groupid && groupid != "0")
        GetGroupMembers(groupid);
    else {
        //try {
        //    $('#example').DataTable().destroy();
        //} catch (e) {

        //}
        $('#leaderGroup').hide();
        $('#example').hide();
        if ($('#example_wrapper').length)
            $('#example_wrapper').hide();
        $('#example').find('tbody').empty();
        $('#example').find('tbody').html('<tr class=".noData"><td colspan="2" class="text-center" style="width: 50px;"><div class="radio dis_in"><label>No Members Found.</label></div></td></tr>');
    }
}

function GetGroupMembers(groupid, onInit) {
    $("#divLoader").show();
    if (!groupid) {
        return false;
    }
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/Assignment/GetGroupMembers",
        data: JSON.stringify({ "groupid": groupid }),
        dataType: "html",
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result)
                    OnMembersBind(result);
                $('#leaderGroup').show();
                $('#example').show();
                if ($('#example_wrapper').length)
                    $('#example_wrapper').show();

                if ($('#hdnAssignmentId').val() && onInit) {
                    $('input[name="iCheckMember"][value="' + $('#txtLeaderID').val() + '"]').parent().addClass('checked');
                    LeaderId = $('#txtLeaderID').val();
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred. Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function GetTeachersStudents(onInit) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/Assignment/GetTeacherStudents",
        data: "",
        dataType: "html",
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result)
                    OnMembersBind(result);
                $('#leaderGroup').show();
                $('#example').show();
                if ($('#example_wrapper').length)
                    $('#example_wrapper').show();
                if ($('#hdnAssignmentId').val() && onInit) {
                    $('input[name="iCheckMember"][value="' + $('#txtLeaderID').val() + '"]').parent().addClass('checked');
                    LeaderId = $('#txtLeaderID').val();
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred. Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function OnMembersBind(result) {
    LeaderId = "";
    $('#example').find('tbody').empty();
    $('#example').find('tbody').html(result);

    if ($('#example').find('tbody tr.noData').length) {
        showAlertBox("Information Alert Message", "No Members Found. There should be at least one member for creating assignments.", gInfo.alertMsgType.information);
    } else {
        $('[name="iCheckMember"]', $('#example')).iCheck({
            checkboxClass: "icheckbox_flat-green",
            radioClass: "iradio_flat-green"
        });
        $('.iradio_flat-green').has('[name="iCheckMember"]').removeClass('checked');
        $('[name="iCheckMember"]', $('#createAssignmentModalStep2')).on('ifChecked', function (response) {
            LeaderId = $(response.currentTarget).val();
        })
        try {
            if (!curMemberDataTableInstance)
                curMemberDataTableInstance = $('#example').DataTable({
                    "scrollY": "180px",
                    "scrollCollapse": true,
                    "paging": false,
                    "destroy": true,
                    "searching": true,
                    "paging": false,
                    "info": false,
                    "order": [[1, "asc"]],
                    language: {
                        search: "<i class='material-icons'>search</i> _INPUT_",
                        searchPlaceholder: "Search members..."
                    }
                });
        } catch (e) {

        }
    }

}
/////////////////// create assignment grid////////////////////////////////////
function CreateAssignmentGrid(id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/Assignment/TeacherAssignmentGrid",
        dataType: "html",
        data: JSON.stringify({ "id": id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#gridResult').empty().html(result);
                    bindCreateAssignmentTeacherTable(id);
                }
                else {
                    $("#divLoader").hide();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred. Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function bindCreateAssignmentTeacherTable(id) {
    var dataTable = $('#assignmentGrids').DataTable({
        "responsive": true,
        "paging": true,
        "ordering": true,
        //"info": false,
        "oLanguage": {
            "sEmptyTable": "No Record(s) Found."
        },
        // "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "processing": false,
        "serverSide": true,
        "searching": true,
        "scroll": true,
        "ajax": {
            url: "/Teacher/Assignment/BindCreateAssignmentDetails?id=" + id, // json datasource
            type: "Post",  // method  , by default get
            error: function () {  // error handling
                jQuery("#tbAdmin").append('<tbody class="business-user-grid-error"><tr><th colspan="8">No data found in the server</th></tr></tbody>');
            }
        },
        "columnDefs": [
             {
                 "targets": 0,
                 "sClass": "mx-w-300",
             },
              {
                  "targets": 1,
                  "sClass": "width_130 td_nowrap",
              },
               {
                   "targets": 2,
                   "sClass": "width_130 td_nowrap",
               },
                {
                    "targets": 3,
                    "sClass": "t-min-w_205",
                },
            {
                "targets": 4,
                "sClass": "t-min-w_115",
            },
            {
                "targets": 5,
                "orderable": false,
                "sClass": "dataTableCenter",
            },

        {
            "targets": 6,
            "orderable": false,
            "sClass": "dataTableCenter",
        },

        {
            "targets": 7,
            "orderable": false,
            "sClass": "dataTableCenter",
        }
        ],

        "initComplete": function (settings, json) {
            $("#divLoader").hide();
        },
        "fnDrawCallback": function () {
            initMadSelect($('.ddlAction').parent());

            if ($('#assignmentGrids tbody tr td').hasClass('dataTables_empty')) {
                $('#count').empty().text(0);
            }
            else {
                $('#count').empty().text($('#assignmentGrids tbody tr').length);
            }
        }
    });
    $("#autocompleteCreateAssignment").keyup(function () {
        dataTable.search($(this).val()).draw();
    });
}


////////////////////  assignmentViewSubmissionGrid////////////////////////
function ViewAssignmentGrid(id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/Submissions/TeacherAssignmentViewGrid/" + id,
        dataType: "html",
        data: "",
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#gridResult').empty().html(result);
                    $('#count').text($('#gridRowCount').val());
                    bindSubmitAssignmentTeacherTable(id);
                }
                else {
                    $("#divLoader").hide();
                }
                // $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred. Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function bindSubmitAssignmentTeacherTable(id) {
    var dataTable = $('#viewAssignmentGrid').DataTable({
        "responsive": true,
        "paging": true,
        "ordering": true,
        //"info": false,
        "oLanguage": {
            "sEmptyTable": "No Record(s) Found."
        },
        // "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "processing": false,
        "serverSide": true,
        "searching": true,
        "scroll": true,
        "ajax": {
            url: "/Teacher/Submissions/BindSubmitAssignmentDetails?id=" + id, // json datasource
            type: "Post",  // method  , by default get
            error: function () {  // error handling
                jQuery("#tbAdmin").append('<tbody class="business-user-grid-error"><tr><th colspan="8">No data found in the server</th></tr></tbody>');
            }
        },
        "columnDefs": [
             {
                 "targets": 0,
                 "sClass": "mx-w-300",
             },
              {
                  "targets": 1,
                  "sClass": "width_130",
              },
               {
                   "targets": 2,
                   "sClass": "width_130",
               },
                {
                    "targets": 3,
                    // "sClass": "t-min-w_205",
                },
            {
                "targets": 4,
                "sClass": "t-min-w_115 user_min-width",
            },
            {
                "targets": 5,
                "orderable": false,
                "sClass": "pos-rel pad_right-50",
            },

        {
            "targets": 6,
            "orderable": false,
            "sClass": "pos-rel t-min-w_90",
        },

        {
            "targets": 7,
            "orderable": false,
            "sClass": "mx-w-120",
        }
        ],

        "initComplete": function (settings, json) {
            $("#divLoader").hide();
        },
        "fnDrawCallback": function () {
            initMadSelect($('.ddlAction').parent());
            initCarouselForAssignment($('div[id^="thumbcarousel_"]'));

            if ($('#viewAssignmentGrid tbody tr td').hasClass('dataTables_empty')) {
                $('#count').empty().text(0);
            }
            else {
                $('#count').empty().text($('#viewAssignmentGrid tbody tr').length);
            }
        }
    });
    $("#autocompleteViewSubmission").keyup(function () {
        dataTable.search($(this).val()).draw();
    });
}

function initCarouselForAssignment(containerEle) {
    if (containerEle && containerEle.length) {
        for (var j = 0; j < containerEle.length; j++) {
            var curContainerEle = $(containerEle[j]);
            var allGroups = curContainerEle.find('.carousel-inner .item:first .groupitem');
            var loopLimit = Math.ceil(allGroups.length / 4);
            for (var i = 1; i < loopLimit; i++) {
                var curItem = $('<div class="item"></div>').appendTo(curContainerEle.find('.carousel-inner'));
                curContainerEle.find('.carousel-inner .item:first .groupitem:gt(3)').filter(':lt(4)').remove().appendTo(curItem);
            }
            curContainerEle.find('.carousel-control').click(function () {
                setCarouselControl(curContainerEle, this);
            });
            if (allGroups.length)
                setCarouselControl(curContainerEle);
            else {
                curContainerEle.find('.right.carousel-control').hide();
                curContainerEle.find('.left.carousel-control').hide();
            }
        }
    }
}

//////////////////////////////////////////////////////////////////Edit Assignment//////////////////////////////////////////////////////////////
function onEditAssignment(AssignmentID) {
    resetAddCreateModal();
    OpenaddAssignmentModal(AssignmentID);
}

function LoadExistingKeywordsCustom(keywordInputEle, ExistingKeywordEle) {
    ExistingKeywordEle = $('.tagsinput[data-tagsinput-init]', $('#createAssignmentModalStep3'));
    if (_existingKeywordsData.length) {
        $.each(_existingKeywordsData, function (index, curData) {
            if ($.isPlainObject(curData)) {
                if (!ExistingKeywordEle.tagExist(curData.name.trim())) {
                    ExistingKeywordEle.addTag(curData.name.trim());
                    if (curData.value) {
                        if ($.isArray(_existingKeywords))
                            _existingKeywords.push(curData.value);
                        if ($('.tagsinput:not([data-tagsinput-init])', $('#createAssignmentModalStep3')).find('span.tag:last').length) {
                            $('.tagsinput:not([data-tagsinput-init])', $('#createAssignmentModalStep3')).find('span.tag:last').find('a[title]').attr('onclick', "removeKeyword(this,'E','" + curData.value + "')");
                        }
                    }
                }
            }
        });
    }
}
/////////////////////////////////////////////////////////////////////END//////////////////////////////////////////////////////////////////////

///////////////////////////Archive And Cancel////////////////////////////////////////////////
function archiveAssignment(id, name, status) {
    $('#removeConfirmationModalBox').show();
    if (status == 1) {
        $('#message').html('Are you sure you want to archive <strong>' + name + ' </strong>, while its status is <strong>In Progress</strong>?');
    }
    else {
        $('#message').html('Are you sure you want to archive <strong>' + name + ' </strong>?');
    }
    $('#submitAlert').attr('onclick', 'ArchiveSubmit(' + id + ')');
}
function ArchiveSubmit(id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/Submissions/ArchiveAssignment/" + id,
        dataType: "html",
        data: "",
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#removeConfirmationModalBox').hide();
                    window.location.reload();

                    $("#divLoader").hide();
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred. Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
function cancelAssignment(id, name, status) {
    $('#removeConfirmationModalBox').show();
    if (status == 1) {
        $('#message').html('Are you sure you want to cancel <strong>' + name + ' </strong>, while its status is <strong>In Progress</strong>?');
    }
    else {
        $('#message').html('Are you sure you want to cancel <strong>' + name + ' </strong>?');
    }
    $('#submitAlert').attr('onclick', 'CancleSubmit(' + id + ')');
}
function CancleSubmit(id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/Submissions/CancelAssignment/" + id,
        dataType: "html",
        data: "",
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#removeConfirmationModalBox').hide();
                    window.location.reload();

                    $("#divLoader").hide();
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred. Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
////////////////////////////////End/////////////////////////////////////////////////////////
