﻿var _newKeywords = '';
var _deletedKeywords = '';
function getKeywordMasterView() {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/KeywordMaster/getKeywordMasterView/",
        dataType: "html",
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#keywordMasterDiv').html(result);
                    $('#keywordMasterModalBox').modal('show');
                    $('#tags_1').tagsInput({ interactive: false });

                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
function AddKeyword() {
    var currentVal = $("#newKeyword_KeywordName").val().trim();
    var scriptPat = /[<>]/g

    if (currentVal == "") {
        showAlertBox("Information Alert Message", "Keyword name is required.", gInfo.alertMsgType.information);
        $("#newKeyword_KeywordName").val("");
        $("#newKeyword_KeywordName").focus();
        return false;
    }
    else if (scriptPat.test(currentVal)) {
        showAlertBox("Information Alert Message", "<,> characters are not allowed.", gInfo.alertMsgType.information)
        $("#newKeyword_KeywordName").val('');
    }

    else {
        if (!$('#tags_1').tagExist($("#newKeyword_KeywordName").val().trim())) {
            $('#tags_1').addTag($("#newKeyword_KeywordName").val().trim());
            if (_newKeywords != '') {
                _newKeywords += "Ω" + $("#newKeyword_KeywordName").val().trim();
            }
            else {
                _newKeywords = $("#newKeyword_KeywordName").val().trim();
            }
            $("#newKeyword_KeywordName").val("");
        }
        else {
            showAlertBox("Alert Message", "Already exists", gInfo.alertMsgType.success);
        }
    }
}

function removeKeyword(_keyword) {
    if (_deletedKeywords != '') {
        _deletedKeywords += "Ω" + _keyword;
    }
    else {
        _deletedKeywords = _keyword;

    }
}
function SaveKeywords() {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/KeywordMaster/SaveKeywords/",
        dataType: "json",
        data: JSON.stringify({ "addedKeywords": _newKeywords, "removedKeywords": _deletedKeywords }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    if (result.EnableError) {
                        showAlertBox("Failure Alert Message", result.ErrorMsg, gInfo.alertMsgType.failure);
                    } else if (result.EnableSuccess) {
                        if (result.SuccessMsg != "") {
                            var callbackFunc = function () {
                                $('#keywordMasterModalBox').modal('hide');
                                window.location.href = "/Teacher/Teacher/Dashboard";
                            }
                            showAlertBox("Success Alert Message", result.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
                        }
                        else
                        {
                            $('#keywordMasterModalBox').modal('hide');
                        }
                    }
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function closeKeywordModal()
{
    $("#divLoader").show();
    window.location.reload();
}