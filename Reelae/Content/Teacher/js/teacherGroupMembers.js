﻿/*Group Functionalities*/
var _serachedGroupStudent = '';
var _searchedGroupStudentName = '';
var groupMembersGroupBGC = ["sub_l_gry", "sub_l_green", "sub_l_blue"];
function validateCreateEditGroup() {
    var groupName = $('#txt_GroupName').val().trim();
    var groupId = $('#hdn_GroupID').val();
    if (!groupName) {
        showAlertBox("Information Alert Message", "Please Enter Group Name.", gInfo.alertMsgType.information);
        return false;
    }
    //$('#submitCreateGroup').trigger('click');
    $('#createeditGroupModalStep1').modal('hide');
    $('#createeditGroupModalStep2').modal('show');
    return true;
}

function validateCreateEditGroupStep2() {
    //var subjectID = $('#ddlSubjects').val();
    var subjectID = $('#ddlSubjectsSelValue').val();
    if (subjectID && parseInt(subjectID, 10) > 0) {
        $('#createeditGroupModalStep1 #hdn_SubjectID').val(subjectID);
    }
    else {
        $('#createeditGroupModalStep1 #hdn_SubjectID').val("");
        //showAlertBox("Information Alert Message", "Please Select Subject For Group Association.", gInfo.alertMsgType.information);
        //return false;
    }
    $('#submitCreateGroup').trigger('click');
    return true;
}

function OnSuccessCreateGroup(response) {
    if (!isSessionOutResponse(response)) {
        if (response.status.toLowerCase() == "success") {
            $('#createeditGroupModalStep1').modal('hide');
            $('#createeditGroupModalStep2').modal('hide');
            var callbackFunc = null;
            if ($('#createeditGroupModalStep1Label').attr('from').toLowerCase() == "addtogroup") {
                callbackFunc = function () {
                    addGroupToAddToGroupModal(response.Data.id, response.Data.name, response.Data.iName);
                }
            }
            else {
               
                callbackFunc = function () {
                    $("#divLoader").show();
                    window.location.href = "/Teacher/GroupMembers/Index?id=" + response.Data.id + "&cur_item=g";
                };
            }
            showAlertBox("Success Alert Message", "Group Saved Successfully.", gInfo.alertMsgType.success, callbackFunc);
            setContainersHeight();
        }
        else {
            $('#createeditGroupModalStep1').modal('hide');
            $('#createeditGroupModalStep2').modal('hide');
            showAlertBox("Failure Alert Message", response.msg, gInfo.alertMsgType.failure);
        }
    }
}

function OnFailureCreateGroup(response) {
    $('#createeditGroupModalStep1').modal('hide');
    showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later.", gInfo.alertMsgType.failure);
}

function searchGroup(_groupId, e) {
    if (e.keyCode == 13) {
        //e.preventDefault();
        // detect the enter key
        getsearchGroupStudent(_groupId);
        return false;
    }

}

function getsearchGroupStudent(_groupId) {
    if ($("#autocomplete_groups").val().trim() != _searchedGroupStudentName) {
        _serachedGroupStudent = $("#autocomplete_groups").val().trim();
    }
    if ($("#autocomplete_groups").val().trim() == "") {
        _serachedGroupStudent = "";
    }
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/GroupMembers/getsearchGroupStudent/",
        dataType: "html",
        data: JSON.stringify({ "id": _groupId, "Email": _serachedGroupStudent }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                $("#studentList").empty().html(result);
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function openGroupModal(id, _view, _count) {
    id = (id) ? id : 0;
    _view = (_view) ? _view : "";
    _count = (_count) ? _count : 0;
    var subjectID = ($('#SubjectId').val()) ? $('#SubjectId').val().trim() : "";
    if (!_view) {
        subjectID = "";
    }
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/GroupMembers/CreateEditGroupView/",
        dataType: "html",
        data: JSON.stringify({ "id": id, "view": _view, "subjectid": subjectID }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#createGroupContainer').html(result);

                    if (id && id != "0") {
                        $('#createeditGroupModalStep1Label').html("Edit Group");
                        $('#createeditGroupModalStep2Label').html("Edit Group");
                        if (_count > 0) {
                            initMadSelect($('#ddlSubjects').parent(), true);
                        }
                        else {
                            initMadSelect($('#ddlSubjects').parent());

                        }
                        //$('#ddlSubjects').prop('disabled', 'disabled');
                    }
                    else {
                        $('#createeditGroupModalStep1Label').html("New Group");
                        $('#createeditGroupModalStep2Label').html("New Group");
                        initMadSelect($('#ddlSubjects').parent());
                        $('#ddlSubjects li').first().trigger('click');
                        //$('#ddlSubjects').prop('disabled', false);
                    }
                    $('#createeditGroupModalStep1').modal('show');
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function setActiveGroups(_this) {
    var groupList = $('#ulGroups');
    $('#latestList>ul>li.active').removeClass('active');
    $('#archiveList>ul>li.active').removeClass('active');
    groupList.find('li.selected').removeClass('selected');
    $(_this).addClass('selected');
}

function getGroupStudents(_groupid, _groupName, _this) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/GroupMembers/getGroupStudents/",
        dataType: "html",
        data: JSON.stringify({ "id": _groupid }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    setActiveGroups(_this);
                    $('#pan_hide').html(result);
                    setContainersHeight();
                    if (_groupName.length > 34) {
                        $("#GroupName").html(_groupName.substring(0, 30) + "...");
                        $("#GroupName").attr("title", _groupName);
                    }
                    else {
                        $("#GroupName").html(_groupName);
                    }
                    initAutoComplete_group();

                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function initAutoComplete_group() {
    _serachedGroupStudent = '';
    _searchedGroupStudentName = '';
    $('#autocomplete_groups').autocomplete({
        lookup: arrayGroupStudents,
        onSelect: function (suggestion) {
            _serachedGroupStudent = suggestion.data;
            _searchedGroupStudentName = suggestion.value;
        }
    });
}

/*Add to Group*/
function openAddToGroupView(stuid, subid) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/GroupMembers/AddToGroupView/",
        dataType: "html",
        data: JSON.stringify({ "studentId": stuid, "subjectId": subid }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#addToGroupContainer').html(result);
                    initCarouselForGroups($('#groupLists'))
                    $('#hdn_StudentID').val(stuid);
                    $('#addToGroupModal').modal('show');
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function OnSelectGroup(_this) {
    var parent = $('#groupLists');
    parent.find('.groupitemselected .txt_light_blue').removeClass('txt_light_blue');
    parent.find('.groupitemselected .selImg').css('display', 'none');
    parent.find('.groupitemselected .mainImg').css('display', 'block');
    parent.find('.groupitemselected').removeClass('groupitemselected');
    $(_this).find('.subject_txt').addClass('txt_light_blue');
    $(_this).addClass('groupitemselected');
    $(_this).find('.selImg').css('display', 'block');
    $(_this).find('.mainImg').css('display', 'none');
}

function OnMultipleSelectGroup(_this) {
    //<div class="subject_sl sub_l_gry">IJ</div>
    var parent = $('#groupLists');
    if ($(_this).hasClass('groupitemselected')) {
        $(_this).find('.txt_light_blue').removeClass('txt_light_blue');
        $(_this).find('.selImg').css('display', 'none');
        $(_this).find('.mainImg').css('display', 'block');
        $(_this).removeClass('groupitemselected');
    }
    else {
        $(_this).find('.subject_txt').addClass('txt_light_blue');
        $(_this).addClass('groupitemselected');
        $(_this).find('.selImg').css('display', 'block');
        $(_this).find('.mainImg').css('display', 'none');
    }
}

function validateAddToGroupMember() {
    var groupList = $('#groupLists .groupitemselected', $('#addToGroupModal'));
    if (groupList.length) {
        var stuId = $('#hdn_StudentID').val();
        var groupIds = [];
        for (var i = 0; i < groupList.length; i++) {
            groupIds.push($(groupList[i]).find('.group_id').text());
        }
        MembersAddToGroup(stuId, groupIds.join());
    }
    else {
        showAlertBox("Information Alert Message", "Select atleast one group.", gInfo.alertMsgType.information);
    }
}

function MembersAddToGroup(stuid, groupIds) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/GroupMembers/AddToGroupMembers/",
        dataType: "json",
        data: JSON.stringify({ "stuid": stuid, "groupIds": groupIds }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result.status.toLowerCase() == "success") {
                    $('#addToGroupModal').modal('hide');
                    var callbackFunc = function () {
                        window.location.reload();
                    }
                    showAlertBox("Success Alert Message", "Member Assigned Successfully.", gInfo.alertMsgType.success, callbackFunc);
                }
                else {
                    showAlertBox("Failure Alert Message", result.msg, gInfo.alertMsgType.failure);
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function MemberRemoveFromGroup(stuid, groupid) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/GroupMembers/MemberRemoveFromGroup/",
        dataType: "json",
        data: JSON.stringify({ "stuId": stuid, "groupId": groupid }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result.status.toLowerCase() == "success") {
                    var callbackFunc = function () {
                        window.location.reload();
                    }
                    showAlertBox("Success Alert Message", "Member Unassigned Successfully.", gInfo.alertMsgType.success, callbackFunc);
                }
                else {
                    showAlertBox("Failure Alert Message", result.msg, gInfo.alertMsgType.failure);
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function MemberRemoveFromGroup(stuid, groupid, _this) {
    var groupName = $('#GroupName').html();
    if ($('#GroupName').attr('title')) {
        groupName = $('#GroupName').attr('title');
    }
    var memberName = $(_this).parents('.user_box').first().find('.user_name .memberName').text();
    var alertMessage = "Do you want to remove student <b>" + memberName + "</b> from group <b>" + groupName + "</b>";

    $('#modalDeactivationMemberAlert .popup_txt').html(alertMessage);
    $('#submitLink').attr('onclick', "InternalMemberRemoveFromGroup('" + stuid + "','" + groupid + "',this)")
    $('#modalDeactivationMemberAlert').modal('show');
}

function InternalMemberRemoveFromGroup(stuid, groupid, _this) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/GroupMembers/MemberRemoveFromGroup/",
        dataType: "json",
        data: JSON.stringify({ "stuId": stuid, "groupId": groupid }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (_this) {
                    $('#modalDeactivationMemberAlert').modal('hide');
                }
                if (result.status.toLowerCase() == "success") {
                    var callbackFunc = function () {
                        window.location.href = "/Teacher/GroupMembers/Index?id=" + groupid + "&cur_item=g";
                    }
                    showAlertBox("Success Alert Message", "Member Unassigned Successfully.", gInfo.alertMsgType.success, callbackFunc);
                }
                else {
                    showAlertBox("Failure Alert Message", result.msg, gInfo.alertMsgType.failure);
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function addGroupToAddToGroupModal(groupid, groupName, initialName) {

    if ($('#groupLists .carousel-inner .groupitem').length == 0)
        $('#groupLists .carousel-inner').html('');

    var nameHtml = '<span class="subject_txt">' + groupName + '</span>';
    if (groupName.trim().length > 20) {
        nameHtml = '<span class="subject_txt" title="' + groupName + '">' + (_groupName.substring(0, 17) + "...") + '</span>';
    }
    var lastitemLength = $('#groupLists .item:last').find('.groupitem').length;
    var curGroupBGColor = groupMembersGroupBGC[lastitemLength - 1];
    var initialNameHtml = "<div class='subject_sl " + curGroupBGColor + " mainImg'>" + initialName + "</div>";
    var a = '<div class="col-xs-4 text-center groupitem" style="cursor:pointer;" onclick="OnSelectGroup(this)">' + initialNameHtml + '<img src="/content/images/sub1.jpg" style="display:none; max-width:80px; max-height:80px; margin:0 auto;" class="img-responsive dis-in-img selImg"><div class="clearfix"></div>' + nameHtml + '<span style="display:none;" class="group_id">' + groupid + '</span></div>';
    if ($('#groupLists .item:last').find('.groupitem').length > 2) {
        var curItem = $('<div class="item"><div class="row"></div></div>').appendTo($('#groupLists .carousel-inner'));
        $(a).appendTo(curItem.find('.row'));
    }
    else {
        if ($('#groupLists .item:last .row').length == 0) {
            $('#groupLists .carousel-inner').append('<div class="item active"><div class="row"></div></div>');
        }
        $('#groupLists .item:last .row').append($(a));
    }
    addGroupToGroupMenuSection(groupid, groupName);
    setCarouselControl($('#groupLists'))
}

function addGroupToGroupMenuSection(groupid, groupName) {
    var _groupName = groupName;
    if (groupName.trim().length > 34) {
        _groupName = (_groupName.substring(0, 30) + "...") + '</span>';
    }
    var grouHtml = '<li title="' + groupName + '" style="cursor:pointer"><a onclick=""><i class="fa fa-folder-o" aria-hidden="true" ></i>' + _groupName + '<span class="badge bg_light pull-right">0</span></a></li>';
    var curGroupEle = $(grouHtml)
    curGroupEle.find('a').attr('onclick', 'getGroupStudents("' + groupid + '", "' + groupName.trim() + '", this)');
    if ($('#ulGroups li.noData').length)
        $('#ulGroups').html('');
    $('#ulGroups').append(curGroupEle);

}


//assign members
function resetDropdown() {

}

function getStudentsAddGroupView(groupId, subjectId) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/GroupMembers/getStudentsAddGroupView/",
        dataType: "html",
        data: JSON.stringify({ "groupid": groupId, "subjectid": subjectId }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#assignMemberGroupModalContainer').html(result);
                    // initAutoCompletAddGroupStudent();

                    $('#tblmembersTeacher').DataTable({
                        "searching": true,
                        "paging": false,
                        "info": false,
                        "order": [[1, "asc"]],
                        "scrollY": 350,
                        "scrollX": false,
                        language: {
                            search: "<i class='material-icons'>search</i> _INPUT_",
                            searchPlaceholder: "Search members..."
                        }
                    });

                    $('#assignMemberGroupModal').modal('show');
                }
                else {
                    showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later.", gInfo.alertMsgType.failure);
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function initAutoCompletAddGroupStudent() {
    _serachedGroupStudent = '';
    _searchedGroupStudentName = '';
    $('#autocompleteAddGroupStudent').autocomplete({
        lookup: arrayAddGroupStudents,
        onSelect: function (suggestion) {
            _serachedGroupStudent = suggestion.data;
            _searchedGroupStudentName = suggestion.value;
        }
    });
}

function getsearchAddGroupStudent(_groupId) {
    if ($("#autocompleteAddGroupStudent").val().trim() != _searchedGroupStudentName) {
        _serachedGroupStudent = $("#autocompleteAddGroupStudent").val().trim();
    }
    if ($("#autocompleteAddGroupStudent").val().trim() == "") {
        _serachedGroupStudent = "";
    }
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/GroupMembers/getsearchAddGroupStudent/",
        dataType: "html",
        data: JSON.stringify({ "id": _groupId, "Email": _serachedGroupStudent }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                $("#tblAddGroupStudent").empty().html(result);
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function searchAddGroupStudent(_groupId, e) {
    if (e.keyCode == 13) {
        //e.preventDefault();
        // detect the enter key
        getsearchAddGroupStudent(_groupId);
        return false;
    }

}

//validate and submit assign member to subjects
function onValidateAssignMember(_this) {
    if ($('#assignMemberGroupModal input[type="checkbox"]:checked').length > 0) {
        var groupId = $('#hdn_selGroupID').val();
        var chkSelMembers = $('#assignMemberGroupModal input[type="checkbox"]:not(.hdChk):checked');
        var studentsID = [];
        chkSelMembers.each(function () {
            studentsID.push($(this).parents('tr').first().find('.clsUserID').val());
        })
        AddMembersToGroup(studentsID.join(','), groupId);
        //$("#divLoader").show();
        //$('#btnAssignMemberGroupSubmit').trigger('click');
    }
    else
        return showAlertBox("Infomartion Alert Message", "Select atleast one member from list", gInfo.alertMsgType.information);
}


function AddMembersToGroup(stuIds, groupId) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/GroupMembers/AddMembersToGroup/",
        dataType: "json",
        data: JSON.stringify({ "studentIds": stuIds, "groupId": groupId }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result.status.toLowerCase() == "success") {
                    $('#assignMemberGroupModal').modal('hide');
                    var callbackFunc = function () {
                        window.location.href = "/Teacher/GroupMembers/Index?id=" + groupId + "&cur_item=g";
                    }
                    showAlertBox("Success Alert Message", "Member Assigned Successfully.", gInfo.alertMsgType.success, callbackFunc);
                }
                else {
                    showAlertBox("Failure Alert Message", result.msg, gInfo.alertMsgType.failure);
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}


function deleteGroupConfirmation(_groupId, _groupName) {
    var message = "Are you sure, do you want to remove <strong>" + _groupName + "</strong>.";
    $('#message').html(message);
    $('#removeConfirmationModalBox').modal('show');
    $('#submitAlert').attr("onclick", "deleteGroup('" + _groupId + "')");
}

function deleteGroup(_groupId) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/GroupMembers/deleteGroup/",
        dataType: "json",
        data: JSON.stringify({ "id": _groupId }),
        async: true,
        success: function (result) {
            $("#divLoader").hide();
            if (!isSessionOutResponse(result)) {
                if (result) {
                    if (result.EnableSuccess) {
                        var callbackFunc = function () {
                            $('#removeConfirmationModalBox').modal('hide');
                            $("#divLoader").show();
                            window.location.reload()
                        }
                        showAlertBox("Success Alert Message", result.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
                    }
                    else {
                        showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);

                    }

                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

//
/**/