﻿function assignmentClick(assignmentId) {
    GetKeyWordGraphDataChart(assignmentId);
}

function GetKeyWordGraphDataChart(assignmentId) {
    $.ajax({
        type: "POST",
        url: "../Assignment/GetKeyWordGraphData",
        data: JSON.stringify({ "assignmentId": assignmentId }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        processdata: true,
        success: function (result) {
            var json = result;
            var _studentNames = [];
            var _keywordCount = [];

            $.each(json, function (arrayID, group) {
                _studentNames.push(group.Name);
                _keywordCount.push(parseInt(group.KeywordCount));
            });

            var colors = ['#c42525', '#a6c96a'];
            // create the chart
            var chart = new Highcharts.chart('container', {
                credits: {
                    enabled: false
                },
                chart: {
                    //  type: 'line'
                    borderColor: '#EBBA95',
                    borderWidth: 2,
                    type: 'column',//'column'
                },
                title: {
                    text: 'Keywords Used'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: _studentNames,
                    font: 'bold 16px "Trebuchet MS", Verdana, sans-serif',

                    labels: {
                        useHTML: true,
                        formatter: function () {
                            var url = findElement(json, "Name", this.value)["UserPicURL"];
                            var picUrl = url == null ? '/Content/images/student.jpg' : url.replace('~', '');
                            return "<img src='" + picUrl + "'  style='width:35px;height:35px;text-align:center;display: block;margin-left: auto; margin-right: auto;' alt='...' class='img-circle mCS_img_loaded' />" + this.value;
                        }
                    }
                },
                yAxis: {
                    tickInterval: 1,
                    title: {
                        text: 'Keyword Count',
                    },
                },
                plotOptions: {
                    columnrange: {
                        colorByPoint: true,
                        colors: ['red', 'blue', 'yellow']
                    },
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    },
                },
                series: [{
                    name: 'Keyword Count',
                    data: _keywordCount,
                    //color: colors,
                    x: -10,
                    dataLabels: {
                        align: 'high',
                        enabled: true
                    },
                    pointWidth: 28,
                    plotOptions: {

                        columnrange: {
                            colorByPoint: true,
                            colors: ['red', 'blue', 'yellow']
                        }
                    }
                }]
                ,
                tooltip: {
                    formatter: function () {

                        var keyWordSummary = findElement(json, "Name", this.x)["KeywordSummary"].replace(';', '<br>');

                        var s = '<b>' + this.x + '</b>' + '<br>' + keyWordSummary;
                        return s;
                    },
                    shared: true
                },
            });
        }
    });
}
function findElement(arr, propName, propValue) {
    for (var i = 0; i < arr.length; i++)
        if (arr[i][propName] == propValue)
            return arr[i];
    // will return undefined if not found; you could return a default instead
}
//Resource Section Chart
function getResourceActivityGraphData(resourceId) {
    $.ajax({
        type: "POST",
        url: "/Resource/GetResourceActivityGraphData",
        data: JSON.stringify({ "resourceId": resourceId }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        processdata: true,
        success: function (result) {
            var json = result;
            var _studentNames = [];
            var _activityStatus = [];

            $.each(json, function (arrayID, group) {
                _studentNames.push(group.StudentName);
                _activityStatus.push(parseInt(group.ActivityCount));
            });
            var activityLabels = ["", "View", "Download", "View & Download"];
            // create the chart
            var chart = new Highcharts.chart('container', {
                credits: {
                    enabled: false
                },
                chart: {
                    //  type: 'line'
                    borderColor: '#EBBA95',
                    borderWidth: 2,
                    type: 'column',
                },
                title: {
                    text: 'Student Resource Activity'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: _studentNames,
                    font: 'bold 16px "Trebuchet MS", Verdana, sans-serif',
                    labels: {
                        useHTML: true,
                        formatter: function () {
                            return "<img src='" + findElement(json, "StudentName", this.value)["StudentPicUrl"] + "'  style='width:35px;height:35px;text-align:center;display: block;margin-left: auto; margin-right: auto;' alt='...' class='img-circle mCS_img_loaded' />" + this.value;
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    max: 3,
                    tickInterval: 1,
                    title: {
                        text: 'Activity',
                    },
                    labels: {
                        useHTML: true,
                        formatter: function () {
                            return activityLabels[this.value];
                        }
                    },
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    },
                },
                series: [{
                    //showInLegend: false,     
                    name: 'Activity Status',
                    data: _activityStatus,
                    x: -10,
                    dataLabels: {
                        align: 'high',
                        enabled: false
                    },
                    pointWidth: 28
                }],
                tooltip: {
                    formatter: function () {
                        var s = '<b>' + this.x + '</b>' + '<br>' + activityLabels[this.y];
                        return s;
                    },
                    shared: true
                },
            });
        }
    });
}
//resource graph visit by Student
function getResourceVisitByStudentGraphData(subjectId) {
    $.ajax({
        type: "POST",
        url: "/Resource/GetResourceVisitGraphData",
        data: JSON.stringify({ "subjectId": subjectId }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        processdata: true,
        success: function (result) {
            var json = result;
            var _studentNames = [];
            var _spendTime = [];

            $.each(json, function (arrayID, group) {
                _studentNames.push(group.StudentName);
                _spendTime.push(parseInt(group.SpendTime));
            });

            var colors = ['#c42525', '#a6c96a'];
            // create the chart
            var chart = new Highcharts.chart('container', {
                credits: {
                    enabled: false
                },
                chart: {
                    //  type: 'line'
                    borderColor: '#EBBA95',
                    borderWidth: 2,
                    type: 'column',//'column'
                },
                title: {
                    text: 'Spend Time'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: _studentNames,
                    font: 'bold 16px "Trebuchet MS", Verdana, sans-serif',
                    labels: {
                        useHTML: true,
                        formatter: function () {
                            return "<img src='" + findElement(json, "StudentName", this.value)["StudentPicUrl"] + "'  style='width:35px;height:35px;text-align:center;display: block;margin-left: auto; margin-right: auto;' alt='...' class='img-circle mCS_img_loaded' />" + this.value;
                        }
                    }
                },
                yAxis: {
                    //tickInterval: 1,
                    title: {
                        text: 'Minutes',
                    },
                },
                plotOptions: {
                    columnrange: {
                        colorByPoint: true,
                        colors: ['red', 'blue', 'yellow']
                    },
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    },
                },
                series: [{
                    name: 'Spend Time',
                    data: _spendTime,
                    //color: colors,
                    x: -10,
                    dataLabels: {
                        align: 'high',
                        enabled: true
                    },
                    pointWidth: 28,
                    plotOptions: {
                        columnrange: {
                            colorByPoint: true,
                            colors: ['red', 'blue', 'yellow']
                        }
                    }
                }]
            });
        }
    });
}
//Attendance chart 
function getAttendenceDataChart(type) {
    $.ajax({
        type: "POST",
        url: "/Teacher/Teacher/GetAnalyticsAttendanceData",
        data: JSON.stringify({ "subjectId": $("#ddlSubject").find(".selected").attr("data-value"), "_startDate": null, "_endDate": null, "type": type }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        processdata: true,
        success: function (result) {
            var json = result;
            var _studentNames = [];
            var _keywordCount = [];
        
            var colors = ['#c42525', '#a6c96a'];
            var maxValue = 100;
            var chartTitle = 'Attendance Percentage';
            var chartHeading = 'Student Attendance Percentage';
            var interval = 10;
         
            if (type == 'ParticipationRating') {
                maxValue = 5;
                chartTitle = 'Participation Rating';
                chartHeading = 'Student Participation Rating';
                interval = 0.5;

                $.each(json, function (arrayID, group) {
                    _studentNames.push(group.Name);
                    _keywordCount.push(parseFloat(group.Avgrating));
                });
            }
            else {
                $.each(json, function (arrayID, group) {
                    _studentNames.push(group.Name);
                    _keywordCount.push(parseInt(group.attendenceperct));
                });
            }
            // create the chart
            var chart = new Highcharts.chart('attendenceContainer', {
                credits: {
                    enabled: false
                },
                chart: {
                    //  type: 'line'
                    borderColor: '#EBBA95',
                    borderWidth: 2,
                    type: 'column',
                },
                title: {
                    text: chartHeading
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: _studentNames,
                    font: 'bold 16px "Trebuchet MS", Verdana, sans-serif',

                    labels: {
                        useHTML: true,
                        formatter: function () {
                            var url=findElement(json, "Name", this.value)["UserPicUrl"];
                            var picUrl = url == null ? '/Content/images/student.jpg' : url;
                            return "<img src='" + (picUrl).replace('~', '') + "'  style='width:35px;height:35px;text-align:center;display: block;margin-left: auto; margin-right: auto;' alt='...' class='img-circle mCS_img_loaded' />" + this.value;
                        }
                    }
                },
                yAxis: {
                    max: maxValue,
                    tickInterval: interval,
                    title: {
                        text: chartTitle,
                    },
                },
                plotOptions: {
                    columnrange: {
                        colorByPoint: true,
                        colors: ['red', 'blue', 'yellow']
                    },
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    },
                },
                series: [{
                    name: chartTitle,
                    data: _keywordCount,
                    x: -10,
                    dataLabels: {
                        align: 'high',
                        enabled: true
                    },
                    pointWidth: 28,
                    plotOptions: {
                        columnrange: {
                            colorByPoint: true,
                            colors: ['red', 'blue', 'yellow']
                        }
                    }
                }]
            });
        }
    });
}