﻿function onUserSelection(_this) {
    var userRole = $(_this).data('value');
    var subjectId = $("#SubjectId").val();
    if (userRole && subjectId && !isNaN(subjectId)) {
        OpenaddUserModal(userRole, subjectId);
    }
    else {
        alert("Some Problem Occur");
    }
}
function OpenaddUserModal(userRole, subjectId) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Admin/GetAddUserView/" + userRole,
        dataType: "html",
        data: JSON.stringify({ "role": userRole, "_subjectId": subjectId }),
        async: false,
        success: function (result) {
            if (result) {
                $('#addUserContainer').html(result);
                resetAddUserModal();
                $('#addUserModalLabel').html("Add " + userRole);
                $('#userRole').val(userRole);
                $('#subjectId').val(subjectId);
                $('#addUserModal').modal('show');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            return false;
        }
    });
}

function OnSuccessAddUser(result) {
    if (result && result.hasOwnProperty("status") && result.status.toLowerCase() == "success") {
        alert(result.message);
        $('#addUserModal').modal("hide");
        resetAddUserModal();
    }
    else {
        alert(result);
    }
}

function OnFailureAddUser(result) {
    alert('failure');
}

function OnBeginAddUser() {

}
function resetAddUserModal() {
    $('#userEmail').val('');
    $('#userPassword').val('');
    $('#userConPassword').val('');
    $('#userName').val('');
    $('#userRole').val('');
    $('#subjectId').val('');
    $('#addUserModalLabel').html('');
}
function validateAddUser() {
    var dataToSend = { "Email": $('#userEmail').val(), "Name": $('#userName').val(), "Password": $('#userPassword').val(), "Role": $('#userRole').val(), "ConPassword": $('#userConPassword').val(), "subjectID": $('#subjectId').val() }
    if (!dataToSend.Email) {
        alert("Email Id is incorrect");
        return false;
    }
    if (!dataToSend.Name) {
        alert("Name Is Required");
        return false;
    }
    if (!dataToSend.Password) {
        alert("Password Is Required");
        return false;
    }
    if (dataToSend.Password != dataToSend.ConPassword) {
        alert("Password And Confirm Password Is not matching");
        return false;
    }
    if (!dataToSend.Role) {
        alert('Some Problem Is There');
        return false;
    }
    if (!dataToSend.Role) {
        alert('Some Problem Is There');
        return false;
    }
    if (!dataToSend.subjectID) {
        alert('Some Problem Is There');
        return false;
    }
    $('#submitAddUser').trigger('click');
    return true;
}

//register click on ddlAddUser


