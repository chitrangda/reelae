﻿/*global Variable*/
var gInfo = {};
gInfo.alertMsgType = { success: "success", failure: "failure", information: "info" };

/*these function for setting scroll dynamically and set height dynamically*/
function returnPageHeight() {
    var totalPageHeight = $('.pageContainer').height();
    var navigationMenuHeight = $('.top_nav .nav_menu').height();
    var remainingHeight = totalPageHeight - navigationMenuHeight;
    return remainingHeight;
}
function returnSubjectContainerWidth() {
    var totalPageHeight = $('.pageContainer').height();
    var navigationMenuHeight = $('.top_nav .nav_menu').height();
    var remainingHeight = totalPageHeight - navigationMenuHeight;
    var teacherStudentMenu = $('.main_menu2 .menu_section').first().height();
    return remainingHeight - teacherStudentMenu;
}
//call on load or any action
function setContainersHeight() {
    $('#studentList').css('height', (returnPageHeight() - $('#studentList').prev().height() - 30));
    //var subContentHeight = returnSubjectContainerWidth() - 130;
    var subContentHeight = returnPageHeight() - 150;
    var halfSharedHeight = subContentHeight / 2;
    $('.main_menu2 .scrollPart:eq(1) div[data-simplebar="init"]').css('height', halfSharedHeight);

    //for subject section
    $('.main_menu2 .scrollPart:eq(0) div[data-simplebar="init"]').css('height', halfSharedHeight);
    if (halfSharedHeight < $('#ulSubjects').height()) {
        $('#ulSubjects span.badge').addClass('right_0');
    }
    else {
        $('#ulSubjects span.badge.right_0').removeClass('right_0');
    }

    //for group section
    $('.main_menu2 .menu_section:eq(1) div[data-simplebar="init"]').css('height', halfSharedHeight);
    if (halfSharedHeight < $('#ulGroups').height()) {
        $('#ulGroups span.badge').addClass('right_0');
    }
    else {
        $('#ulGroups span.badge.right_0').removeClass('right_0');
    }
}
/*End */
/*Generic Popup Alert Functionality*/
function showAlertBox(HeaderMsg, BodyMsg, alertType, callback) {
    var alertEle = $('#genericAlertBox');
    if (!alertEle.length) {
        alert("Sorry! Generic Alert Box is not available");
        return false;
    }
    switch (alertType) {
        case "success":
            alertType = "alert-success";
            break;
        case "failure":
            alertType = "alert-danger";
            break;
        case "info":
            alertType = "alert-info"
            break;
        default:
            alertType = "alert-info";
            break;
    }
    $('.modal-header', alertEle).removeClass("alert-success alert-danger alert-info");
    $('.modal-header', alertEle).addClass(alertType);
    HeaderMsg = HeaderMsg || "Alert Message";
    BodyMsg = BodyMsg || "No messages to show";
    $('#genericAlertBoxModalLabel', alertEle).html(HeaderMsg);
    $('.popup_txt', alertEle).html(BodyMsg);
    if (callback && $.isFunction(callback))
        $("#btnCloseModal").one('click', callback);
    alertEle.modal('show');
}
/*End Generic Popup Alert Functionality*/
/*Check sessionOut Respose*/
function isSessionOutResponse(result) {
    var typeOf = typeof result;
    if (result) {
        if (!$.isPlainObject(result)) {
            try {
                result = JSON.parse(result);
            } catch (e) {
                result = {};
            }
        }
    }
    else
        result = {};
    if (result.hasOwnProperty("LogOutKey") && result.LogOutKey.toLowerCase() == "sessionout") {
        $('#divLoader').hide();
        var callbackFunction = function () { location.href = gInfo.LogoutUrl; }
        showAlertBox("Information Alert Message", "Oops!! Your session has been expired.You are going to redirect to the login page.", gInfo.alertMsgType.information, callbackFunction);
        //location.href = gInfo.LogoutUrl;
        return true;
    }
    else
        return false;
}
/*End Check sessionOut Respose*/
//Alert in case of harmfull input
function onChangeValidate(_this) {
    var currentVal = $(_this).val();
    var scriptPat = /[<>]/g
    if (scriptPat.test(currentVal)) {
        //alert('<,> are not allowed .It can be harmfull.');
        showAlertBox("Information Alert Message", "<,> characters are not allowed.", gInfo.alertMsgType.information)
        $(_this).val('');
    }
}
//et initial date pickers
function initDatePickers() {
    initSignalDatePicker($("#txt_SubjectStartDate"));
    initSignalDatePicker($("#txt_SubjectEndDate"));
}
function initSignalDatePicker(containerEle) {
    containerEle.daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        drops: "up",
        locale: {
            format: 'MM/DD/YYYY'
        }
    },
    function (start, end, label) {
        //var curDate = new Date(start);
        containerEle.data('assignSD', start.format('MM/DD/YYYY'));
    });

    containerEle.on("apply.daterangepicker", function (a, b) {
        console.log("apply event fired, start/end dates are " + b.startDate.format("MMMM D, YYYY") + " to " + b.endDate.format("MMMM D, YYYY"))
        //customize
        containerEle.data('assignSD', b.startDate.format('MM/DD/YYYY'));
        containerEle.data('assignED', b.endDate.format('MM/DD/YYYY'));
    });
}

function init_daterangepicker_rangeCustom(ContainerEle, callBack, cancelCallBack) {
    if ("undefined" != typeof $.fn.daterangepicker && ContainerEle.length > 0) {
        //if (!objRangeOption) {
        //    objRangeOption = {
        //        Today: [moment(), moment()],
        //        Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
        //        "Last 7 Days": [moment().subtract(6, "days"), moment()],
        //        "Last 30 Days": [moment().subtract(29, "days"), moment()],
        //        "This Month": [moment().startOf("month"), moment().endOf("month")],
        //        "Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
        //    }
        //}
        console.log("init_daterangepicker_range");
        var a = function (a, b, c) {
            console.log(a.toISOString(), b.toISOString(), c), ContainerEle.find('span').html(a.format("MMMM D, YYYY") + " - " + b.format("MMMM D, YYYY"))
        },
            b = {
                startDate: moment().subtract(10, "days"),
                endDate: moment(),
                minDate: "01/01/2001",
                maxDate: "12/31/2020",
                dateLimit: {
                    days: 60
                },
                showDropdowns: !0,
                showWeekNumbers: !0,
                timePicker: !1,
                timePickerIncrement: 1,
                timePicker12Hour: !0,
                ranges: {
                    Today: [moment(), moment()],
                    Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
                    "Last 7 Days": [moment().subtract(6, "days"), moment()],
                    "Last 30 Days": [moment().subtract(29, "days"), moment()],
                    "This Month": [moment().startOf("month"), moment().endOf("month")],
                    "Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
                },
                opens: "left",
                buttonClasses: ["btn btn-default"],
                applyClass: "btn-small btn-primary",
                cancelClass: "btn-small",
                format: "MM/DD/YYYY",
                separator: " to ",
                locale: {
                    applyLabel: "Submit",
                    cancelLabel: "Clear",
                    fromLabel: "From",
                    toLabel: "To",
                    customRangeLabel: "Custom",
                    daysOfWeek: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
                    monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                    firstDay: 1,
                    format: "MM/DD/YYYY"
                }
            };
        ContainerEle.find('span').html(moment().subtract(10, "days").format("MMMM D, YYYY") + " - " + moment().format("MMMM D, YYYY")), ContainerEle.daterangepicker(b, a), ContainerEle.on("show.daterangepicker", function () {
            console.log("show event fired")
        }), ContainerEle.on("hide.daterangepicker", function () {
            console.log("hide event fired")
        }), ContainerEle.on("apply.daterangepicker", function (a, b) {
            if (!b) {
                b = ContainerEle.data('daterangepicker')
                ContainerEle.find('span').html(b.startDate.format("MMMM D, YYYY") + " - " + b.endDate.format("MMMM D, YYYY"))
            }
            else {
                console.log("apply event fired, start/end dates are " + b.startDate.format("MMMM D, YYYY") + " to " + b.endDate.format("MMMM D, YYYY"))
                //customize
                ContainerEle.data('assignSD', b.startDate.format("MM/DD/YYYY"));
                ContainerEle.data('assignED', b.endDate.format("MM/DD/YYYY"));
                if (callBack && $.isFunction(callBack)) {
                    callBack(b.startDate, b.endDate);
                }
            }

        }), ContainerEle.on("cancel.daterangepicker", function (a, b) {
            console.log("cancel event fired");
            ContainerEle.data('assignSD', moment().subtract(10, "days").format("MM/DD/YYYY"));
            ContainerEle.data('assignED', moment().format("MM/DD/YYYY"));
            if (cancelCallBack && $.isFunction(cancelCallBack)) {
                cancelCallBack();
            }
        }), $("#options1").click(function () {
            ContainerEle.data("daterangepicker").setOptions(b, a)
        }), $("#options2").click(function () {
            ContainerEle.data("daterangepicker").setOptions(optionSet2, a)
        }), $("#destroy").click(function () {
            ContainerEle.data("daterangepicker").remove()
        })
    }
}


function initCarouselForGroups(containerEle) {
    if (containerEle) {
        var allGroups = containerEle.find('.carousel-inner .item:first .groupitem');
        var loopLimit = Math.ceil(allGroups.length / 3);
        for (var i = 1; i < loopLimit; i++) {
            var curItem = $('<div class="item"><div class="row"></div></div>').appendTo(containerEle.find('.carousel-inner'));
            containerEle.find('.carousel-inner .item:first .groupitem:gt(2)').filter(':lt(3)').remove().appendTo(curItem.find('.row'));
        }
        containerEle.find('.carousel-control').click(function () {
            setCarouselControl(containerEle, this);
        });
        if (allGroups.length)
            setCarouselControl(containerEle);
        else {
            containerEle.find('.right.carousel-control').hide();
            containerEle.find('.left.carousel-control').hide();
        }
    }
}

function setCarouselControl(containerEle, _leftOrRight) {
    if (containerEle) {
        var totalItems = containerEle.find('.carousel-inner .item');
        var curactiveItem = totalItems.filter('.active');
        var itemindex = curactiveItem.index();
        if (_leftOrRight) {
            if ($(_leftOrRight).hasClass('left'))
                itemindex--;
            else
                itemindex++;
        }
        containerEle.find('.right.carousel-control').show();
        containerEle.find('.left.carousel-control').show();
        if (itemindex == 0) {
            containerEle.find('.left.carousel-control').hide();
        }
        if (itemindex == totalItems.length - 1) {
            containerEle.find('.right.carousel-control').hide();
        }
    }
}

function onAssignMemberHeaderCheckChange(_this) {
    var parentEle = $(_this).parents(".dataTables_scroll").first().find('.dataTables_scrollBody table');//$('.dataTables_scrollBody table').first();
    //$(_this).parents("table").first();    
    if ($(_this).is(':checked'))
        parentEle.find('input[type="checkbox"]').prop('checked', 'checked');
    else
        parentEle.find('input[type="checkbox"]').prop('checked', false);
}

function onAssignMemberChildCheckChange(_this, id) {
    var parentEle = $(_this).parents(".dataTables_scroll").first().find('.dataTables_scrollBody table'); //$(_this).parents(".dataTables_scrollBody table").first();
    var totalCheckbox = parentEle.find('input[type="checkbox"]:not(.hdChk)').length;
    var totalCheckedCheckbox = parentEle.find('input[type="checkbox"]:not(.hdChk):checked').length;
    if (totalCheckbox > totalCheckedCheckbox)
        $(_this).parents(".dataTables_scroll").first().find('.dataTables_scrollHead table').find('input[type="checkbox"].hdChk').prop('checked', false);//parentEle.find('input[type="checkbox"].hdChk').prop('checked', false);
    else
        $(_this).parents(".dataTables_scroll").first().find('.dataTables_scrollHead table').find('input[type="checkbox"].hdChk').prop('checked', 'checked');//parentEle.find('input[type="checkbox"].hdChk').prop('checked', 'checked');
}

//new function for Assigning members
function onClickSubmitButton(e) {
    var selectedItem = [];
    var parentEle = $('#tblmembers').first();
    var res = parentEle.find('input[type="checkbox"]:not(.hdChk):checked');
    $(res).each(function (index, value) {
        var valueItem = value.value;
        if ($.isNumeric(valueItem)) {
            selectedItem.push(value.value);
        }
    });
    $('#hdn_selectedItem').val(selectedItem);
    if (selectedItem.length <= 0) {
        e.preventDefault();
        return showAlertBox("Infomartion Alert Message", "Select atleast one member from list", gInfo.alertMsgType.information);
    }
}

//file uploading file type validation
var validExtentions = ["png", "jpg", "jpeg", "mp3", "mp4", "wmv", "avi", "gif", "tif", "zip", "xls", "xlsx", "doc", "docx", "pdf", "txt", "rtf", "rar", "flv", "acc"];
function isValidExtention(inputfile) {
    if (!inputfile) {
        showAlertBox("Failure Alert Message", "Its is not a valid type of file.", gInfo.alertMsgType.information);
        return false;
    }
    var filePart = inputfile.name.trim().split('.');
    if (filePart.length < 2) {
        showAlertBox("Failure Alert Message", "Its is not a valid type of file.", gInfo.alertMsgType.information);
        return false;
    }
    var fileExtension = filePart[filePart.length - 1];
    if (inputfile && validExtentions.indexOf(fileExtension.trim()) > -1) {
        return true;
    }
    else {
        showAlertBox("Failure Alert Message", "Its is not a valid type of file.", gInfo.alertMsgType.information);
        return false;
    }
}