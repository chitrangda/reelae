﻿$(document).ready(function () {
    init_daterangepicker_rangeCustom($('#dateFilterEle'), OnSelectFilerDate, onDateClear);
    //initAttenDatePicker($('#attendanceDate'));
    $("#ddlSubject li:first").trigger("click");
    $('#ddlGroups').children().first().trigger('click');
})

function OnSelectFilerDate(startDate, endDate) {
    //console.log("OnSelect: " + startDate.format('MM/DD/YYYY') + ":" + endDate.format('MM/DD/YYYY'));
    getDashboardCounts(startDate.format('MM/DD/YYYY'), endDate.format('MM/DD/YYYY'));
    getAttedenceRate($("#ddlSubject").find(".selected").attr("data-value"), startDate.format('MM/DD/YYYY'), endDate.format('MM/DD/YYYY'));
}

function onDateClear() {
    getDashboardCounts(null, null);
    getAttedenceRate($("#ddlSubject").find(".selected").attr("data-value"), null, null);
}

function getDashboardCounts(_start, _end) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/Teacher/getDashboardCounts/",
        dataType: "html",
        data: JSON.stringify({ "_startDate": _start, "_endDate": _end }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $("#divDashboardCounts").empty().html(result);
                    $("#divLoader").hide();

                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });

}

function getAttedenceRate(_subjectId, _start, _end) {
    $("#divLoader").show();
    if (!_start && !_end) {
        $("#dateFilterEle").data('daterangepicker').setStartDate(moment().subtract(10, "days").format("MM/DD/YYYY"));
        $("#dateFilterEle").data('daterangepicker').setEndDate(moment().format("MM/DD/YYYY"));
        $("#dateFilterEle").trigger('apply.daterangepicker');
    }
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/Teacher/getAttedenceRate/",
        dataType: "html",
        data: JSON.stringify({ "id": _subjectId, "_startDate": _start, "_endDate": _end }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $("#divAttendenceRate").empty().html(result);
                    if ($("#hdnStudentcount").val() != "0") {
                        $(".progress .progress-bar").progressbar();
                        $('#tblAttendance').DataTable({
                            "searching": false,
                            "paging": false,
                            "info": false,
                            "order": [[1, "asc"]],
                            "scrollY": '40vh',
                            "scrollX": false
                        });
                    }
                    $("#divAttendenceRate").mCustomScrollbar({
                        theme: "dark-3",
                        autoHideScrollbar: true
                    });
                    $("#divLoader").hide();

                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function setRating(_id, _rate) {
    _id = "#" + _id;
    $(_id).rateYo({
        numStars: 5,
        rating: _rate,
        starWidth: "20px",
        halfStar: true,
        readOnly: true
    });
}

function initPresentCheckbox(_id, _mark) {
    _id = "#" + _id;
    $(_id).iCheck({
        checkboxClass: "icheckbox_flat-green",
        labelHover: false,
    });
    if (_mark == "P") {
        $(_id).iCheck('check');
    }

}

function initAttenDatePicker(containerEle) {
    containerEle.daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        drops: "up",
        locale: {
            format: 'MM/DD/YYYY'
        }
    },
    function (start, end, label) {
        //var curDate = new Date(start);
        containerEle.data('assignSD', start.format('MM/DD/YYYY'));
        if ($("#selSubjectID").val()) {
            getAttedenceRate($("#selSubjectID").val());
        }
    });

    containerEle.on("apply.daterangepicker", function (a, b) {
        console.log("apply event fired, start/end dates are " + b.startDate.format("MMMM D, YYYY") + " to " + b.endDate.format("MMMM D, YYYY"))
        //customize
        containerEle.data('assignSD', b.startDate.format('MM/DD/YYYY'));
        containerEle.data('assignED', b.endDate.format('MM/DD/YYYY'));
    });
}

/////////////////////Assignment DDL////////////////////////////////////////////////////////
function groupItemClick(groupID) {
    binAssignmentDDL(groupID);
}
function binAssignmentDDL(groupId) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/Teacher/GetAssignmentListDDL/",
        dataType: "html",
        data: JSON.stringify({ "groupId": groupId }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#assignmentDDL').empty().html(result);
                    initMadSelect($('#ddlAssignment').parent());
                    $("#divLoader").hide();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
//////////////////////////END/////////////////////////////////////////////////////////////
//////////////////////Attandance Graph Functions//////////////////////////////////////////
function analyticsAttendanceClick() {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/Teacher/GetAnalyticsAttendanceGraph/",
        dataType: "html",
        data: JSON.stringify({ "subjectId": 1 }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#divAddSubjectResource').empty();
                    $('#divAddSubjectResource').html(result);
                    $('#analyticsAttendanceGraph').modal('show');
                    $("#divLoader").hide();
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occured.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
///////////////////////////END///////////////////////////////////////////////////////////