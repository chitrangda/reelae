﻿var _serachedStudent = '';
var _searchedStudentName = '';
function getSubjectStudents(_subjectId, _subjectName, _this, _type,e) {
    if (_this === e.target) {
        $("#divLoader").show();
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Teacher/GroupMembers/getSubjectStudents/",
            dataType: "html",
            data: JSON.stringify({ "id": _subjectId }),
            async: true,
            success: function (result) {
                if (!isSessionOutResponse(result)) {
                    if (result) {
                        $('#pan_hide').html(result);
                        setContainersHeight();
                        $(_this).parent().find("li").removeClass('active');
                        $(_this).parent().addClass('active');
                        setActiveSubjects(_this);
                        if (_subjectName.length > 34) {
                            $("#SubjectName").html(_subjectName.substring(0, 30) + "...");
                            $("#SubjectName").attr("title", _subjectName);
                        }
                        else {
                            $("#SubjectName").html(_subjectName);
                        }
                        $("#SubjectId").val(_subjectId);
                        initAutoComplete();
                        if (_type == 'l') {
                            initMadSelect($('#ddlAdd').parent());
                        }
                        else {
                            initMadSelect($('#ddlAdd').parent(), true);

                        }
                        $("#divLoader").hide();

                    }
                }
                $("#divLoader").hide();
                if (archiveButtonClick == "Yes") {
                    $('.text-center').find('.t_group').remove();
                    $('#addStudent a').hide();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#divLoader").hide();
                showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
                return false;
            }
        });
    }
}

function setActiveSubjects(_this) {
    var subjectsList = $('#latestList>ul,#archiveList>ul');
    $('#ulGroups li.selected').removeClass('selected');
    subjectsList.find('li.active').removeClass('active');
    $(_this).parents('li').first().addClass('active');
}

function getStudentProfile(id) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/GroupMembers/getStudentProfile/",
        dataType: "html",
        data: JSON.stringify({ "id": id }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#studentProfileDiv').html(result);
                    $('#studentProfileModalBox').modal('show');
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function getAddSubjectView(_subjectId) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/GroupMembers/getAddSubjectView/",
        data: JSON.stringify({ "id": _subjectId }),
        dataType: "html",
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#addSubjectDiv').html(result);
                    initDatePickers();
                    if ($('#hdn_SubjectStartDate').val())
                        $("#txt_SubjectStartDate").data('daterangepicker').setStartDate(new Date($('#hdn_SubjectStartDate').val()));
                    else
                        $("#txt_SubjectStartDate").data('daterangepicker').setStartDate(new Date());
                    if ($('#hdn_SubjectEndDate').val())
                        $("#txt_SubjectEndDate").data('daterangepicker').setStartDate(new Date($('#hdn_SubjectEndDate').val()));
                    else
                        $("#txt_SubjectEndDate").data('daterangepicker').setStartDate(new Date());
                    $("#txt_SubjectStartDate").data('assignSD', $("#txt_SubjectStartDate").data('daterangepicker').startDate.format("MM/DD/YYYY"));
                    $("#txt_SubjectEndDate").data('assignSD', $("#txt_SubjectEndDate").data('daterangepicker').startDate.format("MM/DD/YYYY"));
                    $('#SubjectModelBox').modal('show');
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function onSubjectSucess(response) {
    $("#divLoader").hide();
    if (!isSessionOutResponse(response)) {
        if (response.EnableError) {
            showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
        } else if (response.EnableSuccess) {
            var callbackFunc = function () {
                $("#divLoader").show();
                window.location.reload();
            }
            showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
        }

    }
}

//Failure Method of Subjects When an error Occurred in adding the subjects
function onSubjectFailure(response) {
    $("#divLoader").hide();
    showAlertBox("Failure Alert Message", "Some Problem Occurred.", gInfo.alertMsgType.failure);
}

function onSubjectBegin() {
    //$('#SubjectModelBox').modal('show');
    //var substartdate = new Date($("#txt_SubjectStartDate").val());
    //var subenddate = new Date($("#txt_SubjectEndDate").val());

    var substartdate = moment($('#txt_SubjectStartDate').data('daterangepicker').startDate.format("MM/DD/YYYY"), "MM/DD/YYYY");
    var subenddate = moment($('#txt_SubjectEndDate').data('daterangepicker').startDate.format("MM/DD/YYYY"), "MM/DD/YYYY");

    if ($("#txt_SubjectName").val().trim() == "") {
        //alert("Please enter subject name");
        showAlertBox("Information Alert Message", "Please enter subject name.", gInfo.alertMsgType.information);
        $("#txt_SubjectName").focus();
        return false;
    } //Validation for startdate must be less than enddate
    else if (substartdate >= subenddate) {
        $("#txt_SubjectEndDate").data('daterangepicker').setStartDate(new Date());
        showAlertBox("Information Alert Message", "Start Date must be less than End Date.", gInfo.alertMsgType.information);
        return false;
    }
    else {
        $("#divLoader").show();
        return true;
    }
}

//before subject save click 
function OnSubjectSaveClick() {
    //set value to hdnfield
    $("#hdn_SubjectStartDate").val($("#txt_SubjectStartDate").data('daterangepicker').startDate.format("MM/DD/YYYY"));
    $("#hdn_SubjectEndDate").val($("#txt_SubjectEndDate").data('daterangepicker').endDate.format("MM/DD/YYYY"));
    //
    if (onSubjectBegin()) {
        $('#addSubjectModalSubmit').trigger('click');
    }
}

function deleteSubjectConfirmation(_subjectId,_subjectName)
{
    var message = "Are you sure, do you want to remove <strong>" + _subjectName + "</strong>.";
    $('#message').html(message);
    $('#removeConfirmationModalBox').modal('show');
    $('#submitAlert').attr("onclick", "deleteSubject('" + _subjectId + "')");
}

function deleteSubject(_subjectId) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/GroupMembers/deleteSubject/",
        dataType: "json",
        data: JSON.stringify({ "id": _subjectId }),
        async: true,
        success: function (result) {
            $("#divLoader").hide();
            if (!isSessionOutResponse(result)) {
                if (result) {
                    if (result.EnableSuccess) {
                        var callbackFunc = function () {
                            $('#removeConfirmationModalBox').modal('hide');
                            $("#divLoader").show();
                            window.location.reload()
                        }
                        showAlertBox("Success Alert Message", result.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
                    }
                    else {
                        showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);

                    }

                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function initAutoComplete() {
    _serachedStudent = '';
    _searchedStudentName = '';
    $('#autocomplete').autocomplete({
        lookup: arrayStudents,
        onSelect: function (suggestion) {
            _serachedStudent = suggestion.data;
            _searchedStudentName = suggestion.value;
        }
    });
}

function getsearchStudent() {
    if ($("#autocomplete").val().trim() != _searchedStudentName) {
        _serachedStudent = $("#autocomplete").val().trim();
    }
    if ($("#autocomplete").val().trim() == "") {
        _serachedStudent = "";
    }
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/GroupMembers/getsearchStudent/",
        dataType: "html",
        data: JSON.stringify({ "id": $("#SubjectId").val(), "email": _serachedStudent }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                $("#studentList").empty().html(result);

                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function getAddStudentView() {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/GroupMembers/getAddStudentView/",
        dataType: "html",
        data: JSON.stringify({ "subjectId": $("#SubjectId").val() }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#divAddStudents').html(result);
                    $("#hdn_subjectId").val($("#SubjectId").val());
                   // initAddStudentAutoComplete();
                    $('#tblmembers').DataTable({
                        "searching": true,
                        "paging": false,
                        "info": false,
                        "order": [[1, "asc"]],
                        "scrollY": 300,
                        "scrollX": false,
                        language: {
                            search: "<i class='material-icons'>search</i> _INPUT_",
                            searchPlaceholder: "Search members..."
                        }
                    });

                    $('#addStudentsModalBox').modal('show');
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function initAddStudentAutoComplete() {
    _serachedStudent = '';
    _searchedStudentName = '';
    $('#autocompleteAddStudent').autocomplete({
        lookup: arrayAddStudents,
        onSelect: function (suggestion) {
            _serachedStudent = suggestion.data;
            _searchedStudentName = suggestion.value;
        }
    });
}

function getsearchAddStudent() {
    if ($("#autocompleteAddStudent").val().trim() != _searchedStudentName.trim()) {
        _serachedStudent = $("#autocompleteAddStudent").val().trim();
    }
    if ($("#autocompleteAddStudent").val().trim() == "") {
        _serachedStudent = "";
    }
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/GroupMembers/getsearchAddStudent/",
        dataType: "html",
        data: JSON.stringify({ "id": $("#hdn_subjectId").val(), "email": _serachedStudent }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                $("#tblAddStudents").empty().html(result);
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}
function searchAddStudent(e) {
    if (e.keyCode == 13) {
        //e.preventDefault();
        // detect the enter key
        e.preventDefault();
        getsearchAddStudent();
        return false;
    }

}
function onAddStudentsuccess(response) {
    if (!isSessionOutResponse(response)) {
        if (response.EnableError) {
            //alert(response.ErrorMsg);
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
        } else if (response.EnableSuccess) {
            //alert(response.SuccessMsg);
            $('#assignMemberModal').modal('hide');
            $("#divLoader").hide();
            var callbackFunc = function () {
                var _curSubjectId = $("#SubjectId").val();
                //getSubjectStudents(_curSubjectId, $("#SubjectName").attr("title"), $("#ulSubjects").find("li[class='selected']"));
                window.location.href = "/Teacher/GroupMembers/Index/" + _curSubjectId;
            }
            showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
        }


    }
}

function onAddStudentFailure(response) {
    $("#divLoader").hide();
    showAlertBox("Failure Alert Message", "Some Problem Occurred.", gInfo.alertMsgType.failure);
}

function onAddStudentBegin() {
    if ($('#addStudentsModalBox input[type="checkbox"]:checked').length < 0) {

        return showAlertBox("Infomartion Alert Message", "Select atleast one member from list", gInfo.alertMsgType.information);
    }
    else {
        return true;
    }
}

function onAssignMemberChildCheckChange() {
    var totalCheckbox = $('#addStudentsModalBox input[type="checkbox"]:not(.hdChk)').length;
    var totalCheckedCheckbox = $('#addStudentsModalBox input[type="checkbox"]:not(.hdChk):checked').length;
    if (totalCheckbox > totalCheckedCheckbox) {
        $('#addStudentsModalBox input[type="checkbox"].hdChk').prop('checked', false);
    }
    else {
        $('#addStudentsModalBox input[type="checkbox"].hdChk').prop('checked', 'checked');
    }
}

function onAssignMemberHeaderCheckChange(_this) {
    if ($(_this).is(':checked')) {
        $('#addStudentsModalBox input[type="checkbox"]').prop('checked', 'checked');
    }
    else {
        $('#addStudentsModalBox input[type="checkbox"]').prop('checked', false);
    }
}


function getAddGroupView(_subjectId) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/GroupMembers/getAddGroupView/",
        dataType: "html",
        data: JSON.stringify({ "id": _subjectId }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#addToGroupContainer').html(result);
                    initCarouselForGroups($('#groupLists'))
                    $('#hdn_SubjectID').val(_subjectId);
                    $('#addGroupModal').modal('show');
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}


function OnMultipleSelectGroup_subject(_this) {
    //<div class="subject_sl sub_l_gry">IJ</div>
    var parent = $('#groupLists');
    if ($(_this).hasClass('groupitemselected')) {
        $(_this).find('.txt_light_blue').removeClass('txt_light_blue');
        $(_this).find('.selImg').css('display', 'none');
        $(_this).find('.mainImg').css('display', 'block');
        $(_this).removeClass('groupitemselected');
    }
    else {
        $(_this).find('.subject_txt').addClass('txt_light_blue');
        $(_this).addClass('groupitemselected');
        $(_this).find('.selImg').css('display', 'block');
        $(_this).find('.mainImg').css('display', 'none');
    }
}

function validateAddGroup() {
    var groupList = $('#groupLists .groupitemselected', $('#addGroupModal'));
    if (groupList.length) {
        var _subjectId = $('#hdn_SubjectID').val();
        var groupIds = [];
        for (var i = 0; i < groupList.length; i++) {
            groupIds.push($(groupList[i]).find('.group_id').text());
        }
        AddGroup_Subject(_subjectId, groupIds.join());
    }
    else {
        showAlertBox("Information Alert Message", "Select atleast one group.", gInfo.alertMsgType.information);
    }
}

function AddGroup_Subject(_subjectId, _groupIds) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/GroupMembers/AddGroup_Subject/",
        dataType: "json",
        data: JSON.stringify({ "subjectId": _subjectId, "groupIds": _groupIds }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result.status.toLowerCase() == "success") {
                    $('#addGroupModal').modal('hide');
                    var callbackFunc = function () {
                        window.location.reload();
                    }
                    showAlertBox("Success Alert Message", "Group added successfully.", gInfo.alertMsgType.success, callbackFunc);
                }
                else {
                    showAlertBox("Failure Alert Message", result.msg, gInfo.alertMsgType.failure);
                }
                $("#divLoader").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function getNewGroupView(_subjectId) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/GroupMembers/getNewGroupView/",
        dataType: "html",
        data: JSON.stringify({ "id": _subjectId }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#createGroupContainer').html(result);
                    $('#createGroupSubject').modal('show');
                    $("#divLoader").hide();
                }
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function OnBeginCreateGroupSubject() {

    if ($("#txt_GroupName").val().trim() == "") {
        showAlertBox("Success Alert Message", "Group name is required.", gInfo.alertMsgType.success, callbackFunc);
        return false;
    }
    else {
        return true;
    }
}

function OnSuccessCreateGroupSubject(response) {
    if (!isSessionOutResponse(response)) {
        if (response.EnableError) {
            showAlertBox("Failure Alert Message", response.ErrorMsg, gInfo.alertMsgType.failure);
        } else if (response.EnableSuccess) {
            var callbackFunc = function () {
                $("#divLoader").show();
                window.location.reload();
            }
            showAlertBox("Success Alert Message", response.SuccessMsg, gInfo.alertMsgType.success, callbackFunc);
        }
    }
    else {
        $('#createGroupSubject').modal('hide');
        showAlertBox("Failure Alert Message", response.msg, gInfo.alertMsgType.failure);
    }
}


function OnFailureCreateGroupsubject(response) {
    $('#createeditGroupModalStep1').modal('hide');
    showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later.", gInfo.alertMsgType.failure);
}

function removeStudentFromSubject(studentId, studentName) {
    var SubjectId = $('#SubjectId').val();
    var message = "Are you sure, do you want to remove <strong>" + studentName + "</strong> from <strong>" + $('#SubjectName').text() + "</strong>";
    $('#message').html(message);
    $('#removeConfirmationModalBox').modal('show');
    $('#submitAlert').attr("onclick", "confirmBtnClick('" + studentId + "', '" + SubjectId + "')");
}

function confirmBtnClick(studentId, subjectId) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/GroupMembers/RemoveStudentFromSubject/",
        dataType: "html",
        data: JSON.stringify({ "studentId": studentId, "subjectId": subjectId }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#removeConfirmationModalBox').modal('hide');
                    // window.location.reload();
                    window.location.href = "/Teacher/GroupMembers/Index/" + subjectId;
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

function removeGroupFromSubject(groupId, groupName) {
    var SubjectId = $('#SubjectId').val();
    var message = "Are you sure, do you want to remove <strong>" + groupName + "</strong> from <strong>" + $('#SubjectName').text() + "</strong>";
    $('#message').html(message);
    $('#removeConfirmationModalBox').modal('show');
    $('#submitAlert').attr("onclick", "confirmGroupModelBtnClick('" + groupId + "', '" + SubjectId + "')");
}

function confirmGroupModelBtnClick(groupId, subjectId) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Teacher/GroupMembers/RemoveGroupFromSubject/",
        dataType: "html",
        data: JSON.stringify({ "groupId": groupId, "subjectId": subjectId }),
        async: true,
        success: function (result) {
            if (!isSessionOutResponse(result)) {
                if (result) {
                    $('#removeConfirmationModalBox').modal('hide');
                    // window.location.reload();
                    window.location.href = "/Teacher/GroupMembers/Index/" + subjectId;
                }
            }
            $("#divLoader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            showAlertBox("Failure Alert Message", "Some Problem Occurred.Please Try Again Later", gInfo.alertMsgType.failure);
            return false;
        }
    });
}

//
function setLocalAndArchiveSelect(_this) {
   
}