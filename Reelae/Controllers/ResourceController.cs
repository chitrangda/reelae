﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Reelae.Repositories;
using Reelae.Areas.Admin.Models;
using ReelaeDataAccessLayer;
using Reelae.Filters;
using System.Web.Script.Serialization;
using ReelaeBusinessLayer.Utilities;
using Reelae.Models;
using System.Transactions;
using Microsoft.AspNet.Identity;
using Reelae.Utilities;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Reelae.Controllers
{
    public class ResourceController : Controller
    {
        IInstitutionRepository _insRepo;
        IUserRepository _userRepo;
        IResourceRepository _resourceRepository;
        SubjectRepository _SubjectRepo;

        public ResourceController(IInstitutionRepository __insRepo, IUserRepository __userRepo, IResourceRepository __resourceRepository, SubjectRepository ___SubjectRepo)
        {
            _insRepo = __insRepo;
            _userRepo = __userRepo;
            _resourceRepository = __resourceRepository;
            _SubjectRepo = ___SubjectRepo;
        }
        // GET: Resource
        public ActionResult Index()
        {
            return View();
        }

        [checkSessionAlive]
        public ActionResult ResourceView()
        {
            int _instituteId = Convert.ToInt32(Session["Institution"]);
            int _userid = Convert.ToInt32(Session["UserId"]);
            Session["Theme"] = _insRepo.Get(_instituteId).Theme.ThemeName;
            ResourceViewModal _memberResourceView = new ResourceViewModal();
            _memberResourceView.userInfo = _userRepo.Get(User.Identity.GetUserId());
            ViewBag.PageHeading = "Resource";
            ViewBag.Subjects = GetSubjectDropDownData();//subject view bag
            return View(_memberResourceView);
        }

        [checkSessionAlive]
        [HttpPost]
        public ActionResult AddResourceDetails()
        {
            // ViewBag.Subjects = GetSubjectDropDownData();//subject view bag
            return PartialView("_PartialAddSubjectResource", new AddResourceViewModal());
        }

        [checkSessionAlive]
        [HttpPost]
        public ActionResult AddResource(AddResourceViewModal _ResourceViewModal)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (_ResourceViewModal.resourceInfo != null)
                    {
                        _resourceRepository.AddResource(_ResourceViewModal);
                        return this.Json(new
                        {
                            EnableSuccess = true,
                            SuccessTitle = MasterConstants.SUCCESSTITLE,
                            SuccessMsg = MasterConstants.SAVESUCCESSFULLY
                        });
                    }
                    else
                    {
                        return this.Json(new
                        {
                            EnableError = true,
                            ErrorTitle = MasterConstants.ERRORTITLE,
                            ErrorMsg = "Users does not exist!"
                        });
                    }
                }
                else
                {
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = error
                    });
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                });
            }
        }

        [checkSessionAlive]
        [HttpPost]
        public ActionResult UploadResourceFile()
        {
            string fname = "";
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    HttpPostedFileBase file = files[0];
                    // Checking for Internet Explorer  
                    if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = file.FileName.Split(new char[] { '\\' });
                        fname = testfiles[testfiles.Length - 1];
                    }
                    else
                    {
                        fname = file.FileName;
                    }
                    var result = _resourceRepository.UploadResource(file);
                    return Json(result);
                }
                catch (Exception ex)
                {
                    ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserId"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                    return this.Json(new
                    {
                        status = MasterConstants.FAILURETITLE,
                        msg = MasterConstants.SOMEPROBLEMOccurred,
                        Data = string.Empty
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(MasterConstants.NOFILESSELECTED);
            }
        }
        [checkSessionAlive]
        private IEnumerable<Areas.Teacher.Models.DropdownViewModal> GetSubjectDropDownData()
        {
            var groupDropdownData = new List<Areas.Teacher.Models.DropdownViewModal>();
            int? _InstitutionID = Session["Institution"] as int?;
            var subjectlst = _resourceRepository.getSubjectsListTeacherId(int.Parse(Session["UserId"].ToString()), Convert.ToInt32(_InstitutionID));
            foreach (var item in subjectlst)
            {
                groupDropdownData.Add(new Areas.Teacher.Models.DropdownViewModal() { Id = item.SubjectID, Value = item.SubjectName });
            }
            return groupDropdownData;
        }

        [checkSessionAlive]
        [HttpPost]
        public ActionResult GetResourcesGridView(int id)
        {
            try
            {
                return PartialView("_PartialResourceGridView", _resourceRepository.GetAll(null));
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                });
            }
        }

        [checkSessionAlive]
        [HttpPost]
        public ActionResult DeleteResourceFile(string fileUrl)
        {
            if (!string.IsNullOrEmpty(fileUrl))
            {
                try
                {
                    var filePath = Server.MapPath(fileUrl);
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }
                    return Json("True");
                }
                catch (Exception ex)
                {
                    ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserId"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                    return this.Json(new
                    {
                        status = MasterConstants.FAILURETITLE,
                        msg = MasterConstants.SOMEPROBLEMOccurred,
                        Data = string.Empty
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(MasterConstants.NOFILESSELECTED);
            }
        }

        [checkSessionAlive]
        [HttpPost]
        public ActionResult GetReourceFileDetails(int id)
        {
            try
            {
                return PartialView("_PartialResourceDetail", _resourceRepository.GetFileDetails(id));
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                });
            }
        }

        [checkSessionAlive]
        public FileResult DownloadFile(int resourceId)
        {
            var result = _resourceRepository.Get(resourceId);
            var test1 = Server.MapPath("~");
            var filePath = System.IO.Path.Combine(test1, "Resource/" + result.URL);
            return File(filePath, MimeMapping.GetMimeMapping(filePath), result.Title);
        }

        [checkSessionAlive]
        [HttpPost]
        public ActionResult Download(int resourceId)
        {
            return this.Json(new
            {
                EnableSuccess = true,
                SuccessTitle = MasterConstants.SUCCESSTITLE,
                SuccessMsg = MasterConstants.SAVESUCCESSFULLY
            });
        }

        [checkSessionAlive]
        [HttpPost]
        public ActionResult GetDownloadActivity(int? subjectId, string sortOrder)
        {
            try
            {
                if (sortOrder == "name_asc")
                {
                    return PartialView("_PartialDownloadActivityList", _resourceRepository.GetDownloadActivityResourceList(null, subjectId).OrderBy(x => x.Title));
                }
                else if (sortOrder == "name_desc")
                {
                    return PartialView("_PartialDownloadActivityList", _resourceRepository.GetDownloadActivityResourceList(null, subjectId).OrderByDescending(x => x.Title));
                }
                return PartialView("_PartialDownloadActivityList", _resourceRepository.GetDownloadActivityResourceList(null, subjectId));//.OrderByDescending(x => x.CreatedDate));
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                });
            }
        }

        //function for filtering
        [checkSessionAlive]
        [HttpPost]
        public ActionResult GetResources(int? subjectId, string sortOrder, string docType, string otherFilter, int? tag, int? labelId)
        {
            try
            {
                var result = _resourceRepository.GetAllResourcePerSubject(subjectId, docType, tag, otherFilter, sortOrder, labelId);
                return PartialView("_PartialResourceGridView", result);
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                });
            }
        }

        [checkSessionAlive]
        [HttpPost]
        public ActionResult EditSubjectResource(int? id)
        {
            try
            {
                ViewBag.Subjects = GetSubjectDropDownData();
                EditResourceViewModal _model = new EditResourceViewModal();
                _model.resourceInfo = _resourceRepository.Get(id);
                return PartialView("_PartialEditSubjectResource", _model);
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                });
            }
        }

        [checkSessionAlive]
        [HttpPost]
        public ActionResult EditSubjectResourceDetail(EditResourceViewModal _ResourceModal)
        {
            try
            {
                _resourceRepository.EditResource(_ResourceModal);
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.SAVESUCCESSFULLY
                });
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                });
            }
        }

        [checkSessionAlive]
        [HttpPost]
        public ActionResult DeleteSubjectResource(int? id)
        {
            try
            {
                if (id != null)
                {
                    _resourceRepository.RemoveResource(id);
                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = "Resource deleted successfully."
                    });
                }
                else
                {
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = error
                    });
                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.PLEASETRYAGAINLATER
                });
            }
        }

        [checkSessionAlive]
        [HttpPost]
        public ActionResult FlagSubjectResource(int? id, bool isFlaged)
        {
            try
            {
                if (id != null)
                {
                    _resourceRepository.FlagResource(id, isFlaged);
                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = "Resource deleted successfully."
                    });
                }
                else
                {
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = error
                    });
                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.PLEASETRYAGAINLATER
                });
            }
        }
        //for tagged 
        [checkSessionAlive]
        [HttpPost]
        public ActionResult TagSubjectResource(int? id)
        {
            try
            {
                if (id != null)
                {
                    var result = _resourceRepository.Get(id);
                    AddTagResourceViewModel obj = new AddTagResourceViewModel();
                    if (result.TagId != null)
                    {
                        obj.TagId = result.TagId;
                        obj.TagName = _resourceRepository.GetTagName(result.TagId);
                    }
                    obj.ResourceId = id.Value;
                    obj.ResourceTags = _resourceRepository.GetResourceTags();
                    return PartialView("_PartialAddResourceTag", obj);
                }
                else
                {
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = error
                    });
                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.PLEASETRYAGAINLATER
                });
            }
        }

        //Update tag
        [checkSessionAlive]
        [HttpPost]
        public ActionResult AddTagToSubjectResource(int? resourceId, int? tagId, string tagName)
        {
            try
            {
                _resourceRepository.AddTagToSubjectResource(resourceId, tagId, tagName);
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = "Tag saved successfully."
                });
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.PLEASETRYAGAINLATER
                });
            }
        }

        //listing of tags
        [checkSessionAlive]
        [HttpPost]
        public ActionResult GetResourceTags(int? id)
        {
            try
            {
                return PartialView("_PartialResourceTags", _resourceRepository.GetResourceTags());
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                });
            }
        }

        [checkSessionAlive]
        [HttpPost]
        public ActionResult AddResourceDescription(int? id)
        {
            try
            {
                if (id != null)
                {
                    return PartialView("_PartialAddDescriptionResource", _resourceRepository.Get(id));
                }
                else
                {
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = error
                    });
                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.PLEASETRYAGAINLATER
                });
            }
        }
        //update description
        [checkSessionAlive]
        [HttpPost]
        public ActionResult UpdateResourceDescription(int? resourceId, string description)
        {
            try
            {
                _resourceRepository.UpdateDescription(resourceId, description);
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = "Description saved successfully."
                });
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.PLEASETRYAGAINLATER
                });
            }
        }
        //update restore file
        [checkSessionAlive]
        [HttpPost]
        public ActionResult RestoreResourceFile(int? id)
        {
            try
            {
                if (id != null)
                {
                    _resourceRepository.RestoreResource(id);
                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = "Resource restore successfully."
                    });
                }
                else
                {
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = error
                    });

                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.PLEASETRYAGAINLATER
                });
            }
        }

        //get Activity detail List
        [checkSessionAlive]
        [HttpPost]
        public ActionResult GetActivityDetailList(int? id)
        {
            try
            {
                return PartialView("_PartialResourceActivity", _resourceRepository.GetActivityList(id));
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                });
            }
        }

        [checkSessionAlive]
        [HttpPost]
        public ActionResult AddResourceLabel(int? id)
        {
            try
            {
                if (id != null)
                {
                    return PartialView("_PartialAddResourceLabel", null);
                }
                else
                {
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = error
                    });
                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.PLEASETRYAGAINLATER
                });
            }
        }

        [checkSessionAlive]
        [HttpPost]
        public ActionResult AddLabelToSubjectResource(int? subjectId, DateTime startDate, DateTime endDate, string labelName)
        {
            try
            {
                int? _teacherId = Session["UserId"] as int?;
                ResourceLabel _resourceLabel = new ResourceLabel()
                {
                    SubjectId = subjectId,
                    StartDate = startDate,
                    EndDate = endDate,
                    LabelName = labelName,
                    TeacherId = _teacherId
                };
                int addedLabelId = _resourceRepository.AddLabelResource(_resourceLabel);
                if (addedLabelId > 0)
                {
                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = "Resource label added successfully.",
                        addedId = addedLabelId
                    });
                }
                var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = error
                });
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.PLEASETRYAGAINLATER
                });
            }
        }

        //resource Lable list
        [checkSessionAlive]
        [HttpPost]
        public ActionResult ResourceLableList(int? subjectId)
        {
            try
            {
                if (subjectId != null)
                {
                    string roles = Session["Role"].ToString();
                    int userId = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
                    if (roles.Contains("Teacher"))
                    {
                        return PartialView("_PartialResourceLabelList", _resourceRepository.GetResourceLabelList(userId, null, subjectId));
                    }
                    else
                    {
                        return PartialView("_PartialResourceLabelList", _resourceRepository.GetResourceLabelList(null, userId, subjectId));
                    }
                }
                else
                {
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = error
                    });
                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.PLEASETRYAGAINLATER
                });
            }
        }
        //resource charts

        [checkSessionAlive]
        [HttpPost]
        public ActionResult ResourceActivityGraph()
        {
            try
            {
                return PartialView("_PartialResourceActivityGraph", null);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.PLEASETRYAGAINLATER
                });
            }
        }
        [checkSessionAlive]
        [HttpPost]
        public JsonResult GetResourceActivityGraphData(int? resourceId)
        {
            try
            {
                var result = _resourceRepository.GetActivityList(resourceId);
                List<ResourceActivityChart> _listCount = new List<ResourceActivityChart>();
                foreach (var item in result)
                {
                    ResourceActivityChart _resourceActivityChart = new ResourceActivityChart();
                    _resourceActivityChart.StudentName = item.Name;

                    if (item.isViewed == true && item.IsDownloaded == 1)
                    {
                        _resourceActivityChart.ActivityCount = 3;
                    }
                    else if (item.isViewed == true)
                    {
                        _resourceActivityChart.ActivityCount = 1;
                    }
                    else if (item.IsDownloaded == 1)
                    {
                        _resourceActivityChart.ActivityCount = 2;
                    }

                    _resourceActivityChart.StudentPicUrl = item.UserPicUrl == null ? "/../Content/images/student_small.jpg" : item.UserPicUrl.Replace("~", "/..");
                    _listCount.Add(_resourceActivityChart);
                }
                return new JsonResult()
                {
                    Data = _listCount,
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                return Json(null);
            }
        }

        //Visit Graph

        [checkSessionAlive]
        [HttpPost]
        public ActionResult ResourceVisitGraph()
        {
            try
            {
                return PartialView("_PartialResourceVisitGraphByStudent", null);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.PLEASETRYAGAINLATER
                });
            }
        }
        [checkSessionAlive]
        [HttpPost]
        public JsonResult GetResourceVisitGraphData(int? subjectId)
        {
            try
            {
                var result = _resourceRepository.GetStudentVisitList(subjectId);
                List<ResourceVisitTimeByStudent> _listCount = new List<ResourceVisitTimeByStudent>();
                foreach (var item in result)
                {
                    ResourceVisitTimeByStudent _resourceActivityChart = new ResourceVisitTimeByStudent();
                    _resourceActivityChart.StudentName = item.User.Name;
                    _resourceActivityChart.SpendTime = (item.EndDateTime-item.StartDateTime).Value.Minutes;
                    _resourceActivityChart.StudentPicUrl = item.User.UserPicUrl == null ? "/../Content/images/student_small.jpg" : item.User.UserPicUrl.Replace("~", "/..");
                    _listCount.Add(_resourceActivityChart);
                }
                return new JsonResult()
                {
                    Data = _listCount,
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                return Json(null);
            }
        }
    }
}