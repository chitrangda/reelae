﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Reelae.Filters;
using Reelae.Repositories;
using Reelae.Utilities;

namespace Reelae.Controllers
{
    [Authorize]
    [checkSessionAlive]
    public class ChatController : Controller
    {
        // GET: Chat
        private HttpContextBase currentContext = null;
        IChatRepository _chatRepo;

        public ChatController(IChatRepository __chatRepo)
        {
            _chatRepo = __chatRepo;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ResumableUploader()
        {
            currentContext = Request.RequestContext.HttpContext;
            if (Request.RequestType.ToUpper() == "GET")
            {
                var queryString = currentContext.Request.QueryString;
                var resumableFilename = queryString.Get("resumableFilename");
                if (!FileUtility.IsValidFileForUpload(resumableFilename))
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Invalid File Type");
                bool result = UploadFile();
                if (result)
                    return new HttpStatusCodeResult(HttpStatusCode.OK);
                else
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            else if (Request.RequestType.ToUpper() == "POST")
            {
                var queryString = currentContext.Request.QueryString;
                var resumableFilename = queryString.Get("resumableFilename");
                if (!FileUtility.IsValidFileForUpload(resumableFilename))
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Invalid File Type");
                else
                    return UploadFile(Request.InputStream);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult DownloadFile()
        {
            currentContext = Request.RequestContext.HttpContext;
            var queryString = currentContext.Request.QueryString;
            if (queryString.Count == 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var filePath = queryString.Get("fP");
            var fileName = queryString.Get("fN");
            if (!string.IsNullOrEmpty(filePath))
            {
                var localFilePath = currentContext.Server.MapPath(filePath);
                var fileInfo = new System.IO.FileInfo(localFilePath);
                if (fileInfo.Exists)
                {
                    currentContext.Response.ContentType = System.Net.Mime.MediaTypeNames.Application.Octet;
                    currentContext.Response.AddHeader("Content-disposition", "attachment; filename=" + fileName);
                    currentContext.Response.TransmitFile(localFilePath);
                    return new HttpStatusCodeResult(HttpStatusCode.OK);
                }
                else
                {
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                }
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        private bool UploadFile()
        {
            string _uploadFolder = "ResumableUpload";
            var queryString = currentContext.Request.QueryString;
            if (queryString.Count == 0) return false;

            // Read parameters
            var uploadToken = queryString.Get("upload_Token");

            int resumableChunkNumber = int.Parse(queryString.Get("resumableChunkNumber"));
            var resumableIdentifier = queryString.Get("resumableIdentifier");
            //var resumableChunkSize = queryString.Get("resumableChunkSize");
            //var resumableTotalSize = queryString.Get("resumableTotalSize");
            var resumableFilename = queryString.Get("resumableFilename");
            uploadToken = (string.IsNullOrEmpty(uploadToken)) ? resumableIdentifier : "";
            var filePath = string.Format("{0}/{1}/{1}.part{2}", _uploadFolder,
                           uploadToken, resumableChunkNumber.ToString("0000"));
            var localFilePath = currentContext.Server.MapPath(filePath);

            // Response
            var fileExists = System.IO.File.Exists(localFilePath);
            return fileExists;
        }

        private ActionResult UploadFile(object data)
        {

            try
            {
                var queryString = currentContext.Request.Form;
                if (queryString.Count == 0) new HttpStatusCodeResult(HttpStatusCode.NotFound);
                string _uploadFolder = "~/ChatUploads";
                // Read parameters
                var uploadToken = queryString.Get("upload_Token");
                int resumableChunkNumber = int.Parse(queryString.Get("resumableChunkNumber"));
                var resumableFilename = queryString.Get("resumableFilename");
                var resumableIdentifier = queryString.Get("resumableIdentifier");
                int resumableChunkSize = int.Parse(queryString.Get("resumableChunkSize"));
                long resumableTotalSize = long.Parse(queryString.Get("resumableTotalSize"));

                uploadToken = (string.IsNullOrEmpty(uploadToken)) ? resumableIdentifier : "";

                var filePath = string.Format("{0}/{1}/{1}.part{2}", _uploadFolder,
                                      uploadToken, resumableChunkNumber.ToString("0000"));
                var localFilePath = currentContext.Server.MapPath(filePath);
                var directory = System.IO.Path.GetDirectoryName(localFilePath);
                if (!System.IO.Directory.Exists(localFilePath)) System.IO.Directory.CreateDirectory(directory);

                if (currentContext.Request.Files.Count == 1)
                {
                    // save chunk
                    if (!System.IO.File.Exists(localFilePath))
                    {
                        currentContext.Request.Files[0].SaveAs(localFilePath);
                    }

                    // Check if all chunks are ready and save file
                    var files = System.IO.Directory.GetFiles(directory);
                    if ((files.Length + 1) * (long)resumableChunkSize >= resumableTotalSize)
                    {
                        filePath = string.Format("{0}/{1}{2}", _uploadFolder,
                          System.IO.Path.GetFileNameWithoutExtension(resumableFilename) + "_" + DateTime.Now.Ticks.ToString(), System.IO.Path.GetExtension(resumableFilename));
                        localFilePath = currentContext.Server.MapPath(filePath);
                        using (var fs = new FileStream(localFilePath, FileMode.CreateNew))
                        {
                            foreach (string file in files.OrderBy(x => x))
                            {
                                var buffer = System.IO.File.ReadAllBytes(file);
                                fs.Write(buffer, 0, buffer.Length);
                                System.IO.File.Delete(file);
                            }
                        }
                        System.IO.Directory.Delete(directory);
                        //after save details in db also and send the final identifier

                        Response.Write("filePath=" + Url.Content(filePath) + "");
                    }
                    return new HttpStatusCodeResult(HttpStatusCode.OK);
                }
                else
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }


        [ValidateInput(false)]
        public ActionResult ExportChat(string userId, string userName)
        {
            int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            string Html = GetChatExportData(_userid, Convert.ToInt32(userId), userName);
            var htmlContent = String.Format("<body>Hello world: {0}</body>",
        DateTime.Now);
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdfBytes = htmlToPdf.GeneratePdf(Html);
            return File(pdfBytes, "application/pdf", "ChatHistroy_" + DateTime.Now.ToShortDateString() + ".pdf");
        }


        public ActionResult ExportCsvChat(string userId, string userName)
        {
            int _userid = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            string Html = GetChatExportDataForCsv(_userid, Convert.ToInt32(userId), userName);
            return Json(new
            {
                status = MasterConstants.SUCCESSTITLE,
                msg = MasterConstants.SAVESUCCESSFULLY,
                Data = Html,
            }, JsonRequestBehavior.AllowGet);
        }



        private string GetFileType(string type)
        {
            string result = "";
            int lastIndexOfDot = type.LastIndexOf('.');
            string extension = type.Substring(lastIndexOfDot + 1, type.Length - lastIndexOfDot - 1).ToLower();

            if (extension == "pdf" || extension == "doc" || extension == "docx" || extension == "txt" || extension == "rtf" || extension == "zip" || extension == "rar")
            {
                result = "doc";
            }
            else if (extension == "png" || extension == "jpg" || extension == "jpeg" || extension == "gif" || extension == "tif")
            {
                result = "image";
            }
            else if (extension == "mp3" || extension == "obb" || extension == "wave")
            {
                result = "audio";
            }
            else if (extension == "mp4" || extension == "avi" || extension == "wmv" || extension == "flv")
            {
                result = "video";
            }
            else
            {
                result = "text";
            }

            return result;
        }

        private string GetFileExtension(string fileName)
        {
            int lastIndexOfDot = fileName.LastIndexOf('.');
            string extension = fileName.Substring(lastIndexOfDot + 1, fileName.Length - lastIndexOfDot - 1).ToLower();
            return extension;
        }

        private string GetChatExportData(int from, int to, string userName)
        {
            userName = (string.IsNullOrEmpty(userName)) ? "" : userName;
            StringBuilder sr = new StringBuilder();
            var UserID = Session["UserId"] as int?;
            var curType = "text";
            bool isSent = false;
            bool isUnreadLabelCreated = false;
            var GetUserIndividualMessages = _chatRepo.GetIndividualChatMessages(from, to);
            foreach (var item in GetUserIndividualMessages)
            {
                string addonClass = "";
                string downloadUrl = Url.Action("DownloadFile", "Chat", new { area = "" });
                string timeString = "";
                curType = (item.IsAttachment) ? GetFileType(item.MessageText).Trim() : "text";
                bool isUnreadMessages = item.IsRead == 0;

                if (item.FromUserID == UserID)
                {
                    addonClass = " bubble_chat-alt white";
                    isSent = true;
                    timeString = "Delivered" + item.MessageDate.Value.ToShortDateString() + " " + item.MessageDate.Value.ToShortTimeString();
                }
                else
                {
                    isSent = false;
                    timeString = "Sent " + item.MessageDate.Value.ToShortDateString() + " " + item.MessageDate.Value.ToShortTimeString();
                }

                if (curType == "text")
                {
                    sr.AppendFormat("<div  class='{0}'><span>{1}</span><span class='time'>{2}</span></div>", "bubble_chat" + addonClass, HttpUtility.HtmlDecode(item.MessageText), timeString);
                }
                else if (curType == "image")
                {
                    downloadUrl += "?" + string.Format("fP={0}&fN={1}", item.AttachmentURL, item.MessageText);
                    sr.AppendFormat("<div  class='{0}'><a class='fileView' target='_blank' href='{1}'><img src='{2}' alt='...' class='img - responsive' /><span class='time'>{3}</span></a></div>", "bubble_chat" + addonClass, Url.Content(downloadUrl), Request.Url.Scheme + "://" + Request.Url.Authority + Url.Content(item.AttachmentURL), timeString);
                }
                else if (curType == "video")
                {
                    downloadUrl += "?" + string.Format("fP={0}&fN={1}", item.AttachmentURL, item.MessageText);
                    sr.AppendFormat("<div  class='{0}'><a class='fileView' target='_blank' href='{1}'><span>{2}</span><span class='time'>{3}</span></a></div>", "bubble_chat" + addonClass, Url.Content(downloadUrl), item.MessageText, timeString);
                }
                else if (curType == "audio")
                {
                    downloadUrl += "?" + string.Format("fP={0}&fN={1}", item.AttachmentURL, item.MessageText);
                    sr.AppendFormat("<div  class='{0}'><a class='fileView' target='_blank' href='{1}'><span>{2}</span><span class='time'>{3}</span></a></div>", "bubble_chat" + addonClass, "#", item.MessageText, timeString);
                }
                else if (curType == "doc")
                {

                    downloadUrl += "?" + string.Format("fP={0}&fN={1}", item.AttachmentURL, item.MessageText);
                    sr.AppendFormat("<div  class='{0}'><a class='fileView' target='_blank' href='{1}'><span>{2}</span><span class='time'>{3}</span></a></div>", "bubble_chat" + addonClass, "#", item.MessageText, timeString);

                }

            }
            return string.Format("<!DOCTYPE html><head><meta http-equiv='content-type' content='text/html; charset=utf-8' /><title>Chat History</title><link href='{0}' rel='stylesheet'><link href='{1}' rel='stylesheet'><link href='{2}' rel='stylesheet'></head><body><div style='text-align:center;'><b>" + userName + "'s Chat History</b></div><br /><div>{3}</div></body></html>", Request.Url.Scheme + "://" + Request.Url.Authority + Url.Content("~/Content/css/style-dash.css"), Request.Url.Scheme + "://" + Request.Url.Authority + Url.Content("~/Content/css/media.css"), Request.Url.Scheme + "://" + Request.Url.Authority + Url.Content("~/Content/css/bootstrap.css"), sr.ToString());
        }

        private string GetChatExportDataForCsv(int from, int to, string userName)
        {
            userName = (string.IsNullOrEmpty(userName)) ? "" : userName;
            StringBuilder sr = new StringBuilder();
            var UserID = Session["UserId"] as int?;
            var curType = "text";
            bool isSent = false;
            bool isUnreadLabelCreated = false;
            var GetUserIndividualMessages = _chatRepo.GetIndividualChatMessages(from, to);
            foreach (var item in GetUserIndividualMessages)
            {
                string addonClass = "";
                string downloadUrl = Url.Action("DownloadFile", "Chat", new { area = "" });
                string timeString = "";
                curType = (item.IsAttachment) ? GetFileType(item.MessageText).Trim() : "text";
                bool isUnreadMessages = item.IsRead == 0;

                if (item.FromUserID == UserID)
                {
                    addonClass = " bubble_chat-alt white";
                    isSent = true;
                    timeString = "Delivered" + item.MessageDate.Value.ToShortDateString() + " " + item.MessageDate.Value.ToShortTimeString();
                }
                else
                {
                    isSent = false;
                    timeString = "Sent " + item.MessageDate.Value.ToShortDateString() + " " + item.MessageDate.Value.ToShortTimeString();
                }


                if (curType == "text")
                {
                    if (isSent)
                        sr.AppendFormat("<tr><td></td><td>{0} ## {1}</td></tr>", item.MessageText, timeString);
                    else
                        sr.AppendFormat("<tr><td>{0} ## {1}</td><td></td></tr>", item.MessageText, timeString);
                }
                else if (curType == "image")
                {
                    if (isSent)
                        sr.AppendFormat("<tr><td></td><td>{0} ## {1}</td></tr>", Request.Url.Scheme + "://" + Request.Url.Authority + Url.Content(item.AttachmentURL), timeString);
                    else
                        sr.AppendFormat("<tr><td>{0} ## {1}</td><td></td></tr>", Request.Url.Scheme + "://" + Request.Url.Authority + Url.Content(item.AttachmentURL), timeString);
                }
                else if (curType == "video")
                {
                    if (isSent)
                        sr.AppendFormat("<tr><td></td><td>{0} ## {1}</td></tr>", Request.Url.Scheme + "://" + Request.Url.Authority + Url.Content(item.AttachmentURL), timeString);
                    else
                        sr.AppendFormat("<tr><td>{0} ## {1}</td><td></td></tr>", Request.Url.Scheme + "://" + Request.Url.Authority + Url.Content(item.AttachmentURL), timeString);
                }
                else if (curType == "audio")
                {
                    if (isSent)
                        sr.AppendFormat("<tr><td></td><td>{0} ## {1}</td></tr>", Request.Url.Scheme + "://" + Request.Url.Authority + Url.Content(item.AttachmentURL), timeString);
                    else
                        sr.AppendFormat("<tr><td>{0} ## {1}</td><td></td></tr>", Request.Url.Scheme + "://" + Request.Url.Authority + Url.Content(item.AttachmentURL), timeString);
                }
                else if (curType == "doc")
                {
                    if (isSent)
                        sr.AppendFormat("<tr><td></td><td>{0} ## {1}</td></tr>", Request.Url.Scheme + "://" + Request.Url.Authority + Url.Content(item.AttachmentURL), timeString);
                    else
                        sr.AppendFormat("<tr><td>{0} ## {1}</td><td></td></tr>", Request.Url.Scheme + "://" + Request.Url.Authority + Url.Content(item.AttachmentURL), timeString);
                }

            }
            return string.Format("<table><thead><tr><td colspan='2'>{0}</td></tr></thead><tbody>{1}</tbody></table>", userName + "'s Chat History", sr.ToString());
        }

    }
}