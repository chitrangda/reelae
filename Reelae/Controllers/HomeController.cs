﻿using Microsoft.AspNet.Identity.Owin;
using Reelae.Models;
using Reelae.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ReelaeDataAccessLayer;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System.Configuration;
using Reelae.Utilities;
using System.Text;
using ReelaeBusinessLayer.Utilities;

namespace Reelae.Controllers
{
    [OutputCache(NoStore = true, Duration = 0)]
    public class HomeController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        IInstitutionRepository _institutionRepo;
        IUserRepository _userRepo;

        ISubjectStudentRepository _subjectStudentRepo;
        ISubjectTeacherRepository _subjectTeacherRepo;

        public HomeController(IUserRepository __userRepo, IInstitutionRepository __institutionRepo, ISubjectStudentRepository __subjectStudentRepo, ISubjectTeacherRepository __subjectTeacherRepo)
        {
            _userRepo = __userRepo;
            _institutionRepo = __institutionRepo;
            _subjectStudentRepo = __subjectStudentRepo;
            _subjectTeacherRepo = __subjectTeacherRepo;

        }

        //public HomeController()
        //{
        //    _userRepo = new UserRepository();

        //}

        public HomeController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }

        public ActionResult Login(string id = null)
        {
            //Encryption64.encryptString("Chitrangda#$56");
            if (id == "s")
            {
                ViewBag.tab1 = "";
                ViewBag.tab2 = "active";

            }
            else
            {
                ViewBag.tab1 = "active";
                ViewBag.tab2 = "";
            }
            return View();

        }

        [HttpPost]
        public async Task<ActionResult> Login(LoginRegisterComplexModel model)
        {
            ViewBag.tab1 = "active";
            ViewBag.tab2 = "";
            ViewBag.Visible = null;
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            if (UserManager.FindByName(model.LoginUser.Email).isEnabled == false)
            {
                ModelState.AddModelError("", MasterConstants.ACCOUNTNOTACTIVATED);
                return View(model);
            }

            var result = await SignInManager.PasswordSignInAsync(model.LoginUser.Email, model.LoginUser.Password, true, shouldLockout: false);

            switch (result)
            {
                case SignInStatus.Success:
                    string userId = UserManager.FindByName(model.LoginUser.Email)?.Id;
                    var _curUserInfo = _userRepo.Get(userId);
                    string Name = _curUserInfo.Name;
                    Session["Institution"] = _curUserInfo.InstitutionID;
                    Session["UserId"] = _curUserInfo.UserID;
                    Session["UserName"] = model.LoginUser.Email;
                    Session["Name"] = (Name == null) ? "" : Name;
                    string _insInitialName = _curUserInfo.Institution.IntialName;
                    Session["InstutionCode"] = string.IsNullOrEmpty(_insInitialName) ? "" : _insInitialName;

                    var roles = UserManager.GetRoles(userId);
                    Session["Role"] = roles.FirstOrDefault();
                    if (roles.Contains("Admin"))
                    {
                        return RedirectToAction("dashboard", "Admin", new { area = "Admin" });
                    }
                    else if (roles.Contains("Teacher"))
                    {
                        return RedirectToAction("Dashboard", "Teacher", new { area = "Teacher" });

                    }
                    else if (roles.Contains("Student"))
                    {
                        return RedirectToAction("Dashboard", "Student", new { area = "Student" });

                    }
                    else
                    {
                        ModelState.AddModelError("", MasterConstants.ACCOUNTNOTACTIVATED);
                        return View(model);
                    }

                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = "", RememberMe = true });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", MasterConstants.INVALIDLOGINATTEMPT);
                    return View(model);
            }
        }

        public ActionResult SignUp()
        {
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public async Task<ActionResult> SignUp(LoginRegisterComplexModel model)
        {
            ViewBag.tab1 = "";
            ViewBag.tab2 = "active";
            if (ModelState.IsValid)
            {
                var _existUser = UserManager.FindByName(model.RegisterUser.Email);
                if (_existUser != null)
                {
                    //if (_existUser.isEnabled)
                    //{
                    ViewBag.Message = MasterConstants.EMAILALREADYEXISTS;
                    return View("Login", model);
                    //}
                    //else
                    //{
                    //    await UserManager.DeleteAsync(_existUser);
                    //    _userRepo.Remove(_existUser.Id);
                    //}
                }
                var user = new ApplicationUser { UserName = model.RegisterUser.Email, Email = model.RegisterUser.Email, Name = model.RegisterUser.Name };
                //await UserManager.DeleteAsync(_user.Result);
                //_user = UserManager.FindByNameAsync("abhishek.upadhyay@srmtechsol.com");
                //await UserManager.DeleteAsync(_user.Result);
                IdentityResult result = await UserManager.CreateAsync(user, model.RegisterUser.Password);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    try
                    {
                        UserManager.AddToRole(user.Id, "Admin");
                    }
                    catch (Exception)
                    {

                    }
                    int _InstitutionId = _institutionRepo.Add(new Institution() { InstitutionName = "", InstitutionType = "TFS", CreatedDate = DateTime.Now, ThemeID = 2 });
                    _userRepo.Add(new User() { InstitutionID = _InstitutionId, Status = 1, CreatedDate = DateTime.Now, UserMemberShipID = user.Id, Name = model.RegisterUser.Name, Email = model.RegisterUser.Email });
                    //string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                    //var callbackUrl = Url.Action("ActivateUser", "Home", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    //string AdminEmailId = ConfigurationManager.AppSettings["AdminEmail"].ToString();
                    //Mail.SendMail(AdminEmailId, "Reelae Sign-Up Request", EmailHtml.getAdminHtml(user.Email, " Activate new sign-up request by clicking <a href=\"" + callbackUrl + "\">here</a>"));
                    return RedirectToAction("newAccount", new { id = 1 });
                }
                else
                {
                    ModelState.AddModelError("", MasterConstants.PASSWORDVALIDATIONVIEWMESSAGE);
                }
            }
            return View("Login", model);
        }

        public ActionResult newAccount(int? id)
        {
            if (id == 1)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }
        public async Task<ActionResult> ActivateUser(string userId, string code)
        {
            var user = await UserManager.FindByIdAsync(userId);
            if (user == null)
            {
                ViewBag.Message = MasterConstants.EMAILREGISTRATIONALERT;
            }
            else
            {
                user.isEnabled = true;
                var result = await UserManager.UpdateAsync(user);
            }
            var callbackUrl = Url.Action("Login", "Home", null, protocol: Request.Url.Scheme);
            Mail.SendMail(user.Email, MasterConstants.ACCOUNTACTIVATIONSUCCESS, EmailHtml.getAccountActivatedHtml(user.Name, user.Email, callbackUrl));
            //await UserManager.SendEmailAsync(user.Id, "Reelae Account Activated", useremailTemplate(user, callbackUrl));
            ViewBag.Message = MasterConstants.EMAILSENTSUCCESS;
            return RedirectToAction("Login");
        }



        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }



        #region Password Management
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user.isEnabled)
                {
                    if (user != null)
                    {
                        string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                        var callbackUrl = Url.Action("ResetPassword", "Home", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                        Mail.SendMail(user.Email, "REELAE - Reset Password Request", EmailHtml.getResetPasswordHtml(user.Name, callbackUrl));
                        //await UserManager.SendEmailAsync(user.Id, "Reelae Reset Password", resetpasswordTemplate(user, callbackUrl));
                        ViewBag.Message = MasterConstants.RESETLINKSENT;
                        return View();

                    }

                    else
                    {
                        ViewBag.Message = MasterConstants.EMAILREGISTRATIONALERT;
                        return View();
                    }
                }
                else
                {
                    ViewBag.Message = MasterConstants.ACCOUNTNOTACTIVATED;
                    return View();
                }
            }
            else
            {
                return View(model);
            }
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string userId, string code)
        {
            return code == null ? View("Error") : View("ResetPassword", new ResetPasswordViewModel() { Code = code });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                ViewBag.Message = MasterConstants.EMAILREGISTRATIONALERT;
                return View();
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                ViewBag.Message = MasterConstants.PASSWORDRESETSUCCESS;
                return View();
            }
            else
            {
                ModelState.AddModelError("", MasterConstants.PASSWORDVALIDATIONVIEWMESSAGE);
            }
            return View();
        }


        #endregion

        public ActionResult LogOff()
        {
            Session.Remove("Institution");
            Session.Remove("UserId");
            Session.Remove("UserName");
            Session.Remove("InstutionCode");
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Home");
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        #region [AddUser]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> AddUserByInstitution(AddMemberNViewModal model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int _InstitutionId = 0;
                    if (!Int32.TryParse(Convert.ToString(Session["Institution"]), out _InstitutionId) && _InstitutionId == 0)
                    {
                        return this.Json(new
                        {
                            EnableError = true,
                            ErrorTitle = MasterConstants.ERRORTITLE,
                            ErrorMsg = MasterConstants.INVALIDINSTITUTION
                        });
                    }
                    if (UserManager.FindByName(model.Email) != null)
                    {
                        return this.Json(new
                        {
                            EnableError = true,
                            ErrorTitle = MasterConstants.ERRORTITLE,
                            ErrorMsg = MasterConstants.ALREADYEXISTS
                        });
                    }
                    var user = new ApplicationUser { UserName = model.Email, Email = model.Email, Name = model.Name, isEnabled = true };
                    IdentityResult result = await UserManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        string CurrentUserID = UserManager.FindByName(model.Email).Id;
                        int _userId = _userRepo.Add(new User() { InstitutionID = _InstitutionId, Status = 1, CreatedDate = DateTime.Now, UserMemberShipID = CurrentUserID, Name = model.Name, Email = model.Email });
                        UserManager.AddToRole(CurrentUserID, model.Role);
                        _userRepo.SetUserCodeBySp(_userId, _InstitutionId);
                        var callbackUrl = Url.Action("Login", "Home", null, protocol: Request.Url.Scheme);
                        bool isSent = Mail.SendMail(model.Email, "Reelae Account Created", EmailHtml.getUserAddHtml(model.Name, model.Email, model.Password, model.Role, callbackUrl));
                        //bool isSent = true;
                        string successMessage = model.Role + " is added and mail has " + ((!isSent) ? "not" : "") + " been sent to the user sucessfully";
                        return this.Json(new
                        {
                            EnableSuccess = true,
                            SuccessTitle = MasterConstants.SUCCESSTITLE,
                            SuccessMsg = successMessage,
                            Role = model.Role
                        });
                    }
                    else
                    {
                        if (result.Errors.First().Contains("Passwords must have"))
                        {
                            return this.Json(new
                            {
                                EnableError = true,
                                ErrorTitle = MasterConstants.ERRORTITLE,
                                ErrorMsg = MasterConstants.PASSWORDVALIDATIONVIEWMESSAGE
                            });
                        }
                        else
                        {
                            return this.Json(new
                            {
                                EnableError = true,
                                ErrorTitle = MasterConstants.ERRORTITLE,
                                ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                            });
                        }
                    }
                }
                else
                {
                    var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = error
                    });
                }
            }
            catch (Exception)
            {
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                });
            }

        }
        #endregion

        public JsonResult setInstitutionType(string type)
        {
            if (type != "")
            {
                _institutionRepo.updateType(_userRepo.Get(User.Identity.GetUserId()).InstitutionID, type);

                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = ""
                });
            }
            else
            {
                return this.Json(new
                {
                    EnableSuccess = false,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.CHOOSEINSTITUTIONTYPE
                });
            }
        }

        public JsonResult sendMailToAdmin()
        {

            var _curUser = UserManager.FindById(User.Identity.GetUserId());
            if (_curUser != null)
            {
                string code = UserManager.GeneratePasswordResetToken(_curUser.Id);
                var callbackUrl = Url.Action("ActivateUser", "Home", new { userId = _curUser.Id, code = code }, protocol: Request.Url.Scheme);
                string AdminEmailId = ConfigurationManager.AppSettings["AdminEmail"].ToString();
                Mail.SendMail(AdminEmailId, MasterConstants.SIGNUPEMAILSUBJECT, EmailHtml.getAdminHtml(_curUser.Email, " Activate new sign-up request by clicking <a href=\"" + callbackUrl + "\">here</a>"));
                Session.Remove("Institution");
                Session.Remove("UserId");
                Session.Remove("UserName");
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            }
            return this.Json(new
            {
                EnableSuccess = true,
                SuccessTitle = "Success",
            });

        }

    }


}
