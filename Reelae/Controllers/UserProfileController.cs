﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Reelae.Models;
using ReelaeDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Reelae.Repositories;
using Reelae.Filters;
using System.IO;
using ReelaeBusinessLayer.Utilities;
using Reelae.Utilities;

namespace Reelae.Controllers
{
    [Authorize]
    [checkSessionAlive]
    public class UserProfileController : Controller
    {
        private ApplicationUserManager _userManager;
        IUserRepository _userRepo;
        ILogRepository _dbLogrepo;
        IAspnetUsersRepository _aspnetuserRepo;
        // GET: UserProfile

        public UserProfileController(IUserRepository __userRepo, ILogRepository __dbLogrepo, IAspnetUsersRepository __aspnetuserRepo)
        {
            _userRepo = __userRepo;
            _dbLogrepo = __dbLogrepo;
            _aspnetuserRepo = __aspnetuserRepo;

        }

        public UserProfileController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ActionResult Index()
        {

            return View();
        }


        public ActionResult ProfileSetup()
        {
            try
            {
                ViewBag.PageHeading = "Profile Setting";
                User model = _userRepo.Get(Convert.ToInt32(Session["UserId"]));
                if (model.Website == null)
                {
                    model.Website = "http://";
                }
                return View(model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserId"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return RedirectToAction("Login");
            }


        }

        [HttpPost]
        public JsonResult SaveProfile(User model)
        {
            if (ModelState.IsValidField("Email") && ModelState.IsValidField("Name"))
            {
                if (model.Website == "http://" || model.Website == "https://")
                {
                    model.Website = null;
                    _userRepo.Update(model, Session["UserName"].ToString());
                    _aspnetuserRepo.update(User.Identity.GetUserId(), model.Name);
                    Session["Name"] = model.Name;

                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = MasterConstants.PROFILESAVESUCCESS
                    });
                }
                else
                {
                    if (ModelState.IsValidField("Website"))
                    {
                        _userRepo.Update(model, Session["UserName"].ToString());
                        _aspnetuserRepo.update(User.Identity.GetUserId(), model.Name);
                        Session["Name"] = model.Name;

                        return this.Json(new
                        {
                            EnableSuccess = true,
                            SuccessTitle = MasterConstants.SUCCESSTITLE,
                            SuccessMsg = MasterConstants.PROFILESAVESUCCESS
                        });
                    }
                    else
                    {
                        var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                        return this.Json(new
                        {
                            EnableError = true,
                            ErrorTitle = MasterConstants.ERRORTITLE,
                            ErrorMsg = error
                        });
                    }
                }
            }
            else
            {
                var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = error
                });
            }
        }
        public ActionResult GetChangePasswordView()
        {
            try
            {
                return PartialView("ChangePassword", new Reelae.Models.ChangePasswordViewModel());
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserId"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return RedirectToAction("Login");
            }

        }
        public ActionResult GetProfileSetupView()
        {
            try
            {
                User model = _userRepo.Get(Convert.ToInt32(Session["UserId"]));
                return PartialView("UserProfileSetup", model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserId"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return RedirectToAction("Login");
            }

        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        public JsonResult ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                var error = this.ModelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = error
                });
            }
            var result = UserManager.ChangePassword(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.PASSWORDCHANGESUCCESS
                });

            }

            else
            {
                if (result.Errors.First().StartsWith("Passwords must have at least"))
                {
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = MasterConstants.PASSWORDVALIDATIONVIEWMESSAGE
                    });
                }
                else
                {
                    return this.Json(new
                    {
                        EnableError = true,
                        ErrorTitle = MasterConstants.ERRORTITLE,
                        ErrorMsg = MasterConstants.OLDPASSWORDINCORRECT
                    });
                }
            }

        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [HttpPost]
        public ActionResult UploadPic()
        {
            // Checking no of files injected in Request object  
            string fname = "";
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        // Get the complete folder path and store the file inside it.  
                        Random rnd = new Random();
                        fname = "UserPhoto_" + rnd.Next(1000) + "_" + Session["UserId"].ToString() + System.IO.Path.GetExtension(fname);
                        //fname = Path.Combine(Server.MapPath("~/InstituteLogo/"), fname);
                        file.SaveAs(Path.Combine(Server.MapPath("~/UserPic/"), fname));
                    }
                    // Returns message that successfully uploaded  
                    //return Json(fname);
                    return Json(new
                    {
                        path = Url.Content(Path.Combine("~/UserPic/", fname)),
                        filename = fname
                    });
                }
                catch (Exception ex)
                {
                    ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserId"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                    new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                    return this.Json(new
                    {
                        status = MasterConstants.FAILURETITLE,
                        msg = MasterConstants.SOMEPROBLEMOccurred,
                        Data = string.Empty
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(MasterConstants.NOFILESSELECTED);
            }
        }

        //Cancel click

        public ActionResult Cancel()
        {

            if (Session["Role"].ToString() == "Teacher")
            {
                return RedirectToAction("dashboard", "Teacher", new { area = "Teacher" });

            }
            else if (Session["Role"].ToString() == "Student")
            {
                return RedirectToAction("dashboard", "Student", new { area = "Student" });

            }
            else
            {
                return RedirectToAction("dashboard", "Admin", new { area = "Admin" });

            }
        }
    }
}