﻿using Reelae.Repositories;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Reelae.Models;
using Reelae.Filters;
using ReelaeBusinessLayer.Utilities;
using Reelae.Utilities;
using System.IO;
using ReelaeDataAccessLayer;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;

namespace Reelae.Controllers
{
    [LoggingFilter]
    [checkSessionAlive]
    [Authorize(Roles = "Student,Teacher")]
    public class ForumController : Controller
    {
        IUserRepository _userRepo;
        ILogRepository _dbLogrepo;
        IGroupRepository _groupRepo;
        IForumRepository _forumRepo;
        // GET: Forum

        public ForumController(IUserRepository __userRepo, ILogRepository __dbLogrepo, IGroupRepository __groupRepo, IForumRepository __forumRepo)
        {
            _userRepo = __userRepo;
            _dbLogrepo = __dbLogrepo;
            _groupRepo = __groupRepo;
            _forumRepo = __forumRepo;
        }
        public ActionResult Index()
        {
            try
            {
                ForumViewModel model = new ForumViewModel();
                int? _userId = Session["UserId"] as int?;
                string _role = Session["Role"].ToString();
                int? _InstutionID = Session["Institution"] as int?;

                model._userinfo = _userRepo.Get(User.Identity.GetUserId());
                if (_role == "Teacher")
                {
                    model._forumTopics = _forumRepo.GetForTeacher(_userId.Value,null);
                }
                else
                {
                    model._forumTopics = _forumRepo.GetForStudent(_userId.Value,null);
                }
                model._subjects = _forumRepo.GetSubjects(_role, _userId, _InstutionID);
                ViewBag.PageHeading = "Forum";
                return View(model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult getForumTopics(string subjectIds)
        {
           
            int? _userId = Session["UserId"] as int?;
            string _role = Session["Role"].ToString();
            int? _InstutionID = Session["Institution"] as int?;
            if (_role == "Teacher")
            {
                var model = _forumRepo.GetForTeacher(_userId.Value, subjectIds);
                return PartialView("_forumTopicsList", model);

            }
            else
            {
                var model = _forumRepo.GetForStudent(_userId.Value, subjectIds);
                return PartialView("_forumTopicsList", model);

            }
        }
        [HttpPost]
        public ActionResult getNewTopicView()
        {
            try
            {
                int? _userId = Session["UserId"] as int?;
                string _role = Session["Role"].ToString();
                int? _InstutionID = Session["Institution"] as int?;
                newTopicViewModel model = new newTopicViewModel();
                model.SubjectList = _forumRepo.GetSubjects(_role, _userId, _InstutionID);
                return PartialView("_newForumTopic", model);
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult addNewTopic(newTopicViewModel model)
        {
            try
            {
                if (model.forum.ForumID == 0)
                {
                    int? _userId = Session["UserId"] as int?;
                    model.forum.UserID = _userId;
                    _forumRepo.Add(model.forum, Session["UserName"].ToString());

                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = MasterConstants.SAVESUCCESSFULLY
                    });

                }
                else
                {
                    _forumRepo.Update(model.forum, Session["UserName"].ToString());
                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = MasterConstants.SAVESUCCESSFULLY
                    });
                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult getEditView(int id)
        {
            try
            {
                int? _userId = Session["UserId"] as int?;
                string _role = Session["Role"].ToString();
                int? _InstutionID = Session["Institution"] as int?;
                newTopicViewModel model = new newTopicViewModel();
                model.SubjectList = _forumRepo.GetSubjects(_role, _userId, _InstutionID);
                model.forum = _forumRepo.Get(id);
                return PartialView("_newForumTopic", model);

            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult RemoveForumTopic(int id)
        {
            try
            {
                _forumRepo.Remove(id);
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.REMOVE_SUCCESS
                });


            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult getContent(int id)
        {
            var model = _forumRepo.Get(id);
            return PartialView("_ReadMore", model);
        }

        public ActionResult getReplyView(int id)
        {
            ForumUserAction model = new ForumUserAction();
            model.ForumID = id;
            return PartialView("_Reply", model);
        }

        [HttpPost]
        public ActionResult saveReply(ForumUserAction model)
        {
            try
            {
                if (model.ForumUserActionID == 0)
                {
                    int? _userId = Session["UserId"] as int?;
                    model.UserID = _userId;
                    model.ReplyUserID = _userId;
                    model.ActionTime = DateTime.Now;
                    _forumRepo.Add(model, Session["UserName"].ToString());

                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = MasterConstants.SAVESUCCESSFULLY
                    });

                }
                else
                {
                    _forumRepo.Update(model, Session["UserName"].ToString());
                    return this.Json(new
                    {
                        EnableSuccess = true,
                        SuccessTitle = MasterConstants.SUCCESSTITLE,
                        SuccessMsg = MasterConstants.SAVESUCCESSFULLY
                    });
                }
            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult getAllReply(int id)
        {
            ViewBag.ForumTopic = _forumRepo.Get(id).Topic;
            var model = _forumRepo.GetAllReply(id);
            return PartialView("_AllReply", model);
        }


        [HttpPost]
        public ActionResult getEditReplyView(int id)
        {
            try
            {
                var model = _forumRepo.GetReply(id);
                return PartialView("_Reply", model);

            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult removeReply(int id)
        {
            try
            {
                _forumRepo.RemoveReply(id);
                return this.Json(new
                {
                    EnableSuccess = true,
                    SuccessTitle = MasterConstants.SUCCESSTITLE,
                    SuccessMsg = MasterConstants.REMOVE_SUCCESS
                });


            }
            catch (Exception ex)
            {
                ReelaeDataAccessLayer.ErrorLog obj = new ReelaeDataAccessLayer.ErrorLog() { PageName = this.RouteData.Values["controller"].ToString(), MethodName = this.RouteData.Values["action"].ToString(), BrowserVersion = Request.Browser.Browser, CreatedBy = Session["UserName"].ToString(), CreatedDate = DateTime.UtcNow, Description = ex.Message };
                new ErrorLogger(_dbLogrepo).WriteDBLog(obj, ex);
                return this.Json(new
                {
                    EnableError = true,
                    ErrorTitle = MasterConstants.ERRORTITLE,
                    ErrorMsg = MasterConstants.SOMEPROBLEMOccurred
                }, JsonRequestBehavior.AllowGet);
            }
        }


    }
}