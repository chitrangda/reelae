//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ReelaeDataAccessLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class ChatUser
    {
        public int ChatUserID { get; set; }
        public Nullable<int> FromUserID { get; set; }
        public Nullable<int> ToUserID { get; set; }
        public Nullable<int> MessageID { get; set; }
        public Nullable<System.DateTime> ChatStartTime { get; set; }
        public Nullable<int> GroupID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public int IsRead { get; set; }
        public int IsMsgRemovedFrom { get; set; }
        public int IsMsgRemovedTo { get; set; }
    
        public virtual Message Message { get; set; }
        public virtual User User { get; set; }
        public virtual User User1 { get; set; }
        public virtual UserChatGroup UserChatGroup { get; set; }
    }
}
