//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ReelaeDataAccessLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class PinUpBoard
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PinUpBoard()
        {
            this.PinUpBoardFiles = new HashSet<PinUpBoardFile>();
        }
    
        public int PinUpBoardID { get; set; }
        public string Content { get; set; }
        public Nullable<int> StudentId { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string Visibilty { get; set; }
        public string GroupId { get; set; }
        public Nullable<int> InstitutionId { get; set; }
    
        public virtual User User { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PinUpBoardFile> PinUpBoardFiles { get; set; }
    }
}
