//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ReelaeDataAccessLayer
{
    using System;
    
    public partial class re_select_attendence_participatingrating_sp_Result
    {
        public Nullable<int> MemberID { get; set; }
        public Nullable<decimal> attendenceperct { get; set; }
        public Nullable<decimal> Avgrating { get; set; }
        public string Name { get; set; }
        public string UserPicUrl { get; set; }
    }
}
