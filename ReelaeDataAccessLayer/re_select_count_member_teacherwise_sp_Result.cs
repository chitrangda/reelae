//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ReelaeDataAccessLayer
{
    using System;
    
    public partial class re_select_count_member_teacherwise_sp_Result
    {
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public Nullable<int> TotalMemberCount { get; set; }
        public Nullable<int> TeacherID { get; set; }
    }
}
